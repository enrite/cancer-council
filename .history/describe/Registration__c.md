2015-02-27 11:05:00
```java
Registration__c Describe:
----------------------------------------------------------------------------------------------------
Total Fields: 	228
----------------------------------------------------------------------------------------------------
Label                                   Name                                    Type                Length 
-----------------------------------------------------------------------------------------------------------
Above Threshold                         Above_Threshold__c                      Formula(string)     1300   
Account                                 Host_Team_Captain_Org__c                reference           18     
Account Postcode                        Account_Postcode__c                     Formula(string)     1300   
Account State                           Account_State__c                        Formula(string)     1300   
Account Street                          Account_Street__c                       Formula(string)     1300   
Account Suburb                          Account_Suburb__c                       Formula(string)     1300   
Actual Number of Guests                 Actual_Number_of_Guests__c              double                     
Age when diagnosed                      Age_when_diagnosed__c                   string              5      
All Receipt books returned              All_Receipt_books_returned__c           boolean                    
Ambassador Project Other                Ambassador_Project_Other__c             string              255    
Ambassador Reason                       Ambassador_Reason__c                    textarea            32768  
Annual  Event                           Annual_Event__c                         picklist            255    
Bequest Type                            Bequest_Type__c                         picklist            255    
CCSA Support                            CCSA_Support__c                         multipicklist       4099   
CCSA Support Other                      CCSA_Support_Other__c                   textarea            255    
CTA Number of Junior Teams              CTA_Number_of_Junior_Teams__c           double                     
CTA Number of Senior Teams              CTA_Number_of_Senior_Teams__c           double                     
Campaign                                Campaign__c                             reference           18     
Campaign Donation Registration          Campaign_Donation_Registration__c       boolean                    
Cancer Connect                          Cancer_Connect__c                       picklist            255    
Cancer Experience                       Cancer_Experience__c                    string              255    
Cancer Type                             Cancer_Type__c                          multipicklist       4099   
Car Available                           Car_Available__c                        boolean                    
Carer Cancer Type                       Carer_Cancer_Type__c                    picklist            255    
Certificate Requested                   Certificate_Requested__c                boolean                    
Changed Amount                          Changed_Amount__c                       currency                   
Children (Ages of)                      Children_Ages_of__c                     string              30     
Children (At Home)                      Children_At_Home__c                     string              50     
Children (Number Of)                    Children_Number_Of__c                   string              5      
Clothing Collection Method              Short_Selection_Rules__c                picklist            255    
Collector Agency                        Collector_Agency__c                     reference           18     
Committee Position                      Committee_Position__c                   picklist            255    
Community Fundraising                   Community_Fundraising__c                boolean                    
Community Fundraising Withdraw          Community_Fundraising_Withdraw__c       boolean                    
Contact                                 Host_Team_Captain__c                    reference           18     
Contact Envelope Salutation             Contact_Envelope_Salutation__c          Formula(string)     1300   
Contact First Name                      Contact_First_Name__c                   Formula(string)     1300   
Contact Last Name                       Contact_Last_Name__c                    Formula(string)     1300   
Contact Letter Salutation               Contact_Letter_Salutation__c            Formula(string)     1300   
Contact Mobile                          Contact_Mobile__c                       Formula(string)     1300   
Contact Phone                           Contact_Phone__c                        Formula(string)     1300   
Contact Postcode                        Contact_Postcode__c                     Formula(string)     1300   
Contact State                           Contact_State__c                        Formula(string)     1300   
Contact Status                          Contact_Status__c                       Formula(string)     1300   
Contact Street1                         Contact_Street1__c                      Formula(string)     1300   
Contact Suburb                          Contact_Suburb__c                       Formula(string)     1300   
Created By ID                           CreatedById                             reference           18     
Created Date                            CreatedDate                             datetime                   
Credit Card Expiry                      Credit_Card_Expiry__c                   string              7      
Current Driver Licence                  Current_Driver_Licence__c               boolean                    
Date Certificate Sent                   Date_Certificate_Sent__c                date                       
Date Deceased                           Date_Deceased__c                        Formula(date)              
Date Host Kit Sent                      Date_Host_Kits_Sent__c                  date                       
Date Host Slip Received                 Date_Host_Slip_Received__c              date                       
Date Notification                       Date_Notification__c                    date                       
Date Released                           Date_Released__c                        date                       
Date Shirts Sent                        Date_Shirts_Sent__c                     date                       
Date Shorts Sent                        Date_Shorts_Sent__c                     date                       
Date Sleeve Sent                        Date_Sleeve_Sent__c                     date                       
Date Slip Returned                      Date_Slip_Returned__c                   date                       
Date Thankyou Letter Sent               Date_Thankyou_Letter_Sent__c            date                       
Date Will Written                       Date_Will_Written__c                    date                       
Day and Time Available                  Day_and_Time_Available__c               multipicklist       4099   
Deleted                                 IsDeleted                               boolean                    
Disclaimer                              Disclaimer__c                           boolean                    
Donation Amount                         Donation_Amount__c                      currency                   
Donation Period                         Donation_Period__c                      picklist            255    
Emergency Contact Number                Emergency_Contact_Number__c             string              255    
Emergency Name                          Emergency_Name__c                       string              255    
Emergency Relationship                  Emergency_Relationship__c               string              255    
Employer                                Employer__c                             string              255    
Est Number of guests                    Est_Number_of_guests__c                 double                     
Event Date                              Event_Date__c                           date                       
Event Information                       Event_Information__c                    picklist            255    
Event Name                              Event_Name__c                           string              80     
Event Type                              Event_Type__c                           picklist            255    
Executor/Trustee Account                Executor_Trustee_Account__c             reference           18     
Executor/Trustee Contact                Executor_Trustee_Contact__c             reference           18     
Expected Amount                         Expected_Amount__c                      currency                   
Experiences                             Experiences__c                          textarea            32768  
External ID                             External_ID__c                          string              255    
Fundraiser Name                         Fundraiser_Name__c                      string              255    
Fundraising Other                       Fundraising_Other__c                    picklist            255    
Fundraising Other Org                   Fundraising_Other_Org__c                string              255    
Fundraising Percent                     Fundraising_Percent__c                  string              80     
Fundraising children                    Fundraising_children__c                 picklist            255    
Fundraising method                      Fundraising_method__c                   textarea            255    
Fundraising states                      Fundraising_states__c                   multipicklist       4099   
Hat Size                                Hat_Size__c                             picklist            255    
Held Reason                             Held_Reason__c                          picklist            255    
Held in Honour of                       Held_in_Honour_of__c                    string              80     
How did you hear about us               How_did_you_hear_us__c                  multipicklist       4099   
In Honour of Relationship               In_Honour_of_Relationship__c            picklist            255    
In Memoriam                             In_Memoriam__c                          string              255    
Kit Size                                Kit_Size__c                             string              10     
Known to Us                             Known_to_Us__c                          picklist            255    
Last Activity Date                      LastActivityDate                        date                       
Last Job Activity                       Last_Job_Activity__c                    date                       
Last Modified By ID                     LastModifiedById                        reference           18     
Last Modified Date                      LastModifiedDate                        datetime                   
Last Referenced Date                    LastReferencedDate                      datetime                   
Last Success Transaction Amount         Last_Success_Transaction_Amount__c      currency                   
Last Success Transaction Date           Last_Success_Transaction_Date__c        date                       
Last Transaction Amount                 Last_Transaction_Amount__c              currency                   
Last Transaction Date                   Last_Transaction_Date__c                date                       
Last Updated by Team Raiser             Last_Updated_by_Team_Raiser__c          datetime                   
Last Viewed Date                        LastViewedDate                          datetime                   
Marital Status                          Marital_Status__c                       picklist            255    
Media Friendly                          Media_Friendly__c                       boolean                    
Merchandise                             Merchandise__c                          picklist            255    
Motivation                              Motivation__c                           picklist            255    
No Paperwork                            No_Paperwork__c                         boolean                    
Non Payment Status                      Non_Payment_Status__c                   picklist            255    
Notes                                   Notes__c                                textarea            32768  
Number of students  participating       Number_of_Students__c                   double                     
On demand                               On_demand__c                            boolean                    
Org Primary Contact                     Org_Primary_Contact__c                  reference           18     
Org Primary Contact Envelope Salutation Primary_Contact_Envelope_Salutation__c  Formula(string)     1300   
Org Primary Contact First Name          Primary_Contact_Org_First_Name__c       Formula(string)     1300   
Org Primary Contact Last Name           Primary_Contact_Org_Last_Name__c        Formula(string)     1300   
Org Primary Contact Letter Salutation   Primary_Contact_Letter_Salutation__c    Formula(string)     1300   
Org Primary Contact Postcode            Primary_Contact_Org_Postcode__c         Formula(string)     1300   
Org Primary Contact State               Primary_Contact_Org_State__c            Formula(string)     1300   
Org Primary Contact Street              Primary_Contact_Org_Street__c           Formula(string)     1300   
Org Primary Contact Suburb              Primary_Contact_Org_Suburb__c           Formula(string)     1300   
Owner ID                                OwnerId                                 reference           18     
Parent/Guardian Name                    Parent_Guardian_Name__c                 string              255    
Parent/Guardian email                   Parent_Guardian_email__c                email               80     
Parent/Guardian first name              Parent_Guardian_first_name__c           string              60     
Parent/Guardian last name               Parent_Guardian_last_name__c            string              80     
Parent/Guardian phone                   Parent_Guardian_phone__c                phone               40     
Participant Goal                        Participant_Goal__c                     currency                   
Participant Type                        Participant_Type__c                     picklist            255    
Patient or Carer                        Patient_or_Carer__c                     picklist            255    
Payment Type                            Payment_Type__c                         picklist            255    
Personal Connection To Cancer           Personal_Connection_To_Cancer__c        multipicklist       4099   
Position Reference Applied              Position_Reference_Applied__c           string              255    
Prize Packs                             Prize_Packs__c                          string              10     
Proposed activity                       Proposed_activity__c                    textarea            255    
Public Liability                        Public_Liability__c                     picklist            255    
RFL Team FR Goal                        RFL_Team_FR_Goal__c                     currency                   
Race Distance                           Race_Distance__c                        string              8      
Receipt Books Range Sent                Receipt_Books_Range_Sent__c             string              100    
Receipt Books Ranges Returned           Receipt_Books_Ranges_Returned__c        string              255    
Record ID                               Id                                      id                  18     
Record Type ID                          RecordTypeId                            reference           18     
Referee Name 1                          Referee_Name_1__c                       string              255    
Referee Name 2                          Referee_Name_2__c                       string              255    
Referee Organisation                    Referee_Organisation__c                 string              120    
Referee Phone 1                         Referee_Phone_1__c                      phone               40     
Referee Phone 2                         Referee_Phone_2__c                      phone               40     
Referee Position/Title                  Referee_Position_Title__c               string              120    
Registrant  Emergergency Name           Registrant_Emergergency_Name__c         string              255    
Registrant  Emergergency Number         Registrant_Emergergency_Number__c       string              50     
Registrant Employer                     Registrant_Employer__c                  string              100    
Registration Date                       Registration_Date__c                    date                       
Registration Expiry Date                Registration_Expiry_Date__c             date                       
Registration Fee Status                 Registration_Fee_Status__c              picklist            255    
Registration Method                     Registration_Method__c                  picklist            255    
Registration Number                     Name                                    string              80     
Registration Reason                     Registration_Reason__c                  picklist            255    
Registration Source                     Registration_Source__c                  reference           18     
Regular Giving Type                     Regular_Giving_Type__c                  picklist            255    
SHAC Leader                             SHAC_Leader__c                          picklist            255    
SUF Number                              SUF_Number__c                           string              255    
School                                  School__c                               reference           18     
Seek Opportunity                        Seek_Opportunity__c                     boolean                    
Shirt Size                              Shirt_Size__c                           picklist            255    
Shirt Type                              Shirt_Type__c                           picklist            255    
Short Size                              Short_Size__c                           picklist            255    
Shorts Type                             Shorts_Type__c                          picklist            255    
Sign Up Location                        Sign_Up_Location__c                     picklist            255    
Skills                                  Skills__c                               multipicklist       4099   
Skills Other                            Skills_Other__c                         textarea            32768  
Sleeve Size                             Sleeve_Size__c                          picklist            255    
Special Needs                           Special_Needs__c                        textarea            32768  
Sponsorship                             Sponsorship__c                          picklist            255    
Sponsorship details                     Sponsorship_details__c                  textarea            255    
Status                                  Status__c                               picklist            255    
Support Group Coordinator               Support_Group_Coordinator__c            picklist            255    
Survivor & Carer  lap?                  Survivor_Carer_Lap__c                   boolean                    
System Modstamp                         SystemModstamp                          datetime                   
TR Registration ID                      TR_Registration_ID__c                   string              255    
TR Team ID                              TR_Team_ID__c                           string              255    
Team Description                        Team_Description__c                     string              255    
Team ID                                 Team_ID__c                              string              255    
Team Name                               Team_Name__c                            string              100    
Team Number                             Team_Number__c                          string              10     
Termination Date                        Termination_Date__c                     date                       
Termination Letter Sent                 Termination_Letter_Sent__c              boolean                    
Termination Reason                      Termination_Reason__c                   picklist            255    
Total Donation                          Total_Donation__c                       currency                   
Total Job Hours                         Total_Job_Hours__c                      double                     
Total Soft Credit                       Total_Soft_Credit__c                    currency                   
Transaction Number                      Transaction_Number__c                   double                     
Transaction Period                      Transaction_Period__c                   picklist            255    
Transaction Success Number              Transaction_Success_Number__c           double                     
Tribute Type                            Tribute_Type__c                         picklist            255    
Venue Address                           Venue_Address__c                        textarea            255    
Volunteer Type                          Volunteer_Type__c                       picklist            255    
Volunteer Work Other                    Volunteer_Work_Other__c                 textarea            255    
Volunteer Work Restrictions             Volunteer_Work_Restrictions__c          multipicklist       4099   
Volunteering Interest                   Volunteering_Interest__c                multipicklist       4099   
Walk the Talk                           Walk_the_Talk__c                        boolean                    
Web Account                             Web_Account__c                          textarea            255    
Web Account Postcode                    Web_Account_Postcode__c                 string              20     
Web Cancer Info                         Web_Cancer_Info__c                      string              120    
Web Country of Birth                    Web_Country_of_Birth__c                 picklist            255    
Web Date of Birth                       Web_Date_of_Birth__c                    date                       
Web Email                               Web_Email__c                            email               80     
Web First Name                          Web_First_Name__c                       string              80     
Web Gender                              Web_Gender__c                           picklist            255    
Web Home Phone                          Web_Phone__c                            string              20     
Web Languages Spoken                    Web_Languages_Spoken__c                 multipicklist       4099   
Web Last Name                           Web_Last_Name__c                        string              60     
Web Mobile                              Web_Mobile__c                           string              15     
Web Postcode                            Web_Postcode__c                         string              20     
Web Registration                        Web_Registration__c                     boolean                    
Web School Phone                        Web_School_Phone__c                     phone               40     
Web School postcode                     Web_School_postcode__c                  string              20     
Web State                               Web_State__c                            string              60     
Web Street                              Web_Street__c                           string              80     
Web Suburb                              Web_Suburb__c                           string              60     
Web University/School                   Web_University_School__c                string              80     
Web Year Level                          Web_Year_Level__c                       string              20     
Web preferred name                      Web_preferred_name__c                   string              80     
Years of Registration                   Years_of_Registration__c                Formula(double)            
Youth Ambassador                        Youth_Ambassador__c                     boolean                    

----------------------------------------------------------------------------------------------------
Record Type Info: 	9
----------------------------------------------------------------------------------------------------
Default        Available Name                               Recordtypeid        
--------------------------------------------------------------------------------
               True      Ambassador                         012800000007d4kAAA  
True           True      Basic                              012800000006O8BAAU  
               True      Bequest                            012900000007eCSAAY  
               True      Hosted Registration                012800000006O8GAAU  
                         Newsletters                        01290000000t8G3AAI  
               True      Regular Giving                     012800000007d67AAA  
               True      Team Event Registration            012800000006O8QAAU  
               True      Volunteer                          01290000000snXjAAI  
               True      Master                             012000000000000AAA  

----------------------------------------------------------------------------------------------------
ChildRelationships Info: 	32
----------------------------------------------------------------------------------------------------
Relationshipname              Cascadedelete                 Field                         Childsobject                  
------------------------------------------------------------------------------------------------------------------------
ActivityHistories             True                          WhatId                        ActivityHistory               
Ambassador_Activities__r                                    Registration__c               Ambassador_Activity__c        
AttachedContentDocuments      True                          LinkedEntityId                AttachedContentDocument       
Attachments                   True                          ParentId                      Attachment                    
Campaign_Members__r                                         Registration__c               CampaignMember                
RecordAssociatedGroups        True                          RecordId                      CollaborationGroupRecord      
CombinedAttachments           True                          ParentId                      CombinedAttachment            
Contact_Registrations__r                                    Registration__c               Contact_Registrations__c      
                              True                          RelatedRecordId               ContentDistribution           
                              True                          LinkedEntityId                ContentDocumentLink           
                                                            FirstPublishLocationId        ContentVersion                
DuplicateRecordItems          True                          RecordId                      DuplicateRecordItem           
FeedSubscriptionsForEntity    True                          ParentId                      EntitySubscription            
Events                        True                          WhatId                        Event                         
                                                            ParentId                      FeedComment                   
                              True                          ParentId                      FeedItem                      
GoogleDocs                    True                          ParentId                      GoogleDoc                     
Job_Roles__r                  True                          Registration__c               Job_Role__c                   
Notes                         True                          ParentId                      Note                          
NotesAndAttachments           True                          ParentId                      NoteAndAttachment             
OpenActivities                True                          WhatId                        OpenActivity                  
Donations__r                                                Registration__c               Opportunity                   
ProcessInstances              True                          TargetObjectId                ProcessInstance               
ProcessSteps                                                TargetObjectId                ProcessInstanceHistory        
Histories                     True                          ParentId                      Registration__History         
Shares                        True                          ParentId                      Registration__Share           
TR_Import_Registrations_del__r                              Registration__c               TR_Import_Registration__c     
TR_Import_Teams__r                                          Registration__c               TR_Import_Team__c             
Tasks                         True                          WhatId                        Task                          
TopicAssignments              True                          EntityId                      TopicAssignment               
Ambassador_Challenges1__r                                   Campaign_Registration__c      Youth_Ambassador_Challenge__c 
Ambassador_Challenges__r      True                          Registration__c               Youth_Ambassador_Challenge__c 


```

