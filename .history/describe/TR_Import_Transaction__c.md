2015-03-12 10:40:52
```java
TR_Import_Transaction__c Describe:
----------------------------------------------------------------------------------------------------
Total Fields: 	60
----------------------------------------------------------------------------------------------------
Label                                   Name                                    Type                Length 
-----------------------------------------------------------------------------------------------------------
Anonymous                               Anonymous__c                            string              255    
Billing City                            Billing_City__c                         string              255    
Billing Country                         Billing_Country__c                      string              255    
Billing First Name                      Billing_First_Name__c                   string              255    
Billing Last Name                       Billing_Last_Name__c                    string              255    
Billing Postcode                        Billing_Postcode__c                     string              255    
Billing State                           Billing_State__c                        string              255    
Billing Street1                         Billing_Street1__c                      string              255    
Billing Street2                         Billing_Street2__c                      string              255    
Billing Title                           Billing_Title__c                        string              255    
CC Number                               CC_Number__c                            string              255    
CC Type                                 CC_Type__c                              string              255    
Created By ID                           CreatedById                             reference           18     
Created Date                            CreatedDate                             datetime                   
Credit Type                             Credit_Type__c                          string              255    
Deleted                                 IsDeleted                               boolean                    
Employer                                Employer__c                             string              255    
Error                                   Error__c                                textarea            32768  
Event  ID                               Event_ID__c                             string              255    
Event Name                              Event_Name__c                           string              255    
File Name                               File_Name__c                            string              255    
First Name                              First_Name__c                           string              255    
Fund Type                               Fund_Type__c                            string              255    
Home City                               Home_City__c                            string              255    
Home Country                            Home_Country__c                         string              255    
Home Postcode                           Home_Postcode__c                        string              255    
Home State                              Home_State__c                           string              255    
Home Street1                            Home_Street1__c                         string              255    
Home Street2                            Home_Street2__c                         string              255    
In Honour First Name                    In_Honour_First_Name__c                 string              255    
In Honour Gift Type                     In_Honour_Gift_Type__c                  string              255    
In Honour Last Name                     In_Honour_Last_Name__c                  string              255    
Last Modified By ID                     LastModifiedById                        reference           18     
Last Modified Date                      LastModifiedDate                        datetime                   
Last Name                               Last_Name__c                            string              255    
Last Referenced Date                    LastReferencedDate                      datetime                   
Last Viewed Date                        LastViewedDate                          datetime                   
Member ID                               Member_ID__c                            string              255    
Merchant Account ID                     Merchant_Account_ID__c                  string              255    
On Employer Behalf                      On_Employer_Behalf__c                   string              255    
Opportunity                             Opportunity__c                          reference           18     
Owner ID                                OwnerId                                 reference           18     
Participant ID                          Participant_ID__c                       string              255    
Payment Gateway ID                      Payment_Gateway_ID__c                   string              255    
Processed                               Processed__c                            datetime                   
Processing Result                       Processing_Result__c                    string              255    
Record ID                               Id                                      id                  18     
Registration ID                         Registration_ID__c                      string              255    
Soft CreditType                         Soft_Credit_Type__c                     string              255    
System Modstamp                         SystemModstamp                          datetime                   
TR Constituent ID                       TR_Constituent_ID__c                    string              255    
TR Import Transaction Name              Name                                    string              80     
TR Last Modified Date                   TR_Last_Modified_Date__c                string              255    
Team ID                                 Team_ID__c                              string              255    
Tender Type                             Tender_Type__c                          string              255    
Title                                   Title__c                                string              255    
Transaction Amount                      Transaction_Amount__c                   string              255    
Transaction Date                        Transaction_Date__c                     string              255    
Transaction ID                          Transaction_ID__c                       string              255    
Transaction Type                        Transaction_Type__c                     string              255    

----------------------------------------------------------------------------------------------------
Record Type Info: 	1
----------------------------------------------------------------------------------------------------
Name                               Recordtypeid        Default        Available 
--------------------------------------------------------------------------------
Master                             012000000000000AAA  True           True      

----------------------------------------------------------------------------------------------------
ChildRelationships Info: 	17
----------------------------------------------------------------------------------------------------
Relationshipname              Field                         Childsobject                  Cascadedelete                 
------------------------------------------------------------------------------------------------------------------------
AttachedContentDocuments      LinkedEntityId                AttachedContentDocument       True                          
Attachments                   ParentId                      Attachment                    True                          
RecordAssociatedGroups        RecordId                      CollaborationGroupRecord      True                          
CombinedAttachments           ParentId                      CombinedAttachment            True                          
                              RelatedRecordId               ContentDistribution           True                          
                              LinkedEntityId                ContentDocumentLink           True                          
                              FirstPublishLocationId        ContentVersion                                              
DuplicateRecordItems          RecordId                      DuplicateRecordItem           True                          
FeedSubscriptionsForEntity    ParentId                      EntitySubscription            True                          
                              ParentId                      FeedComment                                                 
                              ParentId                      FeedItem                      True                          
GoogleDocs                    ParentId                      GoogleDoc                     True                          
Notes                         ParentId                      Note                          True                          
NotesAndAttachments           ParentId                      NoteAndAttachment             True                          
ProcessInstances              TargetObjectId                ProcessInstance               True                          
ProcessSteps                  TargetObjectId                ProcessInstanceHistory                                      
TopicAssignments              EntityId                      TopicAssignment               True                          


```

