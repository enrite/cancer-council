public with sharing class ImportGPSAController {

    //constructor
    public ImportGPSAController() {
        debug = '';
    }
    
    //to hold information from gp list in order to link contact to account and relationship creation
    private class RelationshipLink {
        public string gpId {get;set;}
        public string locationId {get;set;}
        public string gpProviderNumber {get;set;}
        public string relationshipType {get;set;}
        public string relationshipStatus {get;set;}
        public string accountId {get;set;}
        
        public RelationshipLink() {
            relationshipType = 'GPSA';          
        }
    }
    
    //variables to hold raw data (blob) loaded from csv file and convert to string values    
    public transient string uploadFileStr {get;set;}
    public transient blob uploadFileContent {get;set;}
    public transient string fileName {get;set;}
    
    //import type variable
    public string objectTypeSelected {get;set;}

    //display information
    public string debug {get;set;}
    
    private static final Id contactRecordType = [select Id from RecordType where SobjectType = 'Contact' and Name = 'Health Contact'].Id;
    private static final Id accountRecordType = [select Id from RecordType where SobjectType = 'Account' and Name = 'Health Organisation'].Id; 
    
    //varible to hold result from soql        
    transient map<string, Account> existingAccounts;
    
    //varible to hold result from soql    
    transient map<string, Contact> existingContacts;
    
    //variable to hold record already been processed to avoid duplication
    transient map<string, string> processedAccounts;
    //variable to hold existing records to be updated
    transient list<Account> updateAccounts;
    //variable to hold new records to be inserted
    transient list<Account> insertAccounts;

    //variable to hold record already been processed to avoid duplication
    transient map<string, string> processedContacts;
    //variable to hold existing records to be updated
    transient list<Contact> updateContacts;
    //variable to hold new records to be inserted
    transient list<Contact> insertContacts;
    
    //newline break for visualforce page
    string newLine = '\n<br/>';
    
    public PageReference loadFile() {
        try {
            //convert blob values to string
            uploadFileStr = uploadFileContent.toString();           

            //parse in csv data from utility helper class            
            list<list<string>> inputRows = ParseCSVUtil.parseCSV(uploadFileStr, False);
   
            if (objectTypeSelected != '') {
                //existing accounts are need for both account and contact/relationship imports
                getExistingAccounts();
                if (objectTypeSelected == 'Contact') {
                    //getExistingContacts();   
                    saveContact(inputRows);                 
                }
                if (objectTypeSelected == 'Account') {                      
                    saveAccount(inputRows);                 
                }       
            }
        }
        catch (Exception e) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Load File Exception Occured: ' + e.getMessage());
            ApexPages.addMessage(errorMsg);
            email('Section: Load Files\nError Messages: ' + e.getMessage());
        }
        return null;
    }
    
    public void email (string plainBody) {      
        list<string> recipients = new list<string>{'ylow@cancersa.org.au'};
        string replyTo = UserInfo.getUserName();
        string emailSubject = 'Re: Salesforce ImportGPSA Page Error Occured';
        plainBody = 'User Name: ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + '\n' + plainBody;  
        EmailUtil email = new EmailUtil(recipients, replyTo, emailSubject, plainBody);      
    }
      
    public void saveAccount(list<list<string>> inputRows) {
        updateAccounts = new list<Account>{};
        insertAccounts = new list<Account>{};
        processedAccounts = new map<string, string>{};      
        list<string> inputAccountExternalIds = new list<string>{};
        
        try {       
        //starting from line 1 to skip column header row (line 0)
        for (integer i = 1; i < inputRows.size(); i++) {
            Account acc;
            list<string> inputRow = inputRows[i];            
            if (inputRow != null) {
                //to determine if processing row already exists in order to update or insert
                if (existingAccounts.containsKey(inputRow[0]) && !processedAccounts.containsKey(inputRow[0])) {                    
                    acc = existingAccounts.get(inputRow[0]);
                    mapAccountFields(acc, inputRow, 'Update');
                }
                if (!existingAccounts.containsKey(inputRow[0]) && !processedAccounts.containsKey(inputRow[0])) {
                    acc = new Account();
                    mapAccountFields(acc, inputRow, 'Insert');
                }
            }
        }        
        if (!updateAccounts.isEmpty()) {
            update updateAccounts;
            debug += fileName + ' - Account records updated: ' + updateAccounts.size() + newLine; 
            updateAccounts.clear();
        }
        if (!insertAccounts.isEmpty())
            insert insertAccounts;
            debug += fileName + ' - Account records imported: ' + insertAccounts.size() + newLine;
            insertAccounts.clear();
        }
        catch (Exception e) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Save Accounts Exception Occured: ' + e.getMessage());
            ApexPages.addMessage(errorMsg);
            email('Section: Save Accounts\nError Messages: ' + e.getMessage());
        }
    }
    
    public void mapAccountFields(Account a, list<string> input, string saveType) {              
        a.Name = ParseCSVUtil.toProperCase(input[1]);
        a.BillingStreet = ParseCSVUtil.toProperCase(input[2]);
        a.BillingCity = input[3].toUpperCase();
        a.BillingPostalCode = input[4];
        a.BillingState = input[5];
        a.ShippingStreet = ParseCSVUtil.toProperCase(input[6]);
        a.ShippingCity = input[7].toUpperCase();
        a.ShippingPostalCode = input[8];
        a.ShippingState = input[9];
        a.Phone = input[10];
        a.Fax = input[11];
        a.Email__c = input[12];
        a.GPSA_Sourced__c = true;
        a.External_Source__c = 'GPSA';
        a.Organization_Category__c = 'Health';
        a.Sub_Type__c = input[13];
        
        if (saveType == 'Update') {
            updateAccounts.add(a);
        }
        if (saveType == 'Insert') {
            a.External_ID__c = input[0];
            a.RecordTypeId = accountRecordType;
            insertAccounts.add(a);
        }
        processedAccounts.put(input[0], input[0]);
    }
    
    public void saveContact(list<list<string>> inputRows) {        
        updateContacts = new list<Contact>{};
        insertContacts = new list<Contact>{};
        processedContacts = new map<string, string>{};
        list<RelationshipLink> rlList = new list<RelationshipLink>{};
        map<string, Relationship__c> existingRelationships = new map<string, Relationship__c>{};
        
        try {
            getExistingContacts();          
            for (Relationship__c r : [select External_ID__c, Id from Relationship__c where Type__c = 'GPSA']) {
                existingRelationships.put(r.External_ID__c, r);
            }
            //starting from line 1 to skip column header row
            for (integer i = 1; i < inputRows.size(); i++) {
                Contact con;
                list<string> inputRow = inputRows[i];
            
                //check if record already exists and skips the process if record already updated
                //update existing record
                if (existingContacts.containsKey(inputRow[0]) && !processedContacts.containsKey(inputRow[0])) {
                    con = existingContacts.get(inputRow[0]);
                    mapContactFields(con, inputRow, 'Update');                  
                }
                if (!existingContacts.containsKey(inputRow[0]) && !processedContacts.containsKey(inputRow[0])) {
                    con = new Contact();
                    mapContactFields(con, inputRow, 'Insert');                  
                }
                //relationship to be created if no matching against existing records
                if (!existingRelationships.containsKey(inputRow[15]) && existingAccounts.get(inputRow[14]) != null) {               
                    RelationshipLink rl = new RelationshipLink();
                    rl.gpId = inputRow[0];
                    rl.locationId = inputRow[14];
                    rl.gpProviderNumber = inputRow[15];
                    rl.accountId = existingAccounts.get(inputRow[14]).Id;
                    rlList.add(rl);
                }
            }
            existingContacts.clear();
                                            
            if (!updateContacts.isEmpty()) {
                update updateContacts;
                debug += fileName + ' - Contact records updated: ' + updateContacts.size() + newLine;
                updateContacts.clear();
            }
            if (!insertContacts.isEmpty()) {
                insert insertContacts;
                debug += fileName + ' - Contact records imported: ' + insertContacts.size() + newLine;
                insertContacts.clear();
            }
            matchRelationship(rlList);          
        }
        catch (Exception e) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Save Contacts Exception Occured: ' + e.getMessage());
            ApexPages.addMessage(errorMsg);
            email('Section: Save Contacts\nError Messages: ' + e.getMessage());
        }
    }
    
    public void mapContactFields(Contact c, list<string> input, string saveType) {
        c.Salutation = input[1];
        c.FirstName = ParseCSVUtil.toProperCase(input[3]);
        c.LastName = ParseCSVUtil.toProperCase(input[2]);
        c.How_Heard__c = 'GPSA';
        //c.MobilePhone = input[5];
        c.Email = input[6];
        c.Gender__c = input[7];
        c.OtherStreet = ParseCSVUtil.toProperCase(input[22]);
        c.OtherCity = input[23].toUpperCase();
        c.OtherPostalCode = input[24];
        c.OtherState = input[25];
        c.MailingStreet = ParseCSVUtil.toProperCase(input[18]);
        c.MailingCity = input[19].toUpperCase();
        c.MailingPostalCode = input[20];
        c.MailingState = input[21];
        c.Phone = input[26];
        c.Fax = input[27];
        c.GPSA_Sourced__c = true;
        
        if (saveType == 'Update') {
            updateContacts.add(c);
        }
        if (saveType == 'Insert') {
            c.External_ID__c = input[0];
            c.RecordTypeId = contactRecordType;
            insertContacts.add(c);
        }
        processedContacts.put(input[0], input[0]);      
    }
        
    public void matchRelationship(list<RelationshipLink> rl) {
        //refresh existing contact list from recent contacts inserted
        getExistingContacts();
        list<Relationship__c> relationshipList = new list<Relationship__c>{};
        list<Contact> contactList = new list<Contact>{};
        
        try {
            for (integer i = 0; i < rl.size(); i++) {
                Relationship__c rel = new Relationship__c();
                Contact c = existingContacts.get(rl.get(i).gpId);
                rel.Contact__c = c.Id;
                rel.Account__c = rl.get(i).accountId;
                rel.External_Id__c = rl.get(i).gpProviderNumber;
                rel.Type__c = rl.get(i).relationshipType;
                relationshipList.add(rel);
            
                //if contact links to multiple accounts, add the first matched found account to contact with no account linked     
                if (c.AccountId == null) {
                    c.AccountId = rl.get(i).accountId;
                    contactList.add(c);
                }
            }
            if (!relationshipList.isEmpty()) {
                insert relationshipList;
                debug += fileName + ' - Contact Relationships records imported: ' + relationshipList.size() + newLine;
            }
            if (!contactList.isEmpty()) {
                update contactList;
            }
        }
        catch (Exception e) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Save Relationships Exception Occured: ' + e.getMessage());
            ApexPages.addMessage(errorMsg);
            email('Section: Save Relationships\nError Messages: ' + e.getMessage());
        }
    }
    
    public void getExistingAccounts() {        
        existingAccounts = new map<string, Account>{};
                
        for (Account acc : [select Id, External_Id__c from Account where External_Source__c = 'GPSA']) {        
            existingAccounts.put(acc.External_Id__c, acc);
        }
    }

    public void getExistingContacts() {             
        existingContacts = new map<string, Contact>{};
        
        for (Contact con : [select Id, External_Id__c, AccountId from Contact where How_Heard__c = 'GPSA']) {        
            existingContacts.put(con.External_Id__c, con);
        }
    }
    
    //Object Type list
    public list<selectOption> getObjType() {
        list<selectOption> options = new list<selectOption>();
        options.add(new selectOption('', '--None--'));
        options.add(new selectOption('Account', 'Health Organisation'));
        options.add(new selectOption('Contact', 'Health Contact'));                
        return options;
    }
}