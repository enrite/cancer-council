public with sharing class QuitskillsCoursesController
{
    private static final String PROJECT_AREAS_PARAM = 'projectAreas';

    public Map<String, List<Ambassador_Activity__c>> ambassadorActivities{get;set;}

    public QuitskillsCoursesController()
    {
        this.ambassadorActivities = getActivities();
    }

    private Map<string, List<Ambassador_Activity__c>> getActivities()
    {
        Map<String, List<Ambassador_Activity__c>> activitiesByStrings = new Map<String, List<Ambassador_Activity__c>>();
        String[] projectAreas = ApexPages.currentPage().getParameters().containsKey(PROJECT_AREAS_PARAM) ? ApexPages.currentPage().getParameters().get(PROJECT_AREAS_PARAM).split(';') : new String[0];
        List<Ambassador_Activity__c> ambassadorActivities = [
                SELECT Id,
                        Project_Area__c,
                        Web_Name__c
                FROM Ambassador_Activity__c
                WHERE Publish_to_Website__c = true
                AND Project_Area__c IN :projectAreas
                ORDER BY Session_1__c
        ];

        for(Ambassador_Activity__c activity : ambassadorActivities)
        {
            if(!activitiesByStrings.containsKey(activity.Project_Area__c))
            {
                activitiesByStrings.put(activity.Project_Area__c, new List<Ambassador_Activity__c>());
            }

            List<Ambassador_Activity__c> aAct = activitiesByStrings.get(activity.Project_Area__c);

            if(aAct.size()==2) continue;

            aAct.add(activity);
        }

        return activitiesByStrings;
    }
}