public virtual class TRImportBase 
{
    protected final String RESULT_TIMESTAMPCONFLICT = 'Timestamp Conflict';
    protected final String RESULT_MANUALUPDATEREQUIRED = 'Manual update required';
    protected final String RESULT_ERROR = 'Unresolved Error';
    
    protected final String RESULT_CONTACTIDMATCH = 'Constituent TR ID Match';
    protected final String RESULT_MEMBERIDMATCH = 'Constituent ID Match';
    protected final String RESULT_CONTACTEMAILMATCH = 'Constituent Email Match';
    protected final String RESULT_CONTACTADDRESSMATCH = 'Constituent Address Match';
    protected final String RESULT_CONTACTPHONEMATCH = 'Constituent Phone Match';
    protected final String RESULT_NEWCONTACT = 'New Constituent';
    protected final String RESULT_INVALIDPHONE = 'Invalid Mobile Phone';
    protected final String RESULT_SHIRTSIZENOTFOUND = 'Shirt Size Not Found';
    
    protected final String RESULT_CAMPAIGNNOTFOUND = 'Campaign Not Found';
    protected final String RESULT_REGISTRATIONNOTFOUND = 'Registration Not Found';
    protected final String RESULT_TEAMIDMATCH = 'Team ID Match';
    protected final String RESULT_TEAMNAMEMATCH = 'Team Name Match';
    protected final String RESULT_NEWTEAM = 'New Team';
    
    protected final String RESULT_TEAMNOTFOUND = 'Team Not Found';
    protected final String RESULT_CONTACTNOTFOUND = 'Constituent Not Found'; 
    protected final String RESULT_MERGEREQUESTTFOUND = 'Merge Request Already Exists';
    protected final String RESULT_DONORNOTFOUND = 'Donor Not Found';
    protected final String RESULT_NEWCONTACTREGISTRATION = 'New Contact Registration';
    protected final String RESULT_TEAMCAPTAIN = 'Team Captain';
    protected final String RESULT_TEAMMEMBER = 'Team Member';
    protected final String RESULT_NONREGISTERD_PARTICIPANT = 'Non-registered Participant';

    protected final String CONTACTREGISTRATION_CAPTAIN = 'Captain';
    
    protected final String RESULT_TRANSACTIONCREATED = 'Transaction Created';
    protected final String RESULT_REFUNDTRANSACTION = 'Registration Refund Transaction';
    protected final String RESULT_DONATIONREGISTRATIONNOTFOUND = 'Donation Registration Not Found';
    protected final String RESULT_CONTACTINACTIVE = 'Constituent or Donor Inactive';
    
    protected final String INCOMETYPE_REGISTRATIONFEE = 'Registration Fee';
    protected final String INCOMETYPE_DONATION = 'Donation';
    protected final String INCOMETYPE_REFUND = 'Refund';
    protected final String INCOMETYPE_TEAMFR = 'Team FR';
    
    protected final String TRANSACTIONTYPE_TRANSACTION = '1';
    protected final String TRANSACTIONTYPE_FULLREFUND = '2';
    protected final String TRANSACTIONTYPE_PARTIALREFUND = '3';
    protected final String TRANSACTIONTYPE_TRCHANGE = '4';

    protected final String LOTRANSACTIONTYPE_REFUND = '9';
    
    protected final String FUNDTYPE_REGISTRATION = 'R';
    protected final String FUNDTYPE_COLLECTION = 'C';
    protected final String FUNDTYPE_DONATION = 'D';
    protected final String FUNDTYPE_SALE = 'S';
    protected final String FUNDTYPE_UPSELL = 'U';
    
    protected final String TENDERTYPE_CASH = 'Cash';
    protected final String TENDERTYPE_CHEQUE = 'Cheque';
    protected final String TENDERTYPE_CHECK = 'Check';
    protected final String TENDERTYPE_CREDITCARD = 'Credit Card';
    protected final String TENDERTYPE_XCHECKOUT = 'XCheckout';
    
    protected final String PAYMENTTYPE_CASH = 'Cash';
    protected final String PAYMENTTYPE_CHEQUE = 'Cheque/Money Order';
    protected final String PAYMENTTYPE_TEAMRAISERONLINE = 'Teamraiser Online';
    protected final String PAYMENTTYPE_PAYPAL = 'PayPal';
    protected final String PAYMENTTYPE_TEAMRAISER = 'TeamRaiser';
    
    protected final String CANCEREXPERIENCE_IHAVEORHADCANCER = 'I have or had cancer';
    protected final String CANCEREXPERIENCE_CAREGIVERFORSOMEONEWHO = 'Caregiver for someone who';

    protected final String CONTACTREGISTRATION_FRIENDCANCER = 'Friend has/had cancer';
    protected final String CONTACTREGISTRATION_OTHERCANCER = 'Other has/had cancer';
    protected final String CONTACTREGISTRATION_RELATIVECANCER = 'Relative has/had cancer';
    
    protected final String YES = 'Yes';
    protected final String ONE = '1';

    protected final String SURVIVOR_SURVIVOR  = 'Survivor';
    protected final String SURVIVOR_CARER  = 'Carer';
    protected final String SURVIVOR_BOTH  = 'Survivor/Carer';
    protected final String SURVIVOR_YES  = YES;
    
    protected final String STATUS_REGISTERED  = 'Registered';
    protected final String STATUS_CANX = 'CANX';

    protected final String REGISTRATIONMETHOD_TEAMRAISER = 'TeamRaiser';

    protected final String REGISTRATIONFEESTATUS_PAID = 'Paid';

    protected final String RECORDTYPE_CANCELLED = 'Cancellation';

    protected final String CONTACTSTATUS_ACTIVE = 'Active';
    protected final String CONTACTSTATUS_TOBEMERGED = 'To be Merged';
    protected final String CONTACTSTATUS_DECEASEDNONEXTOFKIN = 'Deceased - no Next of Kin';

    protected final String TEAMCAPTAIN = 'Captain';

    protected final String NOSHIRT = 'I do not need a shirt';

    protected Integer processedSuccesfully;
    @TestVisible
    protected Boolean forceTestError = false;

    protected RecordType TransactionRecordType
    {
        get
        {
            if(TransactionRecordType == null)
            {
                TransactionRecordType = [SELECT     ID
                                        FROM        RecordType
                                        WHERE       Name = 'Donation' AND
                                                    sObjectType = 'Opportunity'];
            }
            return TransactionRecordType;
        } 
        set;
    }
    
    protected RecordType RegistrationRecordType
    {
        get
        {
            if(RegistrationRecordType == null)
            {
                RegistrationRecordType = [SELECT        ID
                                            FROM        RecordType
                                            WHERE       Name = 'Team Event Registration' AND
                                                        sObjectType = 'Registration__c'];
            }
            return RegistrationRecordType;
        } 
        set;
    }
    
    protected void throwTestError()
    {
        if(forceTestError && Test.isRunningTest())
        {
            system.debug(1/0);
        }
    }
    
    protected Integer BatchSize
    {
        get
        {
            return Test.isRunningTest() ? 1 : Batch_Size__c.getInstance('TR_Import').Records__c.IntValue();
        }
    }
    
    public Boolean RunThisOnly
    {
        get
        {
            if(RunThisOnly == null)
            {
                return false;
            }
            return RunThisOnly;
        }
        set;
    }
    
    protected Group TeamRaiserConflictQueue
    {
        get
        {
            if(TeamRaiserConflictQueue == null)
            {
                TeamRaiserConflictQueue = [SELECT   ID
                                        FROM        Group
                                        WHERE       Name = 'TR Import Conflict Queue' AND
                                                    Type = 'Queue'];
            }
            return TeamRaiserConflictQueue;
        }
        set;
    }
    
    protected Date parseDate(String s, sObject stagingRecord)
    {
        if(String.isBlank(s))
        {
            return null;
        }
        Date d = null;
        try
        {
            String[] dateTimeParts = s.split(' ');
            String[] dateParts;
            // Cater for both date formats rather than be strict
            if(s.contains('/'))
            {
                // 1/07/2014 10:23 AM
                dateParts = dateTimeParts[0].split('/');
                d = Date.newInstance(Integer.valueOf(dateParts[2]), 
                                    Integer.valueOf(dateParts[1]), 
                                    Integer.valueOf(dateParts[0]));
            }
            else if(s.contains('-'))
            {
                // 2014-01-07 08:45:15
                dateParts = dateTimeParts[0].split('-');
                d = Date.newInstance(Integer.valueOf(dateParts[0]), 
                                    Integer.valueOf(dateParts[2]), 
                                    Integer.valueOf(dateParts[1]));
            }
        }
        catch(Exception ex)
        {
            throw new CCException('Invalid date: ' + s + '\n' + ex.getMessage());
        }
        return d;
    }
    
    protected DateTime parseDateTime(String s, sObject stagingRecord)
    {
        if(String.isBlank(s))
        {
            return null;
        }
        DateTime d = null;
        try
        {
            String[] dateTimeParts = s.split(' ');
            String[] timeParts = dateTimeParts[1].split(':');
            String[] dateParts;
            // Cater for both date time formats rather than be strict
            if(s.contains('/'))
            {
                // 1/07/2014 10:23 AM
                dateParts = dateTimeParts[0].split('/');
                d = DateTime.newInstance(Integer.valueOf(dateParts[2]), 
                                        Integer.valueOf(dateParts[1]), 
                                        Integer.valueOf(dateParts[0]), 
                                        Integer.valueOf(timeParts[0]), 
                                        Integer.valueOf(timeParts[1]), 
                                        0);
            }
            else if(s.contains('-'))
            {
                // 2014-01-07 08:45:15
                dateParts = dateTimeParts[0].split('-');
                d = DateTime.newInstance(Integer.valueOf(dateParts[0]), 
                                        Integer.valueOf(dateParts[2]), 
                                        Integer.valueOf(dateParts[1]), 
                                        Integer.valueOf(timeParts[0]), 
                                        Integer.valueOf(timeParts[1]), 
                                        0);
            }
        }
        catch(Exception ex)
        {
            throw new CCException('Invalid datetime: ' + s + '\n' + ex.getMessage());
        }
        return d;
    }
    
    protected String parseEmail(String s, sObject stagingRecord)
    {
        if(String.isBlank(s))
        {
            return null;
        }
        String emailRegEx = '[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+';
        Pattern myPattern = Pattern.compile(emailRegex);
        Matcher myMatcher = myPattern.matcher(s);
        if(!myMatcher.matches())
        {
            throw new CCException('Invalid email address: ' + s);
        }     
        return s;   
    }
    
    protected Decimal parseDecimal(String s, sObject stagingRecord)
    {
        if(String.isBlank(s))
        {
            return null;
        }
        Decimal d = null;
        try
        {
            d = Decimal.valueOf(s);
        }
        catch(Exception ex)
        {
            throw new CCException('Invalid number: ' + s);
        }
        return d;
    }
    
    protected Integer parseInteger(String s, sObject stagingRecord)
    {
        if(String.isBlank(s))
        {
            return null;
        }
        Integer d = null;
        try
        {
            d = Integer.valueOf(s);
        }
        catch(Exception ex)
        {
           throw new CCException('Invalid number: ' + s);
        }
        return d;
    }
    
    protected Boolean translateBoolean(String s)
    {
        return s == '1';
    }
    
    protected String getTimeStampDifference(DateTime sfLastModified, DateTime trLastModified)
    {
        if(sfLastModified == null || trLastModified == null)
        {
            return '\nTimestamp is null.';
        }
        if(sfLastModified.isSameDay(trLastModified))
        {
            return '\nTimestamp difference: ' + ((sfLastModified.getTime() - trLastModified.getTime()) / 1000 / 60).format() + ' minutes';
        }
        return '\nTimestamp difference: ' + (trLastModified.date().daysBetween(sfLastModified.date())).format() + ' days';
    }

    protected void showMessage(String msg)
    {
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, msg));
    }
    
    protected void showError(Exception ex)
    {
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
    }
    
    protected void setProcessedAndOwnerFields(sObject stagingRecord)
    {
        String err = (String)stagingRecord.get('Error__c');
        stagingRecord.put('Processed__c', String.isBlank(err) ? DateTime.now() : null);
        if(String.isBlank(err))
        {
            stagingRecord.put('OwnerId', UserInfo.getUserID());
        }
    }

    protected String toCapital(String s) 
    {
        if(String.isBlank(s))
        {
            return null;
        }
        return s.toLowerCase().capitalize();
    }

    protected String toProperCase(String s) 
    {
        if(String.isBlank(s))
        {
            return null;
        }
        List<String> words = new List<String>();
        for(String word : s.toLowerCase().split(' ')) 
        {
            words.add(word.capitalize());
        }
        return String.join(words, ' ');
    }

    protected Boolean isOneOrYes(String s)
    {
        return s == YES || s == YES.toUpperCase() || s == ONE;
    }
}