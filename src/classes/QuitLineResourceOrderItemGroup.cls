public class QuitLineResourceOrderItemGroup {
    public String Name {get;set;}

    public List<QuitLineResourceOrderItem> Items {get;set;}

    public QuitLineResourceOrderItemGroup(String groupName) {
    	Name = groupName;
    	Items = new List<QuitLineResourceOrderItem>();
    }

    public void addItem(QuitLineResourceOrderItem item) {
    	if (item != null) {
    		Items.add(item);
    	}
    }
}