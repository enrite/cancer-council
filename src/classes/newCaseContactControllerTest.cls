@isTest
private class newCaseContactControllerTest {

    static testMethod void myNewCaseContactControllerTest() {
        test.startTest();
        
        // Create testing Email Settings record
        Email_Settings__c es = new Email_Settings__c(Name = 'IT Helpdesk', Email__c = 'test@cancersa.org.au.test');
        insert es;        
        
		// Create Quitline Case
		Id caseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Quitline' limit 1].Id;
		Account acc = new Account(Name = 'Test Account');
		insert acc;
		
		Contact con = new Contact(FirstName = 'Test First Name', LastName = 'Test Last Name', Phone = '08 0000 0000', MailingStreet = 'Test Street', 
									MailingCity = 'Test Suburb', MailingState = 'State', MailingPostalCode = '0000', AccountId = acc.Id);
		insert con;
		
		Contact con1 = new Contact(FirstName = 'Test First Name', LastName = 'Test Last Name', Phone = '08 0000 0000', MailingStreet = 'Test Street', 
									MailingCity = 'Test Suburb', MailingState = 'State', MailingPostalCode = '0000', AccountId = acc.Id);		
		insert con1;
		
		Case c = new Case(First_Name__c = 'Test First Name', Last_Name__c = 'Test Last Name', Phone__c = '08 0000 0000',Contact_Street__c = 'Test Street'
							, Suburb__c = 'Test Suburb', State__c = 'State', Contact_Postcode__c = '0000', RecordTypeId = caseRecordTypeId);
		insert c;
		
		
        
		PageReference pageRef = new PageReference('/apex/Casecontactstep1');
		pageRef.getParameters().put('caseid', c.Id);
		
		test.setCurrentPage(pageRef);
		
		newCaseContactController cc = new newCaseContactController();
		
		cc.getAccount();
		cc.getContact();
		cc.getCase();
		
		cc.step1();
		cc.step2();
		cc.step3();
		
		cc.save();
		
		cc.cancel();
        
        test.stopTest();
    }
}