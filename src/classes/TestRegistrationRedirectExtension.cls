@isTest
public class TestRegistrationRedirectExtension
{
    public static testMethod void myUnitTest()
    {       
        Contact c = new Contact(FirstName = 'Test',
                                LastName = 'Test');
        insert c;                                
             
        Registration__c r = new Registration__c(Host_Team_Captain__c = c.Id);
        insert r;
        
        RegistrationRedirectExtension ext = new RegistrationRedirectExtension(new ApexPages.StandardController(r));        
        ext.Redir();
    }
}