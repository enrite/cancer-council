/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestImportGPSA {
    
    public static testMethod void testImportGPSA() {
        
        Account acc = new Account(Name='A F SUTTON HOSTEL', External_Source__c='GPSA', External_Id__c ='9999');
        insert acc;
        
        Contact con = new Contact (FirstName='NATHAN', LastName='ZWECK', How_Heard__c='GPSA', External_Id__c='2678');
        insert con;  
        
        ImportGPSAController accountFile = new ImportGPSAController();
        ImportGPSAController contactFile = new ImportGPSAController();
                    
        test.startTest();
        
        string existAccount = '9998,202 ONTARIO,202 ONTARIO AVENUE,MILDURA,3500,VIC,202 ONTARIO AVENUE,MILDURA,3500,VIC,03 5021 1688,03 5021 0266,,General Practitioner,' + 
                                '\n2417,3EHS DETACHMENT DARWIN,RAAF BASE DARWIN,WINNELLIE,820,NT,RAAF BASE DARWIN,WINNELLIE,820,NT,08 8923 5447,08 8923 5454,,General Practitioner,\n';
        string newAccount = '9999,A F SUTTON HOSTEL,101 LAKE TERRACE EAST,MOUNT GAMBIER,5290,SA,"SHOP 11, MARTINS PLAZA SHOPPING CENTRE, 237 MARTINS ROAD",MOUNT GAMBIER,5290,SA' + 
                                ',08 8725 7377,08 8725 8262,admin@boandiklodge.org.au,Residential Aged Care Facility,boandiklodge';
        accountFile.uploadFileContent = Blob.valueOf('location_id,practice_name,street_address,street_suburb,street_postcode,street_state,postal_address,postal_suburb,postal_postcode,' +
                                'postal_state,practice_phone,practice_fax,practice_email,type1_id,username\n' + existAccount + newAccount);
        accountFile.objectTypeSelected = 'Account';
        accountFile.email('test@test.com');
        accountFile.loadFile();
               
        string existContact = '2678,Dr,ZWECK,NATHAN,,,nathan.zweck@daniladilba.org.au,Male,General Practitioner,General Practitioner,,,,,552,0451787H,Fax,DANILA DILBA HEALTH SERVICE,' + 
                                '32-34 KNUCKEY STREET,DARWIN,800,NT,GPO BOX 2125,DARWIN,801,NT,08 8942 3444,08 8941 3542,info@daniladilba.org.au,General Practitioner,25/10/2011\n';
        string newContact = '0000,Dr,ZWIJNENBURG,ALBERDINA (DIANA),,0421 059 936,doctors@hawkinsmedical.com.au,Female,General Practitioner,General Practitioner,,,,,9999,2698022Y,' + 
                                'Practice Email,A F SUTTON HOSTEL,30 STURT STREET,MOUNT GAMBIER,5290,SA,PO BOX 183,MOUNT GAMBIER,5290,SA,08 8725 5266,08 8723 1297,doctors@hawkinsmedical.com.au,General Practitioner,23/11/2011';
        contactFile.uploadFileContent = Blob.valueOf('gp_id,title,surname,first_name,middle_initial,mobile_number,email_address,gender,type,type1_id,type2_id,type3_id,type4_id,type5_id,' + 
                                        'location_id,gp_provider_number,communication_pref,practice_name,street_address,street_suburb,street_postcode,street_state,postal_address,postal_suburb,' + 
                                        'postal_postcode,postal_state,practice_phone,practice_fax,practice_email,type1_id,practice_last_updated\n' + existContact + newContact);
        contactFile.objectTypeSelected = 'Contact';
        contactFile.loadFile();
        test.stopTest();    
    }
}