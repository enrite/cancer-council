public with sharing class CampaignExtension 
{
    //Variable to hold current record
    Campaign currentRecord;
    public String cloneTo { get; set; }
    public String newCampaignName { get; set; }
    public String newCampaignCode { get; set; }
    public Boolean cloneEventRecords { get; set; }
    
    //Create the constructor to get the current record
    public CampaignExtension(ApexPages.StandardController controller) 
    {
        currentRecord = (Campaign)controller.getRecord();
        
        newCampaignName = 'Copy of ' + currentRecord.Name;
        
        cloneEventRecords = true;
    }
    
    public List<SelectOption> ExistingCampaigns
    {    
        get
        {
            if (ExistingCampaigns == null)
            {
                ExistingCampaigns = new List<SelectOption>();
                ExistingCampaigns.add(new SelectOption('', '--New Campaign--'));
                
                for (Campaign c : [SELECT Id,
                                          Name,
                                          Campaign_Code__c
                                   FROM Campaign
                                   WHERE Event_Date__c >= :Date.today()
                                   ORDER BY Name])
                {
                    ExistingCampaigns.add(new SelectOption(c.Id, c.Name + (c.Campaign_Code__c != null ? ' (' + c.Campaign_Code__c + ')' : '')));
                }                                   
            }
            return ExistingCampaigns;
        }
        set;
    }
    
    public PageReference cloneRecord()
    {
        try
        {
            if (cloneTo == null)
            {
                if(newCampaignName == '' || newCampaignName == '' )
                {                                        
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'You must specify a new Campaign Name and Campaign Code to continue.'));
                    return null;  
                }
                                        
                if (newCampaignName.length() > 80)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'The Campaign Name cannot be more than 80 characters'));
                    return null;
                }    
                
                if (newCampaignCode.length() > 40)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'The Campaign Code cannot be more than 40 characters'));
                    return null;
                }  
            }
        
            PageReference pr;
            Campaign newRecord;
            List<Event_Details__c> insertEVDetails = new List<Event_Details__c>();
                        
            String table = 'Campaign';
            String query_string = 'SELECT ';
            List<String> table_list = new List<String>();
            
            //Set the current Id into a variable so that it can be used in the SOQL Query
            //as this cannot be used as a bind variable.
            String currentIdField = currentRecord.id;
            
            //Add the table to a list of tables
            table_list.add(table);
            
            //get the full list of field names from the table.
            Set<String> table_rows = schema.describeSObjects(table_list)[0].fields.getMap().keyset();
            
            //Loop through each returned fieldname and add it to the query string
            //to build the select statement
            for(String row : table_rows){
                
                query_string += row + ',';
            }
            query_string += 'RecordType.DeveloperName';
            
            //Remove the comma on the last field added to the select.
            //query_string = query_string.removeEnd(',');
            
            //Build the query string
            query_string += ' FROM ' + table + ' where id = \''+currentIdField+'\' ';
        
            //Call the dynamic SOQL Query to retrieve the Campaign record with all its fields 
            //so that the whole object is cloned.
            currentRecord = Database.query(query_string);

            //Clone the record retrieved with all its fields
            newRecord = currentRecord.clone(false);
            
            //Replace the Name and code so they are unique
            if (cloneTo == null)
            {
                newRecord.Name = newCampaignName;
                newRecord.Campaign_Code__c = newCampaignCode;                                
                
                // null some fields based on record type
                if (currentRecord.RecordType.DeveloperName == 'Donations_Campaigns')
                {
                    newRecord.Status = 'In Progress';
                    newRecord.StartDate = null;    
                    newRecord.EndDate = null;    
                    newRecord.Payment_Collection_Source__c= null;    
                    newRecord.Funeral_Org__c = null;    
                    newRecord.Deceased_Name__c = null;    
                    newRecord.Deceased_Contact__c = null;    
                    newRecord.Next_of_Kin__c = null;    
                    newRecord.Letter_Printed__c = false;                        
                    newRecord.Donation_Specified__c = null;    
                    newRecord.Description = null;    
                }
                else if (currentRecord.RecordType.DeveloperName == 'Campaign_Standard')
                {
                    newRecord.Status = 'In Progress';        
                    newRecord.Event_Date__c = null;    
                    newRecord.StartDate = null; 
                    newRecord.EndDate = null;    
                    newRecord.External_Data_Source__c = null;    
                    newRecord.ExpectedRevenue = null;    
                    newRecord.BudgetedCost = null;    
                    newRecord.ActualCost = null;    
                    newRecord.Recipients_Targeted__c = null;    
                    newRecord.Event_ID__c = null;    
                    newRecord.Location_ID__c = null;    
                    newRecord.Registration_Record_Type__c = null;    
                    newRecord.TR_Event_ID__c = null;                           
                    newRecord.Description = null;                           
                }
                else if (currentRecord.RecordType.DeveloperName == 'Appeals_Campaign')
                {
                    newRecord.Status = 'In Progress';
                    newRecord.StartDate = null;  
                    newRecord.EndDate = null;    
                    newRecord.External_Data_Source__c = null;    
                    newRecord.ExpectedRevenue = null;    
                    newRecord.BudgetedCost = null;    
                    newRecord.ActualCost = null;    
                    newRecord.Recipients_Targeted__c = null;    
                    newRecord.Mailhouse_Org__c = null;    
                    newRecord.Printer_Org__c = null;    
                    newRecord.Postage_Org__c = null;      
                    newRecord.Data_Services_Org__c = null;      
                    newRecord.Creative_Services_Org__c = null;      
                    newRecord.Mailing_Costs__c = null;      
                    newRecord.Printers_Costs__c = null;      
                    newRecord.Postage_Costs__c = null;      
                    newRecord.Data_Services_Costs__c = null;      
                    newRecord.Creative_Services_Cost__c = null;      
                    newRecord.Description = null;      
                }
            }
            else
            {
                for (Campaign c : [SELECT Id,
                                          Name,
                                          Campaign_Code__c,
                                          StartDate,
                                          EndDate
                                   FROM Campaign
                                   WHERE Id = :cloneTo])
                {
                    newRecord.Id = c.Id;  
                    newRecord.Name = c.Name;
                    newRecord.Campaign_Code__c = c.Campaign_Code__c;
                    newRecord.StartDate = c.StartDate;
                    newRecord.EndDate = c.EndDate;
                }                                   
            }                        
                            
            upsert newRecord;
            
            if (cloneEventRecords)
            {
                /*Clone the event details*/
                
                List<Event_Details__c> allEVDetails = new List<Event_Details__c>();
                
                 //Cloning the Event Detail table
                String tableRL1 = 'Event_Details__c';
                
                //Create the query string
                String query_RL1 = 'SELECT ';
                
                //Create a list to hold the name of the table 
                List<String> rel_list = new List<String>();
                
                //Get the current Campaign id for use in the SOQL
                String curentRecordId  = currentRecord.id;
                
                //Add the table name to the list
                rel_list.add(tableRL1);
                
                //retrieve the field names from the salesforce metadata.
                Set<String> RL1_rows = schema.describeSObjects(rel_list)[0].fields.getMap().keyset();
                
                //Loop through each field name and add it to the query string
                for(String RL1_row : RL1_rows){
                    query_RL1 += RL1_row + ',';
                }
                
                //Remove the comma from the last field name in the string
                query_RL1 = query_RL1.removeEnd(',');
                
                //Build the final query string
                query_RL1 += ' FROM ' + tableRL1 + ' where Campaign__c = \''+curentRecordId+'\' ';
            
                //Query for all event details against the original Campaign      
                allEVDetails = Database.query(query_RL1);
    
                for( Event_Details__c  evDetail : allEVDetails)
                {
                    //create a new Event Detail
                    Event_Details__c  newEVDetail = evDetail.clone(false);
                    newEVDetail.Campaign__c = newRecord.Id;
                    newEVDetail.Start_Date__c = null;
                    newEVDetail.End_Date__c = null;
                    newEVDetail.Site_approved_storage__c = null;
                    newEVDetail.Event_Volunteers_Required__c = null;
                    newEVDetail.Notes__c = null;
                    newEVDetail.BPay_Number__c = null;
                    insertEVDetails.add(newEVDetail);
                }
                
                if(insertEVDetails.size()>0)
                {
                    insert insertEVDetails;
                }
                
                List<Campaign_Event_Location__c> cels = new List<Campaign_Event_Location__c>();
                for (Campaign_Event_Location__c cel : [SELECT Id,
                                                              Event_Location__c                                                              
                                                       FROM Campaign_Event_Location__c
                                                       WHERE Campaign__c = :curentRecordId])
                {
                    cels.add(new Campaign_Event_Location__c(Campaign__c = newRecord.Id, Event_Location__c = cel.Event_Location__c));    
                }                                                       
                
                if (cels.size() > 0)
                {
                    insert cels;
                }
            }
            
            pr = new PageReference('/' + newRecord.Id);
            
            
            return pr;
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, e.getMessage()));
            return null;
        }
    }   
    
    public PageReference cancelRecord()
    {
        PageReference pr = new PageReference('/'+currentRecord.Id);
        return pr;
    }     
    
}