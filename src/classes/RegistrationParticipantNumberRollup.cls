public with sharing class RegistrationParticipantNumberRollup {
    private static string emailAddress {
        get {
            if (emailAddress == null) {
                emailAddress = [SELECT Id, Email__c FROM Email_Settings__c WHERE Name = 'IT Helpdesk'].Email__c;
            }
            return emailAddress;
        }
    }
    
    private static map<Id, Registration__c> mapRegistration {get;set;}
    private static map<Id, Registration__c> mapOldRegistration {get;set;}
    
    public static void updateParticipantRollupNumber (map<Id, Registration__c> mapRecord, map<Id, Registration__c> mapOldRecord) {
        try {
	        mapRegistration = mapRecord;
	        mapOldRegistration = mapOldRecord;
	
	        // To hold Community Engagement Ids
	        map<Id, Id> mapSObjectId = new map<Id, Id>();
	        
	        // Map lists to hold Stats
	        map<Id, integer> mapCountApplied = new map<Id, integer>();
	        map<Id, integer> mapCountAccepted = new map<Id, integer>();
	        map<Id, integer> mapCountCompleted = new map<Id, integer>();
	        map<Id, integer> mapCountParticipant = new map<Id, integer>();
	        map<Id, integer> mapCountRegistration = new map<Id, integer>();
	        
	        list<Ambassador_Activity__c> listCommunityEngagement = new list<Ambassador_Activity__c>();
	        
	        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Registration__c' AND DeveloperName = 'Enrolment'].Id;
	        
	        if (mapRegistration.size() > 0) {            
	            // Get Community Engagement from Registration lookup field 
	            for (Registration__c reg : mapRegistration.values()) {
	            	if (reg.RecordTypeId == recordTypeId) {
	            		mapSObjectId.put(reg.Community_Engagement__c, reg.Community_Engagement__c);	
	            	}
	            }
	            for (Registration__c reg : mapOldRegistration.values()) {
	            	if (!mapSObjectId.containsKey(reg.Community_Engagement__c) && reg.RecordTypeId == recordTypeId) {
	            		mapSObjectId.put(reg.Community_Engagement__c, reg.Community_Engagement__c);
	            	}
	            }
	        }
	        
	        for (AggregateResult ar : [SELECT COUNT(Id) countNum, Community_Engagement__c 
	        							FROM Registration__c 
	        							WHERE Community_Engagement__c IN :mapSObjectId.keySet() 
	        							AND (Status__c = 'Applied') 
	        							GROUP BY Community_Engagement__c]) {
	        	mapCountApplied.put((Id) ar.get('Community_Engagement__c'), (integer) ar.get('countNum'));
	        }
	        
	        for (AggregateResult ar : [SELECT COUNT(Id) countNum, Community_Engagement__c 
	        							FROM Registration__c 
	        							WHERE Community_Engagement__c IN :mapSObjectId.keySet() 
	        							AND (Status__c = 'Accepted') 
	        							GROUP BY Community_Engagement__c]) {
	        	mapCountAccepted.put((Id) ar.get('Community_Engagement__c'), (integer) ar.get('countNum'));
	        }	        
	
	        for (AggregateResult ar : [SELECT COUNT(Id) countNum, Community_Engagement__c 
	        							FROM Registration__c 
	        							WHERE Community_Engagement__c IN :mapSObjectId.keySet() 
	        							AND Status__c = 'Completed'
	        							GROUP BY Community_Engagement__c]) {
	        	mapCountCompleted.put((Id) ar.get('Community_Engagement__c'), (integer) ar.get('countNum'));
	        }

	        for (AggregateResult ar : [SELECT COUNT(Id) countNum, Community_Engagement__c 
	        							FROM Registration__c 
	        							WHERE Community_Engagement__c IN :mapSObjectId.keySet() 
	        							AND (Status__c = 'Completed'
	        							OR Status__c = 'Did not complete')
	        							GROUP BY Community_Engagement__c]) {
	        	mapCountParticipant.put((Id) ar.get('Community_Engagement__c'), (integer) ar.get('countNum'));
	        }

	        for (AggregateResult ar : [SELECT COUNT(Id) countNum, Community_Engagement__c 
	        							FROM Registration__c 
	        							WHERE Community_Engagement__c IN :mapSObjectId.keySet()
	        							GROUP BY Community_Engagement__c]) {
	        	mapCountRegistration.put((Id) ar.get('Community_Engagement__c'), (integer) ar.get('countNum'));
	        }
	        
	        for (Id i : mapSObjectId.keySet()) {
	        	Ambassador_Activity__c ce = new Ambassador_Activity__c(Id = i);
	        	// Set to 0 if Community Engagement does not have any related stats
	        	ce.Participants_Applied__c = mapCountApplied.get(mapSObjectId.get(i)) == null ? 0 : mapCountApplied.get(mapSObjectId.get(i));        	        	
	        	ce.Participants_Accepted__c = mapCountAccepted.get(mapSObjectId.get(i)) == null ? 0 : mapCountAccepted.get(mapSObjectId.get(i));
	        	ce.Participants_Completed__c = mapCountCompleted.get(mapSObjectId.get(i)) == null ? 0 : mapCountCompleted.get(mapSObjectId.get(i));
	        	ce.Total_Participants__c = mapCountParticipant.get(mapSObjectId.get(i)) == null ? 0 : mapCountParticipant.get(mapSObjectId.get(i));
	        	ce.Total_Registrations__c = mapCountRegistration.get(mapSObjectId.get(i)) == null ? 0 : mapCountRegistration.get(mapSObjectId.get(i));
	        	
	        	listCommunityEngagement.add(ce);
	        }
	        update listCommunityEngagement;
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** ' + RegistrationParticipantNumberRollup.class.getName() + ': RegistrationParticipantNumberRollup' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, RegistrationParticipantNumberRollup.class.getName() + ': RegistrationParticipantNumberRollup', errorMessage, 'HTML');
        }
    }
}