public with sharing class LinkRegistrationContactNew2Controller{
    private Registration__c[] mRegistration;
    private Contact mContact;
    private Account mAccount;
    String mRegistrationID;
    String mAccountID;

    private map<String, Id> accountRecordType {
        get {
            if (accountRecordType == null) {
                accountRecordType = new map<string, Id>();
                
                for (RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType='Account']) {
                    accountRecordType.put(rt.DeveloperName, rt.Id);
                }
            }
            return accountRecordType;
        }
        set;
    }
    
    public LinkRegistrationContactNew2Controller(){

        mRegistrationID = ApexPages.CurrentPage().getParameters().get('regid');
        mAccountID = ApexPages.CurrentPage().getParameters().get('accountid');
        
        mRegistration = [SELECT
            id,
            Host_Team_Captain__c,
            Web_Street__c,
            Web_Suburb__c,
            Web_Postcode__c,
            Web_State__c,
            Web_First_Name__c,
            Web_Last_Name__c,
            Web_Email__c,
            Web_Phone__c,
            Web_Mobile__c,
            Web_Date_of_Birth__c,
            Web_Account__c,
            Web_Gender__c
            FROM Registration__c
            WHERE id = :mRegistrationID];
        
        mContact = new Contact();
        
        mAccount = [SELECT
            id,
            Name,
            RecordTypeId
            FROM Account
            WHERE id = :mAccountID];
        
        if(mRegistration.size() > 0){
            mContact.FirstName = mRegistration[0].Web_First_Name__c;
            mContact.LastName = mRegistration[0].Web_Last_Name__c;
            mContact.MailingStreet = mRegistration[0].Web_Street__c;
            mContact.MailingPostalCode = mRegistration[0].Web_Postcode__c;
            mContact.MailingState = mRegistration[0].Web_State__c;
            mContact.MailingCity = mRegistration[0].Web_Suburb__c;
            mContact.MobilePhone = mRegistration[0].Web_Mobile__c;
            mContact.Phone = mRegistration[0].Web_Phone__c;
            mContact.Email = mRegistration[0].Web_Email__c;
            mContact.BirthDate = mRegistration[0].Web_Date_of_Birth__c;
            mContact.Gender__c = mRegistration[0].Web_Gender__c;
            mContact.AccountId = mAccountID;
            mContact.How_Heard__c =  'Cancer Council Web Site';
        }
        
    }
    
    public PageReference save(){
        insert mContact;        
        if (mAccount.RecordTypeId != accountRecordType.get('Non_Organisation')) {
            mRegistration[0].Host_Team_Captain_Org__c = mAccount.id;
            mRegistration[0].Org_Primary_Contact__c = mContact.id;  
        }
        else {
            mRegistration[0].Host_Team_Captain__c = mContact.id;    
        }
        update mRegistration[0];
        return new PageReference('/' + mRegistrationID);
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mRegistration[0].id);
    }
    
    public Registration__c getRegistration(){
        return mRegistration.size() > 0 ? mRegistration[0] : null;
    }
    
    public Contact getContact(){
        return mContact;
    }
    
    public Account getAccount(){
        return mAccount;
    }
}