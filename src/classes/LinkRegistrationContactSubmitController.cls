public with sharing class LinkRegistrationContactSubmitController{
    String mRegistrationID;
    
    public LinkRegistrationContactSubmitController(){
              
    }
    
    public PageReference goToRegistration(){
        return new PageReference(mRegistrationID);
    }
    
    public PageReference updateRecord(){
        mRegistrationID = ApexPages.CurrentPage().getParameters().get('regid');
        String vContactID = ApexPages.CurrentPage().getParameters().get('contactid');
        
        Registration__c[] vRegistration = [SELECT
            id,
            Host_Team_Captain__c
            FROM Registration__c
            WHERE id = :mRegistrationID];
            
       vRegistration[0].Host_Team_Captain__c = vContactID;
       update vRegistration[0]; 
       return new PageReference('/' + mRegistrationID);
    }
}