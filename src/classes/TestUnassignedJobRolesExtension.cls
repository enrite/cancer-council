@isTest
public class TestUnassignedJobRolesExtension
{
    public static testMethod void myUnitTest()
    {       
        Event_Details__c ed = new Event_Details__c();        
        
        UnassignedJobRolesExtension ext = new UnassignedJobRolesExtension(new ApexPages.StandardController(ed));        
        system.debug(ext.UnassignedCount);
        
        ed.Start_Date__c = Date.today();
        ed.End_Date__c = Date.today();
        
        ext = new UnassignedJobRolesExtension(new ApexPages.StandardController(ed));        
        system.debug(ext.UnassignedCount);
    }
}