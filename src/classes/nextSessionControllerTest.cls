@isTest
private class nextSessionControllerTest {

    static testMethod void myNextSessionPageTest() {
		test.startTest();
		
		// Create Quitline Session records
		list<Quitline_Sessions__c> listQuitlineSession = new list<Quitline_Sessions__c>();
				
		for (integer i = 1; i < 2; i++) {
			Quitline_Sessions__c qs = new Quitline_Sessions__c();
			qs.Name = '1';
			qs.Date__c = date.today().addDays(i);
			qs.Day_Type__c = 'Full Day';
			qs.Call_Type__c = 'Quitline';
			listQuitlineSession.add(qs);
		}
		insert listQuitlineSession;		
		
		// Create Quitline Case
		Id caseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Quitline' limit 1].Id;
		Case c = new Case(RecordTypeId = caseRecordTypeId, Call_Type__c='Quitline');
		insert c;
		
		PageReference pageRef = new PageReference('/apex/nextSessionPage');
		pageRef.getParameters().put('id', c.Id);
		
		test.setCurrentPage(pageRef);

		//Create a new instance of standard controller
		ApexPages.StandardController s = new ApexPages.standardController(c);		
		nextSessionController sc = new nextSessionController(s);
		
		system.assertEquals(sc.renderDate, 'true');
		
		sc.sessionIdSelected = string.valueOf(listQuitlineSession[0].get('Id'));
		system.assert(sc.getDateList().size()>0);
		sc.dateSelected();
		
		sc.timeOfDaySelected = 'AM';
		system.assert(sc.getDayTimeList().size()>0);		
		sc.dayTimeSelected();
		system.assertEquals(sc.timeOfDaySelected, 'AM');
		
		system.assert(sc.getTimeList().size()>0);
		
		sc.timeSelected = '8.30-9.30 AM';
		sc.timeSelected();
		system.assertEquals(sc.timeSelected, '8.30-9.30 AM');
		
		sc.anytimeCheckbox = true;
		sc.timeRequestedTextBox = '4pm';
		
		// Save information to create Call record
		sc.saveCallbackFromCase();
		
		system.assertEquals(sc.renderInitialCall, 'true');
		system.assertEquals(sc.renderInitialCallDelete, 'true');
		
		// To check new Call record is created
		system.assert(string.valueOf(sc.getLinkedCallbackFromCase().get('Name')).length()>0);
		
		sc.deleteRecordFromCase();
		test.stopTest();
    }
    
    static testMethod void myNextSessionPage2Test() {
		test.startTest();
		
		// Create Quitline Session records
		list<Quitline_Sessions__c> listQuitlineSession = new list<Quitline_Sessions__c>();
				
		for (integer i = 1; i < 2; i++) {
			Quitline_Sessions__c qs = new Quitline_Sessions__c();
			qs.Name = '1';
			qs.Date__c = date.today().addDays(i);
			qs.Day_Type__c = 'Full Day';
			qs.Call_Type__c = 'Quitline';
			listQuitlineSession.add(qs);
		}
		insert listQuitlineSession;
		
		// Create Quitline Case
		Id caseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Quitline' limit 1].Id;
		Case c = new Case(RecordTypeId = caseRecordTypeId, Call_Type__c='Quitline');
		insert c;
		
		// Create Callback
		Callback__c cb = new Callback__c(Case__c = c.Id, Quitline_Sessions__c = (Id) listQuitlineSession[0].get('Id'), Status__c = 'Completed');
		insert cb;
		
		//Create a new instance of standard controller
		//ApexPages.StandardController s = new ApexPages.standardController(c);
		
		PageReference pageRef = new PageReference('/apex/nextSessionPage2');
		pageRef.getParameters().put('id', cb.Id);
		
		test.setCurrentPage(pageRef);
		
		nextSessionController sc = new nextSessionController();
		
		system.assertEquals(sc.renderCallDate, 'true');
		
		sc.sessionIdSelected = string.valueOf(listQuitlineSession[0].get('Id'));
		system.assert(sc.getDateList().size()>0);
		sc.dateSelected();

		sc.timeOfDaySelected = 'PM';
		system.assert(sc.getDayTimeList().size()>0);		
		sc.dayTimeSelected();
		system.assertEquals(sc.timeOfDaySelected, 'PM');
		
		system.assert(sc.getTimeList().size()>0);
		
		sc.timeSelected = '2-3 PM';
		sc.timeSelected();
		system.assertEquals(sc.timeSelected, '2-3 PM');
		
		sc.anytimeCheckbox = true;
		sc.timeRequestedTextBox = '3pm';
		
		// Save information to create Call record
		sc.saveCallbackFromCallback();
		
		system.assertEquals(sc.renderNextCallDelete, 'true');

		// To check new Call record is created with no previous callback exists
		system.assert(string.valueOf(sc.getPreviousCallbackName()).length()==0);
		system.assert(string.valueOf(sc.getPreviousCallbackLink()).length()==0);
		system.assert(string.valueOf(sc.getNextCallbackName()).length()>0);
		system.assert(string.valueOf(sc.getNextCallbackLink()).length()>0);
		system.assert(string.valueOf(sc.getLinkedCallback()).length()>0);
		
		
		sc.deleteRecord();
		test.stopTest();
    }

    static testMethod void myNextSessionPage2Test2() {
		test.startTest();
		
		// Create Quitline Session records
		list<Quitline_Sessions__c> listQuitlineSession = new list<Quitline_Sessions__c>();
				
		for (integer i = 1; i < 2; i++) {
			Quitline_Sessions__c qs = new Quitline_Sessions__c();
			qs.Name = '1';
			qs.Date__c = date.today().addDays(i);
			qs.Day_Type__c = 'Full Day';
			qs.Call_Type__c = 'Quitline';
			listQuitlineSession.add(qs);
		}
		insert listQuitlineSession;		
		
		// Create Quitline Case
		Id caseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Quitline' limit 1].Id;
		Case c = new Case(RecordTypeId = caseRecordTypeId, Call_Type__c='Quitline');
		insert c;
		
		// Create Callback
		Callback__c cb = new Callback__c(Case__c = c.Id, Quitline_Sessions__c = (Id) listQuitlineSession[0].get('Id'), Status__c = 'Completed');
		insert cb;
		
		//Create a new instance of standard controller
		//ApexPages.StandardController s = new ApexPages.standardController(c);
		
		PageReference pageRef = new PageReference('/apex/nextSessionPage2');
		pageRef.getParameters().put('id', cb.Id);
		
		test.setCurrentPage(pageRef);
		
		nextSessionController sc = new nextSessionController();
		
		system.assertEquals(sc.renderCallDate, 'true');
		
		sc.sessionIdSelected = string.valueOf(listQuitlineSession[0].get('Id'));
		system.assert(sc.getDateList().size()>0);
		sc.dateSelected();

		sc.timeOfDaySelected = 'Evening';
		system.assert(sc.getDayTimeList().size()>0);		
		sc.dayTimeSelected();
		system.assertEquals(sc.timeOfDaySelected, 'Evening');
		
		system.assert(sc.getTimeList().size()>0);
		
		sc.timeSelected = '7-8 PM';
		sc.timeSelected();
		system.assertEquals(sc.timeSelected, '7-8 PM');
		
		sc.anytimeCheckbox = true;
		sc.timeRequestedTextBox = '7pm';
		
		// Save information to create Call record
		sc.saveCallbackFromCallback();
		
		system.assertEquals(sc.renderNextCallDelete, 'true');

		// To check new Call record is created with no previous callback exists
		system.assert(string.valueOf(sc.getPreviousCallbackName()).length()==0);
		system.assert(string.valueOf(sc.getPreviousCallbackLink()).length()==0);
		system.assert(string.valueOf(sc.getNextCallbackName()).length()>0);
		system.assert(string.valueOf(sc.getNextCallbackLink()).length()>0);
		system.assert(string.valueOf(sc.getLinkedCallback()).length()>0);
		
		sc.deleteRecord();
		test.stopTest();
    }
          
    static testMethod void myAutoCreateInitialCallFromCaseTest() {
		test.startTest();
		
		// Create Quitline Session records
		list<Quitline_Sessions__c> listQuitlineSession = new list<Quitline_Sessions__c>();
				
		for (integer i = 0; i < 2; i++) {
			Quitline_Sessions__c qs = new Quitline_Sessions__c();
			qs.Name = '1';
			qs.Date__c = date.today().addDays(i);
			qs.Day_Type__c = 'Full Day';
			qs.Call_Type__c = 'Quitline';
			listQuitlineSession.add(qs);
		}
		insert listQuitlineSession;		
		
		// Create Quitline Case
		Id caseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Quitline' limit 1].Id;		
		Case c = new Case(RecordTypeId = caseRecordTypeId, Call_Type__c='Quitline');
		insert c;
		
		PageReference pageRef = new PageReference('/apex/AutoCreateInitialCallFromCase');
		pageRef.getParameters().put('id', c.Id);
		
		test.setCurrentPage(pageRef);
		
		nextSessionController sc = new nextSessionController();
		
		sc.autoSaveCallbackFromCase();		
		test.stopTest();
    }

    static testMethod void myCaseAutoCreateOngoingCallTest() {
		test.startTest();
		
		// Create Quitline Session records
		list<Quitline_Sessions__c> listQuitlineSession = new list<Quitline_Sessions__c>();
				
		for (integer i = 0; i < 2; i++) {
			Quitline_Sessions__c qs = new Quitline_Sessions__c();
			qs.Name = '1';
			qs.Date__c = date.today().addDays(i);
			qs.Day_Type__c = 'Full Day';
			qs.Call_Type__c = 'Quitline';
			listQuitlineSession.add(qs);
		}
		insert listQuitlineSession;		
		
		// Create Quitline Case
		Id caseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Quitline' limit 1].Id;
		Case c = new Case(RecordTypeId = caseRecordTypeId, Call_Type__c='Quitline');
		insert c;
		
		Id callbackRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Callback__c' and DeveloperName = 'Initial' limit 1].Id;
		Callback__c cb = new Callback__c (Case__c = c.Id, Quitline_Sessions__c = (Id) listQuitlineSession[0].get('Id'), RecordTypeId = callbackRecordTypeId);
		insert cb;
		
		PageReference pageRef = new PageReference('/apex/CaseAutoCreateOngoingCall');
		pageRef.getParameters().put('id', c.Id);
		
		test.setCurrentPage(pageRef);
		
		nextSessionController sc = new nextSessionController();
		
		sc.autoSaveCallbackFromCaseOngoing();
		test.stopTest();
    }
    
    static testMethod void myCaseAutoCreateOngoingCallCancelTest() {
		test.startTest();
		
		// Create Quitline Session records
		list<Quitline_Sessions__c> listQuitlineSession = new list<Quitline_Sessions__c>();
				
		for (integer i = 0; i < 2; i++) {
			Quitline_Sessions__c qs = new Quitline_Sessions__c();
			qs.Name = '1';
			qs.Date__c = date.today().addDays(i);
			qs.Day_Type__c = 'Full Day';
			qs.Call_Type__c = 'Quitline';
			listQuitlineSession.add(qs);
		}
		insert listQuitlineSession;		
		
		// Create Quitline Case
		Id caseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Quitline' limit 1].Id;
		Case c = new Case(RecordTypeId = caseRecordTypeId, Call_Type__c='Quitline');
		insert c;
		
		Id callbackRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Callback__c' and DeveloperName = 'Initial' limit 1].Id;
		Callback__c cb = new Callback__c (Case__c = c.Id, Quitline_Sessions__c = (Id) listQuitlineSession[0].get('Id'), RecordTypeId = callbackRecordTypeId);
		insert cb;
		
		PageReference pageRef = new PageReference('/apex/CaseAutoCreateOngoingCall');
		pageRef.getParameters().put('id', c.Id);
		
		test.setCurrentPage(pageRef);
		
		nextSessionController sc = new nextSessionController();
		
		sc.goBackToCase();
		test.stopTest();
    }     
}