public class TRExportConstituentBatch 
    extends TRExportBase
    
    implements Database.Batchable<sObject>, Database.Stateful
{
    public TRExportConstituentBatch (TR_Export__c exportRecord)
    {
        super(exportRecord);
    }
    
    public TRExportConstituentBatch(TRExportBase prevBatch)
    {
        super(prevBatch);
    }
    
    private final String ATTACHMENTNAME = getFileName('Constituent');
    
    public Database.QueryLocator start(Database.BatchableContext info)
    {               
        Attachment att = createAttachment(ATTACHMENTNAME, getHeader());
        Set<ID> contactIDs = new Set<ID>();
        for(Registration__c r : [SELECT Host_Team_Captain__c,
                                        Org_Primary_Contact__c
                                FROM    Registration__c
                                WHERE   Campaign__r.TR_Event_Id__c != null AND
                                        (Host_Team_Captain__r.LastModifiedDate > :getLastExportTime() OR
                                        Org_Primary_Contact__r.LastModifiedDate > :getLastExportTime() OR
                                        Campaign__r.Initial_Teamraiser_Export__c = null)])
        {
            if(r.Host_Team_Captain__c != null)
            {
                contactIDs.add(r.Host_Team_Captain__c);
            }
            if(r.Org_Primary_Contact__c != null)
            {
                contactIDs.add(r.Org_Primary_Contact__c);
            }
        }
        for(Contact_Registrations__c cr : [SELECT   Contact__c,
                                                    Last_Updated_by_Team_Raiser__c,
                                                    LastModifiedDate
                                            FROM    Contact_Registrations__c
                                            WHERE   Registration__r.Campaign__r.TR_Event_Id__c != null AND
                                                    (Contact__r.LastModifiedDate > :getLastExportTime()  OR
                                                    Registration__r.Campaign__r.Initial_Teamraiser_Export__c = null)])
        {
            // if the last modified datetime is (basically) the same as the last updated by team raiser datetime then skip the record
            if (cr.Last_Updated_by_Team_Raiser__c != null
                && cr.LastModifiedDate < cr.Last_Updated_by_Team_Raiser__c.addMinutes(1))
            {
                continue;
            }
        
            contactIDs.add(cr.Contact__c);
        }
        for(Contact c :  [SELECT ID
                        FROM    Contact
                        WHERE   ID IN (SELECT   Primary_Contact_at_Organisation__c
                                        FROM    Opportunity
                                        WHERE   Campaign.TR_Event_Id__c != null AND
                                                (Primary_Contact_at_Organisation__r.LastModifiedDate > :getLastExportTime() OR
                                                Campaign.Initial_Teamraiser_Export__c = null))])
        {
            contactIDs.add(c.ID);
        }
        return Database.getQueryLocator([SELECT ID,
                                                TeamRaiser_CONS_ID__c,
                                                Contact_ID__c,
                                                Salutation,
                                                FirstName,
                                                Middle_Name__c, 
                                                LastName, 
                                                Post_Nominals__c,
                                                Professional_Suffix__c, 
                                                Preferred_Name__c, 
                                                Maiden_Name__c, 
                                                Envelope_Salutation__c, 
                                                Letter_Salutation__c,
                                                Gender__c,
                                                BirthDate, 
                                                Status__c,
                                                HasOptedOutOfEmail,
                                                Mail_Opt_Out__c,
                                                Email,
                                                Secondary_Email__c, 
                                                MailingStreet, 
                                                MailingCity,
                                                MailingState, 
                                                MailingPostalcode, 
                                                MailingCountry,
                                                OtherStreet,
                                                OtherCity, 
                                                OtherState, 
                                                OtherPostalcode, 
                                                OtherCountry,
                                                Phone_Type__c,
                                                Fax,
                                                MobilePhone, 
                                                Occupation__c, 
                                                Job_Title__c,
                                                Account.Name,  
                                                Account.BillingStreet,
                                                Account.BillingCity, 
                                                Account.BillingState, 
                                                Account.BillingPostalcode, 
                                                Account.BillingCountry,
                                                LastModifiedDate,
                                                Last_Modified_Teamraiser__c,
                                                Last_Updated_by_Team_Raiser__c
                                        FROM    Contact
                                        WHERE   ID IN :contactIDs]);
    }
    
    public void execute(Database.BatchableContext info, List<Contact> scope)
    {       
        Attachment att = getAttachment(ATTACHMENTNAME);         
        String b = att.Body.toString();
                                    
        for(Contact c : scope)
        { 
            // if the last modified datetime is (basically) the same as the last updated by team raiser datetime then skip the record
            if (c.Last_Updated_by_Team_Raiser__c != null
                && c.LastModifiedDate < c.Last_Updated_by_Team_Raiser__c.addMinutes(1))
            {
                continue;
            }
        
            b += processConstituent(c);     
            ConstituentRecords++;       
        }
        att.Body = Blob.valueOf(b);
        update att;
    } 
    
    public void finish(Database.BatchableContext info)
    {
        if(Test.isRunningTest())
        {
            return;
        }
        TRExportRegistrationBatch regBatch = new TRExportRegistrationBatch(this);
        Database.executeBatch(regBatch, BatchSize);
    }
    
    private String getHeader()
    {
        String line = '';
        line += getField('CONS_ID') + 
                    getField('MEMBER_ID') +
                    getField('CONS_TITLE') +
                    getField('FIRST_NAME') +
                    getField('MIDDLE_NAME') +
                    getField('LAST_NAME') +
                    getField('SUFFIX') +
                    getField('PROF_SUFFIX') +
                    getField('NICKNAME') +
                    getField('MAIDEN_NAME') + 
                    getField('SALUTATION_FORMAL') +
                    getField('SALUTATION_CASUAL') +
                    getField('GENDER') +
                    getField('BIRTH_DATE') + 
                    getField('DECEASED') +
                    getField('ACCEPT_EMAIL') + 
                    getField('ACCEPT_POSTAL_MAIL') + 
                    getField('PRIMARY_EMAIL') + 
                    getField('SECONDARY_EMAIL') +
                    getField('EMAIL_OPT_IN_STATUS') +
                    getField('HOME_STREET1') +
                    getField('HOME_STREET2') +
                    getField('HOME_STREET3') +
                    getField('HOME_CITY') + 
                    getField('HOME_STATEPROV') + 
                    getField('HOME_ZIP') +
                    getField('HOME_COUNTRY') +
                    getField('WORK_STREET1') + 
                    getField('WORK_STREET2') + 
                    getField('WORK_STREET3') + 
                    getField('WORK_CITY') + 
                    getField('WORK_STATEPROV') + 
                    getField('WORK_ZIP') + 
                    getField('WORK_COUNTRY') + 
                    getField('OTHER_STREET1') + 
                    getField('OTHER_STREET2') + 
                    getField('OTHER_STREET3') + 
                    getField('OTHER_CITY') + 
                    getField('OTHER_STATEPROV') + 
                    getField('OTHER_ZIP') + 
                    getField('OTHER_COUNTRY') + 
                    getField('PRIMARY_ADDRESS_TYPE') +
                    getField('ALTERNATE1_ADDRESS_TYPE') +
                    getField('PREFERRED_ADDRESS') +
                    getField('HOME_PHONE') +
                    getField('HOME_FAX') +
                    getField('WORK_PHONE') +
                    getField('MOBILE_PHONE') +
                    getField('PREFERRED_PHONE') +
                    getField('EMPLOYER') +
                    getField('EMPLOYER_INDUSTRY') +
                    getField('CONS_OCCUPATION') +
                    getField('CONS_POSITION');
        return line  + NEW_LINE;
    }
    
    private String processConstituent(Contact c)
    {
        String contactId = c.TeamRaiser_CONS_ID__c == null ? c.Contact_ID__c : null;
        String line = getField(c.TeamRaiser_CONS_ID__c, 11) + 
                    getField(contactId, 32) +
                    getField(c.Salutation) +
                    getField(c.FirstName, 40) +
                    getField(c.Middle_Name__c, 80) +
                    getField(c.LastName, 80) +
                    getField(c.Post_Nominals__c) +
                    getField(c.Professional_Suffix__c, 50) +
                    getField(c.Preferred_Name__c, 30) +
                    getField(c.Maiden_Name__c, 50) + 
                    getField(c.Envelope_Salutation__c, 100) +
                    getField(c.Letter_Salutation__c, 100) +
                    getGender(c) +
                    getField(c.BirthDate) + 
                    getField(c.Status__c == 'Deceased - Next of Kin' ||
                                                c.Status__c == 'Deceased - no Next of Kin') +
                    getField(convertBoolean(!c.HasOptedOutOfEmail)) + 
                    getField(convertBoolean(!c.Mail_Opt_Out__c)) + 
                    getField(c.Email, 80) + 
                    getField(c.Secondary_Email__c, 80) +
                    getField(c.HasOptedOutOfEmail ? '5' : '0') +
                    getStreetAddress(c.MailingStreet, 1) +
                    getStreetAddress(c.MailingStreet, 2) +
                    getStreetAddress(c.MailingStreet, 3) +
                    getField(c.MailingCity, 64) + 
                    getField(c.MailingState, 30) + 
                    getField(c.MailingPostalcode, 40) +
                    getField(c.MailingCountry, 64) +
                    getStreetAddress(c.Account.BillingStreet, 1) +
                    getStreetAddress(c.Account.BillingStreet, 2) +
                    getStreetAddress(c.Account.BillingStreet, 3) +
                    getField(c.Account.BillingCity, 64) + 
                    getField(c.Account.BillingState, 30) + 
                    getField(c.Account.BillingPostalcode, 40) +
                    getField(c.Account.BillingCountry, 64) +
                    getStreetAddress(c.OtherStreet, 1) +
                    getStreetAddress(c.OtherStreet, 2) +
                    getStreetAddress(c.OtherStreet, 3) +
                    getField(c.OtherCity, 64) + 
                    getField(c.OtherState, 30) + 
                    getField(c.OtherPostalcode, 40) +
                    getField(c.OtherCountry, 64) +
                    getField('') + 
                    getField('') +
                    getField('') +
                    getField('') +
                    getField((c.Phone_Type__c == 'Home' ? c.Phone : ''), 50) +
                    getField(c.Fax, 50) +
                    getField(c.MobilePhone, 50) +
                    getField('') +
                    getField(c.Account.Name, 100) +
                    getField('') +
                    getField(c.Occupation__c, 64) +
                    getField(c.Job_Title__c, 64);
        return line + NEW_LINE;
    }   
    
    @TestVisible
    private String getGender(Contact c)
    {
        if(String.isBlank(c.Gender__c) || c.Gender__c == '')
        {
            return getField('');
        }
        return getField(c.Gender__c, 40); 
    }
    
    @TestVisible
    private String getStreetAddress(String streetAddress, Integer part)
    {       
        if(String.isBlank(streetAddress))
        {
            return getField('');
        }
        String[] addressParts = streetAddress.split('\n');
        if(addressParts.size() >= part)
        {
            return getField(addressParts[part - 1], 128);
        }
        return getField('');
    }
    
    protected override String convertBoolean(Boolean b)
    {
        return b ? '1' : '0';
    }
}