public class LinkRegistrationContactControllerAccount{
    private static final Integer cWeightBillingStreet = 20;
    private static final Integer cWeightBillingPostalCode = 10;
    private static final Integer cWeightPhone = 40;
    private static final Integer cWeightName = 30;
    
    private boolean mMatchBillingStreet;
    private boolean mMatchBillingPostalCode;
    private boolean mMatchPhone;
    private boolean mMatchName;
    
    private Account mAccount;
    private Registration__c mRegistration;
    private Integer mScore;
    
    // Binary search tree links
    private LinkRegistrationContactControllerAccount mLeft;
    private LinkRegistrationContactControllerAccount mRight;
    
    
    public LinkRegistrationContactControllerAccount(Account pAccount, Registration__c pRegistration){
        mAccount = pAccount;
        mRegistration = pRegistration;
        mScore = 0;
        
        mMatchBillingStreet = pAccount.BillingStreet != null && pAccount.BillingStreet == pRegistration.Web_Street__c;
        mMatchBillingPostalCode = pAccount.BillingPostalCode != null && pAccount.BillingPostalCode == pRegistration.Web_Postcode__c;
        mMatchPhone = pAccount.Phone != null && pAccount.Phone == pRegistration.Web_Phone__c;
        mMatchName = pAccount.Name != null && pAccount.Name == pRegistration.Web_Account__c;
        
        if(mMatchBillingStreet) mScore += cWeightBillingStreet;
        if(mMatchBillingPostalCode) mScore += cWeightBillingPostalCode;
        if(mMatchPhone) mScore += cWeightPhone;
        if(mMatchName) mScore += cWeightName;
    }
    
    public Integer getScore(){
        return mScore;
    }
    
    public Account getAccount(){
        return mAccount;
    }
    
    public Registration__c registration(){
        return mRegistration;
    }
    
    public void add(LinkRegistrationContactControllerAccount pNode){
        if(pNode.getScore() > mScore){
            if(mRight == null) mRight = pNode;
            else mRight.add(pNode);
        }else{
            if(mLeft == null) mLeft = pNode;
            else mLeft.add(pNode);
        }
    }
    
    public void buildArrayDesc(LinkRegistrationContactControllerAccount[] pArray){
        if(mRight != NULL) mRight.buildArrayDesc(pArray);
        pArray.add(this);
        if(mLeft != NULL) mLeft.buildArrayDesc(pArray);
    }
    
    public boolean getMatchBillingStreet(){
        return mMatchBillingStreet;
    }
    
    public boolean getMatchBillingPostalCode(){
        return mMatchBillingPostalCode;
    }
    
    public boolean getMatchPhone(){
        return mMatchPhone;
    }
    
    public boolean getMatchName(){
        return mMatchName;
    }

}