public with sharing class LinkSunSmartContactController{
    public static final Integer MIN_SCORE_SHOW_POPUP = 40;
    private static final SelectOption[] cMaxResultsPicklist = new SelectOption[]{
        new SelectOption('10','10'),
        new SelectOption('20','20'),
        new SelectOption('50','50')};

    private SunSmart__c[] mSunSmart;
    private Contact[] mContactResults;
    private LinkSunSmartContactControllerContact mRootContactNode;
    private integer mTotalContacts;
    private LinkSunSmartContactControllerContact[] mSortedContacts;
    private LinkSunSmartContactControllerContact[] mSortedLimitedContacts;
    
    public String maxResultsSelected{get; set;}
    public boolean displayPopup {get; set;}
    
    public LinkSunSmartContactController(){
        maxResultsSelected = '10';
        
        String vSunSmartID = ApexPages.CurrentPage().getParameters().get('id');
        mSunSmart = [SELECT
            id,
            Web_Postcode__c,
            Web_State__c,
            Web_Street__c,
            Web_Suburb_Town__c,
            First_Name__c,
            Last_Name__c,
            School_Contact_Email__c,
            School_Email__c,
            School_Name__c,
            School_Phone__c,
            School__c,
            Title__c
            FROM SunSmart__c
            WHERE id = :vSunSmartID];
        
        if(mSunSmart.size() != 1){
            return;
        }
              
        mContactResults = [SELECT
            Email,
            FirstName,
            LastName,
            Contact_ID__c,
            Account.Name,
            Account.Org_ID__c
            FROM Contact
            WHERE
                (Email = :mSunSmart[0].School_Contact_Email__c AND Email != null) OR
                (FirstName = :mSunSmart[0].First_Name__c AND FirstName != null AND LastName = :mSunSmart[0].Last_Name__c AND LastName != null)
            LIMIT 500];
        
        mTotalContacts = mContactResults.size();
        mSortedContacts = new LinkSunSmartContactControllerContact[]{};
        if(mTotalContacts > 0){
            mRootContactNode = new LinkSunSmartContactControllerContact(mContactResults[0], mSunSmart[0]);
            for(integer i = 1; i < mTotalContacts; i++) mRootContactNode.add(new LinkSunSmartContactControllerContact(mContactResults[i], mSunSmart[0]));
            mRootContactNode.buildArrayDesc(mSortedContacts);
        }
        updateMaxResults();
    }
    
    public String getTotalContacts(){
        return String.valueOf(mTotalContacts);
    }
    
    public LinkSunSmartContactControllerContact[] getSortedContacts(){
        if(mSortedLimitedContacts == null) mSortedLimitedContacts = new LinkSunSmartContactControllerContact[]{};
        return mSortedLimitedContacts;
    }
    
    public SunSmart__c getSunSmart(){
        return mSunSmart.size() > 0 ? mSunSmart[0] : null;
    }
    
    public void setMaxResults(integer pMax){
        if(mTotalContacts > 0){
            integer vMax = mSortedContacts.size() > pMax ? pMax : mSortedContacts.size();
            mSortedLimitedContacts = new LinkSunSmartContactControllerContact[vMax];
            for(integer i = 0; i < vMax; i++) mSortedLimitedContacts[i] = mSortedContacts[i];
        }
    }
    
    public SelectOption[] getResultsOptions(){
        return cMaxResultsPicklist;
    }
    
    public void updateMaxResults(){
        setMaxResults(Integer.valueOf(maxResultsSelected));
    }
    
    public String getTotalContactsDisplayed(){
        integer vMax = Integer.valueOf(maxResultsSelected);
        return String.valueOf(mSortedContacts.size() > vMax ? vMax : mSortedContacts.size());
    }
    
    public void closePopup(){        
        displayPopup = false;    
    }
         
    public PageReference newContact(){
        return newContactConfirm();
    }
    
    public PageReference newContactConfirm(){    
        return new PageReference('/apex/LinkSunSmartContactNewPage?id=' + mSunSmart[0].id);
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mSunSmart[0].id);
    }
    
}