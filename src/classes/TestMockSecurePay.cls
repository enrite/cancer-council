@isTest
public class TestMockSecurePay implements HttpCalloutMock {
 	// Implement this interface method
    public HTTPResponse respond(HTTPRequest req)
    {                
        // Create a fake response
        HttpResponse httpRes = new HttpResponse();
        httpRes.setHeader('Content-Type', 'application/xml');
        
        String res = '<?xml version="1.0" encoding="UTF-8"?>';        
        res += '<SecurePayMessage>';
        res += '<MessageInfo>';
        res += '<messageID>8af793f9af34bea0cf40f5fb5c630c</messageID>';
        res += '<messageTimestamp>20041803161316316000+660</messageTimestamp>'; 
        res += '<apiVersion>xml-4.2</apiVersion>'; 
        res += '</MessageInfo>'; 
        res += '<RequestType>Payment</RequestType>'; 
        res += '<MerchantInfo>'; 
        res += '<merchantID>ABC0001</merchantID>'; 
        res += '</MerchantInfo>'; 
        res += '<Status>'; 
        res += '<statusCode>000</statusCode>'; 
        res += '<statusDescription>Normal</statusDescription>'; 
        res += '</Status>'; 
        res += '<Payment>'; 
        res += '<TxnList count="1">'; 
        res += '<Txn ID="1">'; 
        res += '<txnType>0</txnType>'; 
        res += '<txnSource>0</txnSource>'; 
        res += '<amount>1000</amount>'; 
        res += '<purchaseOrderNo>test</purchaseOrderNo>'; 
        res += '<approved>Yes</approved>'; 
        res += '<responseCode>00</responseCode>';
        res += '<responseText>Approved</responseText>'; 
        res += '<settlementDate>20040318</settlementDate>'; 
        res += '<txnID>009844</txnID>'; 
        res += '<CreditCardInfo>'; 
        res += '<pan>444433...111</pan>'; 
        res += '<expiryDate>09/15</expiryDate>'; 
        res += '<cardType>6</cardType>'; 
        res += '<cardDescription>Visa</cardDescription>'; 
        res += '</CreditCardInfo>'; 
        res += '</Txn>'; 
        res += '</TxnList>'; 
        res += '</Payment>'; 
        res += '</SecurePayMessage>';
        
        httpRes.setBody(res);
        httpRes.setStatusCode(200);
        return httpRes;
    }   
}