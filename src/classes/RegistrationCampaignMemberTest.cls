@isTest
private class RegistrationCampaignMemberTest {

    static testMethod void myRegistrationCampaignMemberTest() {
        test.startTest();
        
        TestDataFactory.createAccountContactTestRecords(22);
        TestDataFactory.createCampaignTestRecords(2);
        
        list<Contact> listContact = new list<Contact>([SELECT AccountId, Id FROM Contact]);
        Campaign childCampaign = [SELECT Id, ParentId FROM Campaign WHERE Name LIKE '%1%' Limit 1];
        Campaign childCampaign2 = [SELECT Id, ParentId FROM Campaign WHERE Name LIKE '%2%' Limit 1];
                
        list<Registration__c> listRegistration = new list<Registration__c>();

        list<Registration__c> rList = new list<Registration__c>();
        
        for (integer i = 0; i < listContact.size(); i++) {
            Registration__c r = new Registration__c();
            r.Campaign__c = childCampaign.Id;
            r.Registration_Date__c = date.today();
            r.Status__c = 'Registered';

             // Add to Registration Contact for even index
            if (math.mod(i, 2) == 0) {
                r.Host_Team_Captain__c = listContact.get(i).id;
            }
            else {
                r.Host_Team_Captain_Org__c = listContact.get(i).AccountId;
                r.Org_Primary_Contact__c = listContact.get(i).Id;
            }
                         
            listRegistration.add(r);
        }
        
        // Test for New Registration
        insert listRegistration;
        
        list<CampaignMember> l = new list<CampaignMember>([SELECT Id, CampaignId FROM CampaignMember]);
        
        // Including Campaign and Parent Campaign
        //system.assertEquals(l.size(), 44);        
        
        // Test for Update Registration
        Registration__c r = listRegistration.get(0);
        r.Campaign__c = childCampaign2.Id;
        
        Update r;        
        
        // Test for Delete Registration
        delete listRegistration.get(0);
        
        // After deletion should have 2 CampaignMember deleted from original list
        list<CampaignMember> l2 = new list<CampaignMember>([SELECT Id, CampaignId FROM CampaignMember]);
        //system.assertEquals(l2.size(), 42);        
        
        test.stopTest();
    }
}