public class TRImportTransactionBatch   
    extends TRImportBase
    implements Database.Batchable<sObject>, Database.Stateful
{
    private final String NEWLINE = '\n';

    public Database.QueryLocator start(Database.BatchableContext info)
    {
        /*if(!RunThisOnly)
        {
            delete [SELECT  ID
                    FROM    TR_Import_Transaction__c
                    WHERE   Processed__c != null];
        }*/
                
        if(processedSuccesfully == null) 
        {
            processedSuccesfully = 0;
        }       
        return Database.getQueryLocator([SELECT ID,
                                                Transaction_Type__c, 
                                                Transaction_ID__c, 
                                                Transaction_Date__c, 
                                                Transaction_Amount__c, 
                                                Title__c, 
                                                Team_ID__c, 
                                                TR_Last_Modified_Date__c, 
                                                TR_Constituent_ID__c, 
                                                Soft_Credit_Type__c, 
                                                Registration_ID__c, 
                                                Processing_Result__c, 
                                                Processed__c,
                                                Payment_Gateway_ID__c, 
                                                Participant_ID__c, 
                                                On_Employer_Behalf__c, 
                                                Merchant_Account_ID__c, 
                                                Member_ID__c, 
                                                Last_Name__c, 
                                                In_Honour_Last_Name__c, 
                                                In_Honour_Gift_Type__c, 
                                                In_Honour_First_Name__c, 
                                                Home_Street2__c, 
                                                Home_Street1__c, 
                                                Home_State__c, 
                                                Home_Postcode__c, 
                                                Home_Country__c, 
                                                Home_City__c, 
                                                Fund_Type__c, 
                                                First_Name__c, 
                                                Event_Name__c, 
                                                Event_ID__c, 
                                                Employer__c, 
                                                Credit_Type__c, 
                                                CC_Type__c, 
                                                CC_Number__c, 
                                                Tender_Type__c,
                                                Billing_Title__c, 
                                                Billing_Street2__c, 
                                                Billing_Street1__c, 
                                                Billing_State__c, 
                                                Billing_Postcode__c, 
                                                Billing_Last_Name__c, 
                                                Billing_First_Name__c, 
                                                Billing_Country__c, 
                                                Billing_City__c, 
                                                Anonymous__c,
                                                Settlement_Date__c,
                                                Error__c
                                        FROM    TR_Import_Transaction__c
                                        WHERE   Processed__c = null
                                        ORDER BY ID]);
    }
    
    public void execute(Database.BatchableContext info, List<TR_Import_Transaction__c> scope)
    {       
        for(TR_Import_Transaction__c stagingRecord : scope)
        {
            processedSuccesfully++;
            try
            {
                processTransaction(stagingRecord);              
            }
            catch(Exception ex)
            {
                processedSuccesfully--;
                stagingRecord.Processed__c = null;
                stagingRecord.Opportunity__c = null;
                stagingRecord.Error__c = ex.getMessage() +  '\n' + ex.getStackTraceString();
            }
        }
        update scope;
    }
    
    public void finish(Database.BatchableContext info)
    {
        if(RunThisOnly)
        {
            return;
        }
        TRImportDuplicateBatch dupBatch = new TRImportDuplicateBatch();
        Database.executeBatch(dupBatch, BatchSize);
    }

    private void processTransaction(TR_Import_Transaction__c stagingRecord)
    {
        Datetime trTimestamp = parseDateTime(stagingRecord.TR_Last_Modified_Date__c, stagingRecord); 
        Integer contactId = parseInteger(stagingRecord.TR_Constituent_ID__c, stagingRecord);
        Integer participantID = parseInteger(stagingRecord.Participant_ID__c, stagingRecord); 
        Contact constituent = null;
        Contact participant = null;
        
        stagingRecord.Error__c = '';
        stagingRecord.Processing_Result__c = null;
        stagingRecord.Opportunity__c = null;

        for(Contact c : [SELECT ID,
                                Name,
                                AccountID,
                                Account.Name,
                                TeamRaiser_CONS_ID__c,
                                Status__c,                                
                                (SELECT ID,
                                        Campaign__c
                                FROM    Registrations__r
                                WHERE   Campaign__r.TR_Event_ID__c = :stagingRecord.Event_ID__c AND 
                                        Campaign__r.TR_Event_ID__c != null AND 
                                        Campaign_Donation_Registration__c = false),
                                (SELECT ID,
                                        TR_Registration_Id__c,
                                        Registration__c,
                                        Registration__r.Campaign__c,
                                        Contact__r.AccountID
                                FROM    Contact_Registrations__r
                                WHERE   Registration__r.Campaign__r.TR_Event_ID__c = :stagingRecord.Event_ID__c AND 
                                        Registration__r.Campaign__r.TR_Event_ID__c != null AND 
                                        Registration__r.Campaign_Donation_Registration__c = false)                              
                        FROM    Contact
                        WHERE   TeamRaiser_CONS_ID__c != null AND
                                (TeamRaiser_CONS_ID__c = :contactId OR
                                TeamRaiser_CONS_ID__c = :participantID) AND 
                                Status__c != :CONTACTSTATUS_TOBEMERGED])
        {
            if(c.TeamRaiser_CONS_ID__c == contactId)
            {
                constituent = c;
            }
            // if participant and consituent are the same, leave participant null
            // as we will use this to determine if this is a donation in kind
            if(participantID != null && 
                    participantID != contactId && 
                    c.TeamRaiser_CONS_ID__c == participantID)
            {
                participant = c;
            }
        }
        if(constituent == null) 
        {
            stagingRecord.Processing_Result__c = RESULT_CONTACTNOTFOUND; 
            stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
            return;
        }
        if(participantID != null && 
                participantID != contactId &&
                participant == null)
        {
            stagingRecord.Processing_Result__c = RESULT_DONORNOTFOUND;
            stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
            return;
        }
        if(constituent.Status__c != CONTACTSTATUS_ACTIVE ||
                (participant != null && participant.Status__c != CONTACTSTATUS_ACTIVE))
        {
            stagingRecord.Processing_Result__c = RESULT_CONTACTINACTIVE;
            stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
            return;
        }

        //String transactionID = getLOTransactionID(stagingRecord.Transaction_ID__c);

        String incomeType = null;
        Opportunity opp = null;
        if(stagingRecord.Fund_Type__c == FUNDTYPE_REGISTRATION)
        {
            incomeType = INCOMETYPE_REGISTRATIONFEE;
            if(stagingRecord.Transaction_Type__c == TRANSACTIONTYPE_FULLREFUND ||
                stagingRecord.Transaction_Type__c == TRANSACTIONTYPE_PARTIALREFUND)
            {
                // not recording refunds for registrations
                stagingRecord.Processing_Result__c = RESULT_REFUNDTRANSACTION;
                stagingRecord.Processed__c = DateTime.now();
                return;
            }
        } 
        else if(stagingRecord.Fund_Type__c == FUNDTYPE_DONATION)
        {   
            incomeType = INCOMETYPE_DONATION;
        } 
        else if(stagingRecord.Fund_Type__c == FUNDTYPE_COLLECTION)
        {
            incomeType = INCOMETYPE_TEAMFR;
        }
        for(Opportunity o : [SELECT ID,
                                    PBSI__Sales_Order__c
                            FROM    Opportunity
                            WHERE   TR_Transaction_ID__c != null AND
                                    TR_Transaction_ID__c = :stagingRecord.Transaction_ID__c AND
                                    Income_Type__c != :INCOMETYPE_REFUND AND 
                                    Income_Type__c = :incomeType])
        {
            opp = o;
        }
        if(opp == null || 
                stagingRecord.Transaction_Type__c == TRANSACTIONTYPE_FULLREFUND ||
                stagingRecord.Transaction_Type__c == TRANSACTIONTYPE_PARTIALREFUND)
        {
            opp = createOpportunity(stagingRecord, constituent, participant);
        }
        if(opp == null)
        {
            return;
        }
//        Boolean newOpportunity = opp.ID == null;
        opp.Amount = parseDecimal(stagingRecord.Transaction_Amount__c, stagingRecord);
        opp.CloseDate = parseDate(stagingRecord.Transaction_Date__c, stagingRecord);
        opp.StageName = 'Posted';
        opp.Income_Type__c = incomeType;
        opp.TR_Transaction_ID__c = stagingRecord.Transaction_ID__c;
        if(stagingRecord.Transaction_Type__c == TRANSACTIONTYPE_FULLREFUND ||
            stagingRecord.Transaction_Type__c == TRANSACTIONTYPE_PARTIALREFUND)
        {
            if(stagingRecord.Fund_Type__c == FUNDTYPE_DONATION)
            {
                opp.Income_Type__c = INCOMETYPE_REFUND;
                opp.Amount = opp.Amount * -1.0;
            }
        }
        else if(stagingRecord.Transaction_Type__c == TRANSACTIONTYPE_TRCHANGE)
        {
            if(stagingRecord.Fund_Type__c == FUNDTYPE_REGISTRATION)
            {
                opp.Amount = opp.Amount * -1.0;
            }           
        }
        opp.TR_Transaction_Line__c = getAllTransactionFields(stagingRecord);
        opp.Last_Updated_by_Team_Raiser__c = Datetime.now();
        if(participant == null)
        {
            for(Contact_Registrations__c cr : constituent.Contact_Registrations__r)
            {
                opp.Contact_Registration__c = cr.ID;
            }
        }
        upsert opp;

        // can't set this for new opportunity as it fail Ascent validation rules, update is fine
        if(opp.PBSI__Sales_Order__c == null &&
                opp.Contact_Registration__c != null &&
                stagingRecord.Fund_Type__c == FUNDTYPE_REGISTRATION)
        {
            for(PBSI__PBSI_Sales_Order__c so : [SELECT  ID
                                                FROM    PBSI__PBSI_Sales_Order__c
                                                WHERE   Contact_Registration__c = :opp.Contact_Registration__c
                                                LIMIT 1])
            {
                opp.PBSI__Sales_Order__c = so.ID;
                update opp;
            }
        }

        if([SELECT  COUNT()
            FROM    OpportunityContactRole
            WHERE   ContactId = :constituent.ID AND 
                    OpportunityId = :opp.ID] == 0)
        {
            OpportunityContactRole ocr = new OpportunityContactRole();
            ocr.OpportunityId = opp.ID;
            ocr.ContactId = constituent.ID;
            ocr.Role = 'Other';
            insert ocr; 
        }

        if(participant != null)
        {
            Soft_Credit__c credit = null;
            for(Soft_Credit__c sc : [SELECT ID
                                    FROM    Soft_Credit__c
                                    WHERE   Opportunity__c = :opp.ID])
            {
                credit = sc;
            }
            if(credit == null)
            {
                credit = new Soft_Credit__c(Opportunity__c = opp.ID);
            }
            credit.Contact__c = participant.ID;
            for(Contact_Registrations__c cr : participant.Contact_Registrations__r)
            {
                credit.Contact_Registration__c = cr.ID;
            }
            upsert credit;
        }


        stagingRecord.Processing_Result__c = RESULT_TRANSACTIONCREATED;
        stagingRecord.Opportunity__c = opp.ID;
        setProcessedAndOwnerFields(stagingRecord);
    }
    
    private Opportunity createOpportunity(TR_Import_Transaction__c stagingRecord, 
                                            Contact constituent,
                                            Contact participant)
    {
        Opportunity opp = new Opportunity();
        opp.Name = constituent.Account.Name + ' ' + stagingRecord.Transaction_Date__c;
        opp.RecordTypeId = TransactionRecordType.ID;
        opp.Settlement_Date__c = parseDate(stagingRecord.Settlement_Date__c, stagingRecord);
     
        if(stagingRecord.Tender_Type__c == TENDERTYPE_CASH)
        {
            opp.Payment_Type__c = PAYMENTTYPE_CASH;
        }
        else if(stagingRecord.Tender_Type__c == TENDERTYPE_CHEQUE ||
                    stagingRecord.Tender_Type__c == TENDERTYPE_CHECK)
        {
            opp.Payment_Type__c = PAYMENTTYPE_CHEQUE;
        }
        else if(stagingRecord.Tender_Type__c == TENDERTYPE_CREDITCARD)
        {
            opp.Payment_Type__c = PAYMENTTYPE_TEAMRAISERONLINE;
            opp.Payment_Gateway__c = PAYMENTTYPE_TEAMRAISER;
        }
        else if(stagingRecord.Tender_Type__c == TENDERTYPE_XCHECKOUT)
        {
            opp.Payment_Type__c = PAYMENTTYPE_TEAMRAISERONLINE;
            opp.Payment_Gateway__c = PAYMENTTYPE_PAYPAL;
            
            opp.PayPal_Transaction_ID__c = stagingRecord.Payment_Gateway_ID__c;

        }
        Registration__c registration = null;
        List<Registration__c> registrations = [SELECT   ID,
                                                        TR_Registration_ID__c,
                                                        TR_Team_ID__c,
                                                        Campaign__c
                                                FROM    Registration__c
                                                WHERE   (TR_Registration_ID__c != null AND
                                                        TR_Registration_ID__c = :stagingRecord.Registration_ID__c) OR
                                                        (TR_Team_ID__c != null AND
                                                        TR_Team_ID__c = :stagingRecord.Team_ID__c)];
        // look for matching registration
        for(Registration__c reg : registrations)
        {
            if(reg.TR_Registration_ID__c == stagingRecord.Registration_ID__c)
            {
                registration = reg;
            }
        }
        if(registration == null)
        {
            if(participant == null)
            {
                // look for a constituents registration for the event
                for(Registration__c constituentRegistration : constituent.Registrations__r)
                {
                    registration = constituentRegistration;
                }
            }
            else
            {
                // look for a participant registration for the event
                for(Registration__c participantRegistration : participant.Registrations__r)
                {
                    registration = participantRegistration;
                }
            }
        }
        ID registrationID = null;
        if(registration == null)
        {
            if(participant == null)
            {
                for(Contact_Registrations__c contactRegistration : constituent.Contact_Registrations__r)
                {
                    registrationID = contactRegistration.Registration__c;
                    registration = new Registration__c(Campaign__c = contactRegistration.Registration__r.Campaign__c);
                }
            }
            else 
            {
                for(Contact_Registrations__c participantContactRegistration : participant.Contact_Registrations__r)
                {
                    registrationID = participantContactRegistration.Registration__c;
                    registration = new Registration__c(Campaign__c = participantContactRegistration.Registration__r.Campaign__c);
                }
            }
        }
        // look for matching team
        if(registration == null && String.isNotBlank(stagingRecord.Team_ID__c))
        {
            for(Registration__c reg : registrations)
            {
                if(reg.TR_Team_ID__c == stagingRecord.Team_ID__c)
                {
                    registration = reg;
                }
            }
        }
        // finally, look for default donation registrations
        if(registration == null && registrationID == null)
        {
            registration = new Registration__c();
            for(Campaign c : [SELECT    ID,
                                        (SELECT     ID
                                        FROM        Registrations__r
                                        WHERE       Campaign_Donation_Registration__c = true) 
                                FROM    Campaign
                                WHERE   TR_Event_ID__c != null AND
                                        TR_Event_ID__c = :stagingRecord.Event_ID__c])
            {
                registration.Campaign__c = c.ID;
                for(Registration__c r : c.Registrations__r)
                {
                    registrationID = r.ID;
                }
            }
            if(registration.Campaign__c == null)
            {
                stagingRecord.Processing_Result__c = RESULT_CAMPAIGNNOTFOUND; 
                stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
                return null;
            }
            if(registrationID == null)
            {
                stagingRecord.Processing_Result__c = RESULT_DONATIONREGISTRATIONNOTFOUND; 
                stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
                return null;
            }
        }
        opp.Registration__c = registration.ID == null ? registrationID : registration.ID;
        opp.CampaignID = registration.Campaign__c; 
        opp.AccountID = constituent.AccountID;
        opp.Primary_Contact_at_Organisation__c = constituent.ID;
        if(opp.CampaignID == null)
        {
            stagingRecord.Processing_Result__c = RESULT_CAMPAIGNNOTFOUND;
            stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
            return null;
        }
        return opp;
    }
    
    private String getAllTransactionFields(TR_Import_Transaction__c stagingRecord)
    {
        return 'LO_CONS_ID: ' + substituteNull(stagingRecord.TR_Constituent_ID__c) + 
                NEWLINE + 'LO_MEMBER_ID: ' + substituteNull(stagingRecord.Member_ID__c) + 
                NEWLINE + 'ConstituentTitle: ' + substituteNull(stagingRecord.Title__c) + 
                NEWLINE + 'ConstituentFirstName: ' + substituteNull(stagingRecord.First_Name__c) + 
                NEWLINE + 'ConstituentLastName: ' + substituteNull(stagingRecord.Last_Name__c) +
                NEWLINE + 'Address1: ' + substituteNull(stagingRecord.Home_Street1__c) +
                NEWLINE + 'Address2: ' + substituteNull(stagingRecord.Home_Street2__c) +
                NEWLINE + 'City: ' + substituteNull(stagingRecord.Home_City__c) +
                NEWLINE + 'State: ' + substituteNull(stagingRecord.Home_State__c) +
                NEWLINE + 'Postcode: ' + substituteNull(stagingRecord.Home_Postcode__c) +
                NEWLINE + 'Country: ' + substituteNull(stagingRecord.Home_Country__c) +
                NEWLINE + 'Employer: ' + substituteNull(stagingRecord.Employer__c) +
                NEWLINE + 'BillingTitle: ' + substituteNull(stagingRecord.Billing_Title__c) +
                NEWLINE + 'BillingFirstName: ' + substituteNull(stagingRecord.Billing_First_Name__c) +
                NEWLINE + 'BillingLastName: ' + substituteNull(stagingRecord.Billing_Last_Name__c) +
                NEWLINE + 'BillingStreet1: ' + substituteNull(stagingRecord.Billing_Street1__c) +
                NEWLINE + 'BillingStreet2: ' + substituteNull(stagingRecord.Billing_Street2__c) +
                NEWLINE + 'BillingCity: ' + substituteNull(stagingRecord.Billing_City__c) +
                NEWLINE + 'BillingState: ' + substituteNull(stagingRecord.Billing_State__c) +
                NEWLINE + 'BillingPostCode: ' + substituteNull(stagingRecord.Billing_Postcode__c) +
                NEWLINE + 'BillingCountry: ' + substituteNull(stagingRecord.Billing_Country__c) +
                NEWLINE + 'TransactionAmount: ' + substituteNull(stagingRecord.Transaction_Amount__c) +
                NEWLINE + 'TransactionDate: ' + substituteNull(stagingRecord.Transaction_Date__c) +
                NEWLINE + 'CCType: ' + substituteNull(stagingRecord.CC_Type__c) +
                NEWLINE + 'CCNumber: ' + substituteNull(stagingRecord.CC_Number__c) +
                NEWLINE + 'LOTransactionID: ' + substituteNull(stagingRecord.Transaction_ID__c) +
                NEWLINE + 'LOPaymentGatewayID: ' + substituteNull(stagingRecord.Payment_Gateway_ID__c) +
                NEWLINE + 'LOMerchantAccountID: ' + substituteNull(stagingRecord.Merchant_Account_ID__c) +
                NEWLINE + 'Anonymous: ' + substituteNull(stagingRecord.Anonymous__c) +
                NEWLINE + 'OnBehalfEmployer: ' + substituteNull(stagingRecord.On_Employer_Behalf__c) +
                NEWLINE + 'InHonourGiftType: ' + substituteNull(stagingRecord.In_Honour_Gift_Type__c) +
                NEWLINE + 'InHonourFirstName: ' + substituteNull(stagingRecord.In_Honour_First_Name__c) +
                NEWLINE + 'InHonourLastName: ' + substituteNull(stagingRecord.In_Honour_Last_Name__c) +
                NEWLINE + 'CreditType: ' + substituteNull(stagingRecord.Credit_Type__c) +
                NEWLINE + 'LOTeamRaiserID: ' + substituteNull(stagingRecord.Event_ID__c) +
                NEWLINE + 'EventName: ' + substituteNull(stagingRecord.Event_Name__c) +
                NEWLINE + 'LOTeamID: ' + substituteNull(stagingRecord.Team_ID__c) +
                NEWLINE + 'LOParticipantID: ' + substituteNull(stagingRecord.Participant_ID__c) +
                NEWLINE + 'LORegistrationID: ' + substituteNull(stagingRecord.Registration_ID__c) +
                NEWLINE + 'TransactionType: ' + substituteNull(stagingRecord.Transaction_Type__c) +
                NEWLINE + 'FundType: ' + substituteNull(stagingRecord.Fund_Type__c) +
                NEWLINE + 'TenderType: ' + substituteNull(stagingRecord.Tender_Type__c) +
                NEWLINE + 'LastModified: ' + substituteNull(stagingRecord.TR_Last_Modified_Date__c); 
    }
    
    private String substituteNull(String s)
    {
        return s == null ? '' : s;
    }

    /*private String getLOTransactionID(String compundID)
    {
        if(String.isBlank(compundID))
        {
            return null;
        }
        compundID = compundID.trim();
        return compundID.substring(0,  getCompoundIDLength(compundID) - 12);
    }*/

    /*public String getLOTransactionType(String compundID)
    {
        if(String.isBlank(compundID))
        {
            return null;
        }
        compundID = compundID.trim();
        return compundID.substring(getCompoundIDLength(compundID) - 1);
    }*/

    private Integer getCompoundIDLength(String compundID)
    {
        Integer compundIDLength = compundID.length();
        if(compundIDLength < 12)
        {
            throw new CCException('Transaction ID cannot be parsed: ' + compundID);
        }
        return compundIDLength;
    }
}