public class JobRoleRedirectExtension
{
    public JobRoleRedirectExtension(ApexPages.StandardController c)
    {
        record = (Job_Role__c)c.getRecord();
    }
    
    private Job_Role__c record;
    
    private Map<Id, String> RecordTypes
    {
        get
        {
            if (RecordTypes == null)
            {
                RecordTypes = new Map<Id, String>();
                
                for (RecordType rt : [SELECT Id,
                                             Name
                                      FROM RecordType
                                      WHERE sObjectType = 'Job_Role__c'])
                {
                    RecordTypes.put(rt.Id, rt.Name);
                }                                      
            }
            return RecordTypes;
        }
        set;
    }
    
    public PageReference Redir()
    {
        // return the edit page based on record type        
        PageReference pg = new PageReference('/' + Job_Role__c.sObjectType.getDescribe().getKeyPrefix() + '/e?nooverride=1');
        
        if (record.Id != null)
        {
            pg = new PageReference('/' + record.Id + '/e?nooverride=1');    
        }
        
        if (RecordTypes.get(record.RecordTypeId) == 'Event Volunteer')
        {
            pg = Page.JobRoleEdit;                
        }
        
        for (String key : ApexPages.CurrentPage().getParameters().keySet())
        {
            // ignore the save_new parameter
            if (key == 'save_new' || key == '_CONFIRMATIONTOKEN')
            {
                continue;
            }
                        
            pg.getParameters().put(key, ApexPages.CurrentPage().getParameters().get(key));    
        }
        
        return pg;
    }
}