public without sharing class ServiceProviderSupportSearchController
{
    
    Public String selectedCategory{get;set;}
    Public String selectedServiceType{get;set;}
    Public Boolean atsi{get;set;}
    Public Boolean cald{get;set;}
    Public String locationsString{get;set;}
    Public String urlParameters{get;set;}
    Public Boolean displaySearch {get;set;}
    Public Boolean displayResults {get;set;}
    public Integer resultsCount{get;set;}
    public List<Integer> pages{get;set;}
    public Integer page{get;set;}
    public String reportEmail{get;set;}
    public String reportedError{get;set;}
    public Boolean showSupportServiceError{get;set;}
    
    public ServiceProviderSupportSearchController()
    {        
        createLocationsList();
        List<Public_Data__c> searchResults = new  List<Public_Data__c>();
        displaySearch = true;
        displayResults = false;
        page = 1;
        
        // if parameters have been supplied then populate them
        if (!String.isBlank(ApexPages.CurrentPage().getParameters().get('category')))
        {
            selectedCategory = ApexPages.CurrentPage().getParameters().get('category');
        }
        
        if (!String.isBlank(ApexPages.CurrentPage().getParameters().get('serviceType')))
        {
            selectedServiceType = ApexPages.CurrentPage().getParameters().get('serviceType');
        }
        
        if (!String.isBlank(ApexPages.CurrentPage().getParameters().get('locations')))
        {
            for (SelectWrapper w : locations)
            {
                if (ApexPages.CurrentPage().getParameters().get('locations').contains(w.selectName + ','))
                {
                    w.isSelected = true;
                }
            }                  
        }
        
        if (!String.isBlank(ApexPages.CurrentPage().getParameters().get('atsi')))
        {
            atsi = true;
        }
        
        if (!String.isBlank(ApexPages.CurrentPage().getParameters().get('cald')))
        {
            cald = true;
        }
        
        if (!String.isBlank(ApexPages.CurrentPage().getParameters().get('page')))
        {
            page = Integer.valueOf(ApexPages.CurrentPage().getParameters().get('page'));
        }
        
        if (!String.isBlank(ApexPages.CurrentPage().getParameters().get('search')))
        {
            search();
        }
    }
    
   public List<Public_Data__c> searchResults
    {
        get
        {
            return searchResults;
        }
        set;
    }
   public List<SelectOption> category
    {
        get
        {
            category= new List<SelectOption>();
            category.add(new SelectOption('', '-Select-'));
            for(PickListEntry entry : Reference_Data__c.Cancer_Category__c.getDescribe().getPicklistValues())
            {                   
                if (entry.getLabel() != 'Other Cancer')
                {                 
                    category.add(new SelectOption(entry.getLabel(), entry.getLabel()));                  
                }
            }         
            category.add(new SelectOption('Other', 'Other Cancer'));                     
           
            return category;
        }
        set;
    }
    public List<SelectOption> serviceTypes
    {
        get
        {            
            serviceTypes = new List<SelectOption>(); 
            serviceTypes.add(new SelectOption('', '-Select-'));
            
            for(PickListEntry entry : Public_Data__c.Category__c.getDescribe().getPicklistValues())
            {                    
                 serviceTypes.add(new SelectOption(entry.getLabel(), entry.getLabel()));                  
            } 
                                   
            return serviceTypes;
        }
        set;
    }
    public List<SelectWrapper> locations
    {
        get
        {
            
            return locations;
        }
        set;
    }        
            
    //This creates a list of locations based on the picklist in public data
    public void createLocationsList()
    {
        locations = new List<SelectWrapper>(); 
              
        for(PickListEntry entry : ContentVersion.Location__c.getDescribe().getPicklistValues())
        { 
            if (entry.getValue() != 'National')
            {        
                SelectWrapper newSelect = new SelectWrapper();
                newSelect.isSelected = false;
                newSelect.selectName = entry.getValue();
                locations.add(newSelect); 
            } 
        }                  
    }

    public void newSearch()
    {
        page = 1;
        search();
    }
           
    //This executes the search given the criteria entered on the VF Page.        
    public void search()
    {
        try
        {
            //Set up the variables and initialise
            String categorySearch = '';
            String categorySearchFull = '';
            String location = '';
            locationsString = '';
            urlParameters = '';
            String suppService = '';
            Boolean supportSupplied = false;
            String searchTable = 'Public_Data__c';
            searchResults = new List<Public_Data__c>();
            resultsCount = 0;
            pages = new List<Integer>();
            showSupportServiceError = false;
    
            //If the category is selected on the VF page use the value to pull back all cancer types for the category
            //then build a search string to retrieve any records that have these values.
            if(!String.isBlank(selectedCategory) && selectedCategory != 'Other')
            {
                urlParameters += '&category=' + selectedCategory;
            
                //Get all the cancer types for the category from the reference data table.
                List<Reference_Data__c> allCats = [Select Name from Reference_Data__c where Cancer_Category__c =:selectedCategory];
                if(allCats.size() > 0)
                {
                    //Build a search string for the where clause in a dynamic soql.
                    for(Reference_Data__c cat:allCats)
                    {
                        categorySearch += '\'' + String.escapeSingleQuotes(cat.Name) + '\',';
                    }
                    
                    // add unspecified site to the list so these service providers are always returned
                    categorySearch += '\'Unspecified site (C80)\'';
                    
                    /*                                
                    //Remove the ',' at the end of the search string.
                    if(!String.isBlank(categorySearch ))
                    {
                       categorySearch = categorySearch.removeEnd(',') ;
                    } */                               
                    
                    //Concatenate the search string with the field names to build a string.
                    if(!String.isBlank(categorySearch))
                    {
                        categorySearchFull += '(Cancer_Type_A_E__c in (' + categorySearch + ') OR ';
                        categorySearchFull += 'Cancer_Type_F_M__c in (' + categorySearch + ') OR ';
                        categorySearchFull += 'Cancer_Type_N_R__c in (' + categorySearch + ') OR ';
                        categorySearchFull += 'Cancer_Type_S_Z__c in (' + categorySearch + '))';
                    }
                } 
            }
            //Retrieve the locations object and build a search string out of those records selected on the screen.
            urlParameters += '&locations=';
            for(SelectWrapper loc:locations)
            {
                if(loc.isSelected)
                {
                    location +=  '\'' + String.escapeSingleQuotes(loc.selectName) + '\',';                 
                    locationsString += String.escapeSingleQuotes(loc.selectName) + ', ';
                    urlParameters += loc.selectName + ',';
                }
            }
            locationsString = locationsString.removeEnd(', ');
            // add National to the locations clause so these are always returned
            if (!String.isBlank(location))
            {
                location +=  '\'National\'';
            }
            
            //if there is an additional ',' on the end of the location string, strip it off.
            //location = location.removeEnd(',') ;
             
            //Retrieve the service types and loop through to determine which were selected.  Then build the search string
            if(!String.isBlank(selectedServiceType))
            {
                suppService += ' Category__c includes(\'' + String.escapeSingleQuotes(selectedServiceType) + '\') ';     
                supportSupplied = true;
                urlParameters += '&serviceType=' + selectedServiceType;
            }        
                    
            //if there is an additional OR on the end of the location string, strip it off.
            suppService = suppService.removeEnd(' OR ') ;
         
            //Check that at least 1 support type was chosen as this is mandatory.
            if(supportSupplied)
            {
                //Build all parts of the string into the where clause.
                string whereClause = '';
                if(!String.isBlank(categorySearchFull))
                {
                    whereClause += '(' + categorySearchFull + ') AND ';
                }
                if(!String.isBlank(location))
                {
                    whereClause +=  ' location__c includes(' + location +') AND ';
                }
                if(!String.isBlank(suppService))
                {
                    whereClause +=  '(' + suppService +') AND ';
                }
                if(atsi != null && atsi)
                {
                    whereClause +=  'ATSI__c = \'YES\' AND ';
                    urlParameters += '&atsi=true';
                }
                if(cald != null && cald)
                {
                    whereClause +=  'CALD__c != null AND CALD__c != \'English\' AND ';
                    urlParameters += '&cald=true';
                }
                
                whereClause += ' RecordType.DeveloperName = \'Service_Provider_Services\' ';            
                //whereClause = whereClause.removeEnd(' AND ') ;
    
                //Create the query string 
                String query_string = 'SELECT Name, Website__c,Public_Website__c,Public_Description__c, Associated_Account__r.Name, Associated_Account__r.Phone, Associated_Account__c, Associated_Account__r.BillingStreet,Associated_Account__r.BillingCity, Associated_Account__r.BillingState, Associated_Account__r.BillingPostalCode,ATSI__c,CALD__c, Public_Title__c, Title__c, Content_URL__c ';
                query_string += ' FROM ' + searchTable + ' where ' + whereClause ;                        
                query_string += ' ORDER BY Is_Cancer_Council_SA_Result__c DESC, Order_by_Title__c';

                //query_string = String.escapeSingleQuotes(query_string);
system.debug('debug1:' + query_string);
                // query now to find how many results would be returned with no limit
                resultsCount = ((List<Public_Data__c>)Database.query(query_string)).size();
                
                for (Integer i = 0; i * 10 < resultsCount; i++)
                {
                    pages.add(i + 1);
                }

                // check that our page parameter is not too big                
                if (page > pages.size() && pages.size() > 0)
                {
                    page = pages.size();
                }                

                // add the code to limit if we're not in print mode
                if (!ApexPages.CurrentPage().getParameters().containsKey('print'))
                {
                    query_string += ' LIMIT 10';
                    query_string += ' OFFSET ' + String.valueOf((page - 1) * 10);            
                }
    
                //execute the search
                searchResults =(List<Public_Data__c>)Database.query(query_string );
                
                //Set the boolean values to render the correct 
                //Parts of the VF page.
                displaySearch = false;
                displayResults = true;
            }
            else
            {
                //Required fields not supplied
                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please choose at least 1 support service.');
                CustomException.formatException('Please choose a support service.'); 
                showSupportServiceError = true;
                
                //Set the boolean values to render the correct 
                //Parts of the VF page.
                displaySearch = true;
                displayResults = false; 
            } 
        }
        catch(Exception e)
        {
            CustomException.formatException(e);
        }           
     }
    
    public void ReportError()
    {
        try
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            mail.setToAddresses(new String[] { reportEmail });
            mail.setSubject('An error in a Service Provider listing has been reported');
            mail.setPlainTextBody(reportedError);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch(Exception e)
        {
            CustomException.formatException(e);
        }    
    }    
    
    public void returnToSearch()
    {
        displaySearch = true;
        displayResults = false; 
    
        /*
        PageReference pageRef = new PageReference('/apex/ServiceProviderSupportSearch');
        pageRef.setRedirect(true);
        return(pageRef);
        */
    }
    
    public class SelectWrapper{
    
        public Boolean isSelected {get;set;}
        public String selectName {get;set;}

    }
    
    
    
    
}