@isTest
private class RecordValueChangedAlertTest {

    static testMethod void testRecordUpdateDelete() {
		        
		test.startTest();
		
		// Load test Account and Contact data
		TestDataFactory.createAccountContactTestRecords(22);		
		       
        list<Alert_Record__c> listAlertRecord = new list<Alert_Record__c>();
        
        Alert_Record__c arEducation = new Alert_Record__c(
        										Object_API_Name__c = 'Account'
        										, Email_To__c = 'test@cancersa.org.au'
        										, Field_API_Name_Alert__c= 'Organization_Category__c'
        										, Field_Value_Alert__c = 'Education'        										
        										, Active__c = true
        										);		
		
        Alert_Record__c arGPSA = new Alert_Record__c(
        										Object_API_Name__c = 'Account'
        										, Email_To__c = 'test@cancersa.org.au'
        										, Field_API_Name_Alert__c= 'External_Source__c'
        										, Field_Value_Alert__c = 'GPSA'        										
        										, Active__c = true
        										);
        										
        Alert_Record__c arContactQuitline = new Alert_Record__c(
        										Object_API_Name__c = 'Contact'
        										, Email_To__c = 'test@cancersa.org.au'
        										, Field_API_Name_Alert__c= 'Group_List__c'
        										, Field_Value_Alert__c = 'Quitline'        										
        										, Active__c = true
        										);
        										        										
        listAlertRecord.add(arEducation);
        listAlertRecord.add(arGPSA);
        listAlertRecord.add(arContactQuitline);
        
        insert listAlertRecord;
        
        // Test Field Name data input for Alert Field records
        map<string, string> mapFieldNames = new map<string, string>{'Name'=>'Name'
        															, 'BillingStreet'=>'Street'
        															, 'Phone'=>'Phone'
        															, 'Test'=>'Test'};        
        
        list<Alert_Field__c> listAlertField = new list<Alert_Field__c>();
        
        for (Alert_Record__c ar : listAlertRecord) {        	        	
        	for (string APIName : mapFieldNames.keySet()) {
        		Alert_Field__c af = new Alert_Field__c();
        		af.Alert_Record__c = ar.Id;
	        	af.Field_API_Name__c = APIName;
	        	af.Field_Label_Name__c = mapFieldNames.get(APIName);
	        	
	        	listAlertField.add(af);
        	}
        } 
        
        insert listAlertField;
		
		list<Account> listAccount = new list<Account>();		
		
		for (Account acc : [SELECT Id, Name, Organization_Category__c, External_Source__c FROM Account]) {
			acc.Name = acc.Name + '1';
			acc.BillingStreet = acc.BillingStreet + '1';
			acc.Organization_Category__c = 'Education';
			acc.External_Source__c = 'GPSA';
			
			listAccount.add(acc);			
		}
				
		update listAccount;
		
		merge listAccount[0] listAccount[1];
		
		list<Contact> listContact = new list<Contact>();
		
		// Update Contact records to trigger alert
		for (Contact con : [SELECT Id, Name, Phone, How_Heard__c, Group_List__c FROM Contact]) {
			con.Phone = con.Phone + '1';
			con.How_Heard__c = 'Quitline';
			con.Group_List__c = 'SunSmart;Quitline';
			
			listContact.add(con);
		}
				
		update listContact;		
		
		delete listContact;
		
		test.stopTest();
        
    }
}