@isTest
private class OpportunityReallocatedTest {

    static testMethod void testOpportunityReallocated() {
        test.startTest();
        
        // Load test Account and Contact data
		TestDataFactory.createAccountContactTestRecords(1);
		TestDataFactory.createCampaignTestRecords(1);
        
        Campaign c = new Campaign (Name='Major Gifts', Campaign_Code__c='Major Gifts');
        insert c;
        
        map<string, Id> mapCampaign = new map<string, Id>();
        list<Opportunity> listOpportunity = new list<Opportunity>();
        list<OpportunityContactRole> listOpportunityContactRole = new list<OpportunityContactRole>();       
        
        for (Campaign camp : [SELECT Id, Campaign_Code__c FROM Campaign]) {
        	mapCampaign.put(camp.Campaign_Code__c, camp.Id);
        }
                        
        for (Contact con : [SELECT AccountId, Id, Name FROM Contact]) {        	
        	for (integer i = 4999; i <= 5000; i++ ) {
	        	Opportunity opp = new Opportunity();
	        	opp.Name = 'Test ' + con.Name;
	        	opp.AccountId = con.AccountId;
	        	opp.Primary_Contact_at_Organisation__c = con.Id;
	        	opp.CampaignId= mapCampaign.get('T Parent');
	        	opp.CloseDate = system.now().date();
	        	opp.StageName = 'Posted';
	        	opp.Amount = i;
	        	opp.Payment_Type__c = 'Cash';
	        	listOpportunity.add(opp);        		
        	}
        }
        insert listOpportunity;
        
        if (listOpportunity.size()>0) {
        	for (Opportunity o : listOpportunity) {
        		if (o.Primary_Contact_at_Organisation__c != null) {
        			OpportunityContactRole ocr = new OpportunityContactRole();
					ocr.ContactId = o.Primary_Contact_at_Organisation__c;	
					ocr.Role = 'Donor';
					ocr.OpportunityId = o.id;
					ocr.IsPrimary = true;
					listOpportunityContactRole.add(ocr);
        		}
        	}
        }
        insert listOpportunityContactRole;
        
        // Test for Reallocated checkbox ticked
        for (Opportunity opp : listOpportunity) {
        	if (opp.Amount >= 5000) {
        		opp.Reallocated__c = true;	
        	}
        }
        
        update listOpportunity;
        
        test.stopTest();
    }
}