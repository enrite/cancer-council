public with sharing class ContactRegistrationTeamRaiser {

    private static string emailAddress {
        get {
            return 'ithelpdesk@cancersa.org.au';
        }    
    }
    
    private static map<Id, Registration__c> registrationsToUpdate {
    	get {
    		if (registrationsToUpdate == null) {
    			registrationsToUpdate = new map<Id, Registration__c>();	
    		}
    		return registrationsToUpdate;
    	}
    	set;
	}
    
    public static void teamRaiserUpdate(map<Id, Contact_Registrations__c> newRecord, map<Id, Contact_Registrations__c> oldRecord, string dmlType) {
    	map<Id, Contact_Registrations__c> triggerRecords = newRecord;
    	map<Id, Contact_Registrations__c> triggerOldRecords = oldRecord;
    	list<Contact_Registrations__c> otherCaptains = new list<Contact_Registrations__c>();
	    map<Id, Registration__c> registrationsById = new map<Id, Registration__c>([SELECT ID, Host_Team_Captain_Org__c,
		    																		(SELECT ID,Contact__c
		    																		FROM Contact_Registrations__r
		    																		WHERE Team_Captain__c = 'Captain')
	                                        									FROM Registration__c
	                                        									WHERE RecordType.Name = 'Team Event Registration' 
	                                        									AND ID IN(SELECT Registration__c
	                                                    									FROM Contact_Registrations__c
	                                                    									WHERE ID IN :triggerRecords.keySet())]);    	
	    
	    for (Contact_Registrations__c cr : triggerRecords.values())
	    {
	        Registration__c reg = registrationsById.get(cr.Registration__c);
	        if (reg != null)
	        {
	            if (dmlType != 'Insert')
	            {
	                Contact_Registrations__c oldValue = null;
	                if (cr.ID != null)
	                {
	                    oldValue = triggerOldRecords.get(cr.ID);
	                }
	                if (oldValue != null && oldValue.Team_Captain__c != cr.Team_Captain__c)
	                {
	                    for (Contact_Registrations__c otherCaptain : reg.Contact_Registrations__r)
	                    {
	                        if (otherCaptain.ID != cr.ID)
	                        {
	                            otherCaptain.Team_Captain__c = null;
	                            otherCaptains.add(otherCaptain);
	                        }
	                    }
	                }
	            }
	            if (cr.Team_Captain__c == 'Captain')
	            {
	                setRegistrationFields(cr, reg, otherCaptains.isEmpty());
	            }
	            else if (cr.Team_Captain__c == null && triggerOldRecords != null && triggerOldRecords.get(cr.ID).Team_Captain__c == 'Captain')
	            {
	                // now no team captain
	                setRegistrationFields(null, reg, otherCaptains.isEmpty());
	            }
	        }
	    }
	    
	    if (registrationsToUpdate.size() > 0) {
	    	update registrationsToUpdate.values();
	    	registrationsToUpdate.clear();
	    	
	    }	    
	    
	    if (otherCaptains.size() > 0) {
	    	TriggerByPass.ContactRegistration = true;
	    	update otherCaptains;	    	
	    }
	    TriggerByPass.Registration = true;
    }
    
    private static void setRegistrationFields(Contact_Registrations__c cr, Registration__c reg, Boolean teamHasNoCaptain)
    {
        if (cr == null)
        {
            if (teamHasNoCaptain)
            {
                reg.Host_Team_Captain__c = null;
                reg.Org_Primary_Contact__c = null;
            }
        }
        else
        {
            if (reg.Host_Team_Captain_Org__c != null)
            {
                reg.Org_Primary_Contact__c = cr.Contact__c;
            }
            else
            {
                reg.Host_Team_Captain__c = cr.Contact__c;
            }
        }
        reg.Shirt_Size__c = cr == null ? null : cr.Shirt_Size__c;
        reg.Registration_Method__c = cr == null ? null : cr.Registration_Method__c;
        reg.Date_Shirts_Sent__c = cr == null ? null : cr.Date_Shirt_Sent__c;
        reg.Date_Host_Kits_Sent__c = cr == null ? null : cr.Date_Host_Kit_Sent__c;
        reg.Date_Kit_Printed__c = cr == null ? null : cr.Date_Kit_Printed__c;
        reg.Cancer_Experience__c = cr == null ? null : cr.Cancer_Experience__c;
        reg.Emergency_Name__c = cr == null ? null : cr.Emergency_Name__c;
        reg.Parent_Guardian_Name__c = cr == null ? null : cr.Parent_Guardian_Name__c;
        reg.Participant_Type__c = cr == null ? null : cr.Participation_Type__c;
        reg.Survivor_Carer_Lap__c = cr == null ? false : cr.Survivor_Carer_Lap__c;
        reg.Survivor_or_Carer__c = cr == null ? null : cr.Survivor_or_Carer__c;
        reg.Employer__c = cr == null ? null : cr.Employer__c;
        reg.Registrant_Emergergency_Number__c = cr == null ? null : cr.Registrant_Emergency_Number__c;
        reg.Registrant_Employer__c = cr == null ? null : cr.Registrant_Employer__c;
        reg.Emergency_Contact_Number__c = cr == null ? null : cr.Emergency_Contact_Number__c;
        reg.Parent_Guardian_Email__c = cr == null ? null : cr.Parent_Guardian_email__c;
        reg.Registrant_Emergergency_Name__c = cr == null ? null : cr.Registrant_Emergency_Name__c;
        reg.Shirt_Type__c = cr == null ? null : cr.Shirt_Type__c;
        reg.Short_Selection_Rules__c = cr == null ? null : cr.Clothing_Collection_Method__c;
        
        if (!registrationsToUpdate.containsKey(reg.Id)) {
        	registrationsToUpdate.put(reg.Id, reg);	
        }
    }        
}