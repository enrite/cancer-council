@IsTest
public class PublicDataExtensionTest
{
    private static testMethod void testTrigger()
    {
        Public_Data__c pd = new Public_Data__c();
        pd.Temporary_Content_Document_ID__c = '000000000000000AAA';
        pd.RecordTypeId = Public_Data__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Temporary').getRecordTypeId();
        insert pd;
    }

    private static testMethod void test1()
    {
        Account a = new Account(Name = 'TestContent');
        insert a;

        Contact c = new Contact(AccountId = a.ID);
        c.LastName = 'TestContact';
        c.Email = 'test@test.com';
        insert c;

        ContentVersion cv = new ContentVersion();
        cv.External_Review_Author__c = c.ID;
        cv.Associated_Account__c = a.ID;
        cv.RecordTypeId = ContentVersion.SObjectType.getDescribe().getRecordTypeInfosByName().get('Links').getRecordTypeId();
        cv.Target_Audience__c = 'General Public';
        cv.Type__c = 'Service';
        cv.Title = 'Test';
        cv.ContentUrl = 'http://google.com.au';
        insert cv;

        Public_Data__c pd = new Public_Data__c();
        pd.Content_Document_ID__c = [SELECT ContentDocumentId
                                    FROM ContentVersion
                                    WHERE ID = :cv.ID].ContentDocumentId;
        ID contentDocumentId = pd.Content_Document_ID__c;
        pd.RecordTypeId = Public_Data__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Provider Services').getRecordTypeId();
        insert pd;

        cv.Send_Email_to_Service_Provider__c = true;
        update cv;

        PublicDataExtension ext = new PublicDataExtension(new ApexPages.StandardController(pd));

        ext.redir();
        system.assert(ext.CanEdit != null);

        ext.saveOverride();

        system.assert(ext.HasMessages != null);

        ext.LastReviewDateStart = String.valueOf(Date.today());
        ext.LastReviewDateEnd = String.valueOf(Date.today());
        ext.search();

        ext.ServiceProviders.add(new PublicDataExtension.ServiceProviderWrapper(cv));

        for(PublicDataExtension.ServiceProviderWrapper w : ext.ServiceProviders)
        {
            w.Selected = true;
        }

        ext.sendEmail();

        pd.RecordTypeId = Public_Data__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Temporary').getRecordTypeId();
        pd.Status__c = 'Submitted';
        pd.Temporary_Content_Document_ID__c = cv.ID;
        update pd;

        Public_Data__c pdOriginal = pd.clone(false, true);
        pdOriginal.RecordTypeId = Public_Data__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Provider Services').getRecordTypeId();
        pdOriginal.Content_Document_ID__c = cv.ID;
        insert pdOriginal;
        PublicDataExtension.publishData(pd.ID);

        ContentDocument cd = new ContentDocument(Id = contentDocumentId);
        delete cd;

    }
}