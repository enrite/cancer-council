@isTest
private class SoftCreditAmountRollupTest {

    static testMethod void mySoftCreditAmountRollupTest() {
    	test.startTest();
    	
        // Load test Account, Contact and Campaign data
		TestDataFactory.createAccountContactTestRecords(2);
		TestDataFactory.createCampaignTestRecords(1);
    	
        map<string, Id> mapRecordType = new map<string, Id>
                        {'Registration'=>[SELECT Id FROM RecordType WHERE SObjectType='Registration__c' AND DeveloperName='Basic' LIMIT 1].Id
                        , 'Opportunity'=>[SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND DeveloperName='Donation' LIMIT 1].Id};
		
		list<Opportunity> listOpportunity = new list<Opportunity>();
		
		Id campaignId = [SELECT Id FROM Campaign LIMIT 1].Id;
		Id contactId;
		Id contactId2;
		Id accountId;
		
		list<Contact> listContact = new list<Contact>([SELECT Id, AccountId FROM Contact LIMIT 2]);
		
		contactId = listContact.get(0).Id;
		accountId = listContact.get(0).AccountId;
		
		contactId2 = listContact.get(1).Id;
		
		Registration__c reg = new Registration__c();
		reg.Host_Team_Captain__c = contactId;
		reg.Campaign__c = campaignId;
		reg.RecordTypeId = mapRecordType.get('Registration');
		reg.Status__c = 'Registered';
		insert reg;
		
		Contact_Registrations__c cr = new Contact_Registrations__c();
		cr.Contact__c = contactId;
		cr.Registration__c = reg.Id;
		insert cr;
		
		decimal totalAmount = 0;
		
		for (integer i = 0; i < 3; i++) {
			Opportunity opp = new Opportunity();
			opp.Name = 'Test ' + date.today().format();
			opp.AccountId = accountId;
			opp.Primary_Contact_at_Organisation__c = contactId;
			opp.CampaignId = campaignId;
			opp.Registration__c = reg.Id;
			opp.CloseDate = date.today();
			opp.StageName = 'Posted';
			opp.Amount = 300+i;//Math.round(Math.random()*1000);
			totalAmount += opp.Amount;
			listOpportunity.add(opp);
		}
		
		insert listOpportunity;
		
		// reset TriggerByPass.BPAYNum to continue testing
		TriggerByPass.BPAYNum = false;		
		
		Soft_Credit__c sc = new Soft_Credit__c();
		sc.Opportunity__c = listOpportunity.get(0).Id;
		sc.Contact__c = contactId2;
		sc.Contact_Registration__c = cr.Id;
		insert sc;
		
		Registration__c r = [SELECT Id, Total_Soft_Credit__c FROM Registration__c WHERE Id = :reg.Id];
		Contact_Registrations__c c = [SELECT Id, Total_Soft_Credit__c FROM Contact_Registrations__c WHERE Registration__c = :reg.Id];
		
		system.assertEquals(r.get('Total_Soft_Credit__c'), 300);
		system.assertEquals(c.get('Total_Soft_Credit__c'), 300);
		
		Opportunity o = listOpportunity.get(0);
		o.Amount = 500;
		update o;		
		
		test.stopTest();
    }
}