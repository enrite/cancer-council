public with sharing class OpportunityRollupController {
    //subclass to hold information in one row
    public class OppTotals {
        //properties
        public string groupType {get; set;}
        public integer oppNum {get;set;}
        public decimal oppAmount {get;set;}   
        public decimal balance {get;set;}                             
    
        //subclass constructor            
        public OppTotals (string gt) {
            groupType = gt;
            oppNum = 0;
            oppAmount = 0.0;
            balance = 0.0;                                           
        }
    }
            
    //properties to display information in a table on the page
    public list<OppTotals> byYearRow {get; set;}
    public list<OppTotals> byCampaignRow {get; set;}
    public OppTotals totalRow {get; set;}  
                    
    //id of object of the current page
    Id objId;
                
    //constructor
    public OpportunityRollupController (ApexPages.StandardController controller) {
        byYearRow = new list<OppTotals>();
        byCampaignRow = new list<OppTotals>();
        boolean hasCampaign = false;
                        
        objId = controller.getId();
        //objId = ApexPages.currentPage().getParameters().get('id');                
                
        if (objId != null) {
            //collection for results
            list<sObject> result;
            list<sObject> campaign;
            list<sObject> registration;
                        
            if (controller.getRecord().getSObjectType() == Registration__c.sObjectType){
                registration = [select Expected_Amount__c, Id from Registration__c where Id = :objId];
                result = [select sum(amount) totalAmount, count_distinct(id) countOpp,
                            calendar_year(closeDate) calendarYear                       
                          from Opportunity
                          where registration__c = :objId
                          and stageName = 'posted'                                                                                
                          group by rollup (calendar_year(closeDate))
                          order by calendar_year(closeDate) asc];                                                            
            } else if (controller.getRecord().getSObjectType() == Contact.sObjectType) { 
                result = [select sum(Opportunity.amount) totalAmount, count_distinct(Opportunity.id) countOpp,
                            calendar_year(Opportunity.closeDate) calendarYear
                          from OpportunityContactRole
                          where contactId = :objId                                                                
                          //and Opportunity.stageName = 'posted'
                          group by rollup (calendar_year(Opportunity.closeDate))
                          order by calendar_year(Opportunity.closeDate) desc]; 
                campaign = [select sum(Opportunity.amount) totalAmount, count_distinct(Opportunity.id) countOpp,
                             Opportunity.Campaign.reporting_group__c campaignType
                            from OpportunityContactRole
                            where contactId = :objId                                                                
                            and Opportunity.stageName = 'posted'
                            group by rollup (Opportunity.Campaign.reporting_group__c)
                            order by count_distinct(Opportunity.id) desc, sum(Opportunity.amount) desc];
                                         hasCampaign = true;
            } else if (controller.getRecord().getSObjectType() == Account.sObjectType) { 
                result = [select sum(amount) totalAmount, count_distinct(id) countOpp,
                             calendar_year(closeDate) calendarYear
                          from Opportunity
                          where accountId = :objId                                                                
                          //and stageName = 'posted'
                          group by rollup (calendar_year(closeDate))
                          order by calendar_year(closeDate) desc];                                        
                campaign = [select sum(amount) totalAmount, count_distinct(id) countOpp,
                              Campaign.reporting_group__c campaignType
                            from Opportunity
                            where accountId = :objId                                                                
                            and stageName = 'posted'
                            group by rollup (Campaign.reporting_group__c)
                            order by count_distinct(id) desc, sum(amount) desc];
                hasCampaign = true;             
             }
             
             decimal balance = 0.0;
             decimal total = 0.0;
             decimal prevTotal = 0.0;
             decimal ea = 0.0;
                        
             //assign values to properties
             for (Sobject o : result){
                // get the year for this row
                string d = string.valueOf(o.get('calendarYear'));
                                
                // null year means this is the totals row
                string dt = (d != null ) ? d : 'Total';
                                
                OppTotals currentRow = new OppTotals(dt);                                                                 
                currentRow.oppNum = (integer) o.get('countOpp');
                currentRow.oppAmount = (decimal) o.get('totalAmount');
                
                if (registration != null)
                    for (Sobject r : registration) {
                        if ((decimal) r.get('Expected_Amount__c') != null)
                            ea = (decimal) r.get('Expected_Amount__c');
                        
                        if ( ea > 0 && dt != 'Total') {
                            total =  ea - (decimal) o.get('totalAmount') - prevTotal;
                            prevTotal += (decimal) o.get('totalAmount');                                                        
                        }
                        if ( ea > 0 && dt == 'Total' ) {
                            total =  ea - (decimal) o.get('totalAmount');
                        }
                        currentRow.balance = total;
                    }                
                                
                // add it to the list
                if (dt != 'Total'){
                    byYearRow.add(currentRow);                                  
                } else {
                    totalRow = currentRow;
                }                           
             }
             
             //load second dataset for campaign
             if (hasCampaign){
                for (Sobject o : campaign){
                    // get the year for this row
                    string d = string.valueOf(o.get('campaignType'));
                                
                    // null year means this is the totals row
                    string dt = (d != null ) ? d : 'Total';
                                
                    OppTotals currentRow = new OppTotals(dt);                                                                 
                    currentRow.oppNum = (integer) o.get('countOpp');
                    currentRow.oppAmount = (decimal) o.get('totalAmount');                                               
                                
                    // add it to the list
                    if (dt != 'Total'){
                        byCampaignRow.add(currentRow);                                  
                    } else {
                        totalRow = currentRow;
                    }                    
             }
          }
        }
     }
        
        static testMethod void testRollUp(){
                //create test data
                Account testAccount = new Account(name = 'test account');
                insert testAccount;
                
                Contact testContact = new Contact(firstName = 'test', lastName = 'contact', accountId = testAccount.Id);
                insert testContact;
                
                Campaign testCamp = new Campaign(name = 'test', campaign_code__c = 'test code', campaign_type__c = 'test', status = 'test', reporting_group__c = 'test');
                insert testCamp;
                
                Registration__c testReg = new Registration__c(campaign__c = testCamp.Id, status__c = 'test', host_team_captain__c = testContact.Id, expected_amount__c = 10000);
                insert testReg;
                
                //RecordType testRecordType = [select id from recordType where SobjectType = 'Opportunity' and name = 'Donation' limit 1];
                Opportunity[] newOpps = new Opportunity[]{
                        new Opportunity (name = 'test opp 1', stageName = 'posted', closeDate = system.today(), amount = 100, campaignId = testCamp.Id, registration__c = testReg.Id, accountId = testAccount.Id),
                        new Opportunity (name = 'test opp 2', stageName = 'posted', closeDate = system.today().addDays(-365), campaignId = testCamp.Id, amount = 50, registration__c = testReg.Id, accountId = testAccount.Id)       
                };              
                insert newOpps;
                
                OpportunityContactRole[] newRoles = new OpportunityContactRole[]{
                        new OpportunityContactRole (contactId = testContact.Id, opportunityId = newOpps[0].Id, isPrimary = true),
                        new OpportunityContactRole (contactId = testContact.Id, opportunityId = newOpps[1].Id, isPrimary = true)
                };
                insert newRoles;
                
                Test.startTest();
                OpportunityRollupController rollUp;
                OpportunityRollupController rollUp2;
                OpportunityRollupController rollUp3;
                
                //create a new record to test registration total
                rollUp = new OpportunityRollupController (new ApexPages.Standardcontroller(testReg));
                rollUp2 = new OpportunityRollupController (new ApexPages.Standardcontroller(testContact));
                rollUp3 = new OpportunityRollupController (new ApexPages.Standardcontroller(testAccount));
                
                system.assertEquals(50, rollUp.byYearRow[0].oppAmount);
                system.assertEquals(1, rollUp.byYearRow[0].oppNum);
                system.assertEquals(100, rollUp2.byYearRow[0].oppAmount);
                system.assertEquals(2, rollUp2.byCampaignRow[0].oppNum);                
                system.assertEquals(100, rollUp3.byYearRow[0].oppAmount);
                system.assertEquals(2, rollUp3.byCampaignRow[0].oppNum);                
                
                Test.stopTest();                
        }
}