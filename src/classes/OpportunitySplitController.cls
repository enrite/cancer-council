public with sharing class OpportunitySplitController {

/*
- to split current opportunity (base opportunity) into several opportunity records with most of the statics information provided
and few dynamics information provided by users from screen.
- create Contact Role for Contact and auto create Soft Credit for split opportunity records
*/
    //will hold the Opportunity record to be saved
    public list<Opportunity> oppList;
    public list<OpportunityContactRole> oppContactRoleList;
    public list<Soft_Credit__c> softCreditList;
    public list<Id> conIdList;
    public list<Id> accIdList;
    
    public Opportunity oppBase {get;set;}
    public OpportunityContactRole oppContactRole;

    //no. of rows added/records in the inner class list
    public integer count=1;
    
    //check if splitting amount is more than original amount
    public decimal sumAmount;
    
    //to show the amount to be split from
    public decimal baseAmount {get;set;}    
    
    //check if transaction is valid to be processed
    public boolean isValid=true;
        
    //will indicate the row to be deleted
    public string rowIndex {get;set;}
        
    //list of the inner class
    public list<innerClass> innerClassList {get;set;}
    
    // send email alert when exception thrown
    EmailUtil email;
    list<string> recipients = new list<string>{'ithelpdesk@cancersa.org.au'};
    string replyTo = 'ithelpdesk@cancersa.org.au';  
    
    public OpportunitySplitController(ApexPages.StandardController ctlr) {    	
        try {
            innerClassList = new list<innerClass>();
            addMore();      
            rowIndex = '0';     
            oppBase = [select 
                        StageName
                        , Amount
                        , CloseDate
                        , CampaignId
                        , Payment_Type__c
                        , Payment_Gateway__c
                        , Income_Type__c
                        , Registration__c
                        , Name
                        , AccountId
                        , Primary_Contact_at_Organisation__c
                        , RecordTypeId
                        , Batch_Number__c
                        , Bank_Account_Number__c
                        , Transaction_Reference__c
                        , Credit_Card__c
                        , Cheque_Name__c
                        , Bank_Name__c
                        , Branch_Name__c
                        , Receipt_Number__c
                        , Related_Receipt_Number__c
                        , Contact_Registration__c
                        from Opportunity 
                        where Id = :ApexPages.CurrentPage().getParameters().get('id')];
            baseAmount = oppBase.Amount;
        }
        catch (exception e) {           
            email = new EmailUtil(recipients, replyTo, 'Errors thrown - init OpportunitySplitController Constructor: ', e.getMessage(), 'Plain');
            system.debug('***** Errors thrown - init OpportunitySplitController Constructor: ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'init Constructor errors: ' + e.getMessage()));
        }
    }
    
    //save the records by adding the elements in the inner class list to lstAcct,return to the same page
    public PageReference Save() {
        try {           
            PageReference pr = new PageReference('/apex/OpportunitySplit?id=' + ApexPages.CurrentPage().getParameters().get('id'));
                        
            sumAmount=0;
            accIdList = new list<Id>();
            conIdList = new list<Id>();
            oppList = new list<Opportunity>();
            oppContactRoleList = new list<OpportunityContactRole>();
            softCreditList = new list<Soft_Credit__c>();
            
            for(Integer j = 0; j<innerClassList.size(); j++) {
                
                //set statics fields from base opportunity to new opportunity records
                innerClassList[j].opp.StageName = 'Posted';
                innerClassList[j].opp.Registration__c = oppBase.Registration__c;
                innerClassList[j].opp.CampaignId = oppBase.CampaignId;                  
                innerClassList[j].opp.CloseDate = oppBase.CloseDate;                    
                innerClassList[j].opp.Payment_Type__c = oppBase.Payment_Type__c;
                innerClassList[j].opp.Payment_Gateway__c = oppBase.Payment_Gateway__c;
                innerClassList[j].opp.RecordTypeId = oppBase.RecordTypeId;
                innerClassList[j].opp.Batch_Number__c = oppBase.Batch_Number__c;
                innerClassList[j].opp.Bank_Account_Number__c = oppBase.Bank_Account_Number__c;
                innerClassList[j].opp.Transaction_Reference__c = oppBase.Transaction_Reference__c;
                innerClassList[j].opp.Credit_Card__c = oppBase.Credit_Card__c;
                innerClassList[j].opp.Cheque_Name__c = oppBase.Cheque_Name__c;
                innerClassList[j].opp.Bank_Name__c = oppBase.Bank_Name__c;
                innerClassList[j].opp.Branch_Name__c = oppBase.Branch_Name__c;
                innerClassList[j].opp.Related_Receipt_Number__c = oppBase.Receipt_Number__c;
               	//innerClassList[j].opp.Contact_Registration__c = oppBase.Contact_Registration__c;
                
                if (innerClassList[j].opp.AccountId != null) {
                    accIdList.add(innerClassList[j].opp.AccountId);             
                }
                else if (innerClassList[j].opp.Primary_Contact_at_Organisation__c != null) {
                    conIdList.add(innerClassList[j].opp.Primary_Contact_at_Organisation__c);                
                }
                            
                //account and contact cannot be filled at the same time
                if (innerClassList[j].opp.AccountId != null && innerClassList[j].opp.Primary_Contact_at_Organisation__c != null) {
                    isValid = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Account and Contact cannot be filled at the same time!')); 
                }
                
                //either account or contact need to be filled
                if (innerClassList[j].opp.AccountId == null && innerClassList[j].opp.Primary_Contact_at_Organisation__c == null) {
                    isValid = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Either Account or Contact needs to be filled!'));
                }
                
                //if income_type__c is not filled
                if (innerClassList[j].opp.Income_Type__c == null) {
                    isValid = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Income Type needs to be filled!'));
                }
            }
            
            if (isValid) {
                //loop through contact list to get required dynamic information
                for (Contact c : [select AccountId, Letter_Salutation__c, FirstName, LastName, Id from Contact where Id in :conIdList]) {           
                    for (integer i = 0; i<innerClassList.size(); i++) {
                        if (innerClassList[i].opp.Primary_Contact_at_Organisation__c == c.Id) {
                            innerClassList[i].opp.Donation_Salutation__c = c.Letter_Salutation__c;
                            innerClassList[i].opp.AccountId = c.AccountId;
                            
                            sumAmount += innerClassList[i].opp.Amount;
                            
                            if( c.FirstName == null) {
                                innerClassList[i].opp.Name = c.LastName +'- Donation '+ oppBase.CloseDate.format();
                            }
                            else {
                                innerClassList[i].opp.Name = c.FirstName + ' ' + c.LastName+'- Donation '+ oppBase.CloseDate.format();
                            }
                            oppList.add(innerClassList[i].opp);
                        }
                    }
                }
                //loop through account list to get required dynamic information
                for (Account a : [select Id, Name from Account where Id in :accIdList]) {
                    for (integer i = 0; i<innerClassList.size(); i++) {
                        if (innerClassList[i].opp.AccountId == a.Id) {                          
                            innerClassList[i].opp.Name = a.Name +'- Donation '+ oppBase.CloseDate.format();
                            
                            sumAmount += innerClassList[i].opp.Amount;
                                                        
                            oppList.add(innerClassList[i].opp);
                        }
                    }
                }
                
                //compare total amount of split opportunity to original amount
                if (oppBase.Amount >= sumAmount) {
                    //update base Opportunity Amount for opportunity split process
                    oppBase.Amount = oppBase.Amount - sumAmount;
                    update oppBase;
                    
                    insert oppList;
                    
                    //loop through all records with contact filled and add Opportunity Contact Role to respective Contact
                    for (integer k = 0; k<oppList.size(); k++) {
                        if (oppList[k].Primary_Contact_at_Organisation__c != null) {
                            oppContactRole = new OpportunityContactRole();
                            oppContactRole.ContactId = oppList[k].Primary_Contact_at_Organisation__c;
                            oppContactRole.OpportunityId = oppList[k].Id;
                            oppContactRole.IsPrimary=true;
                            oppContactRole.Role='Donor';
                            oppContactRoleList.add(oppContactRole);
                        }
                    }
                    
                    //insert Opportunity Contact Role if exists
                    if (oppContactRoleList.size() > 0) {
                        insert oppContactRoleList;
                    }
                    
                    //insert soft credit to newly created donation split records
                    for (Opportunity o : oppList) {
                        Soft_Credit__c sc = new Soft_Credit__c();
                        sc.Opportunity__c = o.Id;
                        sc.Contact_Registration__c = oppBase.Contact_Registration__c;
                        
                        if (oppBase.Primary_Contact_at_Organisation__c != null) {
                            sc.Contact__c = oppBase.Primary_Contact_at_Organisation__c;
                        }
                        else {
                            sc.Account__c = oppBase.AccountId;
                        }
                        softCreditList.add(sc);
                    }
                    
                    insert softCreditList;
                    
                    pr.setRedirect(True);
                    return pr;                  
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'New amount is more than original opportunity amount: ' + string.valueOf(sumAmount)));                  
                    return null;
                }
            }
            else {
                //reset isValid to true
                isValid=true;
                return null;
            }                   
        }
        catch (exception e) {
            email = new EmailUtil(recipients, replyTo, 'Errors thrown - Save method Errors: ', e.getMessage(), 'Plain');
            system.debug('***** Errors thrown - Save method Errors: ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Save method error: ' + e.getMessage()));
            return null;
        }       
    }
    
    //add one more row
    public void Add() {
        count = count + 1;
        addMore();      
    }
    
    public void addMore() {
        //call to the inner class constructor
        innerClass objInnerClass = new innerClass(count);
        
        //add record to the inner class list
        innerClassList.add(objInnerClass);
    }
    
    public void Del() {
        innerClassList.remove(integer.valueOf(rowIndex)-1);
        count = count - 1;
    }
    
    /* inner class */
    public class innerClass {
        /* recordCount acts as an index for a row. thi will be helpful to identify the row to be deleted */
        public string recordCount {get;set;}
        
        public Opportunity opp {get;set;}
        
        /* inner class constructor */
        public innerClass(integer intCount) {
            recordCount = string.valueOf(intCount);
            
            /* create a new record */
            opp = new Opportunity();
                                
        }/* end inner class constructor*/
    }/* end inner class */
    
}