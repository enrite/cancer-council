/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestEntryCheck {

    static testMethod void TestEntryCheck() {
        Account acc = new Account(name='Test Test', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com');
        insert acc;
        
        Contact con = new Contact(AccountId=acc.Id, FirstName='Test Test', LastName='Test Test', MailingStreet='Test Test', MailingCity='Test Test'
            , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');
        insert con;
        
        test.startTest();
        
        EntryCheckController testing = new EntryCheckController();
            
        PageReference pageRef = new PageReference('/apex/EntryCheck');
        
        test.setCurrentPage(pageRef);
        
        testing.firstName='Test Test';
        testing.lastName='Test Test';
        testing.company='Test Test';
        testing.street='Test Test';
        testing.suburb='Test Test';
        testing.postcode='Test Test';
        testing.state='Test Test';
        testing.phone='00 0000 0000';
        testing.mobile='0000 000 000';
        testing.email='Test@Test.com';
        
        testing.searchResult();
        
        system.assertEquals(testing.noOfRecordsContact, 1);
        system.assertEquals(testing.noOfRecordsAccount, 1);
        system.assertEquals(testing.contactList.size(), 1);
        system.assertEquals(testing.accountList.size(), 1);

        testing.clearAll();
        
        testing.firstName='Test1 Test';
        testing.lastName='Test1 Test';
        testing.company='Test1 Test';
        testing.street='Test1 Test';
        testing.suburb='Test1 Test';
        testing.postcode='Test1 Test';
        testing.state='Test1 Test';
        testing.phone='001 0000 0000';
        testing.mobile='00001 000 000';
        testing.email='Test1@Test.com';
        
        testing.searchResult();
        
        system.assertEquals(testing.noOfRecordsContact, 0);
        system.assertEquals(testing.noOfRecordsAccount, 0);
        system.assertEquals(testing.contactList.size(), 0);
        system.assertEquals(testing.accountList.size(), 0);
        test.stopTest();
    }
}