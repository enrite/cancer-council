public with sharing class QuitskillsCourseApplicationController extends RecaptchaController
{

    // the name of the referral query string parameter
    private static final String PARAM_REFERRAL = 'referral';

    // Error to be displayed when a field is required.
    private static final String REQUIRED_FIELD = 'Field is required';

    // Error displayed if the phone and mobile phone fields are both not supplied.
    private static final String REQUIRED_PHONE = 'Either the phone or mobile phone is required';

    public Ambassador_Activity__c Course { get; set; }

    public Registration__c Application { get; set; }

    public boolean Submitted { get; set; }

    public boolean IsQuitSkills  {
        get {
            return this.Course != null && String.isNotBlank(this.Course.Project_Area__c) && this.Course.Project_Area__c.indexOf('Quitskills') >=0 ;
        }
    }
    /**
     * Default constructor
     */
    public QuitskillsCourseApplicationController()
    {
        initForm();
    }

    /**
     * This method initialises the form data
     */
    private void initForm()
    {
        try
        {
            // create a new instance of case
            this.Course = getCourse();
            this.Submitted = false;
            this.Application = new Registration__c();
            this.Application.Status__c = 'Applied';
            this.Application.Web_Registration__c = true;
            this.Application.Shirt_Type__c = 'Quitskills';

            Application.Community_Engagement__c = this.Course.Id;
            ValidCaptcha = true;
        }
        catch (Exception ex)
        {
            CustomException.formatException(ex);
        }
    }

    public Boolean ValidCaptcha { get; set; }
    /**
     * This method when the save/submit button is clicked on the form.
     */
    public PageReference saveOverride()
    {
        try
        {
            if (validateForm())
            {
                ValidCaptcha = verifyCaptcha();
                if (!ValidCaptcha)
                {
                    CustomException.formatException('Please verify you are not a robot.');
                    return null;
                }

                upsert Application;

                /*
                EmailTemplate template = [SELECT Id, HtmlValue FROM EmailTemplate where DeveloperName = 'Quitskills_Submitted'];
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                msg.toAddresses = new List<String>{Application.Web_Email__c};
                msg.htmlBody = template.HtmlValue.replace('{!Registration__c.Web_First_Name__c}', Application.Web_First_Name__c)
                        .replace('{!Ambassador_Activity__c.Project_Area__c}', Course.Project_Area__c)
                        .replace('{!Ambassador_Activity__c.Session_1__c}', ((Datetime)Course.Session_1__c).format('dd/MM/YYYY'));

                Messaging.SingleEmailMessage[] messages =
                        new List<Messaging.SingleEmailMessage> {msg};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
				*/

                Submitted = true;
            }
        }
        catch (Exception e)
        {
            CustomException.formatException(e);
        }

        return null;
    }

    public Ambassador_Activity__c getCourse()
    {
        String courseId = ApexPages.currentPage().getParameters().get('courseId');

        return
        [
                SELECT ID, Web_Name__c, Session_1__c, Project_Area__c
                FROM Ambassador_Activity__c
                WHERE Id = :courseId
        ];
    }

    /**
     * This method performs any validation that cannot be performed within validation rules etc.
     * @return true if the form is valid otherwise false will be returned.
     */
    private boolean validateForm()
    {
        ValidCaptcha = true;
        return validateRegisterForm();
    }


    public Integer NumberErrors
    {
        get
        {
            return ApexPages.getMessages().size();
        }
    }
    /**
     * This method validates the registration form. If there are no validation errors then true is returned. If there are
     * validation errors then false is returned and the relevant errors are displayed.
     * @return true if the form is valid otherwise false is returned.
     */
    private boolean validateRegisterForm()
    {
        boolean isValid = true;

        if (String.isBlank(Application.Web_First_Name__c))
        {
            Application.Web_First_Name__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(Application.Web_Last_Name__c))
        {
            Application.Web_Last_Name__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(Application.Web_Street__c))
        {
            Application.Web_Street__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(Application.Web_Suburb__c))
        {
            Application.Web_Suburb__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(Application.Web_State__c))
        {
            Application.Web_State__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(Application.Web_Postcode__c))
        {
            Application.Web_Postcode__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(Application.Web_Email__c))
        {
            Application.Web_Email__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(Application.Web_Phone__c) && String.isBlank(Application.Web_Mobile__c))
        {
            Application.Web_Phone__c.addError(REQUIRED_PHONE);
            isValid = false;
        }


        if (!String.isBlank(Application.Web_Phone__c) && !isValidPhone(Application.Web_Phone__c))
        {
            Application.Web_Phone__c.addError(RecaptchaController.INVALID_PHONE);
            isValid = false;
        }

        if (!String.isBlank(Application.Web_Mobile__c) && !isValidMobile(Application.Web_Mobile__c))
        {
            Application.Web_Mobile__c.addError(RecaptchaController.INVALID_MOBILE);
            isValid = false;
        }

        return isValid;
    }


    public static List<SelectOption> getPickValues(Sobject object_name, String field_name, String first_val, String firstValDescription)
    {
        List<SelectOption> options = new List<SelectOption>(); //new list for holding all of the picklist options
        if (first_val != null)
        { //if there is a first value being provided
            options.add(new selectOption(first_val, firstValDescription)); //add the first option
        }
        Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values)
        { //for all values in the picklist list

            options.add(new SelectOption(a.getValue(), a.getLabel())); //add the value and label to our final list
        }
        return options; //return the List
    }

    private String generateUniqueId()
    {
        // use two hashes to make it a longer string
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(String.valueOf(Crypto.getRandomLong())));
        Blob hash2 = Crypto.generateDigest('MD5', Blob.valueOf(String.valueOf(Crypto.getRandomLong())));
        return EncodingUtil.convertToHex(hash) + EncodingUtil.convertToHex(hash2);
    }
}