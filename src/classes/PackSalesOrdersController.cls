public with sharing class PackSalesOrdersController
{
    public PackSalesOrdersController(ApexPages.StandardSetController ctrlr)
    {
        if(!Test.isRunningTest())
        {
            ctrlr.addFields(new List<String>(fields));
        }
        controller = ctrlr;
        SalesOrders = (List<PBSI__PBSI_Sales_Order__c>) controller.getSelected();
        system.debug(SalesOrders);
    }

    private ApexPages.StandardSetController controller;

    private Set<String> fields = new Set<String>{'Name','Registration__r.Name', 'Contact_Registration__r.Contact__r.Name'};

    public List<PBSI__PBSI_Sales_Order__c> SalesOrders
    {
        get;
        set;
    }

    public PageReference packSalesOrders()
    {
        try
        {
            Set<String> soIds = new Set<String>();
            for(PBSI__PBSI_Sales_Order__c so : SalesOrders)
            {
                soIds.add(so.ID);
                PBSI.Pack4SOControllerNew packController = new PBSI.Pack4SOControllerNew(new ApexPages.StandardController(so));
                packController.soids = soIds;
                system.debug(packController.GetPackLineModels());
                packController.savepacked();
                //PageReference pg = packController.pack();
               // system.debug(pg);
            }
            /*PBSI.Pack4SOcontroller packController = new PBSI.Pack4SOcontroller(new );
            packController.soids = soIds;
            PageReference pg = packController.packAll();

system.debug(pg);
            pg = packController.savepacked();
system.debug(pg);*/
            return null;
        }
        catch(Exception ex)
        {
            return CustomException.formatException(ex);
        }
    }

    public void validateSalesOrders()
    {
        try
        {
            if(SalesOrders.isEmpty())
            {
                throw new CustomException('No Sales Orders were selected');
            }
        }
        catch(Exception ex)
        {
            CustomException.formatException(ex);
        }
    }
}