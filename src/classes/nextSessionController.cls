public with sharing class nextSessionController{
    
	// Class Variables
    String standardController;    

	// Constructors
    public nextSessionController() {
        standardController = ApexPages.CurrentPage().getParameters().get('id');
    }

    public nextSessionController(ApexPages.StandardController controller) {
        standardController = controller.getId();
        redirectUrl = controller.view().getUrl();
        renderTimeOfDay = 'false';
        renderTime = 'false';
        renderSave = 'false';
        renderDate = 'false';
    }
    
	// Class Properties
    public String redirectUrl {public get; private set;}
    public String timeRequestedTextBox { get; set; }
    public Boolean anytimeCheckbox { get; set; }    
    public String timeSelected { get; set; }
    public String timeOfDaySelected { get; set; }
    public String sessionIdSelected { get; set; }
    public String caseCallType { get; set; }
        
    public String renderTimeOfDay { get; set; }
    public String renderTime { get; set; }
    public String renderSave { get; set; }
    
    //On Call page, check if session available
    public String renderCallDate {
        get{
            Callback__c callbackCase = [SELECT Next_Callback__c, Status__c, Case__r.Call_Type__c, Take_Callback__c FROM Callback__c WHERE id = :standardController];
            caseCallType = callbackCase.Case__r.Call_Type__c;
            
            if (caseCallType != 'Quitline' && callbackCase.Take_Callback__c == true && callbackCase.Next_Callback__c == null) {
            	return 'true';
            }
            else if(callbackCase.Status__c == 'Pending' || callbackCase.Status__c == 'Cancelled - System') {
                return 'false';
            }
            else if(callbackCase.Next_Callback__c == null) {
                return 'true';
            }
            else {
                return 'false';
            }            
        } 
        set;
    }  
    
    // On Call page delete Next Call
    public String renderNextCallDelete {
        get{
            Callback__c callbackCase = [SELECT Next_Callback__c FROM Callback__c WHERE id = :standardController];
            Callback__c[] nextCallback = [SELECT id, Status__c FROM Callback__c WHERE id = :callbackCase.Next_Callback__c];
            
            if (nextCallback.size() > 0) {
                if(nextCallback[0].Status__c != 'Pending') {
                    return 'false';
                }
            }
            if(callbackCase.Next_Callback__c == null) {
                return 'false';
            }
            else {
                return 'true';
            }
        } 
        set;
    }
    
    // Date rendered
    public String renderDate {
        get{
            Case callbackCase = [SELECT Initial_Callback__c, Call_Type__c FROM Case WHERE id = :standardController];
            caseCallType = callbackCase.Call_Type__c;
            
            if(callbackCase.Initial_Callback__c == null) {
                return 'true';
            }
            else {
                return 'false';
            }
        } 
        set;
    }
        
    // Has scheduled Initial Call
    public String renderInitialCall {
        get{
            Case callbackCase = [SELECT Initial_Callback__c FROM Case WHERE id = :standardController];
            Callback__c[] nextCallback = [SELECT id, Status__c FROM Callback__c WHERE id = :callbackCase.Initial_Callback__c];
            if(callbackCase.Initial_Callback__c == null) {
                return 'false';
            }
            else {
                return 'true';
            }
        } 
        set;
    }
    
    public String renderInitialCallDelete {
        get{
            Case callbackCase = [SELECT Initial_Callback__c FROM Case WHERE id = :standardController];
            Callback__c[] nextCallback = [SELECT id, Status__c FROM Callback__c WHERE id = :callbackCase.Initial_Callback__c];
            if (nextCallback.size() > 0) {
            	if(nextCallback[0].Status__c != 'Pending') {
                	return 'false';
            	}
            }
            if(callbackCase.Initial_Callback__c == null) {
                return 'false';
            }
            else {
                return 'true';
            }
        } 
        set;
    }
    
    // Retrieve Callback RecordType Ids
    private static map<string, Id> mapCallBackRecordTypeId {
        get{
            if (mapCallBackRecordTypeId == null) {
                mapCallBackRecordTypeId = new map<string, Id>();
                
                for (RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Callback__c']) {
                    mapCallBackRecordTypeId.put(rt.DeveloperName, rt.Id);               
                }               
            }
            return mapCallBackRecordTypeId;
        }
    }
    
    private String sessionRecordType
    {
        get
        {
            if (sessionRecordType == null)
            {
                // find the required record type for sessions based on the parent case  
                String caseRecordType;
                for (Case parentCase : [SELECT RecordType.DeveloperName
                                        FROM Case
                                        WHERE Id = :standardController])                                  
                { 
                    caseRecordType = parentCase.RecordType.DeveloperName;
                }
                
                // if case record type is still null then we may be on a call record
                if (caseRecordType == null)
                {
                    for (Callback__c call : [SELECT Case__r.RecordType.DeveloperName
                                             FROM Callback__c
                                             WHERE Id = :standardController])                                  
                    { 
                        caseRecordType = call.Case__r.RecordType.DeveloperName;
                    }                        
                }
                
                // determine the required session record type based on the case record type
                if (caseRecordType == 'Quitline')
                {                        
                    sessionRecordType = 'Quitline';
                }
                else if (caseRecordType == 'Helpline_Case')
                {                         
                    sessionRecordType = 'X131120';
                }                                                
            } 

            return sessionRecordType;
        }
        set;
    }
    
    public Callback__c getLinkedCallback() {
        Callback__c controller = [SELECT Next_Callback__c FROM Callback__c Where id = :standardController];
        Callback__c nextCallback = [SELECT id, Name, Date__c, Client_Name__c, Session__c, Timeslot__c, Time_Request__c, Day_of_Week__c, Anytime__c FROM Callback__c WHERE id = :controller.Next_Callback__c];
        return nextCallback;
    }
    
    public Callback__c getLinkedCallbackFromCase() {
        Case controller = [SELECT Initial_Callback__c FROM Case Where id = :standardController];
        Callback__c nextCallback = [SELECT id, Name, Date__c, Client_Name__c, Session__c, Timeslot__c, Time_Request__c, Day_of_Week__c, Anytime__c FROM Callback__c WHERE id = :controller.Initial_Callback__c];
        return nextCallback;
    }
    
    public List<SelectOption> getDateList() {                
        Quitline_Sessions__c[] avaliableSessions = [SELECT id, Day_Status__c, Date__c, Day_of_Week__c 
                                                    FROM Quitline_Sessions__c 
                                                    WHERE Day_Status__c = 'Still Open' 
                                                    AND Date__c >= today
                                                    AND RecordType.DeveloperName = :sessionRecordType
                                                    AND Call_Type__c =: caseCallType
                                                    ORDER BY Date__c asc];
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Select one...'));
        if(avaliableSessions.Size() > 0){
            for(Quitline_Sessions__c i : avaliableSessions){
                options.add(new SelectOption(String.valueOf(i.id),String.valueOf(i.Date__c) + ' ' + String.valueOf(i.Day_of_Week__c)));
            }
        }
        return options;
    }

    public List<SelectOption> getDayTimeList() {
                                    
        Quitline_Sessions__c[] avaliableSessionsArray = [SELECT id, Date__c, AM_Available__c, PM_Available__c, Evening_Available__c  FROM Quitline_Sessions__c WHERE id = :sessionIdSelected ];
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Select one...'));
        if(avaliableSessionsArray.size() > 0){
            Quitline_Sessions__c avaliableSessions = avaliableSessionsArray[0];
            if(avaliableSessions.AM_Available__c > 0)options.add(new SelectOption('AM','AM (' + String.valueOf(avaliableSessions.AM_Available__c) + ')'));
            if(avaliableSessions.PM_Available__c > 0)options.add(new SelectOption('PM','PM (' + String.valueOf(avaliableSessions.PM_Available__c) + ')'));
            if(avaliableSessions.Evening_Available__c > 0)options.add(new SelectOption('Evening','Evening (' + String.valueOf(avaliableSessions.Evening_Available__c) + ')'));
        }
        return options;
    }

    public List<SelectOption> getTimeList() {
        Quitline_Sessions__c[] avaliableSessionsArray = [SELECT 
                                                        id
                                                        , Date__c
                                                        , X8_30_9_30_Available__c
                                                        , X9_30_10_30_Available__c
                                                        , X10_30_11_30_Available__c
                                                        , X11_30_12_30_Available__c
                                                        , X12_30_2_00_Available__c
                                                        , X2_3_Available__c
                                                        , X3_4_Available__c
                                                        , X4_5_Available__c
                                                        , X5_6_Available__c
                                                        , X6_7_PM_Available__c
                                                        , X7_8_PM_Available__c
                                                        , X8_9_PM_Available__c
                                                        , X9_9_30_PM_Available__c
                                                        , X9_10_AM_Available__c
                                                        , X10_11_AM_Available__c
                                                        , X11_12_PM_Available__c
                                                        , X12_1_PM_Available__c
                                                        , X1_2_PM_Available__c
                                                        FROM Quitline_Sessions__c 
                                                        WHERE id = :sessionIdSelected ];
                
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Select one...'));
        if(avaliableSessionsArray.size() > 0){
            Quitline_Sessions__c avaliableSessions = avaliableSessionsArray[0];
            if(timeOfDaySelected=='AM'){
                if(avaliableSessions.X8_30_9_30_Available__c > 0)options.add(new SelectOption('8.30-9.30 AM','8.30-9.30 AM (' + String.valueOf(avaliableSessions.X8_30_9_30_Available__c) + ')'));
                if(avaliableSessions.X9_30_10_30_Available__c > 0)options.add(new SelectOption('9.30-10.30 AM','9.30-10.30 AM (' + String.valueOf(avaliableSessions.X9_30_10_30_Available__c) + ')'));
                if(avaliableSessions.X10_30_11_30_Available__c > 0)options.add(new SelectOption('10.30-11.30 AM','10.30-11.30 AM (' + String.valueOf(avaliableSessions.X10_30_11_30_Available__c) + ')'));
                if(avaliableSessions.X11_30_12_30_Available__c > 0)options.add(new SelectOption('11.30-12.30 PM','11.30-12.30 PM (' + String.valueOf(avaliableSessions.X11_30_12_30_Available__c) + ')'));
                
                if(avaliableSessions.X9_10_AM_Available__c > 0)options.add(new SelectOption('9-10 AM','9-10 AM (' + String.valueOf(avaliableSessions.X9_10_AM_Available__c) + ')'));
                if(avaliableSessions.X10_11_AM_Available__c > 0)options.add(new SelectOption('10-11 AM','10-11 AM (' + String.valueOf(avaliableSessions.X10_11_AM_Available__c) + ')'));
                if(avaliableSessions.X11_12_PM_Available__c > 0)options.add(new SelectOption('11-12 PM','11-12 PM (' + String.valueOf(avaliableSessions.X11_12_PM_Available__c) + ')'));
            }else if(timeOfDaySelected=='PM'){
                if(avaliableSessions.X12_30_2_00_Available__c > 0)options.add(new SelectOption('12.30-2 PM','12.30-2 PM (' + String.valueOf(avaliableSessions.X12_30_2_00_Available__c) + ')'));
                if(avaliableSessions.X12_1_PM_Available__c > 0)options.add(new SelectOption('12-1 PM','12-1 PM (' + String.valueOf(avaliableSessions.X12_1_PM_Available__c) + ')'));
                if(avaliableSessions.X1_2_PM_Available__c > 0)options.add(new SelectOption('1-2 PM','1-2 PM (' + String.valueOf(avaliableSessions.X1_2_PM_Available__c) + ')'));                
                if(avaliableSessions.X2_3_Available__c > 0)options.add(new SelectOption('2-3 PM','2-3 PM (' + String.valueOf(avaliableSessions.X2_3_Available__c) + ')'));
                if(avaliableSessions.X3_4_Available__c > 0)options.add(new SelectOption('3-4 PM','3-4 PM (' + String.valueOf(avaliableSessions.X3_4_Available__c) + ')'));
                if(avaliableSessions.X4_5_Available__c > 0)options.add(new SelectOption('4-5 PM','4-5 PM (' + String.valueOf(avaliableSessions.X4_5_Available__c) + ')'));
            }else if(timeOfDaySelected=='Evening'){
                if(avaliableSessions.X5_6_Available__c > 0)options.add(new SelectOption('5-6 PM','5-6 PM (' + String.valueOf(avaliableSessions.X5_6_Available__c) + ')'));
                if(avaliableSessions.X6_7_PM_Available__c > 0)options.add(new SelectOption('6-7 PM','6-7 PM (' + String.valueOf(avaliableSessions.X6_7_PM_Available__c) + ')'));
                if(avaliableSessions.X7_8_PM_Available__c > 0)options.add(new SelectOption('7-8 PM','7-8 PM (' + String.valueOf(avaliableSessions.X7_8_PM_Available__c) + ')'));
                if(avaliableSessions.X8_9_PM_Available__c > 0)options.add(new SelectOption('8-9 PM','8-9 PM (' + String.valueOf(avaliableSessions.X8_9_PM_Available__c) + ')'));
                if(avaliableSessions.X9_9_30_PM_Available__c > 0)options.add(new SelectOption('9-9.30 PM','9-9.30 PM (' + String.valueOf(avaliableSessions.X9_9_30_PM_Available__c) + ')'));
            }
        }
        return options;
    }
    
    String getTimePicklist(){
        Datetime currentTime = Datetime.now();
        String returnValue = '';
        if(currentTime.hour() >= 21){
            returnValue = '9-9.30 PM';
        }else if(currentTime.hour() >= 20){
            returnValue = '8-9 PM';
        }else if(currentTime.hour() >= 19){
            returnValue = '7-8 PM';                        
        }else if(currentTime.hour() >= 18){
            returnValue = '6-7 PM';
        }else if(currentTime.hour() >= 17){
            returnValue = '5-6 PM';
        }else if(currentTime.hour() >= 16){
            returnValue = '4-5 PM';
        }else if(currentTime.hour() >= 15){
            returnValue = '3-4 PM';
        }else if(currentTime.hour() >= 14){
            returnValue = '2-3 PM';
            
        }else if(currentTime.hour() >= 13){
            returnValue = sessionRecordType=='Quitilne' ? '12.30-2 PM' : '1-2 PM';
        
        }else if(currentTime.hour() >= 12 && currentTime.minute() >= 30){
            returnValue = sessionRecordType=='Quitilne' ? '12.30-2 PM' : '12-1 PM';
        }else if(currentTime.hour() >= 12){
            returnValue = sessionRecordType=='Quitilne' ? '11.30-12.30 PM' : '12-1 PM';

        }else if(currentTime.hour() >= 11 && currentTime.minute() >= 30){
            returnValue = sessionRecordType=='Quitilne' ? '11.30-12.30 PM' : '11-12 PM';
        }else if(currentTime.hour() >= 11){
            returnValue = sessionRecordType=='Quitilne' ? '10.30-11.30 AM' : '11-12 PM';

        }else if(currentTime.hour() >= 10 && currentTime.minute() >= 30){
            returnValue = sessionRecordType=='Quitilne' ? '10.30-11.30 AM' : '10-11 AM';
        }else if(currentTime.hour() >= 10){
            returnValue = sessionRecordType=='Quitilne' ? '9.30-10.30 AM' : '10-11 AM';

        }else if(currentTime.hour() >= 9 && currentTime.minute() >= 30){
            returnValue = sessionRecordType=='Quitilne' ? '9.30-10.30 AM' : '9-10 AM';
        }else if(currentTime.hour() >= 9){
            returnValue = sessionRecordType=='Quitilne' ? '8.30-9.30 AM' : '9-10 AM';
        }else{
            returnValue = '8.30-9.30 AM';
        }
        return returnValue;
    }

    public String getNextCallbackName() {
        Callback__c controller = [SELECT Next_Callback__c FROM Callback__c Where id = :standardController];
        Callback__c[] nextCallback = [SELECT Name FROM Callback__c WHERE id = :controller.Next_Callback__c];
        if(nextCallback.size() > 0){
            return nextCallback[0].Name;
        }else{
            return '';
        }
    }

    public String getNextCallbackLink() {
        Callback__c controller = [SELECT Next_Callback__c FROM Callback__c Where id = :standardController];
        Callback__c[] nextCallback = [SELECT id FROM Callback__c WHERE id = :controller.Next_Callback__c];
        if(nextCallback.size() > 0){
            return nextCallback[0].id;
        }else{
            return '';
        }
    }

    public String getPreviousCallbackName() {
        Callback__c controller = [SELECT Previous_Callback__c FROM Callback__c Where id = :standardController];
        Callback__c[] PreviousCallback = [SELECT Name FROM Callback__c WHERE id = :controller.Previous_Callback__c];
        if(PreviousCallback.size() > 0){
            return PreviousCallback[0].Name;
        }else{
            return '';
        }
    }

    public String getPreviousCallbackLink() {
        Callback__c controller = [SELECT Previous_Callback__c FROM Callback__c Where id = :standardController];
        Callback__c[] PreviousCallback = [SELECT id FROM Callback__c WHERE id = :controller.Previous_Callback__c];
        if(PreviousCallback.size() > 0){
            return PreviousCallback[0].id;
        }else{
            return '';
        }
    }

	// Cancel and return to case
    public PageReference goBackToCase(){
        Case caseObject = [SELECT id FROM Case WHERE id = :standardController];
        PageReference casePage = new ApexPages.StandardController(caseObject).view();
        return casePage;
    }

	// Save
    public PageReference saveCallbackFromCase() {
        Callback__c callBack = new Callback__c();
        callBack.Quitline_Sessions__c = sessionIdSelected;
        callBack.Timeslot__c = timeSelected;
        callBack.Anytime__c = anytimeCheckbox;
        callBack.Time_Request__c = (timeRequestedTextBox.length()>100)?timeRequestedTextBox.substring(0,100):timeRequestedTextBox;
        Case callbackCase = [SELECT id, Initial_Callback__c FROM Case Where id = :standardController];
        callBack.Case__c = standardController;
        callBack.Call_Type__c = (sessionRecordType == 'Quitline') ?  'Initial' : caseCallType;
        callBack.recordTypeId = (sessionRecordType == 'Quitline') ?  mapCallBackRecordTypeId.get('Initial') : mapCallBackRecordTypeId.get('X131120');
        callBack.Call_Direction__c = 'Outgoing';
        callBack.Bypass__c = true;
        insert callBack;
        
        callbackCase.Initial_Callback__c = callBack.id;        
        update callbackCase;
        
        return null;
    }

    public PageReference autoSaveCallbackFromCase() {
        Callback__c callBack = new Callback__c();        
        Case callbackCase = [SELECT id, Initial_Callback__c, Call_Type__c FROM Case Where id = :standardController];
        caseCallType = callbackCase.Call_Type__c;

        Quitline_Sessions__c[] currentSession = [SELECT id, Date__c 
                                                 FROM Quitline_Sessions__c 
                                                 WHERE Date__c = today
                                                 AND RecordType.DeveloperName = :sessionRecordType
                                                 AND Call_Type__c =: caseCallType];
        
        if(currentSession.Size() > 0 && callbackCase.Initial_Callback__c == null){
            callBack.Quitline_Sessions__c = currentSession[0].id;
            callBack.Timeslot__c = getTimePicklist();
            callBack.recordTypeId = (sessionRecordType == 'Quitline') ?  mapCallBackRecordTypeId.get('Initial') : mapCallBackRecordTypeId.get('X131120');
            callBack.Case__c = standardController;
            callBack.Call_Type__c = (sessionRecordType == 'Quitline') ?  'Initial' : caseCallType;
            callBack.Call_Direction__c = 'Incoming';
            callback.Last_Call_Result__c = 'Successful';
            callback.Bypass__c = true;
            insert callBack;
            
            callbackCase.Initial_Callback__c = callBack.id;
            update callbackCase;
            
            PageReference callBackPage = new ApexPages.StandardController(callBack).view();
            
            return callBackPage;
        }
        return null;
    }
    
    public PageReference autoSaveCallbackFromCaseOngoing() {        
        Callback__c callBack = new Callback__c();
        
        Case[] callbackCase = [SELECT id, Initial_Callback__c, Call_Type__c FROM Case Where id = :standardController];
        caseCallType = callbackCase[0].Call_Type__c;
        
        Quitline_Sessions__c[] currentSession = [SELECT id, Date__c 
                                                 FROM Quitline_Sessions__c 
                                                 WHERE Date__c = today
                                                 AND RecordType.DeveloperName = :sessionRecordType
                                                 AND Call_Type__c =: caseCallType];        
        
        if(currentSession.size() > 0){
            Callback__c[] nextCallback = [SELECT id, Next_Callback__c FROM Callback__c WHERE Case__c = :standardController AND Next_Callback__c = '' ORDER BY id DESC];
            if(nextCallback.size() > 0){
                Callback__c[] callbacks = [SELECT id, Next_Callback__c, Status__c FROM Callback__c WHERE Case__c = :standardController];
                for(Callback__c tempNextCallback : callbacks){
                    if(tempNextCallback.Status__c == 'Pending') {
                    	tempNextCallback.Status__c = 'Cancelled - System';
                    }
                }
                update callbacks;

                callBack.Quitline_Sessions__c = currentSession[0].id;
                callBack.Timeslot__c = getTimePicklist();
                callBack.recordTypeId = (sessionRecordType == 'Quitline') ?  mapCallBackRecordTypeId.get('Ongoing') : mapCallBackRecordTypeId.get('X131120');
                callBack.Case__c = standardController;
                callBack.Call_Type__c = (sessionRecordType == 'Quitline') ?  'Ongoing' : caseCallType;
                callBack.Call_Direction__c = 'Incoming';
                callback.Last_Call_Result__c = 'Successful';
                callback.Previous_Callback__c = nextCallback[0].id;
				callback.Bypass__c = true;
                insert callBack;
                
                nextCallback[0].Next_Callback__c = callBack.id;
                update nextCallback[0];
                
                callbackCase[0].Ongoing_Callback__c = callBack.id;
                update callbackCase[0];
            
                PageReference callBackPage = new ApexPages.StandardController(callBack).view();

                return callBackPage;
            }else{
                return null;                
            }   
        }
        else{
            return null;            
        }
    }
    
    public PageReference saveCallbackFromCallback() {
        Callback__c callBack = new Callback__c();
        callBack.Quitline_Sessions__c = sessionIdSelected;
        callBack.Timeslot__c = timeSelected;
        callBack.Anytime__c = anytimeCheckbox;
        callBack.Time_Request__c = (timeRequestedTextBox.length()>100)?timeRequestedTextBox.substring(0,100):timeRequestedTextBox;
        Callback__c callbackCase = [SELECT id, Case__c, Next_Callback__c, Status__c, Call_Type__c, RecordType.DeveloperName FROM Callback__c Where id = :standardController];
        callBack.Case__c = callbackCase.Case__c;
        callBack.Previous_Callback__c = callBackCase.id;
        callBack.Bypass__c = true;
        
        
        if (callbackCase.RecordType.DeveloperName == 'X131120') {
        	callBack.recordTypeId = mapCallBackRecordTypeId.get('X131120');
        	callback.Call_Type__c = caseCallType;
        }
        else {
	        if(callbackCase.Call_Type__c == 'Initial' && callbackCase.Status__c != 'Completed'){
	            callBack.recordTypeId = mapCallBackRecordTypeId.get('Initial');
	            callback.Call_Type__c = 'Initial';
	        }
	        else { 
	            callback.Call_Type__c = 'Ongoing';
	            callBack.recordTypeId = mapCallBackRecordTypeId.get('Ongoing');
	        }	
        } 
        
        insert callBack;
        callbackCase.Next_Callback__c = callBack.id;        
        
        update callbackCase;
        
        return null;
    }

	// Delete
    public void deleteRecord(){
        Callback__c controller = [SELECT Next_Callback__c FROM Callback__c Where id = :standardController];
        Callback__c nextCallback = [SELECT id FROM Callback__c WHERE id = :controller.Next_Callback__c];
        controller.Next_Callback__c = null;
        delete nextCallback;
    }
    
    public void deleteRecordFromCase(){
        Case controller = [SELECT Initial_Callback__c FROM Case Where id = :standardController];
        Callback__c nextCallback = [SELECT id FROM Callback__c WHERE id = :controller.Initial_Callback__c];
        controller.Initial_Callback__c = null;
        delete nextCallback;
    }
    
	// Page Actions
    public PageReference dayTimeSelected() {
        renderTime = 'true';
        return null;
    }
    
    public PageReference timeSelected(){
        renderSave = 'true';
        return null;
    }

    public PageReference dateSelected() {
        renderTimeOfDay = 'true';
        return null;
    }
}