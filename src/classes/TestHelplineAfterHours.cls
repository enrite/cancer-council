/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestHelplineAfterHours {

    static testMethod void myTestHelplineAfterHours() {
        // TO DO: implement unit test
        
        Case[] caseList = new Case[]{};
        Id recordTypeId = [select Id from RecordType where SobjectType = 'Case' and name = 'Helpline Case'].Id;
        
        Id quitlineRecordTypeId = [select Id from RecordType where SobjectType = 'Case' and name = 'Quitline'].Id;
        
        
        Case c = new Case(RecordTypeId=recordTypeId, Status='Open', Origin='Phone', Call_Minutes__c=12, Age_Range__c='50-59', Gender__c='Female'
                , Postcode__c='5159', Caller_Type__c='Test', How_Found__c='Test', Why_Called__c='Test', Cancer_Type__c='Test', Used_Before__c='Test'
                , Topics__c='Test', Actions__c='Test', Time_Opened__c=datetime.newInstance(2012, 5, 1, 12, 30, 2));
        caseList.add(c);

        Case c1 = new Case(RecordTypeId=recordTypeId, Status='Open', Origin='Phone', Call_Minutes__c=12, Age_Range__c='50-59', Gender__c='Female'
                , Postcode__c='5159', Caller_Type__c='Test', How_Found__c='Test', Why_Called__c='Test', Cancer_Type__c='Test', Used_Before__c='Test'
                , Topics__c='Test', Actions__c='Test', Time_Opened__c=datetime.newInstance(2012, 5, 1, 17, 30, 2));
        caseList.add(c1);

        Case c2 = new Case(RecordTypeId=recordTypeId, Status='Open', Origin='Phone', Call_Minutes__c=12, Age_Range__c='50-59', Gender__c='Female'
                , Postcode__c='5159', Caller_Type__c='Test', How_Found__c='Test', Why_Called__c='Test', Cancer_Type__c='Test', Used_Before__c='Test'
                , Topics__c='Test', Actions__c='Test', Time_Opened__c=datetime.newInstance(2012, 5, 1, 10, 30, 2));
        caseList.add(c2);

        Case c3 = new Case(RecordTypeId=quitlineRecordTypeId, Status='Open');
        caseList.add(c3);
                        
        test.startTest();
        insert caseList;
        
        Case ct1 = [select After_Hours__c from Case where Id = :caseList[0].Id];
        system.assertEquals(ct1.After_Hours__c, 'No');
        
        Case ct2 = [select After_Hours__c from Case where Id = :caseList[1].Id];
        system.assertEquals(ct2.After_Hours__c, 'Yes');
        
        Case ct3 = [select After_Hours__c from Case where Id = :caseList[2].Id];
        system.assertEquals(ct3.After_Hours__c, 'No');       
                
        Case ct4 = [select After_Hours__c from Case where Id = :caseList[3].Id];
        system.assertEquals(ct4.After_Hours__c, null); 
                        
        test.stopTest();
        
    }
}