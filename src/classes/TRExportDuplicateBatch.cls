public with sharing class TRExportDuplicateBatch 
	extends TRExportBase
    implements Database.Batchable<sObject>, Database.Stateful
{
	public TRExportDuplicateBatch(TR_Export__c exportRecord)
	{
		super(exportRecord);
	}
	
	public TRExportDuplicateBatch(TRExportBase prevBatch)
	{
		super(prevBatch);
	}
	
	private final String ATTACHMENTNAME = getFileName('Duplicate');
	 
    public Database.QueryLocator start(Database.BatchableContext info)
    {               
        Attachment att = createAttachment(ATTACHMENTNAME, getHeader());
                
        return Database.getQueryLocator([SELECT ID,
        										TeamRaiser_CONS_ID__c,
        										Merge_Contact__r.TeamRaiser_CONS_ID__c
                                        FROM    Contact
                                        WHERE   Merge_Contact__c != null AND
                                        		LastModifiedDate > :getLastExportTime()]);
    }
    
    public void execute(Database.BatchableContext info, List<Contact> scope)
    {       
        Attachment att = getAttachment(ATTACHMENTNAME);        
        String b = att.Body.toString();
                                    
        for(Contact c : scope)
        {
            b += processTransaction(c);                
        }
        att.Body = Blob.valueOf(b);
        update att;
    } 
    
    public void finish(Database.BatchableContext info)
    {
    }
    
    private String getHeader()
    {
        String line = '';
        line += getField('MasterCONS_ID') +
                    getField('MergeCONS_ID');   
        return endLine(line);
    }
    
    private String processTransaction(Contact c)
    {
        String line = '';
		line += getField(c.Merge_Contact__r.TeamRaiser_CONS_ID__c, 11);
		line += getField(c.TeamRaiser_CONS_ID__c, 11);
		return endLine(line);
    }
}