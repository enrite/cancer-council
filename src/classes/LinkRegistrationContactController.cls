public with sharing class LinkRegistrationContactController{
    public static final Integer MIN_SCORE_SHOW_POPUP = 40;
    private static final SelectOption[] cMaxResultsPicklist = new SelectOption[]{
        new SelectOption('10','10'),
        new SelectOption('20','20'),
        new SelectOption('50','50')};
    
    private Registration__c[] mRegistration;
    private Contact[] mContactResults;
    private LinkRegistrationContactControllerContact mRootContactNode;
    private integer mTotalContacts;
    private LinkRegistrationContactControllerContact[] mSortedContacts;
    private LinkRegistrationContactControllerContact[] mSortedLimitedContacts;
    
    public String maxResultsSelected{get; set;}
    public boolean displayPopup {get; set;}
    public boolean webRegistration {get; set;}
    public boolean notWebRegistration {get; set;}
    
    public LinkRegistrationContactController(){
        maxResultsSelected = '10';
        
        String vRegistrationID = ApexPages.CurrentPage().getParameters().get('id');
        mRegistration = [SELECT
            id,
            Host_Team_Captain__c,
            Web_Street__c,
            Web_Suburb__c,
            Web_Postcode__c,
            Web_State__c,
            Web_First_Name__c,
            Web_Last_Name__c,
            Web_Email__c,
            Web_Phone__c,
            Web_Mobile__c,
            Web_Date_of_Birth__c,
			Web_Account__c,
            Web_Registration__c 
            FROM Registration__c
            WHERE id = :vRegistrationID];
        
        if(mRegistration.size() != 1){
            mTotalContacts = 0;
            return;
        }
        
        webRegistration = mRegistration[0].Web_Registration__c;
        notWebRegistration = !mRegistration[0].Web_Registration__c;
        
        String vFirstLetter = mRegistration[0].Web_First_Name__c != null && mRegistration[0].Web_First_Name__c.length() > 0 ? mRegistration[0].Web_First_Name__c.substring(1) + '%' : '%';
        
        mContactResults = [SELECT
            FirstName,
            LastName,
            AccountId,
            MailingStreet,
            MailingCity,
            MailingPostalCode,
            MobilePhone,
            HomePhone,
            Email,
            Account.Name, 
            RecordType.Name
            FROM Contact
            WHERE
            (Email = :mRegistration[0].Web_Email__c AND Email != null) OR
            (MobilePhone = :mRegistration[0].Web_Mobile__c AND MobilePhone != null) OR
            (HomePhone = :mRegistration[0].Web_Phone__c AND HomePhone != null) OR
            ((LastName =  :mRegistration[0].Web_Last_Name__c AND LastName != null) AND
            (FirstName LIKE :vFirstLetter AND FirstName != null))];
        
        mTotalContacts = mContactResults.size();
        mSortedContacts = new LinkRegistrationContactControllerContact[]{};
        if(mTotalContacts > 0){
            mRootContactNode = new LinkRegistrationContactControllerContact(mContactResults[0], mRegistration[0]);
            for(integer i = 1; i < mTotalContacts; i++) mRootContactNode.add(new LinkRegistrationContactControllerContact(mContactResults[i], mRegistration[0]));
            mRootContactNode.buildArrayDesc(mSortedContacts);
        }
        updateMaxResults();
    }
    
    public String getTotalContacts(){
        return String.valueOf(mTotalContacts);
    }
    
    public LinkRegistrationContactControllerContact[] getSortedContacts(){
        if(mSortedLimitedContacts == null) mSortedLimitedContacts = new LinkRegistrationContactControllerContact[]{};
        return mSortedLimitedContacts;
    }
    
    public Registration__c getRegistration(){
        return mRegistration.size() > 0 ? mRegistration[0] : null;
    }
    
    public void setMaxResults(integer pMax){
        if(mTotalContacts > 0){
            integer vMax = mSortedContacts.size() > pMax ? pMax : mSortedContacts.size();
            mSortedLimitedContacts = new LinkRegistrationContactControllerContact[vMax];
            for(integer i = 0; i < vMax; i++) mSortedLimitedContacts[i] = mSortedContacts[i];
        }
    }
    
    public SelectOption[] getResultsOptions(){
        return cMaxResultsPicklist;
    }
    
    public void updateMaxResults(){
        setMaxResults(Integer.valueOf(maxResultsSelected));
    }
    
    public String getTotalContactsDisplayed(){
        integer vMax = Integer.valueOf(maxResultsSelected);
        return String.valueOf(mSortedContacts.size() > vMax ? vMax : mSortedContacts.size());
    }
    
    public void closePopup(){        
        displayPopup = false;    
    }
         
    public PageReference newContact(){
        if(mTotalContacts > 0){
            if(Integer.valueOf(mSortedContacts[0].getScore()) > MIN_SCORE_SHOW_POPUP){
                displayPopup = true; 
                return null;
            }
        }
        return newContactConfirm();
    }
    
    public PageReference newContactConfirm(){    
        if(mRegistration[0].Web_Account__c == null){
        	return new PageReference('/apex/LinkRegistrationContactNewPage?regid=' + mRegistration[0].id);
        }else{
        	return new PageReference('/apex/LinkRegistrationContactAccountPage?id=' + mRegistration[0].id);
        }
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mRegistration[0].id);
    }
    
}