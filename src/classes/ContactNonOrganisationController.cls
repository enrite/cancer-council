public with sharing class ContactNonOrganisationController {

    // these variables maintain the state of the wizard.
    // when users enter data into the wizard, their input is stored
    // in these variables.
    public Account account;
    public Contact contact;
    public Opportunity opportunity;
    public OpportunityContactRole role;
    public Registration__c registration;
    public boolean regRequired = true;
    public boolean oppRequired = true;

    //properties used to search Contact record
    public string contactIdStr {get;set;}
    public string finalContactIdStr {get;set;}
    boolean isNew = true;

    // send email alert when exception thrown
    EmailUtil email;
    list<string> recipients = new list<string>{'ithelpdesk@cancersa.org.au'};
    string replyTo = 'ithelpdesk@cancersa.org.au';

    // to store objects' default record types into map list, easy for future reference
    map<string, Id> recordTypeMap = new map<string, Id>
            {'Non Organisation'=>[select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1].Id
            , 'Contact'=>[select id from recordType where sobjectType='Contact' and name='Contact' limit 1].Id
            , 'Opportunity'=>[select id from recordType where sobjectType='Opportunity' and name='Donation' limit 1].Id};

    // map list used to store suburb list information
    map<string, list<Suburb__c>> suburbMap = new map<string, list<Suburb__c>>();
    
    // The following methods return one of each of the member
    // variables. If this is the first time the method is called,
    // it creates an empty record for the variable.    
    public Account getAccount() {
        if (account == null) {
                account = new Account();               
        }
        return account;
    }
    
    public Contact getContact() {       
        if (contact == null) {
                contact = new Contact();                
                //account = new Account();
                getAccount();
        }
        return contact;
    }
    
    public Opportunity getOpportunity() {       
        if (opportunity == null) {
                opportunity = new Opportunity();
                // default value for 'income type' field
                opportunity.Income_Type__c = 'Donation';
        }
        return opportunity;
    }
    
    public OpportunityContactRole getRole() {
        if (role == null) {
                role = new OpportunityContactRole();                    
        }
        return role;
    }

    public Registration__c getRegistration() {
        if (registration == null) {
                registration = new Registration__c();
        }
        return registration;            
    }
    // to set dynamic value for mandatory field based value selected
    public boolean getMandatory() {
        if (objectTypeSelected == 'Reg' || objectTypeSelected == 'RegOpp') {
            return true;
        }
        else if (objectTypeSelected == 'Opp' || objectTypeSelected == 'RegOpp') {
            return true;
        }
        else {
            return false;
        }
    }
    
    //input type variables
    public string objectTypeSelected {get;set;}    
    public string mailingCity {get;set;}
    public string mailingPostcode {get;set;}
    public string mailingState {get;set;}
    public string mailingRegion {get;set;}
    public string postalCity {get;set;}
    public string postalPostcode {get;set;}
    //public string postalRegion {get;set;}
    public string debug {get;set;}
     
    //additional object list
    public list<selectOption> getObjType() {
        list<selectOption> options = new list<selectOption>();
        options.add(new selectOption(' ', '--None--'));
        options.add(new selectOption('Opp', 'Opportunity Only'));
        options.add(new selectOption('Reg', 'Registration Only'));
        options.add(new selectOption('RegOpp', 'Registration & Opportunity'));
        return options;
    }
    
    public void getMailMatching() {     
        for (Suburb__c s : [select Id, Name, Suburb__c, Postcode__c, State__c, Region__c from Suburb__c where Name =: mailingCity]) {
            contact.MailingCity=s.Suburb__c;
            contact.MailingPostalCode=s.Postcode__c;
            contact.MailingState=s.State__c;
            contact.Region__c=s.Region__c;  
        }
    }
    
    public void getPostalMatching() {       
        for (Suburb__c s : [select Id, Name, Suburb__c, Postcode__c, State__c, Region__c from Suburb__c where Name =: postalCity]) {
            contact.OtherCity=s.Suburb__c;
            contact.OtherPostalCode=s.Postcode__c;
            contact.OtherState=s.State__c;
        }
    }
    
    // This method cancels the wizard, and returns the user to the
    // Opportunities tab
    public PageReference cancel() {
        PageReference pageRef = new PageReference('/home/home.jsp');
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
    public void searchContact() {
        isNew = false;
        if (contactIdStr.contains('C') || contactIdStr.contains('c')) {
            if (contactIdStr.contains('-')) {
                //this is salesforce format C-XXX, could be used directly on SOQL
                finalContactIdStr = contactIdStr;
            }
        contact = [select Id, AccountId, Salutation, FirstName, LastName, MailingStreet, MailingCity, MailingPostalCode, MailingState, Region__c, OtherStreet, OtherCity, OtherPostalCode, OtherState, Phone, MobilePhone, Email, Letter_Salutation__c, Mailing_Status__c, Envelope_Salutation__c, OtherPhone, Status__c from Contact where Contact_ID__c = : finalContactIdStr];
        mailingCity = contact.MailingCity;
        mailingPostcode = contact.MailingPostalCode;
        mailingRegion = contact.Region__c;        
        postalCity = contact.OtherCity;
        postalPostcode = contact.OtherPostalCode;        
        }
    }
    
    // This method performs the final save for all objects, and direct user to the detail page
    public PageReference save() {
        // create Account record if this input is for new record e.g. did not search for existing contact id
        if (isNew) {
            saveAccount();          
        }
        saveContact();        
        
        if (objectTypeSelected == 'Reg' || objectTypeSelected == 'RegOpp') {
                saveRegistration();
        }
                
        if (objectTypeSelected == 'Opp' || objectTypeSelected == 'RegOpp') {
                saveOpportunity();
                saveOpportunityContactRole();               
        }
        
        // redirec user to newly created registration/opportunity page
        PageReference opptyPage;
        if (objectTypeSelected == 'Reg' || objectTypeSelected == 'RegOpp') {
                opptyPage = new ApexPages.Standardcontroller(registration).view();
        }
        else if (objectTypeSelected == 'Opp') {
            opptyPage = new ApexPages.StandardController(opportunity).view();
        }
        else {
            opptyPage = new ApexPages.StandardController(contact).view(); 
        }
        opptyPage.setRedirect(true);
        return opptyPage;
    }
    
    public void saveAccount() {
        // Create the account. Before inserting, copy the contact's
        // information into the account.
        try {               
                if (contact.FirstName == null) {
                        account.Name = contact.LastName;
                }
                else {
                        account.Name = contact.FirstName + ' ' + contact.LastName;
                }
                
                account.Phone = contact.Phone;
                account.Fax = contact.Fax;
                account.BillingStreet = contact.MailingStreet;
                account.BillingCity = contact.MailingCity;
                account.BillingPostalCode = contact.MailingPostalCode;
                account.BillingState = contact.MailingState;
                account.ShippingStreet = contact.OtherStreet;
                account.ShippingCity = contact.OtherCity;
                account.ShippingPostalCode = contact.OtherPostalCode;
                account.ShippingState = contact.OtherState;
                account.Region__c = contact.Region__c;
                account.RecordTypeId = recordTypeMap.get('Non Organisation');
                account.Status__c = 'Active';
                
                Database.Saveresult result = database.insert(account, false);
                
                if (!result.isSuccess()) {
                    email = new EmailUtil(recipients, replyTo, 'DML Errors thrown - Insert Account Errors', string.valueof(result.getErrors()[0]), 'Plain');
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, string.valueof(result.getErrors()[0])));                 
                }
        }
        catch (exception e) {
            email = new EmailUtil(recipients, replyTo, 'Errors thrown - Insert Account Errors', e.getMessage(), 'Plain');
            system.debug('***** Errors thrown - Insert Account Errors ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, e.getMessage()));
        }
    }
    
    public void saveContact() {         
        // Create the contact. Before inserting, use the id field
        // generated once the account is inserted to create
        // the relationship between the contact and the account.
        try {               
                Database.Saveresult result;                
                if (isNew && account.Id != null) {
                        contact.AccountId = account.Id;
                        contact.RecordTypeId = recordTypeMap.get('Contact');
                        contact.Status__c = 'Active';                                              
                        result = database.insert(contact, false);
                        
                        if (!result.isSuccess()) {
                            email = new EmailUtil(recipients, replyTo, 'DML Errors thrown - Insert Contact Errors', string.valueof(result.getErrors()[0]), 'Plain');
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, string.valueof(result.getErrors()[0])));
                        }                        
                }
                
                if (!isNew) {
                        result = database.update(contact, false);                           
                }
        }
        catch (exception e) {
            email = new EmailUtil(recipients, replyTo, 'Errors thrown - Insert Contact Errors', e.getMessage(), 'Plain');
            system.debug('***** Errors thrown - Insert Contact Errors ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, e.getMessage()));            
        }
    }
    
    public void saveOpportunity() {
        // Create the opportunity. Before inserting, create
        // another relationship with the account.
        try {
            if (contact.Id != null) {
                    if (objectTypeSelected == 'Opp' || objectTypeSelected == 'RegOpp') {                                                                    
                            opportunity.AccountId = contact.AccountId;
                            opportunity.Primary_Contact_at_Organisation__c = contact.Id;
                            opportunity.RecordTypeId = recordTypeMap.get('Opportunity');
                            //opportunity.Income_Type__c = 'Donation';
                            //opportunity.closedate = System.Today();
                            opportunity.stagename = 'Posted';
                            if( contact.firstname == null) {                                        
                                    opportunity.name = contact.lastname+'- Donation '+opportunity.closedate.format();
                            }
                            else {
                                    opportunity.name = contact.firstname+ ' ' + contact.lastname+'- Donation '+opportunity.closedate.format();
                            }

                                if (Opportunity.donation_salutation__c == null) {                                   
                                    if(contact.Letter_Salutation__c == null) {
                                        if (contact.Salutation == null) {
                                            if (contact.FirstName == null) {
                                                opportunity.Donation_Salutation__c = 'Friend';
                                            }
                                            else {
                                                opportunity.Donation_Salutation__c = contact.FirstName + ' ' + contact.LastName;
                                            }
                                        }
                                        else if (contact.FirstName != null) {
                                            opportunity.Donation_Salutation__c = contact.Salutation + ' ' + contact.FirstName + ' ' + contact.LastName;
                                        }
                                        
                                        /*if(contact.firstname == null) {
                                                if(contact.salutation == null) {
                                                        opportunity.donation_salutation__c = 'Friend';
                                                } 
                                                else {
                                                    Opportunity.donation_salutation__c = contact.salutation +' '+contact.lastname;
                                                }
                                        }
                                        else {
                                            Opportunity.donation_salutation__c = contact.salutation +' '+contact.firstname + ' ' +contact.lastname; 
                                        }*/
                                    }
                                else {
                                    opportunity.donation_salutation__c = contact.Letter_Salutation__c;
                                }
                                }

                                if (objectTypeSelected == 'RegOpp') {
                                        opportunity.Registration__c = registration.Id;  
                                }
                        }
                Database.Saveresult result = database.insert(opportunity, false);
                if (!result.isSuccess()) {
                    email = new EmailUtil(recipients, replyTo, 'DML Errors thrown - Insert Opportunity Errors', string.valueof(result.getErrors()[0]), 'Plain');
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, string.valueof(result.getErrors()[0])));
                }                        
            }
        }
        catch (exception e) {
            email = new EmailUtil(recipients, replyTo, 'Errors thrown - Insert Opportunity Errors', e.getMessage(), 'Plain');
            system.debug('***** Errors thrown - Insert Opportunity Errors ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, e.getMessage()));               
        }
    }
    
    public void saveOpportunityContactRole() {
        // Create the junction contact role between the opportunity
        // and the contact.     
        try {
                if (opportunity.id!=null) {
                        //email = new EmailUtil(recipients, replyTo, 'Errors thrown - Opportunity Id', opportunity.Id);
                        //email = new EmailUtil(recipients, replyTo, 'Errors thrown - Contact Id', contact.Id);
                        role = new OpportunityContactRole();
                        role.opportunityId = opportunity.id;
                        role.contactId = contact.id;
                        role.IsPrimary = true;
                        Database.Saveresult result = database.insert(role, false);
                        if (!result.isSuccess()) {
                            email = new EmailUtil(recipients, replyTo, 'DML Errors thrown - Insert Opportunity Contact Role Errors', string.valueof(result.getErrors()[0]), 'Plain');
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, string.valueof(result.getErrors()[0])));
                        }                        
                }
        }
        catch (exception e) {
            email = new EmailUtil(recipients, replyTo, 'Errors thrown - Insert Opportunity Contact Role Errors', e.getMessage(), 'Plain');
            system.debug('***** contact role Errors thrown - Insert Opportunity Contact Role Errors ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, e.getMessage()));
        }
    }
    
    public void saveRegistration() {
        
        try {
                if (objectTypeSelected == 'Reg' || objectTypeSelected == 'RegOpp') {
                        registration.Campaign__c = opportunity.CampaignId;
                        if (registration.Host_Team_Captain_Org__c != null) {
                                registration.Org_Primary_Contact__c = contact.Id;
                        }
                        else {
                                registration.Host_Team_Captain__c = contact.Id;
                        }
                        Database.Saveresult result = database.insert(registration, false);
                        if (!result.isSuccess()) {
                            email = new EmailUtil(recipients, replyTo, 'DML Errors thrown - Insert Registration Errors', string.valueof(result.getErrors()[0]), 'Plain');
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, string.valueof(result.getErrors()[0])));                            
                        }
                }
        }
        catch (exception e) {
            email = new EmailUtil(recipients, replyTo, 'Errors thrown - Insert Registration Errors', e.getMessage(), 'Plain');
            system.debug('***** Errors thrown - Insert Registration Errors ' + e.getMessage());            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, e.getMessage()));
        }
    }
}