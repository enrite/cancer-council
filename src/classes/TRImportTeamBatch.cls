public class TRImportTeamBatch 
    extends TRImportBase
    implements Database.Batchable<sObject>, Database.Stateful
{
    public Boolean RunThisOnly
    {
        get
        {
            if(RunThisOnly == null)
            {
                return false;
            }
            return RunThisOnly;
        }
        set;
    }
    
    public Database.QueryLocator start(Database.BatchableContext info)
    {
        /*if(!RunThisOnly)
        {
            delete [SELECT  ID
                    FROM    TR_Import_Team__c
                    WHERE   Processed__c != null];
        }*/
                
        if(processedSuccesfully == null)
        {
            processedSuccesfully = 0;
        }           
        return Database.getQueryLocator([SELECT ID,
                                                Team_Name__c, 
                                                Team_ID__c, 
                                                Team_Description__c, 
                                                TR_Last_Modified_Date__c,
                                                Goal__c, 
                                                Event_Name__c, 
                                                Event_ID__c, 
                                                Division_Name__c, 
                                                Captain_ID_c__c,
                                                Error__c
                                        FROM    TR_Import_Team__c
                                        WHERE   Processed__c = null
                                        ORDER BY ID]); 
    }
    
    public void execute(Database.BatchableContext info, List<TR_Import_Team__c> scope)
    {       
        for(TR_Import_Team__c stagingRecord : scope)
        {
            processedSuccesfully++;
            try
            {
                processTeam(stagingRecord);             
            }
            catch(Exception ex)
            {
                processedSuccesfully--;
                stagingRecord.Error__c = ex.getMessage() +  '\n' + ex.getStackTraceString();
            }
        }
        update scope;
    }
    
    public void finish(Database.BatchableContext info)
    {
        if(RunThisOnly || Test.isRunningTest())
        {
            return;
        }
        TRImportRegistrationBatch regBatch = new TRImportRegistrationBatch();
        Database.executeBatch(regBatch, BatchSize);
    }
    
    private void processTeam(TR_Import_Team__c stagingRecord)
    {
        stagingRecord.Error__c = '';
        stagingRecord.Processing_Result__c = null;
        stagingRecord.Registration__c = null;
        Integer captainID = parseInteger(stagingRecord.Captain_ID_c__c, stagingRecord);
        Datetime trTimestamp = parseDateTime(stagingRecord.TR_Last_Modified_Date__c, stagingRecord); 
        Campaign cmpgn = null;
        for(Campaign c : [SELECT    ID,
                                    Name,
                                    TR_Event_ID__c,
                                    LastModifiedDate
                            FROM    Campaign
                            WHERE   TR_Event_ID__c = :stagingRecord.Event_ID__c])
        {
            cmpgn = c;
        }
        if(cmpgn == null)
        {
            stagingRecord.Processing_Result__c = RESULT_CAMPAIGNNOTFOUND; 
            stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
            return;
        }
        Registration__c registration = null;
        List<Registration__c> registrations = [SELECT   ID,
                                                        Last_Updated_by_Team_Raiser__c,
                                                        Team_Name__c,
                                                        TR_Team_ID__c                   
                                                FROM    Registration__c
                                                WHERE   Status__c != 'CANX' AND
                                                        Campaign__r.TR_Event_ID__c = :stagingRecord.Event_ID__c AND 
                                                        TR_Team_ID__c != null AND
                                                        (
                                                            TR_Team_ID__c = :stagingRecord.Team_ID__c 
                                                            OR
                                                            Team_Name__c = :stagingRecord.Team_Name__c
                                                        )];
        // first match on team ID
        for(Registration__c reg : registrations)
        {
            if(stagingRecord.Team_ID__c != null && reg.TR_Team_ID__c == stagingRecord.Team_ID__c)
            {
                registration = reg;
                /*if(reg.Last_Updated_by_Team_Raiser__c < trTimestamp || Test.isRunningTest())
                {
                    stagingRecord.OwnerId = TeamRaiserConflictQueue.ID;
                    stagingRecord.Processing_Result__c = RESULT_TIMESTAMPCONFLICT;                  
                    stagingRecord.Error__c += getTimeStampDifference(reg.Last_Updated_by_Team_Raiser__c, trTimestamp);
                }
                else
                {
                    stagingRecord.Processing_Result__c = RESULT_TEAMIDMATCH;
                }*/
                stagingRecord.Processing_Result__c = RESULT_TEAMIDMATCH;
                break;  
            }
        }
        // now try on team name
        if(registration == null)
        {
            for(Registration__c reg : registrations)
            {
                if(stagingRecord.Team_Name__c == reg.Team_Name__c)
                {
                    registration = reg;
                    /*if(reg.Last_Updated_by_Team_Raiser__c < trTimestamp || Test.isRunningTest())
                    {
                        stagingRecord.OwnerId = TeamRaiserConflictQueue.ID;
                        stagingRecord.Processing_Result__c = RESULT_TIMESTAMPCONFLICT;                  
                        stagingRecord.Error__c += getTimeStampDifference(reg.Last_Updated_by_Team_Raiser__c, trTimestamp);
                    }
                    else
                    {
                        stagingRecord.Processing_Result__c = RESULT_TEAMNAMEMATCH;
                    }*/
                    stagingRecord.Processing_Result__c = RESULT_TEAMNAMEMATCH;
                    break;  
                }
            }
        }
        if(registration != null)
        {
            if(registration.ID == null && registration.Team_Name__c != stagingRecord.Team_Name__c)
            {
                stagingRecord.OwnerId = TeamRaiserConflictQueue.ID;
                stagingRecord.Processing_Result__c = RESULT_MANUALUPDATEREQUIRED;
                stagingRecord.Error__c += 'Team name changed';
                stagingRecord.Processed__c = DateTime.now();
                return;
            }
        }
        if(registration == null && stagingRecord.Team_Name__c != null)
        {
            stagingRecord.Processing_Result__c = RESULT_NEWTEAM;
            registration = new Registration__c(Campaign__c = cmpgn.ID);
            registration.RecordTypeId = RegistrationRecordType.ID;
            
            for(Account acc : [SELECT   ID
                                FROM    Account
                                WHERE   Name = :stagingRecord.Division_Name__c
                                LIMIT 1])
            {
                registration.Host_Team_Captain_Org__c = acc.ID;
            }         
        }
        if(registration == null)
        {
            stagingRecord.OwnerId = TeamRaiserConflictQueue.ID;
            stagingRecord.Processing_Result__c = RESULT_REGISTRATIONNOTFOUND;                   
            return;
        }
        registration.Status__c = 'Registered';
        registration.Registration_Method__c = 'TeamRaiser';
        registration.Registration_Fee_Status__c = 'Paid';
        registration.TR_Team_ID__c = stagingRecord.Team_ID__c;
        registration.Team_Name__c = stagingRecord.Team_Name__c;
        registration.RFL_Team_FR_Goal__c = parseDecimal(stagingRecord.Goal__c, stagingRecord);
        registration.Team_Description__c = stagingRecord.Team_Description__c;
        registration.Web_Registration__c = true;
        registration.Last_Updated_by_Team_Raiser__c = Datetime.now();
        upsert registration;

        stagingRecord.Registration__c = registration.ID;
        setProcessedAndOwnerFields(stagingRecord);
    }
}