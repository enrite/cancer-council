public with sharing class OpportunityReallocated {
// Create Major Gifts Opportunity after Opportunity field 'Reallocated' is checked

    private static string emailAddress {
        get {
            return 'ithelpdesk@cancersa.org.au';
        }    
    }
    
    private static Id campaignMajorGiftsId {
        get {
            return [SELECT Id FROM Campaign WHERE Campaign_Code__c = 'Major Gifts'].Id;
        }
    }

    public static void createMajorGiftsOpportunity(map<Id, Opportunity> mapNewOpportunity, map<Id, Opportunity> mapOldOpportunity) {        
        try {
            list<Opportunity> listOpportunity = new list<Opportunity>();
            list<OpportunityContactRole> listOpportunityContactRole = new list<OpportunityContactRole>();
            list<Note> listNote = new list<Note>();
            
            set<Id> setOpportunityReallocatedId = new set<Id>();
            
            for (Opportunity oc : [SELECT Id, Opportunity_Reallocated__c FROM Opportunity WHERE Opportunity_Reallocated__c IN :mapNewOpportunity.keySet()]) {
                setOpportunityReallocatedId.add(oc.Opportunity_Reallocated__c);
            }
                        
            // Continue to next iteration if Reallocated is not ticked
            for (Opportunity opp : mapNewOpportunity.values()) {
                Opportunity oldOpp = mapOldOpportunity.get(opp.Id);
                
                if (opp.Reallocated__c == false && oldOpp.Reallocated__c == false) {
                    continue;
                }
                
                // Throw error if trying to update Reallocated checkbox while an existing Opportunity Reallocated record exists
                if (setOpportunityReallocatedId.contains(opp.Id) && opp.Reallocated__c == false && oldOpp.Reallocated__c == true) {
                    opp.addError('This Opportunity already has Opportunity Reallocated record linked<br/>To uncheck Reallocated checkbox please delete that linked Opportunity Reallocated', false);
                }
                
                // Set new Major Gifts Opportunity field values if Reallocated checkbox is updated to Checked
                if (opp.Reallocated__c == true && oldOpp.Reallocated__c == false) {
                    Opportunity newOpp = opp.clone(false, false, false, false);
                    newOpp.CampaignId = campaignMajorGiftsId;
                    newOpp.Registration__c = null;
                    newOpp.Opportunity_Reallocated__c = opp.Id;
                    newOpp.Reallocated__c = false;
                    newOpp.No_Receipt__c = true;
                    newOpp.BPAY_Number__c = null;
                    newOpp.OwnerId = UserInfo.getUserId();
                    listOpportunity.add(newOpp);
                }
            }
            insert listOpportunity;
            
            if (listOpportunity.size()>0) {
                for (Opportunity o : listOpportunity) {
                    if (o.Primary_Contact_at_Organisation__c != null) {
                        OpportunityContactRole ocr = new OpportunityContactRole();
                        ocr.ContactId = o.Primary_Contact_at_Organisation__c;   
                        ocr.Role = 'Donor';
                        ocr.OpportunityId = o.id;
                        ocr.IsPrimary = true;
                        listOpportunityContactRole.add(ocr);
                    }
                    
                    Note n = new Note();
                    n.ParentId = o.Id;
                    n.Title = 'Record created from Reallocated Checkbox Trigger';
                    listNote.add(n);
                }
                insert listOpportunityContactRole;
                insert listNote;
            }
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** Error OpportunityReallocated: ' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, 'OpportunityReallocated: ', errorMessage, 'HTML');        
        }       
    }
}