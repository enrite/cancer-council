public class QuitLineResourceOrderItem {
    public static final Integer DEFAULT_MAX_ITEMS = 10;

    public boolean Selected {get {
        return Item.PBSI__Quantity_Needed__c != null && Item.PBSI__Quantity_Needed__c > 0;
    }}

    public Id ItemId {get;set;}

    public String ItemDescription {get;set;}

    public Decimal ItemPrice {get;set;}

    public String ItemUrl {get;set;}
    
    public String ItemThumbnailUrl {get; set;}

    public PBSI__PBSI_Sales_Order_Line__c Item {get;set;}

    public Decimal MaxItemsThatCanBePurchased {get;set;}

    public QuitLineResourceOrderItem(Id id, String description, Decimal price) {
    	this(id, description, price, null);
    }
    public QuitLineResourceOrderItem(Id id, String description, Decimal price, String url) {
    	this(id, description, price, url, '', DEFAULT_MAX_ITEMS);
    }
    public QuitLineResourceOrderItem(Id id, String description, Decimal price, String url, String thumbnailUrl, Decimal maxItems) {
    	ItemId = id;
    	ItemDescription  = description;
    	ItemPrice = price;
    	ItemUrl = url;
    	ItemThumbnailUrl = thumbnailUrl;
    	Item = new PBSI__PBSI_Sales_Order_Line__c();
    	Item.PBSI__Item__c = ItemId;
    	MaxItemsThatCanBePurchased = maxItems;
    }
}