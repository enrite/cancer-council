public class TRExportQuestionBatch 
    extends TRExportBase
    implements Database.Batchable<sObject>, Database.Stateful
{
    public TRExportQuestionBatch(TR_Export__c exportRecord)
    {
        super(exportRecord);
    }
    
    public TRExportQuestionBatch(TRExportBase prevBatch)
    {
        super(prevBatch);
    }
    
    private final String ATTACHMENTNAME = getFileName('Question');
    private final String PARENT_ID = '1570';
    private final String CANCER_ID = '1571';
    private final String SURVIVOR_ID = '1572';
    private final String TSHIRT_ID = '1573';
    private final String MOBILE_ID = '1574';
    
    public Database.QueryLocator start(Database.BatchableContext info)
    {               
        Attachment att = createAttachment(ATTACHMENTNAME, getHeader());
        
        Set<ID> registrationIdsWithContacts = new Set<ID>();
        for(Contact_Registrations__c c : [SELECT        Registration__c,
                                                        Last_Updated_by_Team_Raiser__c,
                                                        LastModifiedDate                                                
                                            FROM        Contact_Registrations__c
                                            WHERE       LastModifiedDate > :getLastExportTime() AND
                                                        Registration__r.Campaign__r.TR_Event_Id__c != null AND
                                                        (Cancer_Experience__c != null OR
                                                        Survivor_Carer_Lap__c != null OR
                                                        Shirt_Size__c != null OR
                                                        Parent_Guardian_Name__c != null)])
        {
            // if the last modified datetime is (basically) the same as the last updated by team raiser datetime then skip the record
            if (c.Last_Updated_by_Team_Raiser__c != null
                && c.LastModifiedDate < c.Last_Updated_by_Team_Raiser__c.addMinutes(1))
            {
                continue;
            }
        
            registrationIdsWithContacts.add(c.Registration__c);
        }
        return Database.getQueryLocator([SELECT ID,
                                                Team_ID__c,                                             
                                                Registration_Date__c,
                                                Cancer_Experience__c,
                                                Survivor_Carer_Lap__c,
                                                Shirt_Size__c,
                                                Parent_Guardian_Name__c,
                                                Host_Team_Captain__c,
                                                Host_Team_Captain__r.TeamRaiser_CONS_ID__c,
                                                Host_Team_Captain__r.Contact_ID__c,
                                                Campaign__r.Name,
                                                Campaign__r.TR_Event_Id__c,
                                                (SELECT     ID,
                                                            Team_ID__c,
                                                            Cancer_Experience__c,
                                                            Survivor_Carer_Lap__c,
                                                            Shirt_Size__c,
                                                            Parent_Guardian_Name__c,
                                                            Contact__r.TeamRaiser_CONS_ID__c,
                                                            Contact__r.Contact_ID__c                                                    
                                                FROM        Contact_Registrations__r
                                                WHERE       LastModifiedDate > :getLastExportTime() AND
                                                            (Cancer_Experience__c != null OR
                                                            Survivor_Carer_Lap__c != null OR
                                                            Shirt_Size__c != null OR
                                                            Parent_Guardian_Name__c != null))
                                        FROM    Registration__c
                                        WHERE   (
                                                    LastModifiedDate > :getLastExportTime() AND 
                                                    Campaign__r.TR_Event_Id__c != null AND
                                                    (
                                                        Parent_Guardian_Name__c != null OR
                                                        Cancer_Experience__c != null OR
                                                        Survivor_Carer_Lap__c != null OR
                                                        Shirt_Size__c != null
                                                    ) 
                                                )
                                                OR
                                                ID IN :registrationIdsWithContacts]);
    }
    
    public void execute(Database.BatchableContext info, List<Registration__c> scope)
    {       
        Attachment att = getAttachment(ATTACHMENTNAME);         
        String b = att.Body.toString();
                                    
        for(Registration__c r : scope)
        {
            if(r.Host_Team_Captain__c != null)
            {
                b += processCaptainQuestion(r); 
            }
            for(Contact_Registrations__c cr : r.Contact_Registrations__r)
            {
                b += processQuestion(r, cr);                
            }
        }
        att.Body = Blob.valueOf(b);
        update att;
    } 
    
    public void finish(Database.BatchableContext info)
    {
        if(Test.isRunningTest())
        {
            return;
        }
        TRExportTransactionBatch transBatch = new TRExportTransactionBatch(this);
        Database.executeBatch(transBatch, BatchSize);
    }
    
    private String getHeader()
    {
        String line = '';
        line += getField('CONS_ID') + 
                    getField('MEMBER_ID') +
                    getField('FR_ID') +
                    getField('EVENT_NAME') +
                    getField('QUESTION_ID') +
                    getField('QUESTION_RESPONSE');   
        return line  + NEW_LINE;
    }
    
    private String processCaptainQuestion(Registration__c r)
    {
        String line = '';
        if(r.Parent_Guardian_Name__c != null)
        {
            line += addCaptainFields(r);
            line += getField(PARENT_ID, 11);
            line += getField(r.Parent_Guardian_Name__c, 255);
            line += NEW_LINE;
        }
        if(r.Cancer_Experience__c != null)
        {
            line += addCaptainFields(r);
            line += getField(CANCER_ID, 11);
            line += getField(r.Cancer_Experience__c, 255);
            line += NEW_LINE;
        }
        if(r.Survivor_Carer_Lap__c != null)
        {
            line += addCaptainFields(r);
            line += getField(SURVIVOR_ID, 11);
            line += getField(convertSurvivorLap(r.Survivor_Carer_Lap__c));
            line += NEW_LINE;
        }
        if(r.Shirt_Size__c != null)
        {
            line += addCaptainFields(r);
            line += getField(TSHIRT_ID, 11);
            line += getField(r.Shirt_Size__c, 255);
            line += NEW_LINE;
        }
        return line;
    }
    
    private String processQuestion(Registration__c r, Contact_Registrations__c cr)
    {
        String line = '';                                                   
        if(cr.Parent_Guardian_Name__c != null)
        {
            line += addContactFields(r, cr);
            line += getField(PARENT_ID, 11);
            line += getField(r.Parent_Guardian_Name__c, 255);
            line += NEW_LINE;
        }
        if(cr.Cancer_Experience__c != null)
        {
            line += addContactFields(r, cr);
            line += getField(CANCER_ID, 11);
            line += getField(r.Cancer_Experience__c, 255);
            line += NEW_LINE;
        }
        if(cr.Survivor_Carer_Lap__c != null)
        {
            line += addContactFields(r, cr);
            line += getField(SURVIVOR_ID, 11);
            line += getField(convertSurvivorLap(r.Survivor_Carer_Lap__c));
            line += NEW_LINE;
        }
        if(cr.Shirt_Size__c != null)
        {
            line += addContactFields(r, cr);
            line += getField(TSHIRT_ID, 11);
            line += getField(r.Shirt_Size__c, 255);
            line += NEW_LINE;
        }                                                           
        return line;
    }
    
    private String addCaptainFields(Registration__c r)
    {
        String linePart = getField(r.Host_Team_Captain__r.TeamRaiser_CONS_ID__c, 11);
        linePart += getField(r.Host_Team_Captain__r.Contact_ID__c, 32);
        linePart += getField(r.Campaign__r.TR_Event_Id__c, 32);
        linePart += getField(r.Campaign__r.Name, 255);
        QuestionRecords++; 
        return linePart;
    }
    
    private String addContactFields(Registration__c r, Contact_Registrations__c cr)
    {
        String linePart = getField(cr.Contact__r.TeamRaiser_CONS_ID__c, 11);
        linePart += getField(cr.Contact__r.Contact_ID__c, 32);
        linePart += getField(r.Campaign__r.TR_Event_Id__c, 32);
        linePart += getField(r.Campaign__r.Name, 255);
        QuestionRecords++; 
        return linePart;
    }
    
    private String convertSurvivorLap(Boolean b)
    {
        return b ? 'YES' : 'NO';
    }
}