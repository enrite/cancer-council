public class PostChatController
{
    public PageReference redirect(){
        PageReference pageRef = new PageReference('https://www.surveymonkey.com/r/OnlinePost');
        pageRef.setRedirect(true);
        return pageRef;
    }
}