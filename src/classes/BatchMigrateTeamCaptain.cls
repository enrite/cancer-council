global class BatchMigrateTeamCaptain implements 
    Database.Batchable<sObject>, Database.Stateful 
{
    // this for a one off job to migrate 
    // Registration__c team captains to 
    // Contact_Registrations__c records
    global BatchMigrateTeamCaptain() 
    {}

    public Set<ID> RegistrationIds
    {
        get
        {
            if(RegistrationIds == null)
            {
                RegistrationIds = new Set<ID>();
            }
            return RegistrationIds;
        }
        set;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        return Database.getQueryLocator([SELECT     ID,
                                                    RecordTypeId, 
                                                    RecordType.Name,
                                                    Campaign__r.Reporting_Group__c,
                                                    Host_Team_Captain__c, 
                                                    RFL_Team_FR_Goal__c, 
                                                    Team_Name__c, 
                                                    Org_Primary_Contact__c, 
                                                    Host_Team_Captain_Org__c, 
                                                    Primary_Contact_Org_Last_Name__c,
                                                    Primary_Contact_Org_First_Name__c, 
                                                    Shirt_Size__c, 
                                                    Date_Shirts_Sent__c, 
                                                    Shirt_Type__c, 
                                                    Shorts_Type__c, 
                                                    Short_Size__c, 
                                                    Date_Shorts_Sent__c, 
                                                    Sleeve_Size__c, 
                                                    Date_Sleeve_Sent__c, 
                                                    Cancer_information__c,
                                                    Parent_Guardian_email__c, 
                                                    Parent_Guardian_first_name__c, 
                                                    Parent_Guardian_last_name__c, 
                                                    Parent_Guardian_phone__c, 
                                                    Primary_Contact_Org_Street__c, 
                                                    Primary_Contact_Org_Suburb__c, 
                                                    Primary_Contact_Org_Postcode__c, 
                                                    Primary_Contact_Org_State__c, 
                                                    Emergency_Name__c, 
                                                    Emergency_Contact_Number__c, 
                                                    Emergency_Relationship__c, 
                                                    Contact_Envelope_Salutation__c, 
                                                    Contact_Letter_Salutation__c, 
                                                    Primary_Contact_Letter_Salutation__c, 
                                                    Primary_Contact_Envelope_Salutation__c,
                                                    Team_ID__c, 
                                                    Primary_Contact_Org_Phone__c, 
                                                    Primary_Contact_Org_Mobile__c, 
                                                    Briefly_describe_your_cancer_experience__c, 
                                                    Contact_Email__c, 
                                                    Salutation__c,
                                                    Cancer_Experience__c, 
                                                    Employer__c, 
                                                    Parent_Guardian_Name__c, 
                                                    Participant_Type__c, 
                                                    Registrant_Emergergency_Name__c, 
                                                    Registrant_Emergergency_Number__c, 
                                                    Registrant_Employer__c, 
                                                    Survivor_Carer_Lap__c, 
                                                    TR_Registration_ID__c, 
                                                    TR_Team_ID__c, 
                                                    Team_Description__c, 
                                                    Last_Updated_by_Team_Raiser__c, 
                                                    Survivor_or_Carer__c, 
                                                    Short_Selection_Rules__c,
                                                    Registration_Method__c,
                                                    Captain_Count__c,
                                                    Receipt_Books_Range_Sent__c,
                                                    Receipt_Books_Ranges_Returned__c,
                                                    No_Paperwork__c,
                                                    Total_Donation__c,
                                                    Total_Soft_Credit__c,
                                                    (SELECT ID,
                                                            Contact__c, 
                                                            Shirt_Size__c, 
                                                            Salutation__c, 
                                                            Team_Captain_Obsolete__c, 
                                                            Team_Name__c, 
                                                            Fundraising_Goal__c, 
                                                            Registration_Method__c, 
                                                            Survivor_or_Carer__c, 
                                                            Date_Shirt_Sent__c, 
                                                            Team_ID__c, 
                                                            Cancer_Experience__c, 
                                                            Emergency_Name__c, 
                                                            Parent_Guardian_Name__c, 
                                                            Participation_Type__c, 
                                                            Survivor_Carer_Lap__c, 
                                                            TR_Registration_ID__c, 
                                                            Team_Captain__c, 
                                                            Employer__c, 
                                                            Registrant_Emergency_Number__c, 
                                                            Registrant_Employer__c, 
                                                            Emergency_Contact_Number__c, 
                                                            Parent_Guardian_Email__c, 
                                                            Registrant_Emergency_Name__c, 
                                                            Shirt_Type__c, 
                                                            Sleeve_Size__c, 
                                                            Clothing_Collection_Method__c, 
                                                            Shorts_Type__c, 
                                                            Shorts_Size__c,
                                                            Date_Sleeve_Sent__c, 
                                                            Date_Shorts_Sent__c 
                                                    FROM    Contact_Registrations__r
                                                    WHERE   Migration_Is_Team_Captain__c = true)
                                        FROM        Registration__c
                                        WHERE       RecordType.Name = 'Team Event Registration' AND
                                                    Migrated_Team_Captain__c = null AND
                                                    (
                                                        ID IN :RegistrationIds OR 
                                                        Name LIKE :(RegistrationIds.isEmpty() ? '%' : '~')
                                                    )]);
    }
    
    global void execute(Database.BatchableContext BC, list<Registration__c> scope) 
    {
        TriggerByPass.ContactRegistration = true;
        TriggerByPass.Registration = true;
        RecordTypeInfo individualEventRtInfo = Schema.SObjectType.Registration__c.getRecordTypeInfosByName().get('Individual Event Registration');

        for(Registration__c reg : scope)
        {
            Savepoint sp = Database.setSavepoint();
            try 
            {
                reg.RecordType_Migration_Error__c = null;
                if(reg.Campaign__r.Reporting_Group__c != 'Relay For Life')
                {
                    reg.Original_RecordType__c = reg.RecordType.Name;
                    reg.RecordTypeId = individualEventRtInfo.getRecordTypeId();
                    update reg;
                }
                else
                {
                    if(reg.Host_Team_Captain__c != null)
                    {                        
                        Contact_Registrations__c captainContactReg = null;
                        for(Contact_Registrations__c cr : reg.Contact_Registrations__r)
                        {
                            captainContactReg = cr;
                        }
                        if(captainContactReg == null)
                        {
                            captainContactReg = new Contact_Registrations__c(Registration__c = reg.ID,
                                                                            Contact__c = reg.Host_Team_Captain__c);
                        }
                        captainContactReg.Team_Captain__c = 'Captain';
                        captainContactReg.Shirt_Size__c = reg.Shirt_Size__c;
                        //??????? Fundraising_Goal__c, 
                        captainContactReg.Registration_Method__c = reg.Registration_Method__c;
                        //captainContactReg.Survivor_or_Carer__c, 
                        captainContactReg.Date_Shirt_Sent__c = reg.Date_Shirts_Sent__c;
                        captainContactReg.Cancer_Experience__c = reg.Cancer_Experience__c;
                        captainContactReg.Emergency_Name__c = reg.Emergency_Name__c;
                        captainContactReg.Parent_Guardian_Name__c = reg.Parent_Guardian_Name__c;
                        captainContactReg.Participation_Type__c = reg.Participant_Type__c;
                        captainContactReg.Survivor_Carer_Lap__c = reg.Survivor_Carer_Lap__c;
                        captainContactReg.Employer__c = reg.Employer__c;
                        captainContactReg.Registrant_Emergency_Number__c = reg.Registrant_Emergergency_Number__c;
                        captainContactReg.Registrant_Employer__c  = reg.Registrant_Employer__c; 
                        captainContactReg.Emergency_Contact_Number__c = reg.Emergency_Contact_Number__c;
                        captainContactReg.Parent_Guardian_Email__c = reg.Parent_Guardian_email__c;
                        captainContactReg.Registrant_Emergency_Name__c = reg.Registrant_Emergergency_Name__c;
                        captainContactReg.Shirt_Type__c = reg.Shirt_Type__c; 
                        captainContactReg.Sleeve_Size__c = reg.Sleeve_Size__c;
                        captainContactReg.Clothing_Collection_Method__c = reg.Short_Selection_Rules__c;
                        captainContactReg.Shorts_Type__c = reg.Shorts_Type__c;
                        captainContactReg.Shorts_Size__c = reg.Short_Size__c;
                        captainContactReg.Date_Sleeve_Sent__c = reg.Date_Sleeve_Sent__c;
                        captainContactReg.Date_Shorts_Sent__c = reg.Date_Shorts_Sent__c;
                        captainContactReg.Date_Host_Kit_Sent__c = reg.Date_Host_Kits_Sent__c;
                        captainContactReg.Receipt_Books_Range_Sent__c = reg.Receipt_Books_Range_Sent__c;
                        captainContactReg.Receipt_Books_Ranges_Returned__c = reg.Receipt_Books_Ranges_Returned__c;
                        captainContactReg.No_Paperwork__c = reg.No_Paperwork__c;
                        captainContactReg.Total_Donation__c = reg.Total_Donation__c;
                        captainContactReg.Total_Soft_Credit__c = reg.Total_Soft_Credit__c;
                        upsert captainContactReg;

                        reg.Migrated_Team_Captain__c = captainContactReg.ID;
                        update reg;
                    }
                }
            }
            catch(Exception ex)
            {
                if(sp != null)
                {
                    Database.rollback(sp);
                }
                TriggerByPass.Registration = true;
                reg.RecordType_Migration_Error__c = ex.getMessage() + '\n' + ex.getStackTraceString();
                update reg;
            }
        }
    }

    global void finish(Database.BatchableContext BC) {}
}