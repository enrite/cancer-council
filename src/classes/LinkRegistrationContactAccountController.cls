public with sharing class LinkRegistrationContactAccountController{
    public static final Integer MIN_SCORE_SHOW_POPUP = 40;
    private static final SelectOption[] cMaxResultsPicklist = new SelectOption[]{
        new SelectOption('10','10'),
        new SelectOption('20','20'),
        new SelectOption('50','50')};
    private static final SelectOption[] cContactAccountRadioPicklist = new SelectOption[]{
        new SelectOption('contact','Contact and account'),
        new SelectOption('account','Account only')};
    
    private Registration__c[] mRegistration;
    private Account[] mAccountResults;
    private LinkRegistrationContactControllerAccount mRootAccountNode;
    private integer mTotalAccounts;
    private LinkRegistrationContactControllerAccount[] mSortedAccounts;
    private LinkRegistrationContactControllerAccount[] mSortedLimitedAccounts;
    
    public String maxResultsSelected{get; set;}
    public boolean displayPopup {get; set;}
    public boolean webRegistration {get; set;}
    
    public LinkRegistrationContactAccountController(){
        maxResultsSelected = '10';
        
        String vRegistrationID = ApexPages.CurrentPage().getParameters().get('id');
        mRegistration = [SELECT
            id,
            Host_Team_Captain__c,
            Web_Street__c,
            Web_Suburb__c,
            Web_Postcode__c,
            Web_State__c,
            Web_First_Name__c,
            Web_Last_Name__c,
            Web_Email__c,
            Web_Phone__c,
            Web_Mobile__c,
            Web_Date_of_Birth__c,
			Web_Account__c,
            Web_Registration__c
            FROM Registration__c
            WHERE id = :vRegistrationID];
        
        if(mRegistration.size() != 1){
            return;
        }
        
        webRegistration = mRegistration[0].Web_Registration__c;
              
        mAccountResults = [SELECT
            BillingStreet,
            BillingPostalCode,
            Phone,
            Name,
            RecordType.Name
            FROM Account
            WHERE
            (Phone = :mRegistration[0].Web_Mobile__c AND Phone != null) OR
            (Phone = :mRegistration[0].Web_Phone__c AND Phone != null) OR
            (BillingStreet = :mRegistration[0].Web_Street__c AND BillingStreet != null) OR
            (Name = :mRegistration[0].Web_Account__c AND Name != null)];
        
        mTotalAccounts = mAccountResults.size();
        mSortedAccounts = new LinkRegistrationContactControllerAccount[]{};
        if(mTotalAccounts > 0){
            mRootAccountNode = new LinkRegistrationContactControllerAccount(mAccountResults[0], mRegistration[0]);
            for(integer i = 1; i < mTotalAccounts; i++) mRootAccountNode.add(new LinkRegistrationContactControllerAccount(mAccountResults[i], mRegistration[0]));
            mRootAccountNode.buildArrayDesc(mSortedAccounts);
        }
        updateMaxResults();
    }
    
    public String getTotalAccounts(){
        return String.valueOf(mTotalAccounts);
    }
    
    public LinkRegistrationContactControllerAccount[] getSortedAccounts(){
        if(mSortedLimitedAccounts == null) mSortedLimitedAccounts = new LinkRegistrationContactControllerAccount[]{};
        return mSortedLimitedAccounts;
    }
    
    public Registration__c getRegistration(){
        return mRegistration.size() > 0 ? mRegistration[0] : null;
    }
    
    public void setMaxResults(integer pMax){
        if(mTotalAccounts > 0){
            integer vMax = mSortedAccounts.size() > pMax ? pMax : mSortedAccounts.size();
            mSortedLimitedAccounts = new LinkRegistrationContactControllerAccount[vMax];
            for(integer i = 0; i < vMax; i++) mSortedLimitedAccounts[i] = mSortedAccounts[i];
        }
    }
    
    public SelectOption[] getResultsOptions(){
        return cMaxResultsPicklist;
    }
    
    public void updateMaxResults(){
        setMaxResults(Integer.valueOf(maxResultsSelected));
    }
    
    public String getTotalAccountsDisplayed(){
        integer vMax = Integer.valueOf(maxResultsSelected);
        return String.valueOf(mSortedAccounts.size() > vMax ? vMax : mSortedAccounts.size());
    }
    
    public void closePopup(){        
        displayPopup = false;    
    }
         
    public PageReference newContact(){
        return newContactConfirm();
    }
    
    public PageReference newContactConfirm(){    
        return new PageReference('/apex/LinkRegistrationContactNewPage?regid=' + mRegistration[0].id);
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mRegistration[0].id);
    }
    
}