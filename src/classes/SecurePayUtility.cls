/**
 * This class provides the functionality for paying with secure pay.
 */
public class SecurePayUtility {
    public static Map<String, String> CreditCardPayment(String purchaseOrder, String cardNumber, String cardExpiry, String cvv, Decimal amount, PBSI__PBSI_Sales_Order__c cart)
    {
        // get the settings for secure pay from the custom settings
        final String url = QuitlineSettings__c.getInstance('SecurePayUrl').Configuration__c;
        final String merchantId = QuitlineSettings__c.getInstance('SecurePayMerchantId').Configuration__c; 
        final String password = QuitlineSettings__c.getInstance('SecurePayPassword').Configuration__c; 
        final String testEnv = QuitlineSettings__c.getInstance('SecurePayTestEnvironment').Configuration__c;
        
        boolean testEnvironment = (String.isBlank(testEnv) || 'true' == testEnv.toLowerCase().trim() || 'yes' == testEnv.toLowerCase());
        //string url = 'http://requestb.in/s2ph7ds2';
        
        
        // if this is a test environment then we need to round the amount off to an integer
        if (testEnvironment)
        {
            amount = amount.round(); 
        }

        // make sure any spaces are removed from the card number
        cardNumber = cardNumber.replace(' ', '');

        // build up the xml message
        final String msg = createRequestMessage(merchantID, password, amount, purchaseOrder, cardNumber, cvv, cardExpiry);
        
        // this will hold any errors
        final Map<String, String> errors = new Map<String, String>();

        if (String.isBlank(purchaseOrder)) {
            errors.put('500','Purchase Order Number not supplied');
            
            return errors;
        }
        try {
            // create the request
            final HttpRequest req = new HttpRequest();
            req.setTimeout(60000);
            req.setEndpoint(url);
            req.setMethod('POST');
            req.setBody(msg);
            
            // send the request
            final Http http = new Http();
            final HttpResponse response = http.send(req);

            // parse the response.
            processResponse(response, cart, errors);
        } catch (Exception e) {
            if (testEnvironment) {
               errors.put('500', 'An unexpected error occurred when trying to process your payment. Please contact your system administrator: ' + e.getMessage());
            } else {
                errors.put('500', 'An unexpected error occurred when trying to process your payment. Please contact your system administrator');
            }
            System.debug(e);
            return errors;
        }
        
        return errors;
    }
    
    /**
     * This message creates the XML message to be sent to secure pay containing all the required details.
     * @param merchantID - the merchant id associated with the secure pay account
     * @param password - the password used for authentication
     * @param amount - the total amount paid
     * @param purchaseOrder - the purchase order number
     * @param cardNumber - the credit card number to pass to secure pay
     * @param cvv - the CVV number on the credit card
     * @param cardExpiry - the card expiry date MM/yy
     * @return an XML string containing the pay request for an order
     */
    private static String createRequestMessage(String merchantID, String password, Decimal amount, String purchaseOrder, String cardNumber, String cvv, String cardExpiry) {
        final Datetime now = Datetime.now();

        String msg = '<?xml version="1.0" encoding="UTF-8"?>'; 
        msg += '<SecurePayMessage>';
        msg += '<MessageInfo>';
        msg += '<messageID>' + GenerateRandomString(30) + '</messageID>';
        msg += '<messageTimestamp>' + now.format('yyyyddMMHHmmssSSS000') + '+' + String.valueOf(UserInfo.getTimeZone().getOffset(now) / 60000) + '</messageTimestamp>';
        msg += '<timeoutValue>60</timeoutValue>';
        msg += '<apiVersion>xml-4.2</apiVersion>';
        msg += '</MessageInfo>';
        msg += '<MerchantInfo>';
        msg += '<merchantID>' + merchantID + '</merchantID>';
        msg += '<password>' + password + '</password>';
        msg += '</MerchantInfo>';
        msg += '<RequestType>Payment</RequestType>';
        msg += '<Payment>';
        msg += '<TxnList count="1">';
        msg += '<Txn ID="1">';
        msg += '<txnType>0</txnType>';
        msg += '<txnSource>23</txnSource>';
        msg += '<amount>' + String.valueOf(Integer.valueOf(amount * 100)) + '</amount>'; // multiplying by 100 since the amount should be in cents
        msg += '<currency>AUD</currency>';
        msg += '<purchaseOrderNo>' + purchaseOrder + '</purchaseOrderNo>';
        msg += '<CreditCardInfo>';
        msg += '<cardNumber>' + cardNumber + '</cardNumber>';
        msg += '<cvv>' + cvv + '</cvv>';
        msg += '<expiryDate>' + cardExpiry + '</expiryDate>';
        msg += '</CreditCardInfo>';
        msg += '</Txn>';
        msg += '</TxnList>';
        msg += '</Payment>';
        msg += '</SecurePayMessage>';

        return msg;
    }

    /**
     * This method processes the response from the SecurePay request. If there are any errors these are added to the errors map passed into this method.
     * @param response - the HttpResponse received from the SecurePay server
     * @param cart - the Sales Order to update details on
     * @param error - map containing error messages retrieved from the response.
     */
    private static void processResponse(HttpResponse response, PBSI__PBSI_Sales_Order__c cart, Map<String, String> errors) {
        // parse the response
        Dom.XmlNode rootNode = response.getBodyDocument().getRootElement();
        for(DOM.Xmlnode level1Node : rootNode.getChildElements())
        {
            if(level1Node.getName() == 'Status')
            {
                String statusCode;
                String statusDescription;
            
                for(DOM.Xmlnode level2Node : level1Node.getChildElements())
                {     
                    // get the status information                                 
                    if(level2Node.getName() == 'statusCode')
                    {                            
                        statusCode = level2Node.getText();                         
                    }
                    else if(level2Node.getName() == 'statusDescription')
                    {
                        statusDescription = level2Node.getText();
                    }
                }
                
                // if status code is not 000 then we have an error to be returned
                if (statusCode != '000')
                {
                    system.debug('status: ' + statusCode + '/' + statusDescription);
                    errors.put(statusCode, 'A problem occurred during processing.  Your application has been received but payment has not been processed.  Please contact a system administrator.');
                    //errors.put(statusCode, statusDescription);
                }
            }
            else if (level1Node.getName() == 'Payment')
            {
                for(DOM.Xmlnode level2Node : level1Node.getChildElements())
                {
                    if(level2Node.getName() == 'TxnList')
                    {
                        for(DOM.Xmlnode level3Node : level2Node.getChildElements())
                        {
                            if(level3Node.getName() == 'Txn')
                            {    
                                String responseCode;
                                String responseText;
                            
                                for(DOM.Xmlnode level4Node : level3Node.getChildElements())
                                {                                                                    
                                    if (level4Node.getName() == 'approved')
                                    {
                                        if (level4Node.getText() == 'Yes')
                                        {
                                            cart.Payment_Status__c = 'Paid';
                                        }      
                                    }
                                    else if (level4Node.getName() == 'responseCode')
                                    {
                                        responseCode = level4Node.getText();
                                        cart.Credit_Card_Response_Code__c = level4Node.getText();
                                    }
                                    else if (level4Node.getName() == 'responseText')
                                    {
                                        responseText = level4Node.getText();
                                        cart.Credit_Card_Response_Text__c = level4Node.getText();
                                    }                                                                                                         
                                    else if (level4Node.getName() == 'txnID')
                                    {
                                        cart.Credit_Card_Transaction_ID__c = level4Node.getText();
                                    }                                                                                                                                                                               
                                }
                                
                                if (responseCode != '00' 
                                    && responseCode != '08'
                                    && responseCode != '11'
                                    && responseCode != '16'
                                    && responseCode != '77')
                                {
                                    errors.put(responseCode, responseText);
                                }
                            }
                        }
                    }
                }        
            }
        }
    }

    /**
     * This method generates a random key to be passed through as the unique message key.
     * @param len - the length of the string to generate
     * @return random string of a given length
     */
    private static String GenerateRandomString(Integer len)
    {
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        return key.substring(0, len);
    }
    
}