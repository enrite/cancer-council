public class TRExportRegistrationBatch 
    extends TRExportBase
    implements Database.Batchable<sObject>, Database.Stateful
{
    public TRExportRegistrationBatch (TR_Export__c exportRecord)
    {
        super(exportRecord);
    }
    
    public TRExportRegistrationBatch(TRExportBase prevBatch)
    {
        super(prevBatch);
    }
    
    private final String ATTACHMENTNAME = getFileName('Registration');
    private final String PLEASE_SELECT =  'Please select';
    private final String PARTICIPANTTYPE_SURVIVOR = 'I am a Survivor or Carer registering for the Survivor & Carer lap';
    private final String PARTICIPANTTYPE_YOUTHSURVIVOR = 'I am a Youth Survivor or Carer registering for the Survivor & Carer lap';
    
    public Database.QueryLocator start(Database.BatchableContext info)
    {               
        Attachment att = createAttachment(ATTACHMENTNAME, getHeader());
        
        return Database.getQueryLocator([SELECT ID,
                                                TR_Team_ID__c,
                                                Team_Name__c,
                                                Campaign__r.Name,
                                                Campaign__r.TR_Event_Id__c,
                                                Host_Team_Captain__c,
                                                Host_Team_Captain__r.TeamRaiser_CONS_ID__c,
                                                Host_Team_Captain__r.Contact_ID__c,     
                                                Org_Primary_Contact__c,
                                                Org_Primary_Contact__r.TeamRaiser_CONS_ID__c,           
                                                Org_Primary_Contact__r.Contact_ID__c,                       
                                                Registration_Date__c,
                                                RFL_Team_FR_Goal__c,
                                                Participant_Type__c,
                                                Emergency_Name__c,
                                                Emergency_Contact_Number__c,
                                                LastModifiedDate,
                                                Last_Updated_by_Team_Raiser__c,
                                                (SELECT     ID,
                                                            Team_ID__c,
                                                            Team_Name__c,
                                                            Team_Captain__c,
                                                            Participation_Type__c,
                                                            Emergency_Name__c,
                                                            Emergency_Contact_Number__c,
                                                            Campaign__c,
                                                            Contact__c,
                                                            Contact__r.TeamRaiser_CONS_ID__c,
                                                            Contact__r.Contact_ID__c                                                    
                                                FROM        Contact_Registrations__r
                                                WHERE       Team_ID__c != null OR
                                                            Team_Name__c != null),
                                                (SELECT     Amount,
                                                            Primary_Contact_at_Organisation__c                                                      
                                                FROM        Donations__r
                                                WHERE       RecordType.Name = 'Donation' AND
                                                            Income_Type__c = 'Registration Fee')
                                        FROM    Registration__c
                                        WHERE   Campaign__r.TR_Event_Id__c != null AND
                                                (TR_Team_ID__c != null OR
                                                Team_Name__c != null) AND
                                                (LastModifiedDate > :getLastExportTime() OR
                                                Campaign__r.Initial_Teamraiser_Export__c = null)]);
    }
    
    public void execute(Database.BatchableContext info, List<Registration__c> scope)
    {       
        Attachment att = getAttachment(ATTACHMENTNAME);     
        String b = att.Body.toString();
                                                    
        for(Registration__c r : scope)
        {         
            // if the last modified datetime is (basically) the same as the last updated by team raiser datetime then skip the record
            if (r.Last_Updated_by_Team_Raiser__c != null
                && r.LastModifiedDate < r.Last_Updated_by_Team_Raiser__c.addMinutes(1))
            {
                continue;
            }
                      
            //b += processRegsistration(r);
            //RegistrationRecords++;
            for(Contact_Registrations__c cr : r.Contact_Registrations__r)
            {
                b += processContactRegsistration(r, cr);    
                RegistrationRecords++;
            }           
        }
        att.Body = Blob.valueOf(b);
        update att;
    } 
    
    public void finish(Database.BatchableContext info)
    {
        if(Test.isRunningTest())
        {
            return;
        }
        TRExportQuestionBatch questBatch = new TRExportQuestionBatch(this);
        Database.executeBatch(questBatch, BatchSize);
    }
    
    private String getHeader()
    {
        String line = '';
        line += getField('CONS_ID') + 
                    getField('MEMBER_ID') +
                    getField('FR_ID') +
                    getField('EVENT_NAME') +
                    getField('PARTICIPATION_TYPE_NAME') +
                    getField('EMERGENCY_NAME') +
                    getField('EMERGENCY_PHONE') +
                    getField('TEAM_ID') +
                    getField('TEAM_NAME') +
                    getField('IS_CAPTAIN') + 
                    getField('TEAM_GOAL') +
                    getField('REGISTRATION_DATE') +
                    getField('FEE_PAID') +
                    getField('TEAM_DIVISION') +
                    getField('IS_ANONYMOUS');   
        return line  + NEW_LINE;
    }
    
    private String processRegsistration(Registration__c r)
    {
        String line = '';
        //BOolean isCaptain = r.Host_Team_Captain__c != null || r.Org_Primary_Contact__c != null;
        if(r.Host_Team_Captain__c != null)
        {
            line += getField(r.Host_Team_Captain__r.TeamRaiser_CONS_ID__c, 11) + 
                    getField((r.Host_Team_Captain__r.TeamRaiser_CONS_ID__c == null ? r.Host_Team_Captain__r.Contact_ID__c : ''), 32);
        }
        else
        {
            line += getField(r.Org_Primary_Contact__r.TeamRaiser_CONS_ID__c, 11) + 
                    getField((r.Org_Primary_Contact__r.TeamRaiser_CONS_ID__c == null ? r.Org_Primary_Contact__r.Contact_ID__c : ''), 32);
        }
        line += getField(r.Campaign__r.TR_Event_Id__c, 11) +
                    getField(r.Campaign__r.Name, 255) +
                    getField(r.Participant_Type__c, 255) +
                    getField(r.Emergency_Name__c, 255) +
                    getField(r.Emergency_Contact_Number__c, 50) +
                    getField(r.TR_Team_ID__c, 11) +
                    getField((r.TR_Team_ID__c == null ? r.Team_Name__c : ''), 100) + 
                    getField(convertBoolean(false)) +
                    getField(r.RFL_Team_FR_Goal__c, 9) +
                    getField(r.Registration_Date__c);
        if(r.Host_Team_Captain__c != null)
        {           
            line += getField(getDonationAmount(r.Donations__r, r.Host_Team_Captain__c), 22);
        }
        else
        {
            line += getField(getDonationAmount(r.Donations__r, r.Org_Primary_Contact__c), 22);
        }
        String teamDivision = (String.isNotBlank(r.Team_Name__c) && String.isBlank(r.TR_Team_ID__c)) ? PLEASE_SELECT : '';
        line += getField(teamDivision) +
                    getField(r.Participant_Type__c == PARTICIPANTTYPE_SURVIVOR ||
                                r.Participant_Type__c == PARTICIPANTTYPE_YOUTHSURVIVOR);
        
        return line + NEW_LINE;
    }   
    
    @TestVisible
    private String processContactRegsistration(Registration__c c, Contact_Registrations__c cr)
    {
        String line = '';
        line += getField(cr.Contact__r.TeamRaiser_CONS_ID__c, 11) + 
                    getField((cr.Contact__r.TeamRaiser_CONS_ID__c == null ? cr.Contact__r.Contact_ID__c : ''), 32) +
                    getField(c.Campaign__r.TR_Event_Id__c, 11) +
                    getField(c.Campaign__r.Name, 255) +
                    getField(cr.Participation_Type__c, 255) +
                    getField(cr.Emergency_Name__c, 255) +
                    getField(cr.Emergency_Contact_Number__c, 50) +
                    getField(cr.Team_ID__c, 11) +
                    getField((cr.Team_ID__c == null ? cr.Team_Name__c : ''), 100) + 
                    getField(convertBoolean(cr.Team_Captain__c == 'Captain')) +
                    getField(c.RFL_Team_FR_Goal__c, 9) +
                    getField(c.Registration_Date__c) +
                    getField(getDonationAmount(c.Donations__r, cr.Contact__c), 22);
                    
                    String teamDivision = (String.isNotBlank(cr.Team_Name__c) && String.isBlank(cr.Team_ID__c)) ? PLEASE_SELECT : '';
        line += getField(teamDivision) +
                    getField(cr.Participation_Type__c == PARTICIPANTTYPE_SURVIVOR ||
                                    cr.Participation_Type__c == PARTICIPANTTYPE_YOUTHSURVIVOR);
        return line + NEW_LINE;
    }   
    
    @TestVisible
    private decimal getDonationAmount(List<Opportunity> donations, ID contactID)
    {
        if(donations == null)
        {
            return 0.0;
        }
        decimal donation = 0.0;
        for(Opportunity opp : donations)
        {
            if(opp.Primary_Contact_at_Organisation__c == contactID)
            {
                donation += opp.Amount == null ? 0.0 : opp.Amount;
            }
        }
        return donation;
    }
}