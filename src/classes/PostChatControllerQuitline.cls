public class PostChatControllerQuitline
{
    public PageReference redirect(){
        PageReference pageRef = new PageReference('https://www.surveymonkey.com/r/QuitChat');
        pageRef.setRedirect(true);
        return pageRef;
    }
}