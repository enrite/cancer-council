public with sharing class ParseCSVUtil {

    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();

        try {
            contents = contents.replaceAll(',",','DBLQT",').replaceAll(',"',',"DBLQT').replaceall('",','DBLQT",');
            List<String> lines = new List<String>();
        
            lines = contents.split('\n');           
            for(String line: lines) {               
                // check for blank CSV lines (only commas)
                if (line.replaceAll(',','').trim().length() == 0) break;                
                List<String> fields = line.split(',', 1000);                    
                List<String> cleanFields = new List<String>();
                String compositeField;
                Boolean makeCompositeField = false;
                for(String field: fields) {                                     
                    if (field.startsWith('"') && field.endsWith('"')) {
                        cleanFields.add(field.replaceAll('DBLQT','"'));
                    } else if (field.startsWith('"')) {
                        makeCompositeField = true;
                        compositeField = field;
                    } else if (field.endsWith('"')) {
                        compositeField += ',' + field;
                        cleanFields.add(compositeField.replaceAll('DBLQT','').replaceAll('"', ''));
                        makeCompositeField = false;                     
                    } else if (makeCompositeField) {
                        compositeField += ',' + field;
                    } else {                        
                        cleanFields.add(field.replaceAll('DBLQT','"'));
                    }
                }       
                allFields.add(cleanFields);
            }
            if (skipHeaders) allFields.remove(0);           
        }
        catch (Exception e) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Parse CSV Exception Occured ', e.getMessage());
            ApexPages.addMessage(errorMsg);
        }       
        return allFields;
    }
    
    public static String toProperCase(String OriginalString){
        List<String> Character_List = new List<String>();
        Character_List = OriginalString.split('');
        
        String NewString = '';
        for(Integer i=1; i<Character_List.size(); i++){
            if( i==1 || Character_List[i-1] == ' '){
                NewString += Character_List[i].toUpperCase();                
            }
            else{
                NewString += Character_List[i].toLowerCase();                
            }
        }                
        return NewString;        
    }   

    public static testMethod void testParseCSVUtility() {

        string existAccount = '9998,202 ONTARIO,202 ONTARIO AVENUE,MILDURA,3500,VIC,202 ONTARIO AVENUE,MILDURA,3500,VIC,03 5021 1688,03 5021 0266,,General Practitioner,' + 
                                '\n2417,3EHS DETACHMENT DARWIN,RAAF BASE DARWIN,WINNELLIE,820,NT,RAAF BASE DARWIN,WINNELLIE,820,NT,08 8923 5447,08 8923 5454,,General Practitioner,\n';
        string newAccount = '9999,A F SUTTON HOSTEL,101 LAKE TERRACE EAST,MOUNT GAMBIER,5290,SA,"SHOP 11, MARTINS PLAZA SHOPPING CENTRE, 237 MARTINS ROAD",MOUNT GAMBIER,5290,SA,08 8725 7377,08 8725 8262,admin@boandiklodge.org.au,Residential Aged Care Facility,boandiklodge';
        blob content = blob.valueOf('location_id,practice_name,street_address,street_suburb,street_postcode,street_state,postal_address,postal_suburb,postal_postcode,postal_state,practice_phone,practice_fax,practice_email,type1_id,username\n' + existAccount + newAccount);
        string contentStr = content.toString();
        List<List<String>> test = ParseCSVUtil.parseCSV(contentStr, False);
        
        system.assertEquals('Abc', ParseCSVUtil.toProperCase('ABC'));
        system.assertEquals(4, test.size());
                    
    }
}