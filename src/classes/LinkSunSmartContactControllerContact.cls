public with sharing class LinkSunSmartContactControllerContact{
    private static final Integer cWeightEmail = 40;
    private static final Integer cWeightFirstName = 30;
    private static final Integer cWeightLastName = 30;
    
    private boolean mMatchEmail;
    private boolean mMatchFirstName;
    private boolean mMatchLastName;
    
    private Contact mContact;
    private SunSmart__c mSunSmart;
    private Integer mScore;
    
    // Binary search tree links
    private LinkSunSmartContactControllerContact mLeft;
    private LinkSunSmartContactControllerContact mRight;
    
    
    public LinkSunSmartContactControllerContact(Contact pContact, SunSmart__c pSunSmart){
        mContact = pContact;
        mSunSmart = pSunSmart;
        mScore = 0;
        
        mMatchEmail = pContact.Email != null && pContact.Email == pSunSmart.School_Contact_Email__c;
        mMatchFirstName = pContact.FirstName != null && pContact.FirstName == pSunSmart.First_Name__c;
        mMatchLastName = pContact.LastName != null && pContact.LastName == pSunSmart.Last_Name__c;
        
        if(mMatchEmail) mScore += cWeightEmail;
        if(mMatchFirstName) mScore += cWeightFirstName;
        if(mMatchLastName) mScore += cWeightLastName;
    }
    
    public Integer getScore(){
        return mScore;
    }
    
    public Contact getContact(){
        return mContact;
    }
    
    public SunSmart__c sunSmart(){
        return mSunSmart;
    }
    
    public void add(LinkSunSmartContactControllerContact pNode){
        if(pNode.getScore() > mScore){
            if(mRight == null) mRight = pNode;
            else mRight.add(pNode);
        }else{
            if(mLeft == null) mLeft = pNode;
            else mLeft.add(pNode);
        }
    }
    
    public void buildArrayDesc(LinkSunSmartContactControllerContact[] pArray){
        if(mRight != NULL) mRight.buildArrayDesc(pArray);
        pArray.add(this);
        if(mLeft != NULL) mLeft.buildArrayDesc(pArray);
    }
    
    public boolean getMatchEmail(){
        return mMatchEmail;
    }
    
    public boolean getMatchFirstName(){
        return mMatchFirstName;
    }
    
    public boolean getMatchLastName(){
        return mMatchLastName;
    }

}