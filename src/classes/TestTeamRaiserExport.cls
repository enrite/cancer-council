/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTeamRaiserExport {

    static testMethod void testExportConstituent1()
    {
    	TR_Export__c ex = insertConstituents();
        TRExportConstituentBatch b = new TRExportConstituentBatch(ex);
        Database.executeBatch(b, 1);
    }
     
    static testMethod void testExportConstituent2()
    {
        TR_Export__c trExport = new TR_Export__c(Export_Time__c = DateTime.now());
        insert trExport;
        TRExportConstituentBatch b = new TRExportConstituentBatch(trExport);
        b = new TRExportConstituentBatch(b);
        
        Contact c = new Contact();
        c.Gender__c = null;
        System.assert(b.getGender(c) != null);
        c.Gender__c = 'Test';
        system.assert(b.getGender(c) != null);
        
        system.assert(b.getStreetAddress(null, 0) != null);
        system.assert(b.getStreetAddress('1\n2\n3\n', 1) != null);
        system.assert(b.getStreetAddress('1\n2\n3\n', 6) != null);
    }
    
    static testMethod void testExportRegistration1()
    {
    	TR_Export__c ex = insertRegistrations(false);
        TRExportRegistrationBatch b = new TRExportRegistrationBatch(ex); 
        Database.executeBatch(b, 2);
    }
    
    static testMethod void testExportRegistration2()
    {
        TR_Export__c trExport = new TR_Export__c(Export_Time__c = DateTime.now());
        insert trExport;
        TRExportRegistrationBatch b = new TRExportRegistrationBatch(trExport);
        b = new TRExportRegistrationBatch(b);
        
        Contact cn = new Contact();
		cn.FirstName = 'Test';
		cn.LastName = 'Test'; 
		insert cn;
		
        Opportunity opp = new Opportunity();
        opp.Amount = 100.00;
        opp.Primary_Contact_at_Organisation__c = cn.ID;
        List<Opportunity> donations = new List<Opportunity>{opp};
        system.assert(b.getDonationAmount(donations, cn.ID) == opp.Amount);
        system.assert(b.getDonationAmount(null, cn.ID) == 0.0);
        
        system.assert(b.processContactRegsistration(new Registration__c(), new Contact_Registrations__c()) != null);
    }
    
    static testMethod void testExportQuestion1()
    {
    	TR_Export__c ex = insertRegistrations(true);
        TRExportQuestionBatch b = new TRExportQuestionBatch(ex);
        Database.executeBatch(b, 2);
    }
    
    static testMethod void testExportTransaction1()
    {
    	TR_Export__c ex = insertTransactions();
        TRExportTransactionBatch b = new TRExportTransactionBatch(ex);
        Database.executeBatch(b, 1);
    }
    
    private static TR_Export__c insertConstituents()
    {
    	Account acc = new Account();
    	acc.Name = 'Test';
    	insert acc; 
    	
    	Campaign c = new Campaign();
    	c.Name = 'Test';
    	c.TR_Event_ID__c = 'Test';
    	insert c;
    	
    	Contact cn = new Contact();
		cn.FirstName = 'Test';
		cn.LastName = 'Test'; 
		cn.TeamRaiser_CONS_ID__c = 1;
		insert cn;
    	
    	Registration__c r = new Registration__c();
    	r.Campaign__c = c.ID;
    	r.Team_ID__c = 'Test';
    	r.Host_Team_Captain__c = cn.ID;
    	insert r;
    	
	    Contact_Registrations__c cr = new Contact_Registrations__c();
	    cr.Registration__c = r.ID;
	    cr.Contact__c = cn.ID;
	    insert cr;
	    
	    TR_Export__c exportRecord = new TR_Export__c(Export_Time__c = DateTime.now());
 		insert exportRecord;
 		return exportRecord;
    }
    
    private static TR_Export__c insertRegistrations(Boolean insertContactRegistrations)
    {
    	Account acc = new Account();
    	acc.Name = 'Test';
    	insert acc; 
    	
    	Campaign c = new Campaign();
    	c.Name = 'Test';
    	c.TR_Event_ID__c = 'Test';
    	insert c;
    	
    	Contact cn = new Contact();
		cn.FirstName = 'Test';
		cn.LastName = 'Test'; 
		cn.TeamRaiser_CONS_ID__c = 1;
		insert cn;
    	
    	Registration__c r = new Registration__c();
    	r.Campaign__c = c.ID;
    	r.Team_ID__c = 'Test';
    	r.Org_Primary_Contact__c = cn.ID;
    	r.Host_Team_Captain_Org__c = acc.ID;
    	r.Cancer_Experience__c = 'Test';
	    r.Survivor_Carer_Lap__c = true;
		r.Shirt_Size__c = '1';
		r.Parent_Guardian_Name__c = 'Test';
    	insert r;
    	
    	r = new Registration__c();
    	r.Campaign__c = c.ID;
    	r.Team_ID__c = 'Test';
    	r.Host_Team_Captain__c = cn.ID;
    	r.Cancer_Experience__c = 'Test';
	    r.Survivor_Carer_Lap__c = true;
		r.Shirt_Size__c = '1';
		r.Parent_Guardian_Name__c = 'Test';
    	insert r;
    	
    	if(insertContactRegistrations)
    	{
		    Contact_Registrations__c cr = new Contact_Registrations__c();
		    cr.Registration__c = r.ID;
		    cr.Contact__c = cn.ID;
		    cr.Cancer_Experience__c = 'Test';
		    cr.Survivor_Carer_Lap__c = true;
			cr.Shirt_Size__c = '1';
			cr.Parent_Guardian_Name__c = 'Test';
		    insert cr;
    	}
	    
	    TR_Export__c exportRecord = new TR_Export__c(Export_Time__c = DateTime.now());
 		insert exportRecord;
 		return exportRecord;
    }
    
    private static TR_Export__c insertTransactions()
    {
    	Campaign c = new Campaign();
    	c.Name = 'Test';
    	c.TR_Event_ID__c = 'Test';
    	insert c;
    	
    	Contact cn = new Contact();
		cn.FirstName = 'Test';
		cn.LastName = 'Test'; 
		cn.TeamRaiser_CONS_ID__c = 1;
		insert cn;
    	
    	Registration__c r = new Registration__c();
    	r.Campaign__c = c.ID;
    	r.Team_ID__c = 'Test';
    	r.Host_Team_Captain__c = cn.ID;
    	r.Cancer_Experience__c = 'Test';
	    r.Survivor_Carer_Lap__c = true;
		r.Shirt_Size__c = '1';
		r.Parent_Guardian_Name__c = 'Test';
    	insert r;
    	
        Account acc = new Account(Name = 'TestTR');
        acc.Status__c = 'Active';
        insert acc;

    	Opportunity opp = new Opportunity();
    	opp.Name = 'Test';
    	opp.CloseDate = Date.today();
    	opp.CampaignID = c.ID;
    	opp.Registration__c = r.ID;
    	opp.RecordTypeId = [SELECT 	ID 
    						FROM 	RecordType 
    						WHERE 	Name = 'Donation' AND 
    								sObjectType = 'Opportunity'].ID;
		opp.Amount = 100.00;
		opp.Income_Type__c = 'Registration Fee';
		opp.StageName = 'Completed';
        opp.AccountId = acc.ID;
        insert opp;     
        
        Soft_Credit__c sc = new  Soft_Credit__c();
        sc.Opportunity__c = opp.ID;
        sc.Contact__c = cn.ID;
        insert sc;
                              			
    	TR_Export__c exportRecord = new TR_Export__c(Export_Time__c = DateTime.now());
 		insert exportRecord;
 		return exportRecord;
    }
}