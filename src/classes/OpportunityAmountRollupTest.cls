@isTest
private class OpportunityAmountRollupTest {

    static testMethod void myOpportunityAmountRollupTest() {
    	test.startTest();
    	
        // Load test Account, Contact and Campaign data
		TestDataFactory.createAccountContactTestRecords(1);
		TestDataFactory.createCampaignTestRecords(1);
		
        Campaign c = new Campaign (Name='Major Gifts', Campaign_Code__c='Major Gifts');
        insert c;		
    	
        map<string, Id> mapRecordType = new map<string, Id>
                        {'Registration'=>[SELECT Id FROM RecordType WHERE SObjectType='Registration__c' AND DeveloperName='Basic' LIMIT 1].Id
                        , 'Opportunity'=>[SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND DeveloperName='Donation' LIMIT 1].Id};
		
		list<Opportunity> listOpportunity = new list<Opportunity>();
		
		Id campaignId = [SELECT Id FROM Campaign WHERE Campaign_Code__c<>'Major Gifts' LIMIT 1].Id;
		Id contactId;
		Id accountId;
		
		for (Contact con : [SELECT Id, AccountId FROM Contact LIMIT 1]) {
			contactId = con.Id;
			accountId = con.AccountId;
		}
		
		Registration__c reg = new Registration__c();
		reg.Host_Team_Captain__c = contactId;
		reg.Campaign__c = campaignId;
		reg.RecordTypeId = mapRecordType.get('Registration');
		reg.Status__c = 'Registered';
		insert reg;
		
		Contact_Registrations__c cr = new Contact_Registrations__c();
		cr.Contact__c = contactId;
		cr.Registration__c = reg.Id;
		insert cr;
		
		Batch__c bat = new Batch__c();
		bat.Batch_Date__c = date.today();
		bat.Batch_Check_Total__c = 20000;
		insert bat;
		
		
		for (integer i = 0; i < 4; i++) {
			Opportunity opp = new Opportunity();
			opp.Name = 'Test ' + date.today().format();
			opp.AccountId = accountId;
			opp.Primary_Contact_at_Organisation__c = contactId;
			opp.CampaignId = campaignId;
			opp.Registration__c = reg.Id;
			opp.CloseDate = date.today();
			opp.StageName = 'Posted';
			opp.Amount = 300+i;//Math.round(Math.random()*1000);
			opp.Batch_Number__c = bat.Id;		
			listOpportunity.add(opp);
		}
		
		insert listOpportunity;
		
		// reset TriggerByPass.BPAYNum to continue testing
		TriggerByPass.BPAYNum = false;	
		
		// Update Opportunity Amount to $5000 for Reallocated=true
		Opportunity o = listOpportunity.get(2);
		o.Amount = 5000;
		o.Contact_Registration__c = cr.Id;
		update o;
		
		// Set Opportunity Reallocated=true
		o.Reallocated__c = true;
		update o;

		Opportunity o0 = listOpportunity.get(0);
		delete o0;
				
		AggregateResult aggResult = [SELECT Registration__c, SUM(Amount) TotalDonation FROM Opportunity WHERE Registration__c = :reg.Id GROUP BY Registration__c];
		system.assertEquals(aggResult.get('TotalDonation'), 5604);
		
		test.stopTest();
    }
}