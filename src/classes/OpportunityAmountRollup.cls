public with sharing class OpportunityAmountRollup {
    private static string emailAddress {
        get {
            if (emailAddress == null) {
                emailAddress = [SELECT Id, Email__c FROM Email_Settings__c WHERE Name = 'IT Helpdesk'].Email__c;
            }
            return emailAddress;
        }
    }
    
    private static map<Id, Opportunity> mapOpportunity {get;set;}
    private static map<Id, Opportunity> mapOldOpportunity {get;set;}
    public static boolean isDelete{
    	get{
    		if (isDelete == null) {
    			isDelete = false;
    		}
    		return isDelete;
    	}
    	set;
    }
    
    public static void totalAmountRollup(map<Id, Opportunity> mapRecord, map<Id, Opportunity> mapOldRecord) {
        mapOpportunity = mapRecord;
        mapOldOpportunity = mapOldRecord;
        
        if (mapOpportunity.size() > 0) {
	        // Also update Soft Credit records that link to Opportunity
	        map<Id, Soft_Credit__c> mapSoftCredit = new map<Id, Soft_Credit__c>([SELECT Id, Opportunity__c, Contact_Registration__c, Name FROM Soft_Credit__c WHERE Opportunity__c IN :mapOpportunity.keySet()]);	        
	        if (mapSoftCredit.size() > 0) {
	        	// Force to delete Soft Credit in order to fire Soft Credit Delete trigger for updates
	        	if (isDelete) {	        		
	        		delete mapSoftCredit.values();
	        	}
	        	else {
	        		SoftCreditAmountRollup.totalAmountRollup(mapSoftCredit, mapSoftCredit, mapOpportunity, mapOldOpportunity);
	        	}
	        }
        	
            opportunityRollup();
            registrationRollup();
            campaignRollup();
            contactRegistrationRollup();
            batchRollup();            
        }
    }
    
    public static void totalAmountRollup(map<Id, Opportunity> mapRecord, map<Id, Opportunity> mapOldRecord, boolean isDeleteTrigger) {
    	isDelete = isDeleteTrigger;
    	totalAmountRollup(mapRecord, mapOldRecord);
    }
    
    private static void opportunityRollup () {
        try {
            boolean hasOpportunitySales = false;
            
            // Registration Id set for dynamic soql     
            set<Id> setSObjectId = new set<Id>();
            
            // Get all Source_Sales_Opportunity__c from Opportunities
            for (Opportunity opp : mapOpportunity.values()) {
                if (opp.Source_Sales_Opportunity__c != null) {                	
                    setSObjectId.add(opp.Source_Sales_Opportunity__c);
                    hasOpportunitySales = true;
                }
                if (mapOldOpportunity.get(opp.Id).Source_Sales_Opportunity__c != null) {
                    setSObjectId.add(mapOldOpportunity.get(opp.Id).Source_Sales_Opportunity__c);
                    hasOpportunitySales = true;                	
                }
            }

            // Opportunity has Source Sales Opportunity
            if (hasOpportunitySales) {              
                list<Opportunity> listOpportunity = new list<Opportunity>();
                
                map<Id, Opportunity> mapOpportunitySales = new map<Id, Opportunity>([SELECT Id, Total_Donation__c, Total_Donation_Reallocated__c FROM Opportunity WHERE Id IN :setSObjectId]);
                
                // Dynamic SOQL query for Opportunity Source Sales Opportunity field on Opportunity
                string query;           
                query = 'SELECT Source_Sales_Opportunity__c Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Source_Sales_Opportunity__c != NULL AND Source_Sales_Opportunity__c IN :setSObjectId GROUP BY Source_Sales_Opportunity__c';            
                map<Id, Decimal> mapAggResult = aggResult(setSObjectId, query);
                
                // Dynamic SOQL query for Opportunity_Reallocated__c field on Opportunity Reallocated=true
                map<Id, Decimal> mapAggResultReallocated;       
                string queryReallocated;
                queryReallocated = 'SELECT Source_Sales_Opportunity__c Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Source_Sales_Opportunity__c != NULL AND Reallocated__c = true AND Source_Sales_Opportunity__c IN :setSObjectId GROUP BY Source_Sales_Opportunity__c';
                mapAggResultReallocated = aggResult(setSObjectId, queryReallocated);
                
                for (Opportunity opp : mapOpportunitySales.values()) {
                    opp.Total_Donation__c = mapAggResult.get(opp.Id);
                    
                    // If Opportunity has Opportunity Reallocated=true then rollup amount
                    if (mapAggResultReallocated.containsKey(opp.Id)) {
                        opp.Total_Donation_Reallocated__c = mapAggResultReallocated.get(opp.Id);
                    }
                    listOpportunity.add(opp);               
                }
                
                if (listOpportunity.size() > 0) {
                    update listOpportunity;                    
                }
            }
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** ' + OpportunityAmountRollup.class.getName() + ': opportunityRollup' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, OpportunityAmountRollup.class.getName() + ': opportunityRollup', errorMessage, 'HTML');
        }
    }
    
    private static void registrationRollup () {
        try {
            boolean hasRegistration = false;
            
            // Registration Id set for dynamic soql     
            set<Id> setSObjectId = new set<Id>();
            
            // Get all Registration__c from Opportunities
            for (Opportunity opp : mapOpportunity.values()) {
                // update new record
                if (opp.Registration__c != null) {                    
                    setSObjectId.add(opp.Registration__c);
                    hasRegistration = true;
                }
                // update old record
                if (mapOldOpportunity.get(opp.Id).Registration__c != null) {
                    setSObjectId.add(mapOldOpportunity.get(opp.Id).Registration__c);
                    hasRegistration = true;
                }                
            }
            
            // Opportunity has Registration__c
            if (hasRegistration) {              
                list<Registration__c> listRegistration = new list<Registration__c>();
                
                map<Id, Registration__c> mapRegistration = new map<Id, Registration__c>([SELECT Id, Total_Donation__c, Total_Donation_Reallocated__c, Total_Registration_Fee__c FROM Registration__c WHERE Id IN :setSObjectId]);
                
                // Dynamic SOQL query for Registration__c field on Opportunity
                string query;           
                query = 'SELECT Registration__c Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Registration__c != NULL AND Registration__c IN :setSObjectId GROUP BY Registration__c';            
                map<Id, Decimal> mapAggResult = aggResult(setSObjectId, query);
                
                // Dynamic SOQL query for Registration__c field on Opportunity_Reallocated__c=true
                map<Id, Decimal> mapAggResultReallocated;       
                string queryReallocated;
                queryReallocated = 'SELECT Registration__c Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Registration__c != NULL AND Reallocated__c = true AND Registration__c IN :setSObjectId GROUP BY Registration__c';
                mapAggResultReallocated = aggResult(setSObjectId, queryReallocated);
                
                // Dynamic SOQL query for Registration__c field on Opportunity Income_Type__c=Registration Fee
                map<Id, Decimal> mapAggResultRegistrationFee;       
                string queryRegistrationFee;
                queryRegistrationFee = 'SELECT Registration__c Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Registration__c != NULL AND Income_Type__c = \'Registration Fee\' AND Registration__c IN :setSObjectId GROUP BY Registration__c';
                mapAggResultRegistrationFee = aggResult(setSObjectId, queryRegistrationFee);                
                
                for (Registration__c reg : mapRegistration.values()) {
                    reg.Total_Donation__c = mapAggResult.get(reg.Id);                   
                    
                    // If Registration has Opportunity_Reallocated__c=true then rollup amount
                    if (mapAggResultReallocated.containsKey(reg.Id)) {
                        reg.Total_Donation_Reallocated__c = mapAggResultReallocated.get(reg.Id);
                    }
                    // If Registration has Opportunity Income_Type__c="Registration Fee" then rollup amount
                    if (mapAggResultRegistrationFee.containsKey(reg.Id)) {
                        reg.Total_Registration_Fee__c = mapAggResultRegistrationFee.get(reg.Id);
                    }
                    
                    listRegistration.add(reg);               
                }
                
                if (listRegistration.size() > 0) {
                    //TriggerByPass.Registration = true;
                    update listRegistration;                    
                }
            }
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** ' + OpportunityAmountRollup.class.getName() + ': registrationRollup' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, OpportunityAmountRollup.class.getName() + ': registrationRollup', errorMessage, 'HTML');
        }
    }
    
    private static void campaignRollup () {
        try {           
            // Campaign Id set for dynamic soql     
            set<Id> setSObjectId = new set<Id>();
            
            // Get all CampaignId from Opportunities
            for (Opportunity opp : mapOpportunity.values()) {
            	// Update new record
            	setSObjectId.add(opp.CampaignId);
            	// update old record
            	setSObjectId.add(mapOldOpportunity.get(opp.Id).CampaignId);                
            }
            
            list<Campaign> listCampaign = new list<Campaign>();
            
            map<Id, Decimal> mapAggResultReallocated;               
            
            map<Id, Campaign> mapCampaign = new map<Id, Campaign>([SELECT Id, Total_Value_Reallocated__c FROM Campaign WHERE Id IN :setSObjectId]);
            
            // Dynamic SOQL query for Campaign field on Opportunity Reallocated=true                    
            string queryReallocated;
            queryReallocated = 'SELECT CampaignId Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Reallocated__c = true AND CampaignId IN :setSObjectId GROUP BY CampaignId';
            mapAggResultReallocated = aggResult(setSObjectId, queryReallocated);
            
            // If Campaign has Opportunity Reallocated=true then rollup amount
            if (mapAggResultReallocated.size() > 0) {
	            for (Campaign camp : mapCampaign.values()) {
	                // Skip to next iteration if value unchanged
	                if (camp.Total_Value_Reallocated__c == mapAggResultReallocated.get(camp.Id)) {
	                    continue;
	                }
	                else {
	                    camp.Total_Value_Reallocated__c = mapAggResultReallocated.get(camp.Id);
	                    listCampaign.add(camp); 
	                }
	            }            	
            }
            
            if (listCampaign.size() > 0) {
                update listCampaign;    
            }
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** ' + OpportunityAmountRollup.class.getName() + ': campaignRollup' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, OpportunityAmountRollup.class.getName() + ': campaignRollup', errorMessage, 'HTML');
        }   
    }
    
    private static void contactRegistrationRollup () {
        try {
            boolean hasContactRegistration = false;
            
            // Contact Registration Id set for dynamic soql     
            set<Id> setSObjectId = new set<Id>();
            
            // Get all Contact_Registration__c from Opportunities
            for (Opportunity opp : mapOpportunity.values()) {
                // Update new record
                if (opp.Contact_Registration__c != null) {                    
                    setSObjectId.add(opp.Contact_Registration__c);
                    hasContactRegistration = true;
                }
                // update old record
                if (mapOldOpportunity.get(opp.Id).Contact_Registration__c != null) {
                    setSObjectId.add(mapOldOpportunity.get(opp.Id).Contact_Registration__c);
                    hasContactRegistration = true;
                }
            }
            
            // Opportunity has Contact_Registration__c
            if (hasContactRegistration) {
                list<Contact_Registrations__c> listContactRegistration = new list<Contact_Registrations__c>();
                
                map<Id, Contact_Registrations__c> mapContactRegistration = new map<Id, Contact_Registrations__c>([SELECT Id, Total_Donation__c, Total_Soft_Credit__c FROM Contact_Registrations__c WHERE Id IN :setSObjectId]);
                
                // Dynamic SOQL query for Contact_Registration__c field on Opportunity
                string query;           
                query = 'SELECT Contact_Registration__c Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Contact_Registration__c != NULL AND Contact_Registration__c IN :setSObjectId GROUP BY Contact_Registration__c';            
                map<Id, Decimal> mapAggResult = aggResult(setSObjectId, query);                
                
                // Dynamic SOQL query for Contact_Registration__c field on Soft Credit
                string querySoftCredit;
                querySoftCredit = 'SELECT Contact_Registration__c Id, SUM(Amount__c) TotalDonation FROM Soft_Credit__c WHERE Contact_Registration__c != NULL AND Contact_Registration__c IN :setSObjectId GROUP BY Contact_Registration__c';
                map<Id, Decimal> mapAggResultSoftCredit = aggResult(setSObjectId, querySoftCredit);
                
                // Dynamic SOQL query for Contact_Registration__c field on Opportunity Income_Type__c=Registration Fee
                map<Id, Decimal> mapAggResultRegistrationFee;       
                string queryRegistrationFee;
                queryRegistrationFee = 'SELECT Contact_Registration__c Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Contact_Registration__c != NULL AND Income_Type__c = \'Registration Fee\' AND Contact_Registration__c IN :setSObjectId GROUP BY Contact_Registration__c';
                mapAggResultRegistrationFee = aggResult(setSObjectId, queryRegistrationFee);
                
                for (Contact_Registrations__c cr : mapContactRegistration.values()) {
                    cr.Total_Donation__c = mapAggResult.size()>0 ? mapAggResult.get(cr.Id) : null;
                    
                    // If Soft Credit records exists, add to Total_Donation__c
                    if (mapAggResultSoftCredit.size() > 0) {
                    	cr.Total_Soft_Credit__c = mapAggResultSoftCredit.get(cr.Id);
                    	                    	
                		cr.Total_Donation__c = cr.Total_Donation__c == null ? cr.Total_Soft_Credit__c : cr.Total_Donation__c + cr.Total_Soft_Credit__c;
                    }
                    
                    // If Contact Registration has Opportunity Income_Type__c="Registration Fee" then rollup amount
                    if (mapAggResultRegistrationFee.containsKey(cr.Id)) {
                        cr.Total_Registration_Fee__c = mapAggResultRegistrationFee.get(cr.Id);
                    }
  
                    listContactRegistration.add(cr);               
                }
                
                if (listContactRegistration.size() > 0) {                    
                    //TriggerByPass.ContactRegistration = true;
                    update listContactRegistration;
                }
            }
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** ' + OpportunityAmountRollup.class.getName() + ': contactRegistrationRollup' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, OpportunityAmountRollup.class.getName() + ': contactRegistrationRollup', errorMessage, 'HTML');
        }
    }
    
    private static void batchRollup () {
       try {
            boolean hasBatch = false;
            
            // Batch Id set for dynamic soql     
            set<Id> setSObjectId = new set<Id>();
            
            // Get all Batch_Number__c from Opportunities
            for (Opportunity opp : mapOpportunity.values()) {
                // update new record
                if (opp.Batch_Number__c != null) {                    
                    setSObjectId.add(opp.Batch_Number__c);
                    hasBatch = true;
                }
                // update old record
                if (mapOldOpportunity.get(opp.Id).Batch_Number__c != null) {
                    setSObjectId.add(mapOldOpportunity.get(opp.Id).Batch_Number__c);
                    hasBatch = true;
                }                
            }
            
            // Opportunity has Batch_Number__c
            if (hasBatch) {              
                list<Batch__c> listBatch = new list<Batch__c>();
                
                map<Id, Batch__c> mapBatch = new map<Id, Batch__c>([SELECT Id, Total_Donation__c FROM Batch__c WHERE Id IN :setSObjectId]);
                
                // Dynamic SOQL query for Batch_Number__c field on Opportunity
                string query;           
                query = 'SELECT Batch_Number__c Id, SUM(Amount) TotalDonation FROM Opportunity WHERE Batch_Number__c != NULL AND Batch_Number__c IN :setSObjectId GROUP BY Batch_Number__c';            
                map<Id, Decimal> mapAggResult = aggResult(setSObjectId, query);
                
                for (Batch__c bat : mapBatch.values()) {
                    bat.Total_Donation__c = mapAggResult.get(bat.Id);  
                    listBatch.add(bat);               
                }
                
                if (listBatch.size() > 0) {
                    update listBatch;                    
                }
            }
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** ' + OpportunityAmountRollup.class.getName() + ': batchRollup' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, OpportunityAmountRollup.class.getName() + ': batchRollup', errorMessage, 'HTML');
        }
    }  
    
    private static map<Id, Decimal> aggResult (set<Id> setSObjectId, string query) {
        list<SObject> listSObject = Database.query(query);
        map<Id, Decimal> mapSObject = new map<Id, Decimal>();
        
        if (listSObject.size() > 0) {
            for (SObject s : listSObject) {
                mapSObject.put((Id)s.get('Id'), (Decimal)s.get('TotalDonation'));
            }           
        }
        return mapSObject;
    }
}