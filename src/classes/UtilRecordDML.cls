public with sharing class UtilRecordDML {
/*
Mainly to be used on Visualforce page that have DML operations (Insert, Update and Delete) and to show error messages back to Visualforce page if occurs
Use [class name].class.getName() to pass in Current Class Name
*/
    private static list<string> emailAddress {
    	get {
    		if (emailAddress == null) {
    			emailAddress = new list<string>{[SELECT Id, Email__c FROM Email_Settings__c WHERE Name = 'IT Helpdesk'].Email__c};
    		}
    		return emailAddress;
    	}    
    }
    
    // Insert single record
	public static boolean dmlInsertRecord(SObject obj, string className) {
		list<SObject> listObj = new list<SObject>();
		listObj.add(obj);
		
		boolean result = insertUpdateRecord(listObj, 'insert', className);
		
		return result;
	}
	
	// Insert list of records
	public static boolean dmlInsertRecord(list<SObject> listObj, string className) {
		boolean result = insertUpdateRecord(listObj, 'insert', className);
		
		return result;
	}
	
    // Update single record
	public static boolean dmlUpdateRecord(SObject obj, string className) {
		list<SObject> listObj = new list<SObject>();
		listObj.add(obj);
		
		boolean result = insertUpdateRecord(listObj, 'update', className);
		
		return result;
	}
	
	// Update list of records
	public static boolean dmlUpdateRecord(list<SObject> listObj, string className) {
		boolean result = insertUpdateRecord(listObj, 'update', className);
		
		return result;
	}
	
    // Delete single record
	public static boolean dmlDeleteRecord(SObject obj, string className) {
		list<SObject> listObj = new list<SObject>();
		listObj.add(obj);
		
		boolean result = deleteRecord(listObj, className);
		
		return result;
	}
	
	// Delete list of records
	public static boolean dmlDeleteRecord(list<SObject> listObj, string className) {
		boolean result = deleteRecord(listObj, className);
		
		return result;
	}		
    
    private static boolean insertUpdateRecord (list<SObject> listObj, string dmlType, string className) {    
		boolean dmlSuccess = false;
		list<Messaging.SingleEmailMessage> listEmail = new list<Messaging.SingleEmailMessage>();
		
        // Optionally, set DML options here, use “DML” instead of “false” in the insert()
        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.allowSave = false;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;
        
        list<Database.SaveResult> listSaveResult = new list<Database.SaveResult>();
        
        if (dmlType == 'insert') {
        	listSaveResult = Database.insert(listObj, dml);
        }
        else if (dmlType == 'update') {
        	listSaveResult = Database.update(listObj, dml);
        }
      
		for (Database.SaveResult saveResult : listSaveResult) {
			if (!saveResult.isSuccess()) {
				for (Database.Error error : saveResult.getErrors()) {	
	         		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();         
	         		string subject = className + ': ' + dmlType +  ' DML Record Error';
	        		mail.setToAddresses(emailAddress);                      
	        		mail.setHtmlBody(string.valueof(error.getMessage()));
	        		mail.setSubject(subject);
	        		listEmail.add(mail);
	        		
		            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, string.valueof(error.getMessage()));
					ApexPages.addMessage(errorMessage);
							        		
					system.debug('***** ' + className + ': ' + dmlType +  ' DML Record Error: ' + error.getMessage());
				}
				dmlSuccess = false;
			}
			else {
				system.debug('***** ' + className + ': ' + dmlType +  ' DML Record Success');
				dmlSuccess = true;
			}			
		}
		
  		if (listEmail.size() > 0) {
    		Messaging.sendEmail(listEmail);
  		}      
 				
		return dmlSuccess;    	
    }
    
    private static boolean deleteRecord (list<SObject> listObj, string className) {
    
		boolean dmlSuccess = false;
		list<Messaging.SingleEmailMessage> listEmail = new list<Messaging.SingleEmailMessage>(); 
        
        list<Database.DeleteResult> listDeleteResult = new list<Database.DeleteResult>();
        
        listDeleteResult = Database.delete(listObj, true);
        
		for (Database.DeleteResult deleteResult : listDeleteResult) {
			if (!deleteResult.isSuccess()) {
				for (Database.Error error : deleteResult.getErrors()) {
	         		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();         
	         		string subject = className + ': Delete DML Record Error';
	        		mail.setToAddresses(emailAddress);                      
	        		mail.setHtmlBody(string.valueof(error.getMessage()));
	        		mail.setSubject(subject);
	        		listEmail.add(mail);

		            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, string.valueof(error.getMessage()));
					ApexPages.addMessage(errorMessage);	
	        		
					system.debug('***** ' + className + ': Delete DML Record Error: ' + error.getMessage());
				}
				dmlSuccess = false;
			}
			else {
				system.debug('***** ' + className + ': Delete DML Record Success');
				dmlSuccess = true;
			}			
		}
		
  		if (listEmail.size() > 0) {
    		Messaging.sendEmail(listEmail);
  		}         
 				
		return dmlSuccess;    	
    }    
}