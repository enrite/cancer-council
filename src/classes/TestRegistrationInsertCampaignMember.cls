@isTest
private class TestRegistrationInsertCampaignMember {

    static testMethod void myRegistrationInsertCampaignMember() {
        
        test.startTest();
        
        Account acc = new Account(name='Test Test', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com');
        insert acc;
        
        list<Contact> contactList = new list<Contact>();
        
        for (integer i=0; i<=10; i++) {
            Contact con = new Contact(AccountId=acc.Id, FirstName='Test Test' + i, LastName='Test Test' + i, MailingStreet='Test Test', MailingCity='Test Test'
                , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');
            contactList.add(con);   
        }
        insert contactList;
        
        Contact primaryCon = new Contact(AccountId=acc.Id, FirstName='Test Test', LastName='Test Test', MailingStreet='Test Test', MailingCity='Test Test'
            , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');
        insert primaryCon;      
        system.debug('***** ' + contactList.size());
        
        Campaign ca = new Campaign (Name='Test Parent', Campaign_Code__c='T Parent', Status = 'In Progress', IsActive=true);
        insert ca;
        Campaign ca1 = new Campaign (Name='Test Child', Campaign_Code__c='T Child', ParentId=ca.id, Status = 'In Progress', IsActive=true);     
        insert ca1;
        
        list<Task> taskList = new list<Task>();
        
        for (integer i=0; i<contactList.size(); i++) {
            Task t = new Task (WhoId=contactList.get(i).Id, Status='Completed', Mailout_Campaign_Id__c=ca.Id, Mailout_Segment_Id__c=ca1.Id);
            taskList.add(t);
        }       
        insert taskList;
        
        list<CampaignMember> cmList = new list<CampaignMember>();
        for (integer i=0; i<=contactList.size()/5; i++) {
            CampaignMember cm = new CampaignMember (CampaignId=ca1.Id, ContactId=contactList.get(i).Id);
            cmList.add(cm); 
        }       
        insert cmList;

        list<Registration__c> rList = new list<Registration__c>();
        
        for (integer i=0; i<contactList.size(); i++) {
            Registration__c r = new Registration__c();
            r.Campaign__c = ca.id;
            //r.Host_Team_Captain_Org__c = acc.id;
            r.Host_Team_Captain__c = contactList.get(i).id;
            r.Registration_Date__c = date.today();
            r.Status__c = 'Registered'; 
            rList.add(r);
        }       
        
        // test add campaign member (org primary contact) to parent campaign
        Registration__c r = new Registration__c();
        r.Campaign__c = ca1.id;
        r.Host_Team_Captain_Org__c = acc.id;
        r.Org_Primary_Contact__c = primaryCon.id;
        r.Registration_Date__c = date.today();
        r.Status__c = 'Registered'; 
        rList.add(r);
        
        // Test if Registration with Org only would throw error of 'Campaign Member Id is null'
        Registration__c r1 = new Registration__c();
        r1.Campaign__c = ca.id;
        r1.Host_Team_Captain_Org__c = acc.id;        
        r1.Registration_Date__c = date.today();
        r1.Status__c = 'Registered';
        rList.add(r1);
        
        insert rList;
        
        list<CampaignMember> l = [select Id, CampaignId from CampaignMember where CampaignId =: ca.Id or CampaignId =: ca1.Id];
        //202 of each Parent Campaign and Child Campaign segment plus 1 Primary Org
        //system.assertEquals(l.size(), 405);
        //system.assertEquals(l.size(), 3);
        
        test.stopTest();        
    }
}