global class TRExport
        extends TRImportBase
{
    public TRExport()
    {
    }

    public TRExport(ApexPages.StandardController ctlr)
    {
    }

    private final String MANUAL_UPDATE_REQUIRED = 'Manual update required';

    public Integer ConstituentErrors
    {
        get
        {
            if(ConstituentErrors == null) {
                setCounts();
            }
            return ConstituentErrors;
        }
        private set;
    }

    public Integer ConstituentSuccess
    {
        get
        {
            if(ConstituentSuccess == null) {
                setCounts();
            }
            return ConstituentSuccess;
        }
        private set;
    }

    public Integer ConstituentWaiting
    {
        get
        {
            if(ConstituentWaiting == null) {
                setCounts();
            }
            return ConstituentWaiting;
        }
        private set;
    }

    public Integer TeamErrors
    {
        get
        {
            if(TeamErrors == null) {
                setCounts();
            }
            return TeamErrors;
        }
        private set;
    }

    public Integer TeamSuccess
    {
        get
        {
            if(TeamSuccess == null) {
                setCounts();
            }
            return TeamSuccess;
        }
        private set;
    }

    public Integer TeamWaiting
    {
        get
        {
            if(TeamWaiting == null) {
                setCounts();
            }
            return TeamWaiting;
        }
        private set;
    }

    public Integer RegistrationErrors
    {
        get
        {
            if(RegistrationErrors == null) {
                setCounts();
            }
            return RegistrationErrors;
        }
        private set;
    }

    public Integer RegistrationSuccess
    {
        get
        {
            if(RegistrationSuccess == null) {
                setCounts();
            }
            return RegistrationSuccess;
        }
        private set;
    }

    public Integer RegistrationWaiting
    {
        get
        {
            if(RegistrationWaiting == null) {
                setCounts();
            }
            return RegistrationWaiting;
        }
        private set;
    }

    public Integer TransactionErrors
    {
        get
        {
            if(TransactionErrors == null) {
                setCounts();
            }
            return TransactionErrors;
        }
        private set;
    }

    public Integer TransactionSuccess
    {
        get
        {
            if(TransactionSuccess == null) {
                setCounts();
            }
            return TransactionSuccess;
        }
        private set;
    }

    public Integer TransactionWaiting
    {
        get
        {
            if(TransactionWaiting == null) {
                setCounts();
            }
            return TransactionWaiting;
        }
        private set;
    }

    public Integer DuplicateErrors
    {
        get
        {
            if(DuplicateErrors == null) {
                setCounts();
            }
            return DuplicateErrors;
        }
        private set;
    }

    public Integer DuplicateSuccess
    {
        get
        {
            if(DuplicateSuccess == null) {
                setCounts();
            }
            return DuplicateSuccess;
        }
        private set;
    }

    public Integer DuplicateWaiting
    {
        get
        {
            if(DuplicateWaiting == null) {
                setCounts();
            }
            return DuplicateWaiting;
        }
        private set;
    }

    private string emailAddress
    {
        get
        {
            return 'ithelpdesk@cancersa.org.au';
        }
    }

    public void reProcessCon()
    {
        try {
            throwTestError();
            TRImportContactBatch b = new TRImportContactBatch();
            b.RunThisOnly = true;
            System.scheduleBatch(b, 'TRImportContact', 0, Batch_Size__c.getInstance('TR_Import').Records__c.IntValue());
            showMessage('Constituent import has been queued.');
        }
        catch(Exception ex) {
            showError(ex);
            string errorMessage = 'Message: ' + ex.getMessage() + '<br/>' +
                    'Cause: ' + ex.getCause() + '<br/>' +
                    'Line Number: ' + ex.getLineNumber() + '<br/>' +
                    'Type Name: ' + ex.getTypeName() + '<br/>' +
                    'Stack Trace String: ' + ex.getStackTraceString();
            EmailUtil email = new EmailUtil(emailAddress, 'TRImportContact: ', errorMessage, 'HTML');
        }
    }

    public void reProcessTeam()
    {
        try {
            throwTestError();
            TRImportTeamBatch b = new TRImportTeamBatch();
            b.RunThisOnly = true;
            System.scheduleBatch(b, 'TRImportTeam', 0, Batch_Size__c.getInstance('TR_Import').Records__c.IntValue());
            showMessage('Team import has been queued.');
        }
        catch(Exception ex) {
            showError(ex);
            string errorMessage = 'Message: ' + ex.getMessage() + '<br/>' +
                    'Cause: ' + ex.getCause() + '<br/>' +
                    'Line Number: ' + ex.getLineNumber() + '<br/>' +
                    'Type Name: ' + ex.getTypeName() + '<br/>' +
                    'Stack Trace String: ' + ex.getStackTraceString();
            EmailUtil email = new EmailUtil(emailAddress, 'TRImportTeam: ', errorMessage, 'HTML');
        }
    }

    public void reProcessReg()
    {
        try {
            throwTestError();
            TRImportRegistrationBatch b = new TRImportRegistrationBatch();
            b.RunThisOnly = true;
            System.scheduleBatch(b, 'TRImportRegistration', 0, Batch_Size__c.getInstance('TR_Import').Records__c.IntValue());
            showMessage('Registration import has been queued.');
        }
        catch(Exception ex) {
            showError(ex);
            string errorMessage = 'Message: ' + ex.getMessage() + '<br/>' +
                    'Cause: ' + ex.getCause() + '<br/>' +
                    'Line Number: ' + ex.getLineNumber() + '<br/>' +
                    'Type Name: ' + ex.getTypeName() + '<br/>' +
                    'Stack Trace String: ' + ex.getStackTraceString();
            EmailUtil email = new EmailUtil(emailAddress, 'TRImportRegistration: ', errorMessage, 'HTML');
        }
    }

    public void reProcessTrans()
    {
        try {
            throwTestError();
            TRImportTransactionBatch b = new TRImportTransactionBatch();
            b.RunThisOnly = true;
            System.scheduleBatch(b, 'TRImportTransaction', 0, Batch_Size__c.getInstance('TR_Import').Records__c.IntValue());
            showMessage('Transaction import has been queued.');
        }
        catch(Exception ex) {
            showError(ex);
            string errorMessage = 'Message: ' + ex.getMessage() + '<br/>' +
                    'Cause: ' + ex.getCause() + '<br/>' +
                    'Line Number: ' + ex.getLineNumber() + '<br/>' +
                    'Type Name: ' + ex.getTypeName() + '<br/>' +
                    'Stack Trace String: ' + ex.getStackTraceString();
            EmailUtil email = new EmailUtil(emailAddress, 'TRImportTransaction: ', errorMessage, 'HTML');
        }
    }

    public void reProcessDuplicate()
    {
        try {
            throwTestError();
            TRImportDuplicateBatch b = new TRImportDuplicateBatch();
            b.RunThisOnly = true;
            System.scheduleBatch(b, 'TRImportDuplicate', 0, Batch_Size__c.getInstance('TR_Import').Records__c.IntValue());
            showMessage('Duplicate import has been queued.');
        }
        catch(Exception ex) {
            showError(ex);
            string errorMessage = 'Message: ' + ex.getMessage() + '<br/>' +
                    'Cause: ' + ex.getCause() + '<br/>' +
                    'Line Number: ' + ex.getLineNumber() + '<br/>' +
                    'Type Name: ' + ex.getTypeName() + '<br/>' +
                    'Stack Trace String: ' + ex.getStackTraceString();
            EmailUtil email = new EmailUtil(emailAddress, 'TRImportDuplicate: ', errorMessage, 'HTML');
        }
    }

    public void newExport()
    {
        try {
            throwTestError();
            TR_Export__c exportRecord = new TR_Export__c(Export_Time__c = DateTime.now());
            insert exportRecord;

            TRExportConstituentBatch batchInstance = new TRExportConstituentBatch(exportRecord);
            Database.executeBatch(batchInstance, Batch_Size__c.getInstance('TR_Export').Records__c.IntValue());

            showMessage('An export has been scheduled, you will be notified by email when it has completed.');
        }
        catch(Exception ex) {
            showError(ex);
        }
    }

    @RemoteAction
    public static String[] getExportAttachmentIds(String exportID)
    {
        List<String> attIds = new List<String>();
        // could just query attachment, but want to make sure we only set TR_Export__c attachments
        for(TR_Export__c ex : [
                SELECT ID, (
                        SELECT ID
                        FROM Attachments
                )
                FROM TR_Export__c
                WHERE ID = :exportID
        ]) {
            for(Attachment att : ex.Attachments) {
                attIds.add(att.ID);
            }
        }
        system.debug(attIds);
        return attIds;
    }

    private void setCounts()
    {
        ConstituentErrors = 0;
        ConstituentSuccess = 0;
        ConstituentWaiting = 0;
        TeamErrors = 0;
        TeamSuccess = 0;
        TeamWaiting = 0;
        RegistrationErrors = 0;
        RegistrationSuccess = 0;
        RegistrationWaiting = 0;
        TransactionErrors = 0;
        TransactionSuccess = 0;
        TransactionWaiting = 0;
        TransactionErrors = 0;
        TransactionSuccess = 0;
        TransactionWaiting = 0;
        DuplicateErrors = 0;
        DuplicateSuccess = 0;
        DuplicateWaiting = 0;

        for(TR_Import_Constituent__c t : [
                SELECT Processed__c,
                        Processing_Result__c
                FROM TR_Import_Constituent__c
                WHERE Processed__c = null OR
                Processing_Result__c = :MANUAL_UPDATE_REQUIRED
        ]) {
            if(t.Processing_Result__c == null) {
                ConstituentWaiting++;
            }
            else {
                ConstituentErrors++;
            }
        }
        for(TR_Import_Team__c t : [
                SELECT Processed__c,
                        Processing_Result__c
                FROM TR_Import_Team__c
                WHERE Processed__c = null OR
                Processing_Result__c = :MANUAL_UPDATE_REQUIRED
        ]) {
            if(t.Processing_Result__c == null) {
                TeamWaiting++;
            }
            else {
                TeamErrors++;
            }
        }
        for(TR_Import_Registration__c t : [
                SELECT Processed__c,
                        Processing_Result__c
                FROM TR_Import_Registration__c
                WHERE Processed__c = null OR
                    Processing_Result__c = :MANUAL_UPDATE_REQUIRED
        ]) {
            if(t.Processing_Result__c == null) {
                RegistrationWaiting++;
            }
            else {
                RegistrationErrors++;
            }
        }
        for(TR_Import_Transaction__c t : [
                SELECT Processed__c,
                        Processing_Result__c
                FROM TR_Import_Transaction__c
                WHERE Processed__c = null OR
                Processing_Result__c = :MANUAL_UPDATE_REQUIRED
        ]) {
            if(t.Processing_Result__c == null) {
                TransactionWaiting++;
            }
            else {
                TransactionErrors++;
            }
        }
        for(TR_Import_Duplicate__c t : [
                SELECT Processed__c,
                        Processing_Result__c
                FROM TR_Import_Duplicate__c
                WHERE Processed__c = null OR
                Processing_Result__c = :MANUAL_UPDATE_REQUIRED
        ]) {
            if(t.Processing_Result__c == null) {
                DuplicateWaiting++;
            }
            else {
                DuplicateErrors++;
            }
        }
    }
}