public with sharing class QuitLineFormController extends RecaptchaController {

    // the name of the referral query string parameter
    private static final String PARAM_REFERRAL = 'referral';

    // Error to be displayed when a field is required.
    private static final String REQUIRED_FIELD = 'Field is required';

    // Error displayed if the phone and mobile phone fields are both not supplied.
    private static final String REQUIRED_PHONE = 'Field is required';

    // list containing all the preferred days
    private static final List<SelectOption> PREFERRED_DAYS;

    // list containing all the best time to call options valid for this form.
    private static final List<SelectOption> BEST_TIME_TO_CALL;

    // list containing all the ATSI OPTIONS
    private static final List<SelectOption> ATSI_OPTIONS;

    // list of yes no options
    private static final List<SelectOption> YES_NO;

    private static final List<SelectOption> HEAR_ABOUT_QUITLINE;

    private static final List<SelectOption> GENDER;

    private static final List<SelectOption> INTEPRETER_REQUIRED;

    private static final List<SelectOption> LANGUAGES;


    private static final String DATE_OF_BIRTH_FUTURE = 'Date of birth cannot be a future date';

    private static final String TOO_OLD_ERROR = 'It is very unlikely that you are 130+ years old. If you are good on you.';

    // static initialisation block populated ref data used by the form.
    static {

        PREFERRED_DAYS = new List<SelectOption>();
        PREFERRED_DAYS.add(new SelectOption('Monday','Monday'));
        PREFERRED_DAYS.add(new SelectOption('Tuesday','Tuesday'));
        PREFERRED_DAYS.add(new SelectOption('Wednesday','Wednesday'));
        PREFERRED_DAYS.add(new SelectOption('Thursday','Thursday'));
        PREFERRED_DAYS.add(new SelectOption('Friday','Friday'));
        PREFERRED_DAYS.add(new SelectOption('Saturday','Saturday'));
        PREFERRED_DAYS.add(new SelectOption('Any day','Any day'));


        BEST_TIME_TO_CALL = new List<SelectOption>();
        BEST_TIME_TO_CALL.add(new SelectOption('Morning', 'Morning'));
        BEST_TIME_TO_CALL.add(new SelectOption('Afternoon', 'Afternoon'));
        BEST_TIME_TO_CALL.add(new SelectOption('Evening', 'Evening'));
        BEST_TIME_TO_CALL.add(new SelectOption('Any time', 'Any time'));

        ATSI_OPTIONS = new List<SelectOption>();
        ATSI_OPTIONS.add(new SelectOption('Non Indigenous','Non Indigenous'));
        ATSI_OPTIONS.add(new SelectOption('Yes, Aboriginal','Yes, Aboriginal'));
        ATSI_OPTIONS.add(new SelectOption('Yes, Torres Strait Islander','Yes, Torres Strait Islander'));
        ATSI_OPTIONS.add(new SelectOption('Yes, both','Yes, both'));

        YES_NO = new List<SelectOption>();
        YES_NO.add(new SelectOption('Yes', 'Yes'));
        YES_NO.add(new SelectOption('No', 'No'));

        HEAR_ABOUT_QUITLINE = new List<SelectOption>();
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Cigarette pack', 'Cigarette pack'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Community services worker', 'Community services worker'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Event', 'Event'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Health professional', 'Health professional'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Online advertising', 'Online advertising'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Radio', 'Radio'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('TV', 'TV'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Youtube', 'Youtube'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Website', 'Website'));
        HEAR_ABOUT_QUITLINE.add(new SelectOption('Other', 'Other'));

        GENDER = new List<SelectOption>();
        GENDER.add(new SelectOption('Male', 'Male'));
        GENDER.add(new SelectOption('Female', 'Female'));
        INTEPRETER_REQUIRED = new List<SelectOption>();
        INTEPRETER_REQUIRED.add(new SelectOption('Required', 'Yes'));
        INTEPRETER_REQUIRED.add(new SelectOption('Not Required', 'No'));

        LANGUAGES = getPickValues(new Case(), 'CALD_Language__c', '', '-- None --');

    }

    // The case to store the details against
    public Case QuitLineCase { get; set; }

    // flag indicating whether the referral quitline request was a referral. This is set by the query string parameter
    public boolean IsReferral  { get; set;}

    // the other comments field
    public String Comments { get; set; }

    public boolean Submitted {get;set;}
    /**
     * Default constructor
     */
    public QuitLineFormController() {
        initForm();
        if(ApexPages.currentPage().getParameters().get('render') == 'pdf')
        {
            Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename=Quitline.pdf');
        }
    }

    /**
     * This method initialises the form data
     */
    private void initForm() {
        try {
            // create a new instance of case
            QuitLineCase = new Case();

            QuitLineCase.RecordTypeId = getQuitLineCaseRecordType().Id;
            QuitLineCase.Origin = 'Web';

            IsReferral =  ApexPages.currentPage().getParameters().containsKey(PARAM_REFERRAL);
            if (IsReferral) {
                QuitLineCase.Report_Progress_to_Referrer__c = 'Yes';
                QuitLineCase.Origin = 'Online Referral';
            }
            ValidCaptcha = true;
        } catch (Exception ex) {
            CustomException.formatException(ex);
        } 
    }

    public Boolean ValidCaptcha {get;set;}
    /**
     * This method when the save/submit button is clicked on the form.
     */
    public PageReference saveOverride() {
        try {
            Submitted = true;
            if (validateForm()) {
                ValidCaptcha = verifyCaptcha();
                if (!ValidCaptcha) {
                    CustomException.formatException('Please verify you are not a robot.');
                    return null;
                }
                QuitLineCase.Nicotene_Dependence_Rating_Hidden__c = null;
                if (QuitLineCase.Interpreter__c == null) {
                    QuitLineCase.Interpreter__c = '';
                }
                QuitLineCase.Unique_Id__c = generateUniqueId();
                insert QuitLineCase;
                // create a case comment if we have one supplied
//                if (!String.isBlank(Comments)) {
//                    CaseComment cm = new CaseComment();
//                    cm.CommentBody = Comments;
//                    cm.ParentId = QuitLineCase.Id;
//                    insert cm;
//                }
                // redirect to the success page.
                PageReference pg = Page.QuitlineFormSuccess;
                pg.getParameters().put('id', QuitLineCase.Unique_Id__c);
                return pg.setRedirect(true);
            }
        } catch (Exception e) {
            CustomException.formatException(e);
        }

        return null;
    }

    public PageReference getCase()
    {
        String uniqueId = ApexPages.currentPage().getParameters().get('id');
        QuitLineCase = new Case();
        IsReferral = false;
        for(Case c : [SELECT ID,
                            First_Name__c,
                            Last_Name__c,
                            Contact_Street__c,
                            Suburb__c,
                            State__c,
                            Contact_Postcode__c,
                            Phone__c,
                            Mobile__c,
                            Email__c,
                            Gender__c,
                            D_O_B__c,
                            ATSI__c,
                            Interpreter__c,
                            CALD_Language__c,
                            CALD_Languages_other__c,
                            Language_Spoken__c,
                            Preferred_Day__c,
                            Preferred_Time_s__c,
                            Date_Of_First_Call__c,
                            Report_Progress_to_Referrer__c,
                            Referrer_Title__c,
                            Referrer_Name__c,
                            Referrer_Last_Name__c,
                            Referrer_Street__c,
                            Referrer_Suburb__c,
                            Referrer_State__c,
                            Referrer_Postcode__c,
                            Referrer_Phone__c,
                            Referrer_Email__c,
                            Unique_ID__c
                    FROM    Case
                    WHERE   Unique_Id__c = :uniqueId
                                //AND CreatedDate >= :Datetime.now().addMinutes(-5)
                            ])
        {
            QuitLineCase = c;
            IsReferral = c.Referrer_Last_Name__c != null;
        }
        return null;
    }

    /**
     * This method performs any validation that cannot be performed within validation rules etc.
     * @return true if the form is valid otherwise false will be returned.
     */
    private boolean validateForm() {
        ValidCaptcha = true;
        final boolean registrationValid = validateRegisterForm();
        final boolean referralDetailsValid = (IsReferral) ? validateReferralDetails() : true;
        return registrationValid && referralDetailsValid;
    }


    public Integer NumberErrors {
        get {
            return ApexPages.getMessages().size();
        }
    }
    /**
     * This method validates the registration form. If there are no validation errors then true is returned. If there are
     * validation errors then false is returned and the relevant errors are displayed.
     * @return true if the form is valid otherwise false is returned.
     */
    private boolean validateRegisterForm() {
        boolean isValid = true;

        if (String.isBlank(QuitLineCase.First_Name__c)) {
            QuitLineCase.First_Name__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(QuitLineCase.Last_Name__c)) {
            QuitLineCase.Last_Name__c.addError(REQUIRED_FIELD);
            isValid = false;
        }


        if (String.isBlank(QuitLineCase.Contact_Postcode__c)) {
            QuitLineCase.Contact_Postcode__c.addError(REQUIRED_FIELD);
            isValid = false;
        }


        if (!String.isBlank(QuitLineCase.Preferred_Day__c) && !String.isBlank(QuitLineCase.Preferred_Time_s__c)) {
            if (QuitLineCase.Preferred_Day__c == 'Saturday' && QuitLineCase.Preferred_Time_s__c != 'Afternoon') {
                QuitLineCase.Preferred_Time_s__c.addError('Afternoon can only be selected on Saturdays');
                isValid = false;
            }
        }

        if (!String.isBlank(QuitLineCase.Phone__c) && !isValidPhone(QuitLineCase.Phone__c)) {
            QuitLineCase.Phone__c.addError(RecaptchaController.INVALID_PHONE);
            isValid = false;
        }

        return isValid;
    }

    /**
     * This method validates the referral specific fields. If the form is valid then true is returned, otherwise false is returned.
     * @return true if the form is valid otherwise false is returned.
     */
    private boolean validateReferralDetails() {
        boolean isValid = true;

        if (String.isBlank(QuitLineCase.Referrer_Name__c)) {
            QuitLineCase.Referrer_Name__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(QuitLineCase.Referrer_Last_Name__c)) {
            QuitLineCase.Referrer_Last_Name__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(QuitLineCase.Referrer_Organisation__c)) {
            QuitLineCase.Referrer_Organisation__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(QuitLineCase.Referrer_Phone__c)) {
            QuitLineCase.Referrer_Phone__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(QuitLineCase.Referrer_Street__c)) {
            QuitLineCase.Referrer_Street__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(QuitLineCase.Referrer_Suburb__c)) {
            QuitLineCase.Referrer_Suburb__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(QuitLineCase.Referrer_State__c)) {
            QuitLineCase.Referrer_State__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(QuitLineCase.Referrer_Postcode__c)) {
            QuitLineCase.Referrer_Postcode__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        final boolean interpretterRequired =  !String.isBlank(QuitLineCase.Interpreter__c)
                && QuitLineCase.Interpreter__c == 'Required';

        if (interpretterRequired && String.isBlank(QuitLineCase.CALD_Language__c)) {
            QuitLineCase.CALD_Language__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (interpretterRequired && QuitLineCase.CALD_Language__c == 'Other' && String.isBlank(QuitLineCase.CALD_Languages_other__c)) {
            QuitLineCase.CALD_Languages_other__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (!String.isBlank(QuitLineCase.Referrer_Phone__c) && !isValidPhone(QuitLineCase.Referrer_Phone__c)) {
            QuitLineCase.Referrer_Phone__c.addError(RecaptchaController.INVALID_PHONE);
            isValid = false;
        }

        return isValid;
    }


    /**
     * @return the quitline record type for the case or null if a match is not found.
     */
    private RecordType getQuitLineCaseRecordType() {
        final List<RecordType> recordTypes = [SELECT Id, Name
                           FROM RecordType
                           WHERE sObjectType = 'Case' 
                           AND Name='Quitline'];

        return (recordTypes.isEmpty()) ? null : recordTypes.get(0);
    }

    /**
     * This method returns the Preferred Days options for the quitline form
     * @return a list containing all the valid preferred days options.
     */
    public List<SelectOption> PreferredDays {
        get { return PREFERRED_DAYS; }
    }

    /**
     * This method returns a list containing all the valid options for the best times to call.
     * @return a list containing all the best time to call.
     */
    public List<SelectOption> BestTimeToCall {
        get { return BEST_TIME_TO_CALL; }
    }

    /**
     * This method returns a list containing all the valid options for the ATSI question.
     * @return a list containg the ATSI options
     */
    public List<SelectOption> AtsiOptions {
        get { return ATSI_OPTIONS; }
    }

    public List<SelectOption> YesNoOptions {
        get { return YES_NO; }
    }

    public List<SelectOption> HowDidYouHearAboutQuitline {
        get { return HEAR_ABOUT_QUITLINE; }
    }

    public List<SelectOption> Genders {
        get {return GENDER;}
    }

    public List<SelectOption> InterpreterRequired {
        get {return INTEPRETER_REQUIRED;}
    }

    public List<SelectOption> InterpreterLanguages {
        get {return LANGUAGES;}
    }

    public static List<SelectOption> getPickValues(Sobject object_name, String field_name, String first_val, String firstValDescription) {
          List<SelectOption> options = new List<SelectOption>(); //new list for holding all of the picklist options
          if (first_val != null) { //if there is a first value being provided
             options.add(new selectOption(first_val, firstValDescription)); //add the first option
          }
          Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
          Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
          Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
          List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
          for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
                      
                options.add(new SelectOption(a.getValue(), a.getLabel())); //add the value and label to our final list
          }
          return options; //return the List
    }

    private String generateUniqueId()
    {
        // use two hashes to make it a longer string
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(String.valueOf(Crypto.getRandomLong())));
        Blob hash2 = Crypto.generateDigest('MD5', Blob.valueOf(String.valueOf(Crypto.getRandomLong())));
        return EncodingUtil.convertToHex(hash) + EncodingUtil.convertToHex(hash2);
    }
}