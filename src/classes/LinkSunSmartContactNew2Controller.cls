public with sharing class LinkSunSmartContactNew2Controller{
    private SunSmart__c[] mSunSmart;
    private Contact mContact;
    String mSunSmartID;
    String mContactID;
    
    public LinkSunSmartContactNew2Controller(){

        mSunSmartID = ApexPages.CurrentPage().getParameters().get('id');
        mContactID = ApexPages.CurrentPage().getParameters().get('Contactid');
        
        mSunSmart = [SELECT
            id,
            Web_Postcode__c,
            Web_State__c,
            Web_Street__c,
            Web_Suburb_Town__c,
            First_Name__c,
            Last_Name__c,
            School_Contact_Email__c,
            School_Email__c,
            School_Name__c,
            School_Phone__c,
            School__c,
            Title__c
            FROM SunSmart__c
            WHERE id = :mSunSmartID];
        
        mContact = [SELECT
            id,
            FirstName,
            LastName
            FROM Contact
            WHERE id = :mContactID];
        
        
    }
    
    public PageReference save(){
        mSunSmart[0].Key_Contact__c = mContact.id;
        update mSunSmart[0];
        return new PageReference('/' + mSunSmartID);
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mSunSmart[0].id);
    }
    
    public SunSmart__c getSunSmart(){
        return mSunSmart.size() > 0 ? mSunSmart[0] : null;
    }
    
    public Contact getContact(){
        return mContact;
    }
}