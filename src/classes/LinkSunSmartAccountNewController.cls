public with sharing class LinkSunSmartAccountNewController{
    private SunSmart__c[] mSunSmart;
    private Account mAccount;
    String mSunSmartID;
    
    public LinkSunSmartAccountNewController(){

        mSunSmartID = ApexPages.CurrentPage().getParameters().get('id');
        
        mSunSmart = [SELECT
            id,
            Web_Postcode__c,
            Web_State__c,
            Web_Street__c,
            Web_Suburb_Town__c,
            School_Email__c,
            School_Name__c,
            School_Phone__c
            FROM SunSmart__c
            WHERE id = :mSunSmartID];
        
        mAccount = new Account();
        if(mSunSmart.size() > 0){
            mAccount.Name = mSunSmart[0].School_Name__c;
            mAccount.RecordTypeId = '01280000000UDUp'; //Organisation
            mAccount.BillingStreet = mSunSmart[0].Web_Street__c;
            mAccount.BillingPostalCode = mSunSmart[0].Web_Postcode__c;
            mAccount.BillingState = mSunSmart[0].Web_State__c;
            mAccount.BillingCity = mSunSmart[0].Web_Suburb_Town__c;
            mAccount.Phone = mSunSmart[0].School_Phone__c;
            mAccount.Email__c = mSunSmart[0].School_Email__c;
            mAccount.AccountSource = 'SunSmart';
            mAccount.Organization_Category__c = 'Education';
        }
        
    }
    
    public PageReference save(){
        insert mAccount;
        mSunSmart[0].School__c = mAccount.id;
        update mSunSmart[0];
        return new PageReference('/' + mSunSmartID);
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mSunSmart[0].id);
    }
    
    public SunSmart__c getRegistration(){
        return mSunSmart.size() > 0 ? mSunSmart[0] : null;
    }
    
    public Account getAccount(){
        return mAccount;
    }
}