public with sharing class RegistrationInsertCampaignMember {
        
    public static void returnCampaignMember(list<Registration__c> rego, map<Id, Id> campaignMap) {
        // list to merge 'Host Team Captain' and 'Org Primary Contact' into single field 'Host Team Captain'
        list<Registration__c> registrationList = new list<Registration__c>();
        
        // set list to hold unique Contact and Campaign Ids for SOQL query
        set<Id> campaignSet = new set<Id>();
        set<Id> contactSet = new set<Id>();
        
        list<CampaignMember> cmList = new list<CampaignMember>();
        list<Campaign> campList = new list<Campaign>();

        //to prevent duplicate Contact and Campaign ids inserted to cause errors
        set<string> uniqueCampaignMemberInserted = new set<string>();
                
        // send email alert when exception thrown
        EmailUtil email;
        List<String> recipients = new list<String>{'ithelpdesk@cancersa.org.au'};
        string replyTo = 'ithelpdesk@cancersa.org.au';
        
        //variables to hold common information
        Id campaignId;
        Id contactId;
        Id registrationId;
        
        try {
            // loop through values passed from Registration trigger.new
            // to combine both 'Host Team Captain' and 'Org Primary Contact' into 'Host Team Captain'
            // to make sure Campaign Member Contact field is not null
            for (integer i = 0; i < rego.size(); i++) {
                contactId = null;
                if (rego.get(i).Host_Team_Captain__c != null) {                 
                    contactId = rego.get(i).Host_Team_Captain__c;
                }
                if (rego.get(i).Org_Primary_Contact__c != null) {                   
                    contactId = rego.get(i).Org_Primary_Contact__c;
                }
                
                if (contactId != null) {
                    campaignId = rego.get(i).Campaign__c;
                    registrationId = rego.get(i).Id;
                
                    Registration__c r = new Registration__c (Id = registrationId);
                    r.Campaign__c = campaignId; 
                    r.Host_Team_Captain__c = contactId;
                
                    registrationList.add(r);
                
                    //set lists used for retrieving Task records
                    campaignSet.add(campaignId);
                    contactSet.add(contactId);
                
                    // add primary campaign (not mailout segment/child campaign) to campaign member whenever Registration to Campaign is created
                    CampaignMember cm = new CampaignMember();
                    cm.CampaignId = campaignId;         
                    cm.Registration__c = registrationId;
                    cm.ContactId = contactId;
                    cmList.add(cm);                 
                    
                    // add campaign member to parent campaign (if any)
                    if (campaignMap.get(campaignId)!=null) {
                        CampaignMember cm1 = new CampaignMember();
                        cm1.CampaignId = campaignMap.get(campaignId);           
                        cm1.Registration__c = registrationId;
                        cm1.ContactId = contactId;
                        cmList.add(cm1);                        
                    }
                }
            }
            
            // loop through Task records in order to map Registration records to Task 'Mailout Segment' child campaign          
            for (Task t : [select WhoId, Mailout_Campaign_Id__c, Mailout_Segment_Id__c from Task where WhoId in : contactSet and Mailout_Campaign_Id__c in : campaignSet]) {
                for (integer i = 0; i < registrationList.size(); i++) {                 
                    if (t.WhoId == registrationList.get(i).Host_Team_Captain__c && !uniqueCampaignMemberInserted.contains(t.WhoId + t.Mailout_Segment_Id__c)) {                                     
                        CampaignMember cmSegment = new CampaignMember();
                        cmSegment.CampaignId = t.Mailout_Segment_Id__c;
                        cmSegment.ContactId = t.WhoId;
                        cmSegment.Registration__c = registrationList.get(i).Id;
                        cmList.add(cmSegment);
                        uniqueCampaignMemberInserted.add(t.WhoId + t.Mailout_Segment_Id__c);
                    }
                }
            }
            database.Saveresult[] saveResult = database.insert(cmList, false);
            
            // check if errors occured during insert process and send email to alert
            for (integer i = 0; i < saveResult.size(); i++) {
                //ignore error message of trying to insert duplicated campaign member
                if (!saveResult[i].isSuccess() && saveResult[i].getErrors()[0].getMessage()!= 'This entity is already a member of this campaign' && !saveResult[i].getErrors()[0].getMessage().contains('Attempted to add an entity')) {
                    string bodyMsg = 'RE: Error message during saving: ' + saveResult[i].getErrors()[0].getMessage() + '\n' + 'Added by ' + UserInfo.getUserName() + '\n' + 'Id ' + saveResult[i].getId();                 
                    email = new EmailUtil(recipients, replyTo, 'RE: Insert Campaign Member Errors From Registration', bodyMsg, 'Plain');
                    System.debug('***** Error: could not create sobject ' + 'for array element ' + i + '.');
                    System.debug('***** The error reported was: ' + saveResult[i].getErrors()[0].getMessage() + '\n');              
                }
            }
        }
        catch (exception e) {
            email = new EmailUtil(recipients, replyTo, 'Errors thrown - Campaign Member From Registration', e.getMessage(), 'Plain');
            system.debug('***** ' + e.getMessage());
        }       
    }
}