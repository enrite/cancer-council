public class BPAYNum {

    private static string emailAddress {
        get {
            return 'ithelpdesk@cancersa.org.au';
        }    
    }
    
    public static void generateBpayNum(string objectName, string template, map<Id, sObject> newRecord) {
        try {   
            list<sObject> listSObjectUpdate = new list<sObject>();
            
            // Store Custom Field Name for Account, Contact or other objects that do not use Name as auto generated id
            map<string, string> mapConvertField = new map<string, string>{'Contact'=>'Contact_Id__c'
                                                                          , 'Account'=>'Org_Id__c'
                                                                          , 'Opportunity'=>'Receipt_Number__c'};
            
            for (sObject newObject : newRecord.values()) {
                // Get field name of auto generated id
                string convertField = (mapConvertField.get(objectName) != null) ? mapConvertField.get(objectName) : 'Name';

                // Split Auto Generated Id field by splitting '-' into list 
                list<string> listId = string.valueOf(newObject.get(convertField)).split('-');

                for (string s : listId) {
                    // Id need to be less than and equal to 8 charaters before adding leading digit and check digit
                    if (s.isNumeric() && s.length() <= 8) {
                        // To create new sObject using specified ObjectName type with given Id, for updating record
                        schema.sObjectType objectType = schema.getGlobalDescribe().get(objectName);
                        sObject newObj = objectType.newSObject(newObject.Id);
                        
                        // Update calculated BPAY Number to current record
                        newObj.put('BPay_Number__c', calculateBpayNum(s, template));

                        listSObjectUpdate.add(newObj);                  
                    }
                }
            }
            TriggerByPass.BPAYNum = true; 
            update listSObjectUpdate;
        }
        catch (exception e) {
            system.debug('***** Error BPAYNum generateBpayNum: ' + e.getMessage());
            EmailUtil email = new EmailUtil(emailAddress, 'BPAYNum generateBpayNum: ', e.getMessage(), 'Plain'); 
        }
    }
    
    private static string calculateBpayNum(string rawNum, string template) {
        /* 
            - Brief explanation of Luhn Algorithm used here
            - For example 9 digits of rawNum = 800712059
            - Final bpayCode should be 10 digits 800712059x (x is check-digit) and loop each digit from right to left
            - Multiply 2 to digit of even position index from right to left (800712059x), so should be 9*2, 0*2, 1*2, 0*2, 8*2
            - If result is 10 or more after multiplied, need to separate the result and add up to get added up number, eg 9*2=18->1+8=9, 8*2=16->1+6=7, 2*2=4->4
            - Sum up every digit/result from loop 8(*2) + 0 + 0(*2) + 7 + 1(*2) + 2 + 0(*2) + 5 + 9(*2)
            - 8: 8*2=16->7
            - 0:       ->0
            - 0: 0*2=0 ->0
            - 7:       ->7
            - 1: 1*2=2 ->2
            - 2:       ->2
            - 0: 0*5=0 ->0
            - 5:       ->5
            - 9: 9*2=18->9
            - x
            - Total    ->32
            - Multiply total result by 9 and then modulus result by 10, eg 32*9=288, 288%10=8
            - 8 is the final check-digit, final bpayCode = 8007120598
        */ 
        string bpayCode = '';
        
        try {
            integer sum = 0;
            integer checkSum = 0;
            
            // Get remainderLength out of 10 digits
            integer remainderLength = 10 - rawNum.length() - 1;
            
            // Set Bpay number to first 9 digits
            bpayCode = template.left(remainderLength) + string.valueOf(rawNum);         
            
            // Loop from right to left
            for (integer i = bpayCode.length() - 1; i >= 0 ; i--) {             
                integer currDigit = integer.valueOf(bpayCode.substring(i, i+1));
                
                // If is even index position (considereing check-digit is added to right most), value then multiply 2 to currDigit
                if (math.mod(i, 2) == 0) {
                    currDigit = currDigit * 2;              
                }
                
                // If currDigit greater than 9 then minus 9 from it otherwise just sum up
                // Another way to explain is when currDigit is 10 or more then need to sum up like 10:1+0=1, 11:1+1=2, 18:1+8=9
                sum += (currDigit > 9) ? currDigit - 9 : currDigit;
            }
            
            // For final result just multiply by 9 then modulus by 10 and remainder digit is checkSum
            // eg sum=32: 32*9=288->288%10=8
            checkSum = math.mod(sum * 9, 10);
            
            bpayCode = bpayCode + checkSum;
            system.debug('***** input value: ' + template.left(remainderLength) + string.valueOf(rawNum) + '\t final checksum: ' + checkSum + '\t bpayCode: ' + bpayCode );         
        }
        catch (exception e) {
            system.debug('***** Error BPAYNum calculateBpayNum: ' + e.getMessage());
            EmailUtil email = new EmailUtil(emailAddress, 'BPAYNum calculateBpayNum: ', e.getMessage(), 'Plain'); 
        }
        return bpayCode;    
    }
}