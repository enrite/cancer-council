@isTest
public class TestLinkExistingEventLocationsExtension
{
    public static testMethod void myUnitTest()
    {                
        Campaign c = new Campaign(Name = 'Test');
        insert c;                
        
        RecordType rt = [SELECT Id
                         FROM RecordType
                         WHERE sObjectType = 'Account'
                         AND Name = 'Event Location'];
        
        Account e = new Account(Name = 'Test', RecordTypeId = rt.Id);
        insert e;
        
        LinkExistingEventLocationsExtension ext = new LinkExistingEventLocationsExtension(new ApexPages.StandardController(c));
        system.assert(ext.EventLocations != null);
        ext.EventLocations[0].Checked = true;        
        ext.Link();
    }
}