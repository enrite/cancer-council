public with sharing class SoftCreditAmountRollup {
    private static string emailAddress {
        get {
            if (emailAddress == null) {
                emailAddress = [SELECT Id, Email__c FROM Email_Settings__c WHERE Name = 'IT Helpdesk'].Email__c;
            }
            return emailAddress;
        }
    }
    
    private static map<Id, Soft_Credit__c> mapSoftCredit {get;set;}
    private static map<Id, Soft_Credit__c> mapOldSoftCredit {get;set;}
    private static map<Id, Opportunity> mapOpportunity {
    	get {
    		if (mapOpportunity == null) {
    			mapOpportunity = new map<Id, Opportunity>();
    		}
    		return mapOpportunity;
    	}
    	set;
    }
    private static map<Id, Opportunity> mapOldOpportunity {get;set;}
    
    public static void totalAmountRollup(map<Id, Soft_Credit__c> mapRecord, map<Id, Soft_Credit__c> mapOldRecord) {
        mapSoftCredit = mapRecord;
        mapOldSoftCredit = mapOldRecord;
        
        if (mapSoftCredit.size() > 0) {
            registrationRollup();
            contactRegistrationRollup();
        }
    }

    public static void totalAmountRollup(map<Id, Soft_Credit__c> mapRecord, map<Id, Soft_Credit__c> mapOldRecord, map<Id, Opportunity> mapOpportunityRecord, map<Id, Opportunity> mapOldOpportunityRecord) {
        mapOpportunity = mapOpportunityRecord;
        mapOldOpportunity = mapOldOpportunityRecord;
        
        totalAmountRollup(mapRecord, mapOldRecord);
    }
   
    private static void registrationRollup () {
        try {
            boolean hasRegistration = false;
            
            // Registration Id set for dynamic soql     
            set<Id> setSObjectId = new set<Id>();            
            set<Id> setOpportunity = new set<Id>();
            
            for (Soft_Credit__c sc : mapSoftCredit.values()) {
            	setOpportunity.add(sc.Opportunity__c);
            }
            
            for (Soft_Credit__c sc : mapOldSoftCredit.values()) {
            	setOpportunity.add(sc.Opportunity__c);
            }            
            
            // Check if mapSoftCredit contains field Opportunity Registration
            for (Opportunity opp : [SELECT Id, Registration__c FROM Opportunity WHERE Id IN :setOpportunity]) {
                if (opp.Registration__c != null) {
                    setSObjectId.add(opp.Registration__c);
                    hasRegistration = true;
                }
            }
            
            // Opportunity has Registration
            if (hasRegistration) {
                list<Registration__c> listRegistration = new list<Registration__c>();
                
                map<Id, Registration__c> mapRegistration = new map<Id, Registration__c>([SELECT Id, Total_Soft_Credit__c FROM Registration__c WHERE Id IN :setSObjectId]);
                
                // Dynamic SOQL query for Registration field on Opportunity
                string query;
                query = 'SELECT Opportunity__r.Registration__c Id, SUM(Amount__c) TotalSoftCredit FROM Soft_Credit__c WHERE Opportunity__r.Registration__c IN :setSObjectId GROUP BY Opportunity__r.Registration__c';            
                map<Id, Decimal> mapAggResult = aggResult(setSObjectId, query);
                
                for (Registration__c reg : mapRegistration.values()) {
                    reg.Total_Soft_Credit__c = mapAggResult.get(reg.Id);
                    listRegistration.add(reg);               
                }
                
                if (listRegistration.size() > 0) {
                	//TriggerByPass.Registration = true;
                    update listRegistration;                    
                }
            }
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** ' + OpportunityAmountRollup.class.getName() + ': registrationRollup' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, OpportunityAmountRollup.class.getName() + ': registrationRollup', errorMessage, 'HTML');
        }
    }
   
    private static void contactRegistrationRollup () {
        try {
            boolean hasContactRegistration = false;
            
            // Contact Registration Id set for dynamic soql     
            set<Id> setSObjectId = new set<Id>();
            // To get Opportunity record that Soft Credit linked to
            set<Id> setOpportunityId = new set<Id>();
            
            // Get Contact Registration information from Opportunity
            // Check if Opportunity contains field Contact Registration to continue rollup process
            if (mapOpportunity.size() > 0) {
            	for (Opportunity opp : mapOpportunity.values()) {
            		if (opp.Contact_Registration__c != null) {
            			setOpportunityId.add(opp.Id);
            			setSObjectId.add(opp.Contact_Registration__c);
            			hasContactRegistration = true;
            		}
            		if (mapOldOpportunity.get(opp.Id).Contact_Registration__c != null) {
            			setOpportunityId.add(mapOldOpportunity.get(opp.Id).Id);
            			setSObjectId.add(mapOldOpportunity.get(opp.Id).Contact_Registration__c);
            			hasContactRegistration = true;
            		}            		
            	}
            }
            
            // Check if mapSoftCredit contains field Contact Registration to continue rollup process
            for (Soft_Credit__c sc : mapSoftCredit.values()) {
                // Update new record
                if (sc.Contact_Registration__c != null) {
                    setSObjectId.add(sc.Contact_Registration__c);
                    hasContactRegistration = true;
                }
                // Update old record
                if (mapOldSoftCredit.get(sc.Id).Contact_Registration__c != null) {
                    setSObjectId.add(mapOldSoftCredit.get(sc.Id).Contact_Registration__c);
                    hasContactRegistration = true;
                }
            }
            
            // Has Contact Registration
            if (hasContactRegistration) {              
                list<Contact_Registrations__c> listContactRegistration = new list<Contact_Registrations__c>();
                map<Id, Contact_Registrations__c> mapContactRegistration = new map<Id, Contact_Registrations__c>([SELECT Id, Total_Soft_Credit__c, Total_Donation__c FROM Contact_Registrations__c WHERE Id IN :setSObjectId]);
               	
                // Dynamic SOQL query for Contact Registration field on Soft Credit
                string query;
                query = 'SELECT Contact_Registration__c Id, SUM(Amount__c) TotalSoftCredit FROM Soft_Credit__c WHERE Contact_Registration__c IN :setSObjectId GROUP BY Contact_Registration__c';            
                map<Id, Decimal> mapAggResult = aggResult(setSObjectId, query);
                map<Id, Decimal> mapAggResultOpp;
                
                // Get Opportunity aggregate amount if Opportunity info exists                
                string queryOpp;
                queryOpp = 'SELECT Contact_Registration__c Id, SUM(Amount) TotalSoftCredit FROM Opportunity WHERE Contact_Registration__c IN :setSObjectId GROUP BY Contact_Registration__c';            
                mapAggResultOpp = aggResult(setSObjectId, queryOpp);                
                
                for (Contact_Registrations__c cr : mapContactRegistration.values()) {
                    cr.Total_Soft_Credit__c = mapAggResult.size()>0 ? mapAggResult.get(cr.Id) : null;
                    // Set Total_Donation__c information from sum of Total_Donation__c and Total_Soft_Credit__c
                    // Set Opportunity Aggregate amount to Total_Donation__c 
                    if (mapAggResultOpp.size() > 0) {                   	
                		cr.Total_Donation__c = mapAggResultOpp.get(cr.Id);
                    }
                    // Sum amount of Total_Soft_Credit__c if exists
                	if (cr.Total_Soft_Credit__c != null) {
                		cr.Total_Donation__c = cr.Total_Donation__c == null ? mapAggResult.get(cr.Id) : cr.Total_Donation__c + mapAggResult.get(cr.Id);
                	}                    
                      
                    listContactRegistration.add(cr);               
                }
                
                if (listContactRegistration.size() > 0) {
                	//TriggerByPass.ContactRegistration = true;
                    update listContactRegistration;
                }
            }
        }
        catch (exception e) {
            string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                                'Cause: ' + e.getCause() + '<br/>' +
                                'Line Number: ' + e.getLineNumber() + '<br/>' +
                                'Type Name: ' + e.getTypeName() + '<br/>' +
                                'Stack Trace String: ' + e.getStackTraceString();
            system.debug('***** ' + OpportunityAmountRollup.class.getName() + ': contactRegistrationRollup' + errorMessage);
            EmailUtil email = new EmailUtil(emailAddress, OpportunityAmountRollup.class.getName() + ': contactRegistrationRollup', errorMessage, 'HTML');
        }
    }        
    
    private static map<Id, Decimal> aggResult (set<Id> setSObjectId, string query) {
        list<SObject> listSObject = Database.query(query);
        map<Id, Decimal> mapSObject = new map<Id, Decimal>();
        
        if (listSObject.size() > 0) {
            for (SObject s : listSObject) {
                mapSObject.put((Id)s.get('Id'), (Decimal)s.get('TotalSoftCredit'));
            }           
        }
        return mapSObject;
    }    
}