public with sharing class QuitLineResourceOrderFormController  extends RecaptchaController {

    // Error to be displayed when a field is required.
    private static final String REQUIRED_FIELD = 'Field is required';

    // list of yes no options
    private static final List<SelectOption> YES_NO;

    // credit card number field name
    private static final String FIELD_CREDIT_CARD_NUMBER = 'Credit Card Number';

    // name on card field name
    private static final String FIELD_NAME_ON_CARD = 'Name on Card';

    // expiry date field name
    private static final String FIELD_EXPIRY_DATE = 'Expiry Date';

    // cvv field name
    private static final String FIELD_CVV = 'CVV';

    // list success page
    //private static final String SUCCESS_PAGE = '/apex/quitlineformsuccess';

    // TODO add this to the base
    static {
        YES_NO = new List<SelectOption>();
        YES_NO.add(new SelectOption('Yes', 'Yes'));
        YES_NO.add(new SelectOption('No', 'No'));
    }

    // The resource order
    public PBSI__PBSI_Sales_Order__c SalesOrder {get;set;}

    // All the items that the use can order.
    public LIst<QuitLineResourceOrderItemGroup> ItemGroups {get;set;}

    // The credit card holer name
    public String CreditCardHolderName {get;set;}

    // credit card holder name errors
    public String CreditCardHolderNameErrors {get {
        return getCreditCardError(FIELD_NAME_ON_CARD);
    }}

    // The credit card number
    public String CreditCardNumber {get;set;}

    // The credit card number
    public String CreditCardNumberErrors {get {return getCreditCardError(FIELD_CREDIT_CARD_NUMBER);}}

    // The card expiry date
    public String CreditCardExpiryDate {get;set;}

    public String CreditCardExpiryDateErrors {get {return getCreditCardError(FIELD_EXPIRY_DATE);}}

    // The card card cvv
    public String CreditCardCvv {get;set;}

    public String CreditCardCvvErrors {get {return getCreditCardError(FIELD_CVV);}}

    // Flag set when the payment process failed
    public boolean PaymentErrored {get;set;}

    public boolean ProceedToCheckout {get;set;}

    public String DeleteItemId {get;set;}

    public String MakeCreditCardPayment {get;set;}

    public List<SelectOption> NewItems {
        get {
            final List<SelectOption> items = new List<SelectOption>();

            for (QuitLineResourceOrderItemGroup grp : ItemGroups) {
                for (QuitLineResourceOrderItem item : grp.Items) {
                    if (!item.Selected) {
                        items.add(new SelectOption(item.ItemId, item.ItemDescription + ' ($' + String.valueOf(item.ItemPrice) + ')' ));
                    }
                }
            }
            return items;
        }
    }

    // Property that returns the yes/no answers
    public List<SelectOption> YesNoOptions {
        get { return YES_NO; }
    }


    // This property returns the total amount payable
    public Decimal TotalAmountPayable {
        get {
            Decimal amount = 0;
            if (ItemGroups == null) {
                return amount;
            }
            for(QuitLineResourceOrderItemGroup selGroup : ItemGroups) {

                for(QuitLineResourceOrderItem item : selGroup.Items) {
                    if (item.Selected != null && item.Selected && item.ItemPrice != null && item.Item.PBSI__Quantity_Needed__c != null && item.Item.PBSI__Quantity_Needed__c > 0) {
                        amount += (item.ItemPrice * item.Item.PBSI__Quantity_Needed__c);
                    }
                } 
            }
            return amount;
        }
    }

    public String NewItemId {get;set;}

    public Decimal NewItemQuantity {get;set;}

    public String NewItemQuantityError {get;set;}

    private Map<String, String> creditCardErrors;

    /**
     * The default constructor
     */
    public QuitLineResourceOrderFormController() {
        try {
            initForm();
        } catch (Exception e) {
            CustomException.formatException(e);
        }
        
    }

    /**
     * This method initialises the resource order form.
     */
    private void initForm() {
        SalesOrder = new PBSI__PBSI_Sales_Order__c();
        SalesOrder.RecordTypeId = getQuitLineOrderRecordType().Id;
        ItemGroups = new List<QuitLineResourceOrderItemGroup>();
        ProceedToCheckout = false;
        PaymentErrored = false;
        MakeCreditCardPayment = 'Yes';
        populateFormItems();
    }

    /**
     * This method popylates the items that can be pruchased on the form.
     */
    private void populateFormItems() {
        final List<PBSI__PBSI_Item__c> dbItems = [SELECT Id, 
                                                   Name, 
                                                   Item_Name__c,
                                                   Item_Group_Webname__c,
                                                   Webname__c,
                                                   PBSI__description__c,
                                                   PBSI__salesprice__c,
                                                   Maximum_Items_That_Can_Be_Purchased__c,
                                                   (SELECT Id, Name From Attachments WHERE name like 'quitline-item-%')
                                            FROM PBSI__PBSI_Item__c 
                                            WHERE PBSI__Item_Group__r.Name = 'Quit Resources'
                                            AND Item_Group_Webname__c != ''
                                            AND Publish_to_Website__c = true
                                            ORDER BY Item_Group_Webname__c];

        if (dbItems.isEmpty()) {
            CustomException.formatException('Resources not found.');
            return;
        }
        // get the first record and add the first group.
        String groupName = dbItems.get(0).Item_Group_Webname__c;//Item_Name__c;
        ItemGroups.add(new QuitLineResourceOrderItemGroup(groupName));
        for (PBSI__PBSI_Item__c dbItem : dbItems) {
            if (groupName != dbItem.Item_Group_Webname__c) { //Item_Name__c) {
                groupName = dbItem.Item_Group_Webname__c;//Item_Name__c;
                // add the new group
                ItemGroups.add(new QuitLineResourceOrderItemGroup(groupName));
            }

            final QuitLineResourceOrderItem item = new QuitLineResourceOrderItem(dbItem.Id,
                    //dbItem.PBSI__description__c,
                    dbItem.Webname__c,
                    dbItem.PBSI__salesprice__c);
            item.MaxItemsThatCanBePurchased = (dbItem.Maximum_Items_That_Can_Be_Purchased__c == null) ? QuitLineResourceOrderItem.DEFAULT_MAX_ITEMS 
                                                : dbItem.Maximum_Items_That_Can_Be_Purchased__c;

            // get the attachments
            for (Attachment att : dbItem.Attachments) {
                if (att.Name.startsWith('quitline-item-thumbnail')) {
                    item.ItemThumbnailUrl = '/servlet/servlet.FileDownload?file=' + att.Id;
                }

                if (att.Name.startsWith('quitline-item-download')) {
                    item.ItemUrl = '/servlet/servlet.FileDownload?file=' + att.Id;
                }
            }

            ItemGroups.get(ItemGroups.size() -1).addItem(item);
        }
    }

    /**
     * This method is called when the user clicks the checkout button. 
     */
    public void checkout() { 
        // set the value of the validateForm method to the value of the ProceedToCheckout property, determine which fields get shown on the resource order form.
        ProceedToCheckout = validateForm();
    }

    /**
     * This method is called when the user clicks back on the payment page.
     */
    public void back() {
        ProceedToCheckout = false;
    }
    /**
     * This method is called when the user submits the order.
     */
    public PageReference save() {
        PageReference pageRef = null;
        try {
            pageRef = processSave();
        } catch (Exception e) {
            CustomException.formatException(e);
        }
        return pageRef;
    }

    /**
     * This process the and saves it to the database. If the total amount payable is 0 then the user is redirected to the receipt page.
     * If the TotalAmountPayable is greater than 0 then null will be returned, meaning the user will remain on the current page. The visual force page
     * has javascript on it which will look to see if the sales order has been saved and the amount payable is greater than 0, if so the performPayment action will 
     * be invoked on this class.
     */
    private PageReference processSave() {
        // We need to validate the form is valid before going further
        if (validateForm()) {
            if (!verifyCaptcha() && !Test.isRunningTest()) {
                CustomException.formatException('Please verify you are not a robot.');
                return null;
            }

            if ('No' == MakeCreditCardPayment) {
                SalesOrder.PBSI__Status__c = 'Invoice';
            }

            PaymentErrored = false;
            // insert/update the order
            if (SalesOrder.Id == null) {
                insert SalesOrder;
            } else {
                update SalesOrder;
            }

            // add the sales order id to the order line.
            final List<PBSI__PBSI_Sales_Order_Line__c> orderItems = getSelectedItems();
            if (!orderItems.isEmpty()) {
                if (SalesOrder.Id != null) {
                    // delete all the previously linked intems incase the user has changed their order.
                    delete [select id from PBSI__PBSI_Sales_Order_Line__c where PBSI__Sales_Order__c = :SalesOrder.Id];
                }
                List<PBSI__PBSI_Sales_Order_Line__c> orderLines = new List<PBSI__PBSI_Sales_Order_Line__c>();
                // set the order id on each item add to the order
                for (PBSI__PBSI_Sales_Order_Line__c orderItem : orderItems) {
                    //orderItem.PBSI__Sales_Order__c = SalesOrder.Id;
                    //orderItem.Id = null;
                    PBSI__PBSI_Sales_Order_Line__c item = orderItem.clone(false);
                    item.PBSI__Sales_Order__c = SalesOrder.Id;
                    orderLines.add(item);
                }

                // save the order items
                insert orderLines;//OrderItems;
            }
            // if we don't have to process the payment then return to the success page.
            if (TotalAmountPayable == 0 || 'No' == MakeCreditCardPayment) {
                return new PageReference(getReceiptUrl());
            }   
        }

        return null;
    }
    /**
     * This method performs the payment after a redirect is done once the record has been saved. This will prevent
     * an uncommitted record error when making the call out to the web service.
     */
    public PageReference performPayment() {
        if (TotalAmountPayable > 0) {
            // call the process payment to make all the necessary calls to secure pay.
            final Map<String, String> paymentErrors = processPayments();
            // if we don't have any issues with the payments then perform a redirect to the success page.
            if (paymentErrors.isEmpty()) {
                PaymentErrored = false;

                update SalesOrder;

//              final Payment_Received__c paymentReceived = new Payment_Received__c(Amount_Paid__c=TotalAmountPayable,
//                      Date_Processed__c = Date.today(), Payment_Method__c = 'Credit Card', Sales_Order__c = SalesOrder.Id);
//              insert paymentReceived;

                return new PageReference(getReceiptUrl());
            }

            PaymentErrored = true;
            return null;
        }

        if (SalesOrder.Id == null) {
            PaymentErrored = true;
            CustomException.formatException('Order cannot be paid for before placed.');
            return null;
        }

        return new PageReference(getReceiptUrl());
    }

    public void deleteItem() {
        if (String.isBlank(DeleteItemId)) {
            CustomException.formatException('Could not find the item to delete');
            return;
        }

        for (QuitLineResourceOrderItemGroup grp : ItemGroups) {
            for (QuitLineResourceOrderItem  item : grp.Items) {
                if (item.ItemId == DeleteItemId) {
                    item.Item.PBSI__Quantity_Needed__c = 0;
                    break;
                }
            }
        }

        DeleteItemId = '';
    }

    public void addItem() {
        boolean error = false;
        NewItemQuantityError = '';
        if (String.isBlank(NewItemId)) {
            NewItemQuantityError += 'Please select the item to add to your order';
            error = true;
        }

        if (NewItemQuantity == null || NewItemQuantity <= 0) {
            NewItemQuantityError += 'Amount must be greater than 0';
            error = true;
        }

        if (error) return;

        for (QuitLineResourceOrderItemGroup grp : ItemGroups) {
            for (QuitLineResourceOrderItem item : grp.Items) {
                if (item.ItemId == NewItemId) {
                    Decimal maxVal = item.MaxItemsThatCanBePurchased;
                    if (NewItemQuantity > maxVal) {
                        NewItemQuantityError = 'The amount ordered cannot exceed the maximum of ' + maxVal;
                    } else {
                        item.Item.PBSI__Quantity_Needed__c = NewItemQuantity;
                    }
                    break;
                }
            }
        }
        // reset the value.
        NewItemId = '';
        NewItemQuantity = null;
    }
    /**
     * This method validates the resource order form and return true if it valid otherwise false is returned.
     * @return returns true if the form is valid otherwise false is returned.
     */
    private boolean validateForm() {
        boolean isValid = true;
        final List<PBSI__PBSI_Sales_Order_Line__c> orderItems = getSelectedItems();

        if (orderItems.isEmpty()) {
            CustomException.formatException('At least one item must be ordered');
            isValid = false;
        }

        if (String.isBlank(SalesOrder.First_Name__c)) {
            SalesOrder.First_Name__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(SalesOrder.Last_Name__c)) {
            SalesOrder.Last_Name__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(SalesOrder.Street__c)) {
            SalesOrder.Street__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(SalesOrder.City__c)) {
            SalesOrder.City__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (String.isBlank(SalesOrder.State__c)) {
            SalesOrder.State__c.addError(REQUIRED_FIELD);
            isValid = false;
        } else if (!'SA'.equalsIgnoreCase(SalesOrder.State__c)) {
            SalesOrder.addError('These resources are provided to organisations within SA. Interstate orders can be accepted and will attract a fee to cover costs of production and postage of resources. Please contact us at quitline@cancersa.org.au if you wish to order resources.');
            isValid = false;
        }

        // validate the phone number
        if (String.isBlank(SalesOrder.Phone__c)) {
            SalesOrder.Phone__c.addError(REQUIRED_FIELD);
            isValid = false;
        } else if (!isValidPhone(SalesOrder.Phone__c)) {
            SalesOrder.Phone__c.addError(RecaptchaController.INVALID_PHONE);
            isValid = false;
        }

        if (String.isBlank(SalesOrder.Postcode__c)) {
            SalesOrder.Postcode__c.addError(REQUIRED_FIELD);
            isValid = false;
        }

        if (TotalAmountPayable > 0 && ProceedToCheckout && 'Yes' == MakeCreditCardPayment) {
            // re initialise the credit card errors.

            this.creditCardErrors = new Map<String, String>();
            if (!validRequiredProperty(FIELD_CREDIT_CARD_NUMBER, CreditCardNumber)) {
                isValid = false;
            }

            if (!validRequiredProperty(FIELD_NAME_ON_CARD, CreditCardHolderName)) {
                isValid = false;
            }


            if (!validRequiredProperty(FIELD_EXPIRY_DATE, CreditCardExpiryDate)) {
                isValid = false;
            } else {
                //Pattern expPattern = Pattern.compile('[0-9]*[0-9]+/[0-9][0-9]');
                Boolean isValidExpiryDate = Pattern.matches('[0-9]*[0-9]+/[0-9][0-9]', CreditCardExpiryDate);
                if (!isValidExpiryDate) {
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Card Expiry must be in the format of MM/YY'));
                    addCreditCardError(FIELD_EXPIRY_DATE, 'Card Expiry must be in the format of MM/YY');
                    isValid = false;
                }
            }

            if (!validRequiredProperty('CVV', CreditCardCvv)) {
                isValid = false;
            } else {
                Boolean isValidCvv = Pattern.matches('[0-9][0-9][0-9]', CreditCardCvv);
                if (!isValidCvv) {
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Card CVV must contain 3 digits'));
                    addCreditCardError(FIELD_CVV, 'Card CVV must contain 3 digits');
                    isValid = false;
                }
            }
        }
        return isValid & validateItems();
    }

    /**
     * This method validate a required property that is not an SObject. It wil add a message to the page.
     * @param name - the name of the property being validated this is used in the error message
     * @param value - the value being validated
     * @return true is returned if the value is not blank otherwise false is returned.
     */
    private boolean validRequiredProperty(String name, String value) {
        final boolean propertyValid;
        if (String.isBlank(value)) {//
            String errorMsg = name + ': ' + REQUIRED_FIELD;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg));
            addCreditCardError(name, errorMsg);
            propertyValid = false;
        } else {
            propertyValid = true;
        }
        return propertyValid;
    }

    private void addCreditCardError(String fieldName, String error) {
        String errorStr = '';
        if (creditCardErrors.containsKey(fieldName)) {
            errorStr = creditCardErrors.get(fieldName) + ', ';
            creditCardErrors.remove(fieldName);
        }
        errorStr += error;
        creditCardErrors.put(fieldName, errorStr);
    }

    private String getCreditCardError(String fieldName) {
        return (this.creditCardErrors == null || !this.creditCardErrors.containsKey(fieldName)) ? '' : this.creditCardErrors.get(fieldName);
    }
    /**
     * This method validates the items selected, it will return true if valid otherwise false is returned.
     * @return true if the items selected are validated.
     */
    private boolean validateItems() {
        boolean isValid =true;

        for (QuitLineResourceOrderItemGroup groupSel : ItemGroups) {
            for (QuitLineResourceOrderItem groupItem : groupSel.Items) {
                
                if (!groupItem.Selected) { continue; }
                
                final PBSI__PBSI_Sales_Order_Line__c item = groupItem.Item;
                if (String.isBlank(item.PBSI__Item__c)) {
                    item.PBSI__Item__c.addError(REQUIRED_FIELD);
                    isValid = false;
                }

                if (item.PBSI__Quantity_Needed__c == null) {
                    item.PBSI__Quantity_Needed__c.addError(REQUIRED_FIELD);
                    isValid = false;
                } else if (item.PBSI__Quantity_Needed__c < 1) {
                    item.PBSI__Quantity_Needed__c.addError('At least 1 product must be ordered');
                    isValid = false;
                } else if (item.PBSI__Quantity_Needed__c > groupItem.MaxItemsThatCanBePurchased) {
                    item.PBSI__Quantity_Needed__c.addError('The amount ordered cannot exceed the maximum of ' + groupItem.MaxItemsThatCanBePurchased);
                    isValid = false;
                }
            }
        }

        return isValid;
    }
    
    /**
     * This method calls secure pay to make the payment and stores any errors within a map. These errors are returned as well as being added to the 
     * APexPage messages. If no errors are returned then an empty Mpa will be returned to the calling process.
     * @return Map<String, String> containg the errors returned by the payment gateway. The key value is the HttpStatus Code returned by the
     * pyament gateway and the value is the message returned. If no errors were encountered then an enpty map will be returned.
     */
    private Map<String, String> processPayments() {
    
        String purchaseOrder = SalesOrder.Name; 
        if (String.isBlank(SalesOrder.Name)) {
            List<PBSI__PBSI_Sales_Order__c> ods = [SELECT ID, Name FROM PBSI__PBSI_Sales_Order__c WHERE Id = :SalesOrder.Id LIMIT 1];
            purchaseOrder = ods.isEmpty() ? '' : ods.get(0).Name;
        }
        
        Map<String, String> errors = SecurePayUtility.CreditCardPayment(purchaseOrder, CreditCardNumber, CreditCardExpiryDate, CreditCardCvv, TotalAmountPayable, SalesOrder);
        if (!errors.isEmpty()) {
            // re-initiliase the credit card errors.
            this.creditCardErrors = new Map<String, String>();
            for (String error : errors.values()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
                addCreditCardError(FIELD_CREDIT_CARD_NUMBER, error);
            }
        } 

        return errors;
    }

    /**
     * This method returns the list of selected items for the order.
     * @return list containing all the items ordered by the user.
     */
    private List<PBSI__PBSI_Sales_Order_Line__c> getSelectedItems() {
        final List<PBSI__PBSI_Sales_Order_Line__c> selectedItems = new List<PBSI__PBSI_Sales_Order_Line__c>();

        for (QuitLineResourceOrderItemGroup groupSel : ItemGroups) {
            for (QuitLineResourceOrderItem groupItem : groupSel.Items) {
                if (!groupItem.Selected) { 
                    continue; 
                }
                selectedItems.add(groupItem.Item);
            }
        }

        return selectedItems;
    }

    /**
     * This method returns the quit record type for the sales order.
     * @return Quit record type for the sales order.
     */
    private RecordType getQuitLineOrderRecordType() {
        final List<RecordType> recordTypes = [SELECT Id, Name
                           FROM RecordType
                           WHERE sObjectType = 'PBSI__PBSI_Sales_Order__c' 
                           AND Name='Quit'];

        return (recordTypes.isEmpty()) ? null : recordTypes.get(0);
    }

    private String getReceiptUrl() {
        return '/apex/quitlineresourceorderformreceipt?id=' + SalesOrder.Id;
    }
}