public class NrtListController {
    public Id caseId {get;set;}
    
    public map<Id, Send_NRT_Resource__c> mapNrtResources {
        get {
            if (mapNrtResources == null) {
                mapNrtResources = new map<Id, Send_NRT_Resource__c>();
                for (Send_NRT_Resource__c r : [SELECT 
                                                Id
                                                , Qty_Sent__c
                                                , Delete__c
                                                , Date_Sent__c
                                                , Item_Sent_to_Order__c
                                                , Item_Sent__c
                                                , Item_NRT__r.Name
                                                , Item_NRT__r.shortDescription__c
                                                , Item_NRT__r.Item_Group__c
                                                , Qty_to_Send__c
                                                , Item_NRT__r.Default_Unit_of_Measure__c
                                                , Case__c 
                                                FROM Send_NRT_Resource__c 
                                                WHERE Case__c = :caseId]) {
                    mapNrtResources.put(r.Id, r);               
                }
            }
            return mapNrtResources;
        }
        set;
    }
    
    public NrtListController(ApexPages.StandardController controller) {
        caseId = ApexPages.currentPage().getParameters().get('Id');
    }
    
    public PageReference updateTable(){
        boolean valid = true;
        for (Send_NRT_Resource__c r : mapNrtResources.values()) {
            //if NRT Resource had been sent out, no more changes should be made to it and removed from update list
            if (r.Item_Sent__c != null) {
                mapNrtResources.remove(r.Id);
            }
            
            if (r.Qty_to_Send__c <= 0) {
                valid = false;
                break;              
            }
        }        

        if (valid) {
            upsert(mapNrtResources.values());
        }
                
        //clear out mapNrtResources to refresh list with new data
        mapNrtResources = null;        
        return null;
       
    }
    
    public void insertRow(){
        Send_NRT_Resource__c r = new Send_NRT_Resource__c(Case__c = caseId);
        mapNrtResources.put(r.Id, r);
    }
    
    public PageReference deleteRow(){
        Send_NRT_Resource__c r = mapNrtResources.get(ApexPages.currentPage().getParameters().get('lineNo'));//System.currentPagereference().getParameters().get('lineNo'));
        
        if (r != null) {
            delete r;
        }
        
        //clear out mapNrtResources to refresh list with new data        
        mapNrtResources = null;
        return null;
    }
}