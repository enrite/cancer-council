@isTest
public class TestEventDetailJobRoles
{
    public static testMethod void myUnitTest()
    {                                         
        Registration__c r = new Registration__c(RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Registration__c' AND Name = 'Event Volunteer'].Id,
                                                Auto_generated_JR__c = true);
        insert r;
        
        Account el = new Account(Name = 'test',
                                 RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Event Location'].Id);
        insert el;
        
        Campaign c = new Campaign(Name = 'test');
        insert c;
        
        Event_Details__c ed = new Event_Details__c(Event_Location__c = el.Id,
                                                   Campaign__c = c.Id,
                                                   Start_Date__c = Date.today(),
                                                   End_Date__c = Date.today().addDays(1));
        insert ed;
        
        ed.Event_Volunteers_Required__c = '2';
        update ed;
    }
}