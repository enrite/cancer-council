public without sharing class ServiceProviderSupportSearchDemo
{
    public ServiceProviderSupportSearchDemo()
    {
        PopulateCategories();    
        PopulateLocations();    
        PopulateTags();
    }

    public String OrganisationId { get; set; }
    public String CancerType { get; set; }
    public List<SelectOption> AvailableCategories { get; set; }        
    public List<SelectOption> SelectedCategories { get; set; }
    public List<SelectOption> AvailableLocations { get; set; }        
    public List<SelectOption> SelectedLocations { get; set; }
    public List<SelectOption> AvailableTags { get; set; }        
    public List<SelectOption> SelectedTags { get; set; }
    public List<ContentVersion> SearchResults { get; set; }
    public List<String> Parameters { get; set; }
    
    public void Search()
    {
        try
        {
            SearchResults = new List<ContentVersion>();
            Parameters = new List<String>();
        
            String q = 'SELECT Id,';
            q += 'Title,';                             
            q += 'ContentURL,';
            q += 'Description,';
            q += 'Associated_Account__r.Name,';
            q += 'Associated_Account__r.BillingStreet,';
            q += 'Associated_Account__r.BillingCity,';      
            q += 'Associated_Account__r.BillingState,';
            q += 'Associated_Account__r.BillingPostalCode,';
            q += 'Associated_Account__r.Phone ';
            q += 'FROM ContentVersion ';            
            q += 'WHERE PublishStatus = \'P\' ';
            
            if (OrganisationId != null)
            {
                q += 'AND Associated_Account__c = \'' + OrganisationId + '\' ';
                
                for (Account a : [SELECT Id,
                                         Name
                                  FROM Account
                                  WHERE Id = :OrganisationId])
                {
                    Parameters.add('Organisation: ' + a.Name);         
                }                         
            }
            
            if (CancerType != null)
            {
                Parameters.add('Cancer Type: ' + CancerType);       
            }
            
            if (SelectedCategories.size() > 0)
            {      
                String cat = 'Categories: ';
                
                for (SelectOption o : SelectedCategories)
                {
                    cat += o.getLabel() + ', ';
                }
                
                cat = cat.left(cat.length() - 2);
                                 
                Parameters.add(cat);       
            }
            
            if (SelectedLocations.size() > 0)
            {      
                String loc = 'Locations: ';
                
                for (SelectOption o : SelectedLocations)
                {
                    loc += o.getLabel() + ', ';
                }
                
                loc = loc.left(loc.length() - 2);
                                 
                Parameters.add(loc);       
            }
            
            if (SelectedTags.size() > 0)
            {      
                String tag = 'Tags: ';
                
                for (SelectOption o : SelectedTags)
                {
                    tag += o.getLabel() + ', ';
                }
                
                tag = tag.left(tag.length() - 2);
                                 
                Parameters.add(tag);       
            }
            
            SearchResults = Database.query(q);
        }
        catch(Exception e)
        {
            CustomException.formatException(e);
        }
    }
    
    public void ReturnToSearch()
    {
        try
        {
            SearchResults = null;
        }
        catch(Exception e)
        {
            CustomException.formatException(e);
        }
    }
    
    public List<SelectOption> Organisations
    {
        get
        {
            if (Organisations == null)
            {
                Organisations = new List<SelectOption>();
                Organisations.add(new SelectOption('', '-Select-'));
                
                for (Account a : [SELECT Id,
                                         Name
                                  FROM Account
                                  ORDER BY Name])
                {
                    Organisations.add(new SelectOption(a.Id, a.Name));
                }                                                  
            }
            return Organisations;
        }
        set;
    }       
    
    public List<SelectOption> CancerTypes
    {
        get
        {
            if (CancerTypes == null)
            {
                CancerTypes = new List<SelectOption>();
                CancerTypes.add(new SelectOption('', '-Select-'));
                                
                for(PickListEntry entry : ContentVersion.Cancer_Type_A_E__c.getDescribe().getPicklistValues())
                {                    
                    CancerTypes.add(new SelectOption(entry.getLabel(), entry.getValue()));                  
                }
                
                for(PickListEntry entry : ContentVersion.Cancer_Type_F_M__c.getDescribe().getPicklistValues())
                {                    
                    CancerTypes.add(new SelectOption(entry.getLabel(), entry.getValue()));                  
                }
                
                for(PickListEntry entry : ContentVersion.Cancer_Type_N_R__c.getDescribe().getPicklistValues())
                {                    
                    CancerTypes.add(new SelectOption(entry.getLabel(), entry.getValue()));                  
                }
                
                for(PickListEntry entry : ContentVersion.Cancer_Type_S_Z__c.getDescribe().getPicklistValues())
                {                    
                    CancerTypes.add(new SelectOption(entry.getLabel(), entry.getValue()));                  
                }            
            }
            return CancerTypes;
        }
        set;
    }
    
    public void PopulateCategories()
    {
        AvailableCategories = new List<SelectOption>(); 
        SelectedCategories = new List<SelectOption>(); 
    
        AvailableCategories.add(new SelectOption('Accommodation', 'Accommodation'));
        AvailableCategories.add(new SelectOption('Allied Health', 'Allied Health'));
        AvailableCategories.add(new SelectOption('CSG - Cancer Support Group', 'CSG - Cancer Support Group'));
        AvailableCategories.add(new SelectOption('Carer Support', 'Carer Support'));
        AvailableCategories.add(new SelectOption('Community Health', 'Community Health'));
        AvailableCategories.add(new SelectOption('Council', 'Council'));
        AvailableCategories.add(new SelectOption('Counselling', 'Counselling'));
        AvailableCategories.add(new SelectOption('Home care/Home help', 'Home care/Home help'));
        AvailableCategories.add(new SelectOption('Hospital', 'Hospital'));
        AvailableCategories.add(new SelectOption('Medical Supplies', 'Medical Supplies'));
        AvailableCategories.add(new SelectOption('Palliative Care', 'Palliative Care'));
        AvailableCategories.add(new SelectOption('Practical', 'Practical'));
        AvailableCategories.add(new SelectOption('Transport', 'Transport'));
        AvailableCategories.add(new SelectOption('Insurance', 'Insurance'));
        AvailableCategories.add(new SelectOption('Treatment', 'Treatment'));
        AvailableCategories.add(new SelectOption('Other - lot of individual services', 'Other - lot of individual services'));
    }
    
    public void PopulateLocations()
    {
        AvailableLocations = new List<SelectOption>(); 
        SelectedLocations = new List<SelectOption>(); 
    
        AvailableLocations.add(new SelectOption('Eastern Adelaide', 'Eastern Adelaide'));
        AvailableLocations.add(new SelectOption('Northern Adelaide', 'Northern Adelaide'));
        AvailableLocations.add(new SelectOption('Southern Adelaide', 'Southern Adelaide'));
        AvailableLocations.add(new SelectOption('Western Adelaide', 'Western Adelaide'));
        AvailableLocations.add(new SelectOption('Adelaide Hills', 'Adelaide Hills'));
        AvailableLocations.add(new SelectOption('Barossa, Light and Lower North', 'Barossa, Light and Lower North'));
        AvailableLocations.add(new SelectOption('Fleurieu and Kangaroo Island', 'Fleurieu and Kangaroo Island'));
        AvailableLocations.add(new SelectOption('Eyre and Western', 'Eyre and Western'));
        AvailableLocations.add(new SelectOption('Far North', 'Far North'));
        AvailableLocations.add(new SelectOption('Limestone Coast', 'Limestone Coast'));
        AvailableLocations.add(new SelectOption('Murray and Mallee', 'Murray and Mallee'));
        AvailableLocations.add(new SelectOption('Yorke and Mid North', 'Yorke and Mid North'));        
    }
    
    public void PopulateTags()
    {
        AvailableTags = new List<SelectOption>(); 
        SelectedTags = new List<SelectOption>(); 
    
        AvailableTags.add(new SelectOption('Aboriginal', 'Aboriginal'));
        AvailableTags.add(new SelectOption('Accommodation', 'Accommodation'));
        AvailableTags.add(new SelectOption('Adolescents', 'Adolescents'));
        AvailableTags.add(new SelectOption('Aged', 'Aged'));
        AvailableTags.add(new SelectOption('All cancer types', 'All cancer types'));
        AvailableTags.add(new SelectOption('Assistance', 'Assistance'));
        AvailableTags.add(new SelectOption('ATSI', 'ATSI'));
        AvailableTags.add(new SelectOption('Bereavement', 'Bereavement'));
        AvailableTags.add(new SelectOption('Bereavment', 'Bereavment'));
        AvailableTags.add(new SelectOption('Brain', 'Brain'));
        AvailableTags.add(new SelectOption('Breast', 'Breast'));
        AvailableTags.add(new SelectOption('Breast cancer', 'Breast cancer'));
        AvailableTags.add(new SelectOption('Breast prosthesis', 'Breast prosthesis'));
        AvailableTags.add(new SelectOption('CALD', 'CALD'));
        AvailableTags.add(new SelectOption('Cancer', 'Cancer'));
        AvailableTags.add(new SelectOption('Cancer support group', 'Cancer support group'));
        AvailableTags.add(new SelectOption('Cancer treatment', 'Cancer treatment'));
        AvailableTags.add(new SelectOption('Carers', 'Carers'));
        AvailableTags.add(new SelectOption('Cervix', 'Cervix'));
        AvailableTags.add(new SelectOption('Chemotherapy', 'Chemotherapy'));
        AvailableTags.add(new SelectOption('Children', 'Children'));
        AvailableTags.add(new SelectOption('Community centre', 'Community centre'));
        AvailableTags.add(new SelectOption('Community health', 'Community health'));
        AvailableTags.add(new SelectOption('Counselling', 'Counselling'));
        AvailableTags.add(new SelectOption('Country', 'Country'));
        AvailableTags.add(new SelectOption('Death', 'Death'));
        AvailableTags.add(new SelectOption('Diagnosis', 'Diagnosis'));
        AvailableTags.add(new SelectOption('Diet', 'Diet'));
        AvailableTags.add(new SelectOption('Donors', 'Donors'));
        AvailableTags.add(new SelectOption('Early detection', 'Early detection'));
        AvailableTags.add(new SelectOption('Education', 'Education'));
        AvailableTags.add(new SelectOption('Education program', 'Education program'));
        AvailableTags.add(new SelectOption('Elderly', 'Elderly'));
        AvailableTags.add(new SelectOption('Equipment', 'Equipment'));
        AvailableTags.add(new SelectOption('Exercise', 'Exercise'));
        AvailableTags.add(new SelectOption('Family care', 'Family care'));
        AvailableTags.add(new SelectOption('Fatigue', 'Fatigue'));
        AvailableTags.add(new SelectOption('Financial', 'Financial'));
        AvailableTags.add(new SelectOption('Financial assistance', 'Financial assistance'));
        AvailableTags.add(new SelectOption('Fitness', 'Fitness'));
        AvailableTags.add(new SelectOption('Funerals', 'Funerals'));
        AvailableTags.add(new SelectOption('Gay', 'Gay'));
        AvailableTags.add(new SelectOption('General', 'General'));
        AvailableTags.add(new SelectOption('GP Plus', 'GP Plus'));
        AvailableTags.add(new SelectOption('Grief', 'Grief'));
        AvailableTags.add(new SelectOption('Group', 'Group'));
        AvailableTags.add(new SelectOption('HACC', 'HACC'));
        AvailableTags.add(new SelectOption('Hair donations', 'Hair donations'));
        AvailableTags.add(new SelectOption('Hair loss', 'Hair loss'));
        AvailableTags.add(new SelectOption('Hats', 'Hats'));
        AvailableTags.add(new SelectOption('Health', 'Health'));
        AvailableTags.add(new SelectOption('Health care', 'Health care'));
        AvailableTags.add(new SelectOption('Help', 'Help'));
        AvailableTags.add(new SelectOption('Helpline', 'Helpline'));
        AvailableTags.add(new SelectOption('Hire', 'Hire'));
        AvailableTags.add(new SelectOption('Home help', 'Home help'));
        AvailableTags.add(new SelectOption('Home nursing. Cleaning', 'Home nursing. Cleaning'));
        AvailableTags.add(new SelectOption('Hospital', 'Hospital'));
        AvailableTags.add(new SelectOption('Hypnosis', 'Hypnosis'));
        AvailableTags.add(new SelectOption('Information', 'Information'));
        AvailableTags.add(new SelectOption('Insurance', 'Insurance'));
        AvailableTags.add(new SelectOption('Legal', 'Legal'));
        AvailableTags.add(new SelectOption('Lesbian', 'Lesbian'));
        AvailableTags.add(new SelectOption('Leukaemia', 'Leukaemia'));
        AvailableTags.add(new SelectOption('Loss', 'Loss'));
        AvailableTags.add(new SelectOption('Lymphoedema', 'Lymphoedema'));
        AvailableTags.add(new SelectOption('Massage', 'Massage'));
        AvailableTags.add(new SelectOption('Mastectomy', 'Mastectomy'));
        AvailableTags.add(new SelectOption('Medical supplies', 'Medical supplies'));
        AvailableTags.add(new SelectOption('Meditation', 'Meditation'));
        AvailableTags.add(new SelectOption('Melanoma', 'Melanoma'));
        AvailableTags.add(new SelectOption('Men', 'Men'));
        AvailableTags.add(new SelectOption('Mental health', 'Mental health'));
        AvailableTags.add(new SelectOption('Mindfulness', 'Mindfulness'));
        AvailableTags.add(new SelectOption('Nurse', 'Nurse'));
        AvailableTags.add(new SelectOption('Nutrition', 'Nutrition'));
        AvailableTags.add(new SelectOption('Oncology massage', 'Oncology massage'));
        AvailableTags.add(new SelectOption('Outings', 'Outings'));
        AvailableTags.add(new SelectOption('Ovary', 'Ovary'));
        AvailableTags.add(new SelectOption('Paliative care', 'Paliative care'));
        AvailableTags.add(new SelectOption('Palliative', 'Palliative'));
        AvailableTags.add(new SelectOption('Palliative care', 'Palliative care'));
        AvailableTags.add(new SelectOption('Palliatve care', 'Palliatve care'));
        AvailableTags.add(new SelectOption('Pap smears', 'Pap smears'));
        AvailableTags.add(new SelectOption('Prostate', 'Prostate'));
        AvailableTags.add(new SelectOption('Prosthesis', 'Prosthesis'));
        AvailableTags.add(new SelectOption('Publication', 'Publication'));
        AvailableTags.add(new SelectOption('Radiotherapy', 'Radiotherapy'));
        AvailableTags.add(new SelectOption('Reconstruction', 'Reconstruction'));
        AvailableTags.add(new SelectOption('Registry', 'Registry'));
        AvailableTags.add(new SelectOption('Respite', 'Respite'));
        AvailableTags.add(new SelectOption('Screening', 'Screening'));
        AvailableTags.add(new SelectOption('Self image/body image', 'Self image/body image'));
        AvailableTags.add(new SelectOption('Seniors', 'Seniors'));
        AvailableTags.add(new SelectOption('Service', 'Service'));
        AvailableTags.add(new SelectOption('Service.support', 'Service.support'));
        AvailableTags.add(new SelectOption('Sexual counselling', 'Sexual counselling'));
        AvailableTags.add(new SelectOption('Shopping', 'Shopping'));
        AvailableTags.add(new SelectOption('Shoppng', 'Shoppng'));
        AvailableTags.add(new SelectOption('Side effects', 'Side effects'));
        AvailableTags.add(new SelectOption('Sleeves', 'Sleeves'));
        AvailableTags.add(new SelectOption('Statistics', 'Statistics'));
        AvailableTags.add(new SelectOption('Suicide', 'Suicide'));
        AvailableTags.add(new SelectOption('Support', 'Support'));
        AvailableTags.add(new SelectOption('Support group', 'Support group'));
        AvailableTags.add(new SelectOption('Surgery', 'Surgery'));
        AvailableTags.add(new SelectOption('Transport', 'Transport'));
        AvailableTags.add(new SelectOption('Travel', 'Travel'));
        AvailableTags.add(new SelectOption('Treatment', 'Treatment'));
        AvailableTags.add(new SelectOption('Turbans', 'Turbans'));
        AvailableTags.add(new SelectOption('War veterans', 'War veterans'));
        AvailableTags.add(new SelectOption('Website', 'Website'));
        AvailableTags.add(new SelectOption('Wigs', 'Wigs'));
        AvailableTags.add(new SelectOption('Women', 'Women'));
        AvailableTags.add(new SelectOption('Yoga', 'Yoga'));
        AvailableTags.add(new SelectOption('Young people', 'Young people'));
    }
}