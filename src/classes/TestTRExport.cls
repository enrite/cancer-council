/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTRExport {

    static testMethod void myUnitTest() 
    {
        TRExport cl = new TRExport(); 
        
        system.assert(cl.ConstituentErrors != null);
        system.assert(cl.ConstituentSuccess != null);
        system.assert(cl.TeamErrors != null);
        system.assert(cl.TeamSuccess != null);
        system.assert(cl.RegistrationErrors != null);
        system.assert(cl.RegistrationSuccess != null);
        system.assert(cl.TransactionSuccess != null); 
        system.assert(cl.TransactionErrors != null);  
        
		cl.reProcessCon();
		cl.reProcessTeam();
		cl.reProcessReg();
		cl.reProcessTrans();
		cl.newExport();
    }
    
    static testMethod void testErrorHandlers()
    {
        TRExport cl = new TRExport();
        cl.forceTestError = true;
        try
        {
			cl.reProcessCon(); 
        }
        catch(Exception ex){}
        try
        {
			cl.reProcessTeam();
		}
        catch(Exception ex){}
		try
        {
			cl.reProcessReg();
		}
        catch(Exception ex){}
		try
        {
			cl.reProcessTrans();
		}
        catch(Exception ex){}
		try
        {
			cl.newExport();
		}
        catch(Exception ex){}
    }
}