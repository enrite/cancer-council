public with sharing class VolunteerSearchCCController {

    //sub class to hold information of records of query passed in
    public class Volunteer {
        public string name {get;set;}
        public string cancerType {get;set;}
        public string ageRange {get;set;}
        public string ageRangeDOB {get;set;}
        public string category {get;set;}
        public string treatment {get;set;}
        public string id {get;set;}
        public string carerOrPatient {get;set;}
        public string volunteerStatus {get;set;}
        public string experiences {get;set;}
        public string lastCcReferralMatchDate {get;set;}                
        
        //constructor
        public Volunteer() {}
    }
 
        //the collection of contacts to display
        public list<Contact> contacts {get;set;}
        //final collection of results to display on screen
        public list<Volunteer> volList {get;set;}
        
        string paramCancerType = '';
        string paramAgeRange = '';
        string paramTreatment = '';
        string paramCategory = '';
        string treatmentConcat;
        string categoryConcat;
            
        //format the soql for display on the visualforce page for debug
        /*public String debugSoql {
            get{  
                string debug = 'Criteria selected includes Contacts ';
                if (!(paramCancerType.equals('') && paramAgeRange.equals(''))) {
                    if (!paramCancerType.equals(''))
                        debug += ' and ' + paramCancerType;
                    if (!paramAgeRange.equals(''))
                        debug += ' and '+ paramAgeRange;
                    if (!paramTreatment.equals(''))
                        debug += ' and '+ paramTreatment;
                    if (!paramCategory.equals(''))
                        debug += ' and '+ paramCategory;                        
                }
                return debug + ' ' + soql;
            }
            set;
        }*/
                
        //constructor and set default soql statement
        public VolunteerSearchCCController() {}
        
        //runs dynamic soql query
        public void runQuery(string s) {
                try {                   
                        contacts = database.query(s); 
                        volList = new list<Volunteer>();                        
                        
                        if (contacts.size()>0) {                            
                            for(Contact c : contacts) {
                            Volunteer vol = new Volunteer();
                            treatmentConcat = '';
                            categoryConcat = '';
                                
                            //retrieve Treatment__c records, if exists, through relationship from Contact, from soql query
                            if (c.Treatments__r.size()>0) {
                                list<Treatment__c> t = c.Treatments__r;
                                
                                //looping through records of Treatment__c and concatenate fields of Treament__c and Category__c
                                for (integer i = 0; i < t.size(); i++) {
                                    treatmentConcat += t.get(i).Treatment__c + ', ';
                                    categoryConcat += t.get(i).Category__c + ', '; 
                                }
                            }
                                                
                            vol.name = c.Name;
                            vol.ageRange = c.Age_Range__c;
                            vol.ageRangeDOB = c.Age_Range_DOB__c;
                            vol.cancerType = c.Cancer_Type_del__c;
                            vol.carerOrPatient = c.Patient_or_Carer__c;
                            vol.volunteerStatus = c.Volunteer_Status__c;
                            vol.id = c.Id;
                            vol.treatment = treatmentConcat;
                            vol.category = categoryConcat;
                            vol.experiences = c.Experiences__c;
                            vol.lastCcReferralMatchDate = string.valueOf(c.Last_CC_Referral_Match_Date__c);
                            volList.add(vol);                           
                            }                                                                                           
                        }                        
                }
                catch (exception e) {                   
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                }               
        }
                
        //runs the search with parameters passed via javascript
        //appends the dynamic soql
        public PageReference runSearch() {
                string soql = 'select c.Cancer_Type_del__c, c.Carer_Cancer_Type__c, c.Patient_or_Carer__c, Experiences__c, Last_CC_Referral_Match_Date__c, c.Age_Range__c, c.Age_Range_DOB__c, c.Id, c.Name, c.FirstName, c.LastName, c.Status__c, c.Volunteer_Status__c, c.Cancer_Connect__c, (Select t.Category__c, t.Treatment__c From Treatments__r t) from Contact c where c.Cancer_Connect__c = \'Active\' and c.Cancer_Type_del__c != null';
                                
                paramCancerType = ApexPages.currentPage().getParameters().get('cancerType');
                paramAgeRange= ApexPages.currentPage().getParameters().get('ageRange');
                paramTreatment= ApexPages.currentPage().getParameters().get('treatment');
                paramCategory= ApexPages.currentPage().getParameters().get('category');
                
                if(!(paramCancerType .equals('') && paramAgeRange.equals('') && paramTreatment.equals('') && paramCategory.equals(''))) {
                        if (!paramCancerType .equals(''))
                            soql += ' and c.Cancer_Type_del__c like \'%'+String.escapeSingleQuotes(paramCancerType )+'%\'';                                                 
                        if (!paramAgeRange.equals(''))
                            soql += ' and (c.Age_Range__c like \'%'+String.escapeSingleQuotes(paramAgeRange)+'%\'' + ' or c.Age_Range_DOB__c like \'%'+String.escapeSingleQuotes(paramAgeRange)+'%\')';
                        
                        //set soql query for Treatment__c object if related parameters passed in
                        if (!paramTreatment.equals('') || !paramCategory.equals('')) {
                            string soqlSub = ' and id in (select tr.CC_Volunteer_Contact__c from Treatment__c tr where tr.CC_Volunteer_Contact__c != null';                         
                            if (!paramTreatment.equals(''))
                                soqlSub += ' and tr.Treatment__c like \'%'+String.escapeSingleQuotes(paramTreatment)+'%\'';
                            if (!paramCategory.equals(''))
                                soqlSub += ' and tr.Category__c like \'%'+String.escapeSingleQuotes(paramCategory)+'%\'';
                                
                            soql = soql + soqlSub + ')';
                        }
                        //execute the query
                        runQuery(soql + ' limit 50');                        
                }
                return null;            
        }
        
        //use apex describe to build the picklist values
        public list<string> cancerType {
                get {
                        if (cancerType == null) {
                                cancerType = new list<string>();
                                Schema.Describefieldresult field = Contact.Cancer_Type_del__c.getDescribe();
                                
                                for (Schema.Picklistentry f : field.getPicklistValues())
                                        cancerType.add(f.getLabel());
                        }
                        return cancerType;                                              
                }               
                set;
        }
        
        public list<string> ageRange {
                get {
                        if (ageRange == null) {
                                ageRange = new list<string>();
                                Schema.Describefieldresult field = Contact.Age_Range__c.getDescribe();
                                
                                for (Schema.Picklistentry f : field.getPicklistValues())
                                        ageRange.add(f.getLabel());
                        }                       
                        return ageRange;
                }
                set;
        }
        
        public list<string> Treatment {
                get {
                        if (treatment == null) {
                                treatment = new list<string>();
                                Schema.Describefieldresult field = Treatment__c.Treatment__c.getDescribe();
                                
                                for (Schema.Picklistentry f : field.getPicklistValues())
                                        treatment.add(f.getLabel());
                        }                       
                        return treatment;
                }
                set;
        }  
        
        public list<string> Category {
                get {
                        if (category == null) {
                                category = new list<string>();
                                Schema.Describefieldresult field = Treatment__c.Category__c.getDescribe();
                                
                                for (Schema.Picklistentry f : field.getPicklistValues())
                                        category.add(f.getLabel());
                        }                       
                        return category;
                }
                set;
        }               
                
        static testMethod void testVolunteer() {
            
            Account testAccount = new Account(name = 'Test Account');
            insert testAccount;
            
            Contact testContact = new Contact(firstname = 'Test first name', lastname = 'Test last name', accountid = testAccount.id, cancer_type_del__c = 'test type', age_range__c = 'test age range', birthdate = Date.newInstance(1970,1,1));
            insert testContact;
            
            Treatment__c testTreatment = new Treatment__c(cc_volunteer_contact__c = testContact.id, category__c = 'test category', treatment__c = 'test treatment');
            insert testTreatment;
            
            test.startTest();
            VolunteerSearchCCController testing = new VolunteerSearchCCController();
            
            PageReference pageRef = new PageReference('/apex/VolunteerSearch');
                        
            pageRef.getParameters().put('cancerType', 'test type');
            pageRef.getParameters().put('ageRange', 'test age range');
            pageRef.getParameters().put('treatment', 'test treatment');
            pageRef.getParameters().put('category', 'test category'); 
            
            test.setCurrentPage(pageRef);
            
            testing.runSearch();
            //positive test for normal process
            string s = 'select c.Cancer_Type_del__c, c.Carer_Cancer_Type__c, c.Patient_or_Carer__c, c.Age_Range__c, c.Id, c.Name, c.FirstName, c.LastName, c.Status__c, c.Cancer_Connect__c, c.Volunteer_Status__c, (Select t.Category__c, t.Treatment__c From Treatments__r t) from Contact c where c.Cancer_Connect__c = \'Active\' and c.Cancer_Type_del__c != null limit 20';
            testing.runQuery(s);
            //negative test for erorr message
            string s1 = 'select c.test_wrong__c, c.Cancer_Type_del__c, c.Carer_Cancer_Type__c, c.Patient_or_Carer__c, c.Age_Range__c, c.Id, c.Name, c.FirstName, c.LastName, c.Status__c, c.Cancer_Connect__c, c.Volunteer_Status__c, (Select t.Category__c, t.Treatment__c From Treatments__r t) from Contact c where c.Cancer_Connect__c != null and c.Cancer_Type_del__c != null limit 20';
            testing.runQuery(s1);           
            
            list<string> testCancerType = new list<string>();
            Schema.Describefieldresult field1 = Contact.Cancer_Type_del__c.getDescribe();
            for (Schema.Picklistentry f : field1.getPicklistValues())
                testCancerType.add(f.getLabel());           
            system.assertEquals(testCancerType.size(), testing.cancerType.size());

            list<string> testAgeRange = new list<string>();
            Schema.Describefieldresult field2 = Contact.Age_Range__c.getDescribe();
            for (Schema.Picklistentry f : field2.getPicklistValues())
                testAgeRange.add(f.getLabel());         
            system.assertEquals(testAgeRange.size(), testing.AgeRange.size());

            list<string> testTreatment1 = new list<string>();
            Schema.Describefieldresult field3 = Treatment__c.Treatment__c.getDescribe();
            for (Schema.Picklistentry f : field3.getPicklistValues())
                testTreatment1.add(f.getLabel());           
            system.assertEquals(testTreatment1.size(), testing.Treatment.size());
            
            list<string> testCategory = new list<string>();
            Schema.Describefieldresult field4 = Treatment__c.Category__c.getDescribe();
            for (Schema.Picklistentry f : field4.getPicklistValues())
                testCategory.add(f.getLabel());         
            system.assertEquals(testCategory.size(), testing.Category.size());
                                                                    
            for (integer i = 0; i < testing.volList.size(); i++)
                if (testing.volList.get(i).id == testTreatment.CC_Volunteer_Contact__c)
                    system.assertEquals(testing.volList.get(i).treatment, testTreatment.Treatment__c);
            
            system.assertEquals('test type', testing.paramCancerType);
            system.assertEquals('test age range', testing.paramAgeRange);
            system.assertEquals('test treatment', testing.paramTreatment);
            system.assertEquals('test category', testing.paramCategory);
                        
            test.stopTest();
        }
}