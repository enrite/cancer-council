@isTest
public class TestJobRoleRedirectExtension
{
    public static testMethod void myUnitTest()
    {       
        Contact c = new Contact(FirstName = 'Test',
                                LastName = 'Test');
        insert c;                                
             
        RecordType rt = [SELECT Id
                         FROM RecordType
                         WHERE sObjectType = 'Registration__c'
                         AND Name = 'Event Volunteer'];
             
        Registration__c r = new Registration__c(Host_Team_Captain__c = c.Id, RecordTypeId = rt.Id);
        insert r;
        
        Job_Role__c j = new Job_Role__c(Registration__c = r.Id);
        insert j;
        
        JobRoleRedirectExtension ext = new JobRoleRedirectExtension(new ApexPages.StandardController(j));        
        ext.Redir();
    }
}