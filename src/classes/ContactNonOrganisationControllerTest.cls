/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ContactNonOrganisationControllerTest {
    // to test search existing record and update, then create registration and opportunity records
    static testMethod void myUnitTestExistingRecord() {

        map<string, Id> recordTypeMap = new map<string, Id>
                        {'Non Organisation'=>[select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1].Id
                        , 'Contact'=>[select id from recordType where sobjectType='Contact' and name='Contact' limit 1].Id
                        , 'Opportunity'=>[select id from recordType where sobjectType='Opportunity' and name='Donation' limit 1].Id};
        
        Account acc = new Account(name='Test Test', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com', RecordTypeId=recordTypeMap.get('Non Organisation'));
        insert acc;        
        
        Contact con = new Contact(AccountId=acc.Id, Salutation='Mr', FirstName='Test Test', LastName='Test Existing', MailingStreet='Test Test', MailingCity='Adelaide'
            , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');
        insert con;
        // contact id will be used in soql to search contact record
        Contact c = [select Contact_ID__c from Contact where Id = :con.Id];        
        
        Campaign camp = new Campaign (Name='Test Parent', Campaign_Code__c='T Parent', Status = 'In Progress', IsActive=true);
        insert camp;
        
        test.startTest();
        ContactNonOrganisationController cno = new ContactNonOrganisationController();
        // create registration and opportunity records
        cno.objectTypeSelected = 'RegOpp';
        system.assert(cno.getObjType().size()>0);
        
        PageReference pageRef = new PageReference('/apex/ContactNonOrganisation');        
        test.setCurrentPage(pageRef);
        // simulated cancel button clicked
        cno.cancel();
        // set back to testing page
        test.setCurrentPage(pageRef);
        
        cno.contact = con;
        // search existing contact record by contact id
        cno.contactIdStr = c.Contact_ID__c;
        cno.searchContact();
        
        // create registration and opportunity record for found existing contact record        
        cno.getRegistration();
        cno.getOpportunity();
        cno.opportunity.CampaignId = camp.Id;//comment this out for negative test
        cno.registration.Host_Team_Captain_Org__c = con.AccountId;
        cno.getRole();
        
        cno.save();
        
        test.stopTest();
    }

    // to test create new contact record, then create registration and opportunity records
    static testMethod void myUnitTestNewRecord() {

        map<string, Id> recordTypeMap = new map<string, Id>
                        {'Non Organisation'=>[select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1].Id
                        , 'Contact'=>[select id from recordType where sobjectType='Contact' and name='Contact' limit 1].Id
                        , 'Opportunity'=>[select id from recordType where sobjectType='Opportunity' and name='Donation' limit 1].Id};

        Contact contact=new Contact(FirstName='Test Test', LastName='Test New', MailingStreet='Test Test', MailingCity='Adelaide'
                    , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');                                
        
        Campaign camp = new Campaign (Name='Test Parent', Campaign_Code__c='T Parent', Status = 'In Progress', IsActive=true);
        insert camp;
        
        test.startTest();
        ContactNonOrganisationController cno = new ContactNonOrganisationController();
        
        cno.getContact();
        cno.contact = contact;
        cno.save();
        
        test.stopTest();
    }

    // to test create new contact record (no first name), then create registration and opportunity records
    static testMethod void myUnitTestNewNoFirstNameRecord() {

        map<string, Id> recordTypeMap = new map<string, Id>
                        {'Non Organisation'=>[select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1].Id
                        , 'Contact'=>[select id from recordType where sobjectType='Contact' and name='Contact' limit 1].Id
                        , 'Opportunity'=>[select id from recordType where sobjectType='Opportunity' and name='Donation' limit 1].Id};

        Contact contact=new Contact(LastName='Test New No FirstName', MailingStreet='Test Test', MailingCity='Adelaide'
                    , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');                                
        
        Campaign camp = new Campaign (Name='Test Parent', Campaign_Code__c='T Parent', Status = 'In Progress', IsActive=true);
        insert camp;
        
        test.startTest();
        ContactNonOrganisationController cno = new ContactNonOrganisationController();
        
        cno.getContact();
        cno.contact = contact;        
        
        // create registration and opportunity record for found existing contact record  
        cno.objectTypeSelected = 'RegOpp';
        cno.getRegistration();
        cno.getOpportunity();
        cno.opportunity.CampaignId = camp.Id;
        cno.getRole();
        cno.getMailMatching();
        cno.getPostalMatching();        
                
        cno.save();
        
        test.stopTest();
    }
    
    // to do negative testing
    static testMethod void myUnitTestNegative() {
        
        test.startTest();
        ContactNonOrganisationController cno = new ContactNonOrganisationController();
        
        cno.getContact();
        cno.objectTypeSelected = 'RegOpp';
        cno.getRegistration();
        cno.getOpportunity();
        cno.getRole();
        
        cno.save();
        test.stopTest();
    }    
}