public class TRImportRegistrationBatch
        extends TRImportBase
        implements Database.Batchable<sObject>, Database.Stateful
{
    public Database.QueryLocator start(Database.BatchableContext info)
    {
        /*if(!RunThisOnly)
        {
            delete [SELECT  ID
                    FROM    TR_Import_Registration__c
                    WHERE   Processed__c != null];
        }*/
                
        if(processedSuccesfully == null)
        {
            processedSuccesfully = 0;
        }           
        return Database.getQueryLocator([SELECT ID,
                                                Team_Name__c,
                                                Team_ID__c, 
                                                TShirt_Size__c, 
                                                TR_Transaction_ID__c, 
                                                TR_Registration_ID__c, 
                                                TR_Constituent_ID__c, 
                                                Survivor_Lap__c, 
                                                Registration_Fee__c, 
                                                Registration_Date__c, 
                                                Participant_Type__c, 
                                                Last_Name__c, 
                                                Is_Captain__c, 
                                                Company_Name__c,
                                                First_Name__c, 
                                                Emergency_Phone__c, 
                                                Emergency_Name__c, 
                                                Cancer_Experience__c, 
                                                Parent_Name__c, 
                                                Parent_Phone__c,
                                                Parent_Email__c,
                                                Relative_Cancer__c,
                                                Friend_Cancer__c,
                                                Other_Cancer__c,
                                                Campaign_ID__c,
                                                Survivor__c,
                                                Carer__c,
                                                TR_Last_Modified_Date__c,
                                                Error__c,
                                         		Record_Type__c
                                        FROM    TR_Import_Registration__c
                                        WHERE   Processed__c = null
                                        ORDER BY ID]);
    }
    
    public void execute(Database.BatchableContext info, List<TR_Import_Registration__c> scope)
    {       
        for(TR_Import_Registration__c stagingRecord : scope)
        {
            processedSuccesfully++;
            Savepoint sp = Database.setSavepoint();
            try
            {
                stagingRecord.Error__c = ''; 
                stagingRecord.Processing_Result__c = null;
                stagingRecord.Registration__c = null;
                stagingRecord.Contact_Registration__c = null;

                if(stagingRecord.Participant_Type__c == 'Adult Survivor/Carer Lap Participant' &&
                        String.isBlank(stagingRecord.Team_Name__c) &&
                        String.isBlank(stagingRecord.Team_ID__c))
                {
                    processNonRegistered(stagingRecord);
                }
                else 
                {
                    processRegistration(stagingRecord);    
                }         
            }
            catch(Exception ex)
            {
                if(sp != null)
                {
                    Database.rollback(sp);
                }
                processedSuccesfully--;
                stagingRecord.Processed__c = null;
                stagingRecord.Registration__c = null;
                stagingRecord.Contact_Registration__c = null;
                stagingRecord.Error__c = ex.getMessage() +  '\n' + ex.getStackTraceString();
            }
        }
        update scope;
    }
    
    public void finish(Database.BatchableContext info)
    {
        if(RunThisOnly || Test.isRunningTest())
        {
            return;
        }
        TRImportTransactionBatch regBatch = new TRImportTransactionBatch();
        Database.executeBatch(regBatch, BatchSize);
    }

    private void processNonRegistered(TR_Import_Registration__c stagingRecord)
    {
        Campaign cmpgn = getCampaign(stagingRecord);
        if(cmpgn == null)
        {
            return;
        }
        Contact constituent = getContact(stagingRecord);
        if(constituent == null)
        {
            return;
        }
        Integer constituentId = parseInteger(stagingRecord.TR_Constituent_ID__c, stagingRecord);
        Contact_Registrations__c contactRegistration = null;
        for(Contact_Registrations__c cr : [SELECT   ID,
                                                    Registration__c
                                            FROM    Contact_Registrations__c
                                            WHERE   TR_Registration_ID__c = :stagingRecord.TR_Registration_ID__c AND
                                                    TR_Registration_ID__c != null])
        {
            contactRegistration = cr;
        }
        if(contactRegistration == null)
        {
            Registration__c registration = new Registration__c(Campaign__c = cmpgn.ID);
            setRegistrationFields(stagingRecord, registration, true);
            registration.Host_Team_Captain__c = constituent.ID;
            registration.Status__c = 'Survivor/Carer';
            insert registration;

            contactRegistration = new Contact_Registrations__c(Registration__c = registration.ID, Contact__c = constituent.ID);
            setContactRegistrationFields(stagingRecord, contactRegistration, true);
            contactRegistration.Status__c = 'Survivor/Carer';
            insert contactRegistration;

            stagingRecord.Registration__c = registration.ID;
            stagingRecord.Processing_Result__c = RESULT_NONREGISTERD_PARTICIPANT;
        }
        else
        {
            contactRegistration.Survivor_or_Carer__c = getSurvivorCarer(stagingRecord);
            contactRegistration.Survivor_Carer_Lap__c = isOneOrYes(stagingRecord.Survivor_Lap__c);
            update contactRegistration;

            stagingRecord.Registration__c = contactRegistration.Registration__c;
            stagingRecord.Processing_Result__c = RESULT_TEAMMEMBER;
        }
        stagingRecord.Contact_Registration__c = contactRegistration.ID;
        setProcessedAndOwnerFields(stagingRecord);
    }
    
    private void processRegistration(TR_Import_Registration__c stagingRecord)
    {       
        Boolean newRegistration = false;        
        Boolean newContactRegistration = false; 
        Datetime trTimestamp = parseDateTime(stagingRecord.TR_Last_Modified_Date__c, stagingRecord);
        Registration__c registration = null; 
        Registration__c teamRegistration = null; 
        Contact constituent = getContact(stagingRecord);
        if(constituent == null)
        {
            stagingRecord.Processing_Result__c = RESULT_CONTACTNOTFOUND;
            stagingRecord.Error__c += 'Constituent not found';
            return;
        }
        for(Registration__c reg : [SELECT   ID,
                                            TR_Registration_ID__c,
                                            TR_Team_ID__c,
                                            Status__c,
                                            Registration_Date__c,
                                            Host_Team_Captain_Org__c,
                                            Org_Primary_Contact__c,
                                            Host_Team_Captain__r.TeamRaiser_CONS_ID__c,
                                            Team_Name__c,
                                            Last_Updated_by_Team_Raiser__c
                                    FROM    Registration__c
                                    WHERE   (TR_Registration_ID__c != null AND
                                            TR_Registration_ID__c = :stagingRecord.TR_Registration_ID__c) OR
                                            (TR_Team_ID__c != null AND
                                            TR_Team_ID__c = :stagingRecord.Team_ID__c)])
        {
            if(reg.TR_Registration_ID__c != null)
            {
                registration = reg;
            }
            if(reg.TR_Team_ID__c != null && reg.TR_Registration_ID__c == null)
            {
                teamRegistration = reg;
            }
        }
        // no match on Registration ID, try match on Team ID
        if(registration == null)
        {
            registration = teamRegistration;
        }
        Boolean existingSurvivorCarer = registration.Status__c == 'Survivor/Carer';
        if(registration != null && registration.Host_Team_Captain__c != null)
        {
            if((translateBoolean(stagingRecord.Is_Captain__c) && String.valueOf(registration.Host_Team_Captain__r.TeamRaiser_CONS_ID__c) != stagingRecord.TR_Constituent_ID__c) ||
                    (!translateBoolean(stagingRecord.Is_Captain__c) && String.valueOf(registration.Host_Team_Captain__r.TeamRaiser_CONS_ID__c) == stagingRecord.TR_Constituent_ID__c))
            {
                stagingRecord.OwnerId = TeamRaiserConflictQueue.ID;
                stagingRecord.Processing_Result__c = RESULT_MANUALUPDATEREQUIRED;
                stagingRecord.Error__c += 'Team captain changed';
                stagingRecord.Processed__c = DateTime.now();
                return;
            }
        }
        if(registration == null)
        {
            // team not found, try and create one
            Campaign cmpgn = getCampaign(stagingRecord);
            if(cmpgn == null)
            {
                stagingRecord.Processing_Result__c = RESULT_CAMPAIGNNOTFOUND;
                stagingRecord.Error__c += 'Campaign not found';
                return;
            }
            registration = new Registration__c(Campaign__c = cmpgn.ID);
            setRegistrationFields(stagingRecord, registration, true);
            registration.Host_Team_Captain__c = constituent.ID; 
            registration.Host_Team_Captain_Org__c = null;
            insert registration;
            stagingRecord.Processing_Result__c = RESULT_NEWTEAM;
            newRegistration = true;
        }

        Contact_Registrations__c contactRegistration = null;
        for(Contact_Registrations__c cr : [SELECT   ID,
                                                    Status__c,
                                                    Shirt_Size__c,
                                                    Last_Updated_by_Team_Raiser__c,
                                                    Registration__r.TR_Team_ID__c,
                                                    Registration__r.Team_Name__c
                                            FROM    Contact_Registrations__c
                                            WHERE   TR_Registration_ID__c = :stagingRecord.TR_Registration_ID__c AND
                                                    TR_Registration_ID__c != null])
                                            //WHERE   Registration__c = :registration.ID AND
                                            //        Contact__c = :constituent.ID])
        {
            /*if(cr.Last_Updated_by_Team_Raiser__c > trTimestamp)
            {
                stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
                stagingRecord.Contact_Registration__c = cr.ID;
                stagingRecord.Registration__c = registration.ID;
                stagingRecord.Processing_Result__c = RESULT_TIMESTAMPCONFLICT;
                stagingRecord.Error__c += getTimeStampDifference(cr.Last_Updated_by_Team_Raiser__c, trTimestamp);
                return;
            }*/
            if(stagingRecord.Team_ID__c != cr.Registration__r.TR_Team_ID__c ||
                    stagingRecord.Team_Name__c != cr.Registration__r.Team_Name__c)
            {
                stagingRecord.OwnerId = TeamRaiserConflictQueue.ID;
                stagingRecord.Processing_Result__c = RESULT_MANUALUPDATEREQUIRED;
                stagingRecord.Error__c += 'Team changed';
                stagingRecord.Processed__c = DateTime.now();
                return;
            }
            stagingRecord.Processing_Result__c = RESULT_TEAMMEMBER;
            contactRegistration = cr;
        }
        if(contactRegistration != null)
        {
            Boolean conflict = false;
            if(!existingSurvivorCarer && contactRegistration.Status__c != getContactRegistrationStatus(stagingRecord))
            {
                stagingRecord.Error__c += 'Contact Registration status changed';
                conflict = true;
            }
            if(contactRegistration.Shirt_Size__c != stagingRecord.TShirt_Size__c)
            {
                stagingRecord.Error__c += 'Contact Registration shirt size changed';
                conflict = true;
            }
            if(conflict)
            {
                stagingRecord.OwnerId = TeamRaiserConflictQueue.ID;
                stagingRecord.Processing_Result__c = RESULT_MANUALUPDATEREQUIRED;
                stagingRecord.Processed__c = DateTime.now();
                return;
            }
        }
        if(contactRegistration == null)
        {
            stagingRecord.Processing_Result__c = RESULT_NEWCONTACTREGISTRATION;
            contactRegistration = new Contact_Registrations__c(Contact__c = constituent.ID, Registration__c = registration.ID);
            newContactRegistration = true;
            // if this is null then we either have a new record or the contact has been moved to another team.
            // Do we need to cleanup the old contact registrations?
        }
        if(String.isNotBlank(stagingRecord.TShirt_Size__c))
        {
            contactRegistration.Shirt_Size__c = stagingRecord.TShirt_Size__c;
        }
        setContactRegistrationFields(stagingRecord, contactRegistration, !existingSurvivorCarer);
        upsert contactRegistration;

        // if registration was previously cancelled but adding a new contact, activate the registration
        if(contactRegistration.Status__c == STATUS_REGISTERED && registration.Status__c == STATUS_CANX)
        {
            registration.Status__c = STATUS_REGISTERED;
            update registration;
        }
        // if this contact is cancelled and no other active contact, cancel registration
        if(contactRegistration.Status__c == STATUS_CANX && registration.Status__c == STATUS_REGISTERED)
        {
            if([SELECT  COUNT() 
                FROM    Contact_Registrations__c 
                WHERE   Registration__c = :registration.ID AND 
                        Status__c = :STATUS_REGISTERED] == 0)
            {
                registration.Status__c = STATUS_CANX;
                update registration;
            }
        }

        stagingRecord.Contact_Registration__c = contactRegistration.ID;
        stagingRecord.Registration__c = registration.ID;
        
        if(constituent.Survivor__c != SURVIVOR_YES)
        {
            Boolean updateContact = false;
            if(stagingRecord.Cancer_Experience__c == CANCEREXPERIENCE_IHAVEORHADCANCER)
            {
                constituent.Survivor__c = SURVIVOR_YES;
                updateContact = true;
            }
            else if(stagingRecord.Cancer_Experience__c == CANCEREXPERIENCE_CAREGIVERFORSOMEONEWHO && 
                        constituent.Survivor__c != SURVIVOR_CARER)
            {
                constituent.Survivor__c = SURVIVOR_CARER;
                updateContact = true;
            }
            if(updateContact)
            {
                update constituent;
            }
        }
                
        // update opportunity record if one is found
        Boolean oppFound = false;
        Opportunity opp = null;
        for (Opportunity o : [SELECT Id,
                                     Amount
                              FROM Opportunity
                              WHERE Income_Type__c = 'Registration Fee'
                              AND TR_Transaction_ID__c != null
                              AND TR_Transaction_ID__c = :stagingRecord.TR_Transaction_ID__c])
        {
            opp = o;
            if (oppFound)
            {
                stagingRecord.Error__c += 'Multiple Opportunities found\n';
            }
            else
            {
                try
                {
                    if(String.isNotBlank(stagingRecord.Registration_Fee__c))
                    {
                        o.Amount = Decimal.valueOf(stagingRecord.Registration_Fee__c);
                        update o;
                    }
                }
                catch(Exception e)
                {
                    stagingRecord.Error__c += 'Error updating Opportunity\n';    
                }
                oppFound = true;    
            }
        }
// 31/10/2016 not ready for production as yet
//        if(newContactRegistration)
//        {
//            createSalesOrder(stagingRecord, registration, contactRegistration, constituent, opp, getCampaign(stagingRecord));
//        }
        setProcessedAndOwnerFields(stagingRecord);                
    }

    private String getSurvivorCarer(TR_Import_Registration__c stagingRecord)
    {
        String s = null;
        if(isOneOrYes(stagingRecord.Survivor__c) && 
            isOneOrYes(stagingRecord.Carer__c))
        {
            s = SURVIVOR_BOTH;
        }
        else if(isOneOrYes(stagingRecord.Survivor__c))
        {
            s = SURVIVOR_SURVIVOR;
        }
        else if(isOneOrYes(stagingRecord.Carer__c))
        {
            s = SURVIVOR_CARER;
        }
        return s;
    }

    private String getCancerRelationship(TR_Import_Registration__c stagingRecord)
    {
        String s = '';
        if(isOneOrYes(stagingRecord.Relative_Cancer__c))
        {
            s += CONTACTREGISTRATION_RELATIVECANCER + ';';
        }
        if(isOneOrYes(stagingRecord.Other_Cancer__c))
        {
            s += CONTACTREGISTRATION_OTHERCANCER + ';';
        }
        else if(isOneOrYes(stagingRecord.Friend_Cancer__c))
        {
            s += CONTACTREGISTRATION_FRIENDCANCER + ';';
        }
        return String.isBlank(s) ? null : s;
    }
    
    @TestVisible
    private void setRegistrationFields(TR_Import_Registration__c stagingRecord, Registration__c registration, Boolean updateSurvivorCarer)
    {
        registration.Status__c = (stagingRecord.Record_Type__c == RECORDTYPE_CANCELLED) ? STATUS_CANX : STATUS_REGISTERED;
        registration.RecordTypeId = RegistrationRecordType.ID; 
        registration.Team_Name__c = stagingRecord.Team_Name__c;
        registration.Registration_Method__c = 'TeamRaiser';
        registration.Emergency_Name__c = stagingRecord.Emergency_Name__c;
        registration.Emergency_Contact_Number__c = stagingRecord.Emergency_Phone__c;
        registration.Employer__c = stagingRecord.Company_Name__c;
        registration.Registration_Date__c = parseDate(stagingRecord.Registration_Date__c, stagingRecord);
        if(String.isNotBlank(stagingRecord.TShirt_Size__c))
        {
            registration.Shirt_Size__c = stagingRecord.TShirt_Size__c;
        }
        registration.Shirt_Type__c = 'RFL Shirt';
        registration.Cancer_Experience__c = stagingRecord.Cancer_Experience__c;
        registration.Parent_Guardian_Name__c = stagingRecord.Parent_Name__c;
        if(updateSurvivorCarer)
        {
            registration.Survivor_Carer_Lap__c = isOneOrYes(stagingRecord.Survivor_Lap__c);
        }
        registration.Participant_Type__c = stagingRecord.Participant_Type__c;
        registration.TR_Team_ID__c = stagingRecord.Team_ID__c;
        registration.TR_Registration_ID__c = stagingRecord.TR_Registration_ID__c;
        //registration.Web_Registration__c = true;
    }

     private void setContactRegistrationFields(TR_Import_Registration__c stagingRecord, Contact_Registrations__c contactRegistration, Boolean updateSurvivorCarer)
    {
        contactRegistration.Team_Captain__c = translateBoolean(stagingRecord.Is_Captain__c) ? CONTACTREGISTRATION_CAPTAIN : null;
        contactRegistration.Parent_Guardian_Name__c = stagingRecord.Parent_Name__c;
        contactRegistration.Parent_Guardian_Email__c = stagingRecord.Parent_Email__c;
        contactRegistration.Parent_Guardian_Phone__c = stagingRecord.Parent_Phone__c;
        contactRegistration.Cancer_Experience__c = stagingRecord.Cancer_Experience__c;
        if(updateSurvivorCarer)
        {
            contactRegistration.Survivor_or_Carer__c = getSurvivorCarer(stagingRecord);
            contactRegistration.Survivor_Carer_Lap__c = isOneOrYes(stagingRecord.Survivor_Lap__c);
        }
        contactRegistration.Cancer_Relationship__c = getCancerRelationship(stagingRecord);
        contactRegistration.Shirt_Size__c = stagingRecord.TShirt_Size__c;
        contactRegistration.Shirt_Type__c = 'RFL Shirt';
        contactRegistration.TR_Registration_ID__c = stagingRecord.TR_Registration_ID__c;
        contactRegistration.Participation_Type__c = stagingRecord.Participant_Type__c;
        contactRegistration.Emergency_Name__c = stagingRecord.Emergency_Name__c;
        contactRegistration.Emergency_Contact_Number__c = stagingRecord.Emergency_Phone__c;

        // if the team raiser record type is cancelled then we set the contact registrations to cancelled.
        contactRegistration.Status__c = getContactRegistrationStatus(stagingRecord); //(stagingRecord.Record_Type__c == RECORDTYPE_CANCELLED) ? STATUS_CANX : STATUS_REGISTERED;
        contactRegistration.Last_Updated_by_Team_Raiser__c = Datetime.now();
    }

    private void createSalesOrder(TR_Import_Registration__c stagingRecord, 
                                    Registration__c reg, 
                                    Contact_Registrations__c cr, 
                                    Contact constituent, 
                                    Opportunity opp,
                                    Campaign campgn)
    {
        if(String.isBlank(stagingRecord.TShirt_Size__c) || stagingRecord.TShirt_Size__c == NOSHIRT)
        {
            return;
        }
        PBSI__PBSI_Item__c item = null;
        for(PBSI__PBSI_Item__c i : [SELECT  ID,
                                            PBSI__description__c
                                    FROM    PBSI__PBSI_Item__c 
                                    WHERE   PBSI__Item_Group__c = :campgn.Item_Group__c AND 
                                            Item_Size__c = :cr.Shirt_Size__c])
        {
            item = i;
        }
        if(item == null)
        {
            stagingRecord.Processing_Result__c = RESULT_SHIRTSIZENOTFOUND;
            stagingRecord.Error__c += 'Sales Order Item not found for this Shirt size';
            return;
        }
        PBSI__PBSI_Sales_Order__c so = new PBSI__PBSI_Sales_Order__c();
        so.Contact_Registration__c = cr.ID;
        so.Registration__c = reg.ID;
        so.RecordTypeId = PBSI__PBSI_Sales_Order__c.sObjectType.getDescribe().getRecordTypeInfosByName().get('BDU').getRecordTypeID();
        so.Campaign__c = campgn.ID;
        if(reg.Host_Team_Captain_Org__c == null)
        {
            so.PBSI__Customer__c = constituent.AccountId;
            so.PBSI__Contact__c = constituent.ID;
        }
        else
        {
            for(Registration__c r : [SELECT ID,
                                            Host_Team_Captain_Org__c,
                                            Host_Team_Captain__c,
                                            Host_Team_Captain_Org__r.Name
                                    FROM    Registration__c
                                    WHERE   ID = :reg.ID])
            {
                so.PBSI__Customer__c = r.Host_Team_Captain_Org__c;
                so.PBSI__Contact__c = r.Host_Team_Captain__c;
                so.PBSI__Delivery_Company__c = r.Host_Team_Captain_Org__r.Name;
            }
        }
        
        so.PBSI__Order_Date__c = reg.Registration_Date__c;
        so.PBSI__Status__c = 'Partially Complete';
        so.Business_Unit__c = 'BDU';
        so.PBSI__Delivery_ATTN_to__c = constituent.Full_Name__c;
        so.PBSI__Delivery_ATTN_to_Phone__c = constituent.Phone;
        so.PBSI__Delivery_Streetnew__c = constituent.MailingStreet;
        so.PBSI__Delivery_City__c = constituent.MailingCity;
        so.PBSI__Delivery_State__c = constituent.MailingState;
        so.PBSI__Delivery_Postal_Code__c = constituent.MailingPostalCode;
        so.PBSI__Mode_of_Delivery__c = 'Australia Post';
        if(opp != null)
        {
            so.PBSI__Opportunity__c = opp.ID;
        }
        insert so;

        /*if(opp != null)
        {
            Sales_Order_Opportunity__c soo = new Sales_Order_Opportunity__c();
            soo.Opportunity__c = opp.ID;
            soo.Sales_Order__c = so.ID;
            insert soo;
        }*/

        PBSI__PBSI_Sales_Order_Line__c soLine = new PBSI__PBSI_Sales_Order_Line__c(PBSI__Sales_Order__c = so.ID);
        soLine.PBSI__Quantity_Needed__c = 1.0;
        soLine.PBSI__Item__c = item.ID;
        soLine.Prepaid__c = true;
        insert soLine;

        stagingRecord.Sales_Order__c = so.ID;
    }

    private Contact getContact(TR_Import_Registration__c stagingRecord)
    {
        Integer contactId = parseInteger(stagingRecord.TR_Constituent_ID__c, stagingRecord);
        for(Contact c : [SELECT ID,
                                AccountId,
                                Survivor__c,
                                Full_Name__c,
                                Phone,
                                MailingStreet,
                                MailingCity,
                                MailingState,
                                MailingPostalCode
                        FROM    Contact
                        WHERE   TeamRaiser_CONS_ID__c != null AND 
                                TeamRaiser_CONS_ID__c = :contactId AND 
                                Status__c != :CONTACTSTATUS_TOBEMERGED])
        {
            return c;
        }
        stagingRecord.Processing_Result__c = RESULT_CONTACTNOTFOUND;
        stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
        return null;
    }

    private Campaign getCampaign(TR_Import_Registration__c stagingRecord)
    {
        for(Campaign c : [SELECT    ID,
                                    Name,
                                    LastModifiedDate,
                                    Item_Group__c
                            FROM    Campaign
                            WHERE   TR_Event_ID__c = :stagingRecord.Campaign_ID__c])
        {
            return c;
        }
        stagingRecord.Processing_Result__c = RESULT_CAMPAIGNNOTFOUND; 
        stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
        return null;
    }

    private String getContactRegistrationStatus(TR_Import_Registration__c stagingRecord)
    {
        return stagingRecord.Record_Type__c == RECORDTYPE_CANCELLED ? STATUS_CANX : STATUS_REGISTERED;
    }
}