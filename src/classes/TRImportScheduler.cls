public class TRImportScheduler
	implements Schedulable
{
 	public void execute(SchedulableContext info)
 	{
 		TRImportContactBatch batchInstance = new TRImportContactBatch();
 		Database.executeBatch(batchInstance, Batch_Size__c.getInstance('TR_Import').Records__c.IntValue());
 	}
}