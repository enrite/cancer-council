public with sharing class EntryCheckController {

    public EntryCheckController() {
        contactTitle = 'Contact';
        accountTitle = 'Account';
    }
    
    //input parameter properties
    public string firstName {get;set;}
    public string lastName {get;set;}   
    public string email {get;set;}
    
    public string company {get;set;}
    
    public string street {get;set;}
    public string suburb {get;set;}
    public string postcode {get;set;}
    public string state {get;set;}
    public string phone {get;set;}
    public string mobile {get;set;}
    
    //properties to show section header
    public string contactTitle {get;set;}
    public string accountTitle {get;set;}
    
    public string debug {get;set;}
    private boolean contactHasValue;
    private boolean accountHasValue;
    
    public integer noOfRecordsContact {get; set;}
    public integer noOfRecordsAccount {get; set;}
    public Integer size{get;set;}
    
    public list<Contact> contactList {
        get {
            if (setConContact != null) {                
                return (list<Contact>) setConContact.getRecords();
            }
            else
                return null;
        }
        set;
    }
    public list<Account> accountList {
        get {
            if (setConAccount != null) {                
                return (list<Account>) setConAccount.getRecords();
            }
            else
                return null;
        }
        set;
    }
    
    public ApexPages.StandardSetController setConContact {get; set;}
    public ApexPages.StandardSetController setConAccount {get; set;}

    public PageReference searchResult() {

        defaultValue();        
        contactList = new list<Contact>{};
        accountList = new list<Account>{};
                
        string soqlContact = 'Select Contact_Id__c, Id, MailingStreet, MailingState, MailingPostalCode, MailingCity,' 
            + ' LastName, FirstName, Email, Phone, MobilePhone, Status__c, RecordType.Name From Contact Where';
        string soqlAccount = 'Select Phone, Org_ID__c, Name, Id, Fax, Email__c, BillingStreet, BillingState,' 
            + ' BillingPostalCode, BillingCity, Status__c, RecordType.Name From Account Where';
        
        string soqlSubContact = '';        
         
        if (firstName != '' || lastName != '' || company != '' || street != '' || suburb != '' || postcode != '' || state != '') {
            system.debug('*** debug - ' + firstName + ' ' + lastName);            
            if (firstName != '') {
                soqlSubContact += 'Firstname like \'%' + firstName + '%\' and ';
                contactHasValue = true;            
            }
            if (lastName != '') {
                soqlSubContact += 'LastName like \'%' + lastName + '%\' and ';
                contactHasValue = true;            
            }
            if (company != '' || street != '' || suburb != '' || postcode != '' || state != '') {
                soqlAccount += ' (Id <> null ';
                if (company != '') {
                    soqlAccount += 'and Name like \'%' + company + '%\' ';
                    accountHasValue = true;
                }
                if (street != '') {
                    soqlSubContact += 'MailingStreet like \'%' + street + '%\' and ';
                    soqlAccount += 'and BillingStreet like \'%' + street + '%\' ';
                    contactHasValue = true;
                    accountHasValue = true;
                }
                if (suburb != '') {
                    soqlSubContact += 'MailingCity like \'%' + suburb + '%\' and ';
                    soqlAccount += 'and BillingCity like \'%' + suburb + '%\' ';
                    contactHasValue = true;
                    accountHasValue = true;
                }
                if (postcode != '') {
                    soqlSubContact += 'MailingPostalCode like \'%' + postcode + '%\' and ';
                    soqlAccount += 'and BillingPostalCode like \'%' + postcode + '%\' ';
                    contactHasValue = true;
                    accountHasValue = true;
                }
                if (state != '') {
                    soqlSubContact += 'MailingState like \'%' + state + '%\' and ';
                    soqlAccount += 'and BillingState like \'%' + state + '%\' ';
                    contactHasValue = true;
                    accountHasValue = true;
                }
                soqlAccount += ')'; 
            }
            else {
                soqlAccount += ' Id = null';
            }            
                if (soqlSubContact != '') {
                    soqlContact += ' (' + soqlSubContact + ' Id <> null)';              
                }
                else {
                    soqlContact += '(Id = null)';
                }                             
        }
        else {
            soqlContact += ' Id = null';
            soqlAccount += ' Id = null';            
        }
        if (phone != '' || email != '' || mobile != '') {
            soqlContact += ' or (';
            soqlAccount += ' or (';
            if (phone != '') {
                soqlContact += 'Phone like \'%' + phone + '%\' or ';
                soqlAccount += 'Phone like \'%' + phone + '%\' or ';
                contactHasValue = true;
                accountHasValue = true;
            }
            if (email != '') {
                soqlContact += 'Email like \'%' + email + '%\' or ';
                soqlAccount += 'Email__c like \'%' + email + '%\' or ';
                contactHasValue = true;
                accountHasValue = true;
            }        
            if (mobile != '') {
                soqlContact += 'MobilePhone like \'%' + mobile + '%\' or ';
                contactHasValue = true;
            }
            soqlContact += 'Id = null)';
            soqlAccount += 'Id = null)';
        }
        soqlContact += ' limit 1000';
        soqlAccount += ' limit 1000';
        try {
            if (contactHasValue) {
                debug += soqlContact + '\n<br/>';
                system.debug('*** debug ' + debug);
                //contactList = database.query(soqlContact);                
                setConContact = new ApexPages.StandardSetController(database.query(soqlContact));
                noOfRecordsContact = setConContact.getResultSize();
                setConContact.setPageSize(size);                
                //debug += 'Contact: ' + database.query(soqlContact) + ' (size)' + contactList.size() + '\n<br/>';
                contactTitle = 'Contact [' + string.valueof(setConContact.getResultSize()) + ']';
            }
        
            if (accountHasValue) {
                debug += soqlAccount + '\n<br/>';
                //accountList = database.query(soqlAccount);
                setConAccount = new ApexPages.StandardSetController(database.query(soqlAccount));
                noOfRecordsAccount = setConAccount.getResultSize();
                setConAccount.setPageSize(size);                
                //debug += 'Account: ' + database.query(soqlAccount) + '\n<br/>';
                accountTitle = 'Account [' + string.valueof(setConAccount.getResultSize()) + ']';
            }
        }
        catch (Exception e) {
             ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Load SOQL Exception Occured: ' + soqlContact);
            ApexPages.addMessage(errorMsg);
        }
        return null;
    }
    
    public void defaultValue() {
        debug = '';
        contactHasValue = false;
        accountHasValue = false;
        setConContact = null;
        setConAccount = null;
        size = 20;
        noOfRecordsAccount = null;
        noOfRecordsContact = null;
        contactTitle = 'Contact';
        accountTitle = 'Account';
    }
    
    public PageReference clearAll() {
        firstName = '';
        lastName = '';
        email = '';
        company = '';
        street = '';
        suburb = '';
        postcode = '';
        state = '';
        phone = '';
        mobile = '';
        this.accountList = null;
        this.contactList = null;        
        return null;
    }
    
    public PageReference createContact() {
        PageReference pf = new PageReference('/003/e?');
        pf.setRedirect(true);
        return pf;
    }
}