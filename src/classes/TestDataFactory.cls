@isTest
public class TestDataFactory {
// Test class to generate test data for object records

  public static void createAccountContactTestRecords(integer numAccts) {    
    list<Account> listAccount = new list<Account>();
    Id recordTypeId;
    
        map<string, Id> recordTypeMap = new map<string, Id>
                        {'Non Organisation'=>[select id from recordType where sobjectType='Account' and DeveloperName='Non_Organisation' limit 1].Id
                        , 'Organisation'=>[select id from recordType where sobjectType='Account' and DeveloperName='Organisation' limit 1].Id};    
    
    for (integer i = 1; i<= numAccts; i++) {
      // To separate half of Accounts to Organisation and Non Organisation
      recordTypeId = (i <= numAccts/2) ? recordTypeMap.get('Organisation') : recordTypeMap.get('Non Organisation');
      
      Account acc = new Account(Name = 'Test Account' + i
                                  , BillingStreet='Test Street' + i
                                  , BillingCity='Test City' + i
                                  , BillingState='Test State' + i
                                  , BillingPostalCode='Test Postcode' + i                                  
                                  , Phone='00 0000 000' + i
                  , RecordTypeId = recordTypeId);
      listAccount.add(acc);
    }    
    
    insert listAccount;    
    
    list<Contact> listContact = new list<Contact>();
    for (integer j = 0; j < listAccount.size(); j++) {
      Account acc = listAccount[j];
      
      // For each account just inserted, add contacts
      for (integer k = j; k < j+1; k++) {
        listContact.add(new Contact (FirstName = 'TFirst Name' + k
                      , LastName = 'TLast Name ' + k
                      , MailingStreet = acc.BillingStreet
                      , MailingCity = acc.BillingCity                      
                      , MailingState = acc.BillingState
                      , MailingPostalCode = acc.BillingPostalCode
                      , Phone = '00 0000 000' + k
                      , MobilePhone = '0000 000 00' + k
                      , AccountId = acc.Id));
      }
    }
    // Insert all contacts for all accounts
    insert listContact;    
  }  
  
  public static void createCampaignTestRecords(integer numCamps) {
    list<Campaign> listCampaign = new list<Campaign>();
    
    Campaign parent = new Campaign (Name='Test Parent', Campaign_Code__c='T Parent', Status = 'In Progress', IsActive=true);
        insert parent;
            
    for (integer i = 1; i<= numCamps; i++) {
      Campaign camp = new Campaign (Name='Test Child ' + i, Campaign_Code__c='T Child ' + i, ParentId=parent.id, Status = 'In Progress', IsActive=true);
      listCampaign.add(camp);
    }
    
    insert listCampaign;
  }
  
}