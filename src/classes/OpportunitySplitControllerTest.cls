/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OpportunitySplitControllerTest {

    static testMethod void myPositiveTest() {
        // TO DO: implement unit test

        map<string, Id> recordTypeMap = new map<string, Id>
                        {'Non Organisation'=>[select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1].Id
                        , 'Organisation'=>[select id from recordType where sobjectType='Account' and name='Organisation' limit 1].Id
                        , 'Contact'=>[select id from recordType where sobjectType='Contact' and name='Contact' limit 1].Id};
                        
        Account acc = new Account(name='Test Test', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com', RecordTypeId=recordTypeMap.get('Non Organisation'));
        insert acc;
        
        Account accOrg = new Account(name='Test Test Organisation', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com', RecordTypeId=recordTypeMap.get('Organisation'));
        insert accOrg;            
        
        Contact con = new Contact(AccountId=acc.Id, Salutation='Mr', FirstName='Test Test', LastName='Test Existing', MailingStreet='Test Test', MailingCity='Adelaide'
            , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');
        insert con;
        
        Contact conNoFirstName = new Contact(AccountId=acc.Id, Salutation='Mr', LastName='Test Existing', MailingStreet='Test Test', MailingCity='Adelaide'
            , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');
        insert conNoFirstName;
        
        Campaign camp = new Campaign (Name='Test Parent', Campaign_Code__c='T Parent', Status = 'In Progress', IsActive=true);
        insert camp;
        
        Opportunity opp = new Opportunity (Name='Test Opportunity', Amount=100, StageName='Posted', AccountId=acc.Id, Primary_Contact_at_Organisation__c=con.Id, CampaignId=camp.Id, CloseDate=System.Today());
        insert opp;
        
        Opportunity opp1 = new Opportunity (Name='Test Opportunity', Amount=100, StageName='Posted', AccountId=accOrg.Id, CampaignId=camp.Id, CloseDate=System.Today());
        insert opp1;
        
        
        test.startTest();
        
        //test case for contact opportunity
        PageReference pageRef = new PageReference('/apex/OpportunitySplit?id=' + opp.Id);
        test.setCurrentPage(pageRef);
                
        OpportunitySplitController osc = new OpportunitySplitController(new ApexPages.Standardcontroller(opp));
        
        system.assertEquals(osc.oppBase.id, opp.Id);
        
        //test for first opportunity split row
        osc.innerClassList[0].opp.AccountId=accOrg.Id;
        osc.innerClassList[0].opp.Amount=20;
        osc.innerClassList[0].opp.Income_Type__c='Corporate';
        
        //test for second opportunity split row
        osc.Add();
        osc.innerClassList[1].opp.Primary_Contact_at_Organisation__c=con.Id;
        osc.innerClassList[1].opp.Amount=30;
        osc.innerClassList[1].opp.Income_Type__c='Host FR';       

        //test for third opportunity split row, no contact first name
        osc.Add();
        osc.innerClassList[2].opp.Primary_Contact_at_Organisation__c=conNoFirstName.Id;
        osc.innerClassList[2].opp.Amount=5;
        osc.innerClassList[2].opp.Income_Type__c='Host FR';
        
        osc.Add();
        
        //set rowIndex for row deletion
        osc.rowIndex=string.valueOf(osc.count);        
        osc.Del();
        
        osc.Save();
        
        system.assertEquals(osc.innerClassList[2].opp.Name, conNoFirstName.LastName +'- Donation '+ opp.CloseDate.format());       
        system.assertEquals(osc.sumAmount, osc.innerClassList[0].opp.Amount + osc.innerClassList[1].opp.Amount +osc.innerClassList[2].opp.Amount);
                
        system.debug('***** Positive test osc.innerClassList[0].opp.AccountId ***** ' + osc.innerClassList[0].opp.AccountId);
        system.debug('***** Positive test accOrg.id ***** ' + accOrg.Id);
        
        system.debug('***** Positive test osc.innerClassList[1].opp.Primary_Contact_at_Organisation__c ' + osc.innerClassList[1].opp.Primary_Contact_at_Organisation__c);
        system.debug('***** Positive test con.id ***** ' + con.Id);
 
 
 		//test case for organisation opportunity
        PageReference pageRef1 = new PageReference('/apex/OpportunitySplit?id=' + opp1.Id);
        test.setCurrentPage(pageRef1);
                
        OpportunitySplitController osc1 = new OpportunitySplitController(new ApexPages.Standardcontroller(opp1));
        
        system.assertEquals(osc1.oppBase.id, opp1.Id);
        
        //test for first opportunity split row
        osc1.innerClassList[0].opp.AccountId=accOrg.Id;
        osc1.innerClassList[0].opp.Amount=20;
        osc1.innerClassList[0].opp.Income_Type__c='Corporate';
        
        osc1.Save();
 
        system.assertEquals(osc1.innerClassList[0].opp.Name, accOrg.Name +'- Donation '+ opp1.CloseDate.format());       
        system.assertEquals(osc1.sumAmount, osc1.innerClassList[0].opp.Amount);
        
        test.stopTest();
    }
    
    static testMethod void myAboveAmountTest() {

       map<string, Id> recordTypeMap = new map<string, Id>
                        {'Non Organisation'=>[select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1].Id
                        , 'Organisation'=>[select id from recordType where sobjectType='Account' and name='Organisation' limit 1].Id
                        , 'Contact'=>[select id from recordType where sobjectType='Contact' and name='Contact' limit 1].Id};
                        
        Account acc = new Account(name='Test Test', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com', RecordTypeId=recordTypeMap.get('Non Organisation'));
        insert acc;           

        Account accOrg = new Account(name='Test Test Organisation', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com', RecordTypeId=recordTypeMap.get('Organisation'));
        insert accOrg; 
                
        Contact con = new Contact(AccountId=acc.Id, Salutation='Mr', FirstName='Test Test', LastName='Test Existing', MailingStreet='Test Test', MailingCity='Adelaide'
            , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');
        insert con;
        
        Campaign camp = new Campaign (Name='Test Parent', Campaign_Code__c='T Parent', Status = 'In Progress', IsActive=true);
        insert camp;
        
        Opportunity opp = new Opportunity (Name='Test Opportunity', Amount=100, StageName='Posted', AccountId=acc.Id, Primary_Contact_at_Organisation__c=con.Id, CampaignId=camp.Id, CloseDate=System.Today());
        insert opp;
        
        test.startTest();
        
        PageReference pageRef = new PageReference('/apex/OpportunitySplit?id=' + opp.Id);
        test.setCurrentPage(pageRef);
                
        OpportunitySplitController osc = new OpportunitySplitController(new ApexPages.Standardcontroller(opp));
        
        system.assertEquals(osc.oppBase.id, opp.Id);
        
        //test for first opportunity split row
        osc.innerClassList[0].opp.AccountId=accOrg.Id;
        osc.innerClassList[0].opp.Amount=20;
        osc.innerClassList[0].opp.Income_Type__c='Corporate';
        
        //test for second opportunity split row
        osc.Add();
        osc.innerClassList[1].opp.Primary_Contact_at_Organisation__c=con.Id;
        osc.innerClassList[1].opp.Amount=100;
        osc.innerClassList[1].opp.Income_Type__c='Host FR';        
        
        osc.Save();
        system.assertEquals(osc.sumAmount, 120);
        
        system.debug('***** Above amount osc.innerClassList[0].opp.AccountId ***** ' + osc.innerClassList[0].opp.AccountId);
        system.debug('***** Above amount accOrg.id ***** ' + acc.Id);
        system.debug('***** Above amount sumAmount ***** ' + osc.sumAmount + ' oppBase ' + osc.oppBase.Amount);
        
        system.debug('***** Above amount osc.innerClassList[1].opp.Primary_Contact_at_Organisation__c ' + osc.innerClassList[1].opp.Primary_Contact_at_Organisation__c);
        system.debug('***** Above amount con.id ***** ' + con.Id);
        
        test.stopTest();        	
    }
    
    static testMethod void myNegativeTest() {
	   	//negative test when no opportunity id provided when page loaded and save method called
       	map<string, Id> recordTypeMap = new map<string, Id>
                        {'Non Organisation'=>[select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1].Id
                        , 'Organisation'=>[select id from recordType where sobjectType='Account' and name='Organisation' limit 1].Id
                        , 'Contact'=>[select id from recordType where sobjectType='Contact' and name='Contact' limit 1].Id};
                        
        Account acc = new Account(name='Test Test', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com', RecordTypeId=recordTypeMap.get('Non Organisation'));
        insert acc;           

        Account accOrg = new Account(name='Test Test Organisation', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com', RecordTypeId=recordTypeMap.get('Organisation'));
        insert accOrg; 
                
        Contact con = new Contact(AccountId=acc.Id, Salutation='Mr', FirstName='Test Test', LastName='Test Existing', MailingStreet='Test Test', MailingCity='Adelaide'
            , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com');
        insert con;
        
        Campaign camp = new Campaign (Name='Test Parent', Campaign_Code__c='T Parent', Status = 'In Progress', IsActive=true);
        insert camp;
        
        Opportunity opp = new Opportunity (Name='Test Opportunity', Amount=100, StageName='Posted', AccountId=acc.Id, Primary_Contact_at_Organisation__c=con.Id, CampaignId=camp.Id, CloseDate=System.Today());
        insert opp;
        
        test.startTest();
        
        PageReference pageRef = new PageReference('/apex/OpportunitySplit');
        test.setCurrentPage(pageRef);
        //would cause error because of no opportunity id provided
        OpportunitySplitController osc = new OpportunitySplitController(new ApexPages.Standardcontroller(opp));        
        
        system.debug('***** Negative test osc.innerClassList[0].opp.AccountId ***** ' + osc.innerClassList[0].opp.AccountId);
        system.debug('***** Negative test accOrg.id ***** ' + accOrg.Id);
                
        //would cause error because of no opportunity id provided
        osc.Save();
        
        test.stopTest();    	
    }
}