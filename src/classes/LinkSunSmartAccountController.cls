public with sharing class LinkSunSmartAccountController{
    public static final Integer MIN_SCORE_SHOW_POPUP = 40;
    private static final SelectOption[] cMaxResultsPicklist = new SelectOption[]{
        new SelectOption('10','10'),
        new SelectOption('20','20'),
        new SelectOption('50','50')};

    private SunSmart__c[] mSunSmart;
    private Account[] mAccountResults;
    private LinkSunSmartAccountControllerAccount mRootAccountNode;
    private integer mTotalAccounts;
    private LinkSunSmartAccountControllerAccount[] mSortedAccounts;
    private LinkSunSmartAccountControllerAccount[] mSortedLimitedAccounts;
    
    public String maxResultsSelected{get; set;}
    public boolean displayPopup {get; set;}
    
    public LinkSunSmartAccountController(){
        maxResultsSelected = '10';
        
        String vSunSmartID = ApexPages.CurrentPage().getParameters().get('id');
        mSunSmart = [SELECT
            id,
            Web_Postcode__c,
            Web_State__c,
            Web_Street__c,
            Web_Suburb_Town__c,
            First_Name__c,
            Last_Name__c,
            School_Email__c,
            School_Name__c,
            School_Phone__c
            FROM SunSmart__c
            WHERE id = :vSunSmartID];
        
        if(mSunSmart.size() != 1){
            return;
        }
              
        mAccountResults = [SELECT
            BillingStreet,
            BillingPostalCode,
            BillingCity,
            Phone,
            Name,
            Org_ID__c
            FROM Account
            WHERE
            (RecordTypeId = '01280000000UDUp') AND (
                (Phone = :mSunSmart[0].School_Phone__c AND Phone != null) OR
                (BillingStreet = :mSunSmart[0].Web_Street__c AND BillingStreet != null) OR
                (Name = :mSunSmart[0].School_Name__c AND Name != null))
            LIMIT 500];
        
        mTotalAccounts = mAccountResults.size();
        mSortedAccounts = new LinkSunSmartAccountControllerAccount[]{};
        if(mTotalAccounts > 0){
            mRootAccountNode = new LinkSunSmartAccountControllerAccount(mAccountResults[0], mSunSmart[0]);
            for(integer i = 1; i < mTotalAccounts; i++) mRootAccountNode.add(new LinkSunSmartAccountControllerAccount(mAccountResults[i], mSunSmart[0]));
            mRootAccountNode.buildArrayDesc(mSortedAccounts);
        }
        updateMaxResults();
    }
    
    public String getTotalAccounts(){
        return String.valueOf(mTotalAccounts);
    }
    
    public LinkSunSmartAccountControllerAccount[] getSortedAccounts(){
        if(mSortedLimitedAccounts == null) mSortedLimitedAccounts = new LinkSunSmartAccountControllerAccount[]{};
        return mSortedLimitedAccounts;
    }
    
    public SunSmart__c getSunSmart(){
        return mSunSmart.size() > 0 ? mSunSmart[0] : null;
    }
    
    public void setMaxResults(integer pMax){
        if(mTotalAccounts > 0){
            integer vMax = mSortedAccounts.size() > pMax ? pMax : mSortedAccounts.size();
            mSortedLimitedAccounts = new LinkSunSmartAccountControllerAccount[vMax];
            for(integer i = 0; i < vMax; i++) mSortedLimitedAccounts[i] = mSortedAccounts[i];
        }
    }
    
    public SelectOption[] getResultsOptions(){
        return cMaxResultsPicklist;
    }
    
    public void updateMaxResults(){
        setMaxResults(Integer.valueOf(maxResultsSelected));
    }
    
    public String getTotalAccountsDisplayed(){
        integer vMax = Integer.valueOf(maxResultsSelected);
        return String.valueOf(mSortedAccounts.size() > vMax ? vMax : mSortedAccounts.size());
    }
    
    public void closePopup(){        
        displayPopup = false;    
    }
         
    public PageReference newContact(){
        return newContactConfirm();
    }
    
    public PageReference newContactConfirm(){    
        return new PageReference('/apex/LinkSunSmartAccountNewPage?id=' + mSunSmart[0].id);
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mSunSmart[0].id);
    }
    
}