/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTeamRaiserImport 
{
	private static Integer teamRaiserConsID = 1234;
	private static Integer teamRaiserDonorConsID = 5678;
	private static String teamRaiserTeamID = '1234';
	private static String teamRaiserEventID = '1234';
	
    static testMethod void testImportContact1() 
    {
    	insertImportConstituent(true);
		TRImportContactBatch b = new TRImportContactBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportContact2() 
    {
    	TR_Import_Constituent__c tc = insertImportConstituent(true);
		Contact c = new Contact();
		c.FirstName = 'Test';
		c.LastName = 'Test'; 
		c.Phone = tc.Home_Phone__c;
        c.MobilePhone = '0412 345 678';
        c.Email = tc.Primary_Email__c;
		insert c;
		TRImportContactBatch b = new TRImportContactBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportContact3() 
    {
    	insertImportConstituent(true);
		Contact c = new Contact();
		c.FirstName = 'NoMatch';
		c.LastName = 'NoMatch'; 
		c.TeamRaiser_CONS_ID__c = teamRaiserConsID;
		insert c;
		TRImportContactBatch b = new TRImportContactBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportContact4() 
    {
    	insertImportConstituent(false);
		Contact c = new Contact();
		c.FirstName = 'NoMatch';
		c.LastName = 'NoMatch'; 
		c.TeamRaiser_CONS_ID__c = teamRaiserConsID;
		insert c;
		TRImportContactBatch b = new TRImportContactBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportContact5() 
    {
    	insertImportConstituent(true);	
		TRImportContactBatch b = new TRImportContactBatch();
		b.RunThisOnly = true;
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportTeam1() 
    {
    	insertImportTeam(true);
		TRImportTeamBatch b = new TRImportTeamBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportTeam2() 
    {
    	TR_Import_Team__c t = insertImportTeam(true);
    	t.Event_ID__c = 'NotExists';
    	update t;
		TRImportTeamBatch b = new TRImportTeamBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportTeam3() 
    {
    	TR_Import_Team__c t = insertImportTeam(true);
    	t.Team_ID__c = 'NotExists';
    	update t;
		TRImportTeamBatch b = new TRImportTeamBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportTeam4() 
    {
    	TR_Import_Team__c t = insertImportTeam(true);
    	t.Team_ID__c = 'NotExists';
    	t.Team_Name__c = null;
    	update t;
		TRImportTeamBatch b = new TRImportTeamBatch();
		Database.executeBatch(b, 1);
    }
   
    static testMethod void testImportRegistration1() 
    {
    	TR_Import_Registration__c t = insertImportRegistration(true, true);
		TRImportRegistrationBatch b = new TRImportRegistrationBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportRegistration2() 
    {
    	TR_Import_Registration__c t = insertImportRegistration(true, true);
    	t.Team_ID__c = 'NotExists';
    	update t;
		TRImportRegistrationBatch b = new TRImportRegistrationBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportRegistration3() 
    {
    	TR_Import_Registration__c t = insertImportRegistration(true, true);
    	t.TR_Constituent_ID__c = 'NotExists';
    	update t;
		TRImportRegistrationBatch b = new TRImportRegistrationBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportRegistration4() 
    {
    	TR_Import_Registration__c t = insertImportRegistration(true, true);
    	t.Is_Captain__c = '0';
    	update t;
		TRImportRegistrationBatch b = new TRImportRegistrationBatch();
		Database.executeBatch(b, 1);
    }
     
    static testMethod void testImportRegistration5() 
    {
    	TR_Import_Registration__c t = insertImportRegistration(true, false);
    	t.Is_Captain__c = '0';
    	update t;
		TRImportRegistrationBatch b = new TRImportRegistrationBatch();
		Database.executeBatch(b, 1);
		
		b.setRegistrationFields(t, new Registration__c(), false);
		b.setRegistrationFields(t, new Registration__c(), true);
    }
    
    static testMethod void testImportTransaction1() 
    {
    	TR_Import_Transaction__c t = insertImportTransaction(true);
		TRImportTransactionBatch b = new TRImportTransactionBatch();
		b.RunThisOnly = true;
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportTransaction2() 
    {
    	TR_Import_Transaction__c t = insertImportTransaction(true);
    	t.Team_ID__c = 'NotFound';
    	update t;
		TRImportTransactionBatch b = new TRImportTransactionBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportTransaction3() 
    {
    	TR_Import_Transaction__c t = insertImportTransaction(true);
    	t.TR_Constituent_ID__c = 'NotFound';
    	update t;
		TRImportTransactionBatch b = new TRImportTransactionBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportTransaction4() 
    {
    	TR_Import_Transaction__c t = insertImportTransaction(true);
		t.Participant_ID__c = 'NotFound';
    	update t;
		TRImportTransactionBatch b = new TRImportTransactionBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportDuplicate1() 
    {
    	TR_Import_Duplicate__c t = insertImportDuplicate(true);
		TRImportDuplicateBatch b = new TRImportDuplicateBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportDuplicate2() 
    {
    	TR_Import_Duplicate__c t = insertImportDuplicate(false);
		TRImportDuplicateBatch b = new TRImportDuplicateBatch();
		Database.executeBatch(b, 1);
    }
    
    static testMethod void testImportDuplicate3() 
    {
    	TR_Import_Duplicate__c t = insertImportDuplicate(false);
    	t.TR_Constituent_ID__c = 'NotFound';
		t.TR_Duplicate_Constituent_ID__c = 'NotFound';
		update t;
		TRImportDuplicateBatch b = new TRImportDuplicateBatch();
		Database.executeBatch(b, 1);
    }
    
    private static TR_Import_Constituent__c insertImportConstituent(Boolean futureModifiedDate)
    {
    	TR_Import_Constituent__c ic = new TR_Import_Constituent__c();
       	ic.TR_ID__c = teamRaiserConsID;
		ic.Member_ID__c = '';
		ic.Suffix__c = 'Mr';
		ic.First_Name__c = 'Test';
		ic.Last_Name__c = 'Test';
		ic.Accept_Email__c = '1';
		ic.Accepts_Post__c = '1';
		ic.Birthdate__c = '01/01/1969';
		ic.Employer__c = 'Test';
		ic.Gender__c = 'Male';
		ic.Home_City__c = 'Test';
		ic.Home_Phone__c = '1234567890';
		ic.Home_Postcode__c = '5000';
		ic.Home_State__c = 'SA';
		ic.Home_Street1__c = '1 Test St';
		ic.Home_Street2__c = '2 Test St';
		ic.Home_Street3__c = '3 Test St';
		ic.Primary_Email__c = 'a@test.com';
		ic.Title__c = 'Mr';
		ic.Professional_Suffix__c = 'Professor';
		Integer hourAdjustment = futureModifiedDate ? 1 : -1;
		ic.TR_Last_Modifed_Date__c = (DateTime.now().addHours(hourAdjustment)).format();
		insert ic;
		return ic;
    }
    
    private static TR_Import_Team__c insertImportTeam(Boolean futureModifiedDate)
    {
    	Account acc = new Account();
    	acc.Name = 'Test';
    	insert acc; 
    	
    	Campaign c = new Campaign();
    	c.Name = 'Test';
    	c.TR_Event_ID__c = teamRaiserEventID;
    	insert c;
    	
    	Contact cn = new Contact();
		cn.FirstName = 'NoMatch';
		cn.LastName = 'NoMatch'; 
		cn.TeamRaiser_CONS_ID__c = teamRaiserConsID;
		insert cn;
    	
    	Registration__c r = new Registration__c();
    	r.Campaign__c = c.ID;
    	r.Team_ID__c = teamRaiserTeamID;
    	r.Host_Team_Captain__c = cn.ID;
    	insert r;

    	TR_Import_Team__c it = new TR_Import_Team__c();
       	it.Team_Name__c = 'Test';
		it.Team_ID__c = teamRaiserTeamID;
		it.Team_Description__c = 'Test';
		it.Goal__c = 'Test';
		it.Event_Name__c = 'Test';
		it.Event_ID__c = teamRaiserEventID;
		it.Division_Name__c = 'Test';
		it.Company_Name__c = acc.Name;
										
		Integer hourAdjustment = futureModifiedDate ? 1 : -1;
		it.TR_Last_Modified_Date__c = (DateTime.now().addHours(hourAdjustment)).format();
		insert it;
		return it;		
    }
    
    private static TR_Import_Registration__c insertImportRegistration(Boolean futureModifiedDate, Boolean insertContactRegistrations)
    {
    	Account acc = new Account();
    	acc.Name = 'Test';
    	insert acc; 
    	
    	Campaign c = new Campaign();
    	c.Name = 'Test';
    	c.TR_Event_ID__c = teamRaiserEventID;
    	insert c;
    	
    	Contact cn = new Contact();
		cn.FirstName = 'NoMatch';
		cn.LastName = 'NoMatch'; 
		cn.TeamRaiser_CONS_ID__c = teamRaiserConsID;
		insert cn;
    	
    	Registration__c r = new Registration__c();
    	r.Campaign__c = c.ID;
    	r.Team_ID__c = teamRaiserTeamID;
    	r.Host_Team_Captain__c = cn.ID;
    	insert r;
    	
    	if(insertContactRegistrations)
    	{
	    	Contact_Registrations__c cr = new Contact_Registrations__c();
	    	cr.Registration__c = r.ID;
	    	cr.Contact__c = cn.ID;
	    	insert cr;
    	}
    	
    	TR_Import_Registration__c ir = new TR_Import_Registration__c();
    	ir.Team_Name__c = 'Test';
		ir.Team_ID__c = teamRaiserTeamID; 
		ir.TShirt_Size__c = 'L'; 
		//ir.TR_Transaction_ID__c 
		ir.TR_Registration_ID__c = 'Test';
		ir.TR_Constituent_ID__c = teamRaiserConsID.format().replace(',', '');
		ir.Survivor_Lap__c = '1';
		ir.Registration_Fee__c  = '1.00';
		ir.Registration_Date__c = '01/01/2010'; 
		ir.Participant_Type__c = '1061';
		ir.Parent_Name__c = 'Test'; 
		ir.Number_of_Students__c = '1';
		ir.Last_Name__c = 'Test';
		ir.Is_Captain__c = '1'; 
		ir.First_Name__c = 'Test';
		ir.Emergency_Phone__c = '1234567890';
		ir.Emergency_Name__c = 'Test';
		ir.Cancer_Experience__c = 'Test';
		ir.Campaign_ID__c = 'Test';
		Integer hourAdjustment = futureModifiedDate ? 1 : -1;
		ir.TR_Last_Modified_Date__c = (DateTime.now().addHours(hourAdjustment)).format();
    	insert ir;
		return ir;
    }
    
    private static TR_Import_Transaction__c insertImportTransaction(Boolean futureModifiedDate)
    {
    	Account acc = new Account();
    	acc.Name = 'Test';
    	insert acc; 
    	
    	Campaign c = new Campaign();
    	c.Name = 'Test';
    	c.TR_Event_ID__c = teamRaiserEventID;
    	insert c;
    	
    	Contact cn = new Contact();
		cn.FirstName = 'Test';
		cn.LastName = 'Test'; 
		cn.TeamRaiser_CONS_ID__c = teamRaiserConsID;
		insert cn;
		
		Contact donor = new Contact();
		donor.FirstName = 'Donor';
		donor.LastName = 'Donor'; 
		donor.TeamRaiser_CONS_ID__c = teamRaiserDonorConsID;
		insert donor;
    	
    	Registration__c r = new Registration__c();
    	r.Campaign__c = c.ID;
    	r.Team_ID__c = teamRaiserTeamID;
    	r.Host_Team_Captain__c = cn.ID;
    	insert r;

    	TR_Import_Transaction__c it = new TR_Import_Transaction__c();
    	it.Transaction_Type__c = 'Test'; 
		it.Transaction_ID__c = 'Test'; 
		it.Transaction_Date__c = '01/01/2010';
		it.Transaction_Amount__c = '45.90';
		it.Title__c = 'Test';
		it.Team_ID__c = teamRaiserTeamID;
		it.TR_Constituent_ID__c = teamRaiserConsID.format().replace(',', '');
		it.Soft_Credit_Type__c = 'Test';
		it.Registration_ID__c = 'Test';
		it.Payment_Gateway_ID__c = 'Test';
		it.Participant_ID__c = teamRaiserDonorConsID.format().replace(',', ''); 
		it.On_Employer_Behalf__c = '1'; 
		it.Merchant_Account_ID__c = 'Test'; 
		it.Member_ID__c = 'Test'; 
		it.Last_Name__c = 'Test'; 
		it.In_Honour_Last_Name__c = 'Test'; 
		it.In_Honour_Gift_Type__c = 'Test'; 
		it.In_Honour_First_Name__c = 'Test'; 
		it.Home_Street2__c = 'Test'; 
		it.Home_Street1__c = 'Test'; 
		it.Home_State__c = 'Test'; 
		it.Home_Postcode__c = 'Test'; 
		it.Home_Country__c = 'Test'; 
		it.Home_City__c = 'Test';
		it.Fund_Type__c = 'Test';
		it.First_Name__c = 'Test';
		it.Event_Name__c = 'Test';
		it.Event_ID__c = 'Test';
		it.Employer__c = 'Test';
		it.Credit_Type__c = 'Test';
		it.CC_Type__c = 'Test';
		it.CC_Number__c = 'Test';
		it.Billing_Title__c = 'Test';
		it.Billing_Street2__c = 'Test';
		it.Billing_Street1__c = 'Test';
		it.Billing_State__c = 'Test';
		it.Billing_Postcode__c = 'Test';
		it.Billing_Last_Name__c = 'Test';
		it.Billing_First_Name__c = 'Test';
		it.Billing_Country__c = 'Test';
		it.Billing_City__c = 'Test';
		it.Anonymous__c = 'Test';
		Integer hourAdjustment = futureModifiedDate ? 1 : -1;
		it.TR_Last_Modified_Date__c = (DateTime.now().addHours(hourAdjustment)).format();
    	insert it;
		return it;
    }
    
    private static TR_Import_Upsell__c insertImportUpsell(Boolean futureModifiedDate)
    {
    	TR_Import_Upsell__c iu = new TR_Import_Upsell__c();
		//Integer hourAdjustment = futureModifiedDate ? 1 : -1;
		//it.TR_Last_Modified_Date__c = (DateTime.now().addHours(hourAdjustment)).format();
    	insert iu;
		return iu;
    }
    
    private static TR_Import_Duplicate__c insertImportDuplicate(Boolean createDuplicate)
    {
    	Contact cn = new Contact();
		cn.FirstName = 'Test';
		cn.LastName = 'Test'; 
		cn.TeamRaiser_CONS_ID__c = teamRaiserConsID;
		insert cn;
		
		Contact dup = new Contact();
		dup.FirstName = 'Duplicate';
		dup.LastName = 'Duplicate'; 
		dup.TeamRaiser_CONS_ID__c = teamRaiserDonorConsID;
		if(createDuplicate)
		{
			dup.Merge_Contact__c = cn.ID;
		}
		insert dup;
		
    	TR_Import_Duplicate__c td = new TR_Import_Duplicate__c();
    	td.TR_Member_ID__c = 'Test';
    	td.TR_Constituent_ID__c = cn.TeamRaiser_CONS_ID__c.format().replace(',', ''); 
		td.TR_Duplicate_Member_ID__c = 'Test';
		td.TR_Duplicate_Constituent_ID__c = dup.TeamRaiser_CONS_ID__c.format().replace(',', '');
		insert td;
		return td;
		
    }
}