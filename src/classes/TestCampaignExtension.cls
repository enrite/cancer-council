@isTest
public class TestCampaignExtension
{
    public static testMethod void myUnitTest()
    {                                         

        
        Account el = new Account(Name = 'test',
                                 RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Event Location'].Id);
        insert el;
        
        Campaign c = new Campaign(Name = 'test');
        insert c;
        
        Event_Details__c ed = new Event_Details__c(Event_Location__c = el.Id,
                                                   Campaign__c = c.Id,
                                                   Start_Date__c = Date.today(),
                                                   End_Date__c = Date.today().addDays(1));
        insert ed;
        
        
        //Generate the Page for the test
        PageReference pageRef = new PageReference('/apex/CloneCampaign?Id=' + c.Id);    
    
        //Test condition, the page is created  
        System.assertNotEquals(null,pageRef);
        System.assertEquals('/apex/CloneCampaign?Id='+c.Id,pageRef.getUrl());
        Test.setCurrentPageReference(pageRef);

        
        //Get the standard controller for the apex page then the controller extention
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        CampaignExtension myCE = new CampaignExtension(sc);
    
        myCE.newCampaignName = '';
        myCE.newCampaignCode = '';
            
        //Clone without setting field values first and test for page message
        myCE.cloneRecord();
         
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        for(Apexpages.Message msg:msgs)
        {
            if (msg.getDetail().contains('Campaign Name and Campaign Code'))
            {
            
                b = true;
            }
        }
        system.assert(b);
    
        myCE.newCampaignName = 'test1';
        myCE.newCampaignCode = 'test1';
        
        myCE.cloneRecord();
  
        List<Campaign> allCampaigns = [SELECT Id FROM Campaign];
    
        System.assert(allCampaigns.size() == 2);
        
        List<Event_Details__c > allEvDetails = [SELECT Id FROM Event_Details__c ];
        System.assert(allEvDetails.size() == 2);
        
        
    }
}