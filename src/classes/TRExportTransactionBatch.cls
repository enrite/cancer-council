public with sharing class TRExportTransactionBatch 
    extends TRExportBase
    implements Database.Batchable<sObject>, Database.Stateful
{
    public TRExportTransactionBatch (TR_Export__c exportRecord)
    {
        super(exportRecord);
    }
    
    public TRExportTransactionBatch(TRExportBase prevBatch)
    {
        super(prevBatch);
    }
    
    private final String ATTACHMENTNAME = getFileName('Transaction');
    private final String INCOMETYPE_DONATION = 'Donation';
    
    public Database.QueryLocator start(Database.BatchableContext info)
    {               
        Attachment att = createAttachment(ATTACHMENTNAME, getHeader());
                
        return Database.getQueryLocator([SELECT ID,
                                                Amount,
                                                StageName,
                                                Income_Type__c,
                                                CloseDate,
                                                Donation_Salutation__c,
                                                Payment_type__c,
                                                Primary_Contact_at_Organisation__c,
                                                Primary_Contact_at_Organisation__r.TeamRaiser_CONS_ID__c, 
                                                Primary_Contact_at_Organisation__r.Contact_ID__c,
                                                Registration__r.TR_Team_ID__c,
                                                Registration__r.Team_Name__c,
                                                Registration__r.Campaign__r.Name,
                                                Registration__r.Campaign__r.TR_Event_ID__c,
                                                Registration__r.Host_Team_Captain__c,
                                                Registration__r.Host_Team_Captain__r.TeamRaiser_CONS_ID__c,
                                                Registration__r.Host_Team_Captain__r.Contact_ID__c,
                                                Registration__r.Org_Primary_Contact__c,
                                                Registration__r.Org_Primary_Contact__r.TeamRaiser_CONS_ID__c,           
                                                Registration__r.Org_Primary_Contact__r.Contact_ID__c, 
                                                LastModifiedDate,
                                                Last_Updated_by_Team_Raiser__c,  
                                                (SELECT     ID,
                                                            Account__c,
                                                            Contact__r.TeamRaiser_CONS_ID__c,
                                                            Contact__r.Contact_ID__c
                                                FROM        Soft_Credits__r)
                                        FROM    Opportunity
                                        WHERE   RecordType.Name = 'Donation' AND
                                                Income_Type__c != 'Registration Fee' AND
                                                Registration__r.Campaign__r.TR_Event_ID__c != null AND
                                                Amount != 0 AND
                                                TR_Transaction_ID__c = null AND
                                                Teamraiser_Export_Date__c = null]);
    }
    
    public void execute(Database.BatchableContext info, List<Opportunity> scope) 
    {       
        Attachment att = getAttachment(ATTACHMENTNAME);        
        String b = att.Body.toString();
                                    
        for(Opportunity c : scope)
        {
            // if the last modified datetime is (basically) the same as the last updated by team raiser datetime then skip the record
            if (c.Last_Updated_by_Team_Raiser__c != null
                && c.LastModifiedDate < c.Last_Updated_by_Team_Raiser__c.addMinutes(1))
            {
                continue;
            }
        
            if(c.Soft_Credits__r.isEmpty())
            {
                b += processTransaction(c); 
                TransactionRecords++;   
            }
            for(Soft_Credit__c sc : c.Soft_Credits__r)        
            {
                b += processSoftCredit(c, sc); 
                TransactionRecords++;    
            }  
            c.Teamraiser_Export_Date__c = TRExport.Export_Time__c;
            try
            {
                update c;
            }
            catch(Exception ex)
            {
                TR_Export_Error__c err = new TR_Export_Error__c(TR_Export__c = TRExport.ID);
                err.Message__c = ex.getMessage() + '\n' + ex.getStackTraceString();
                insert err;
            }
        }
        att.Body = Blob.valueOf(b);
        update att;
    } 
    
    public void finish(Database.BatchableContext info)
    {
        finaliseBatch();        
    }
    
    private String getHeader()
    {
        String line = '';
        line += getField('FR_ID') +
                    getField('EVENT_NAME') +
                    getField('PART_CONS_ID') +
                    getField('PART_MEMBER_ID') +
                    getField('GIFT_ID') +
                    getField('GIFT_AMOUNT') +
                    getField('GIFT_CONFIRMED') +
                    getField('GIFT_ANONYMOUS') + 
                    getField('PAYMENT_TYPE') +
                    getField('TEAM_ID') +
                    getField('TEAM_NAME') +
                    getField('DONOR_CONS_ID') +
                    getField('DONOR_MEMBER_ID') + 
                    getField('GIFT_DATE') +
                    getField('DONOR_RECOGNITION_NAME') +
                    getField('BATCH_ID');  
        return endLine(line);
    }
    
    private String processTransaction(Opportunity c)
    {

        String line = '';
        line += getEventFields(c);

        if(c.Primary_Contact_at_Organisation__c == null || c.Income_Type__c == 'Team FR')
        {
            line += getField('');
            line += getField('');
        }
        else 
        {

            line += getField(c.Primary_Contact_at_Organisation__r.TeamRaiser_CONS_ID__c, 11);
            line += getField(c.Primary_Contact_at_Organisation__r.Contact_ID__c, 11);
        }

        line += getOpportunityFields(c);
     
        line += getField(TRExport.Name, 255);

        return endLine(line);
    }
    
    private String processSoftCredit(Opportunity c, Soft_Credit__c sc)
    {
         String line = '';
         line += getEventFields(c);
         line += getField(sc.Contact__r.TeamRaiser_CONS_ID__c, 11);
         line += getField(sc.Contact__r.Contact_ID__c, 32);
         line += getOpportunityFields(c);     
         line += getField(TRExport.Name, 255);
         return endLine(line);
    }
    
    private String getEventFields(Opportunity c)
    {   
        String line = '';
        line += getField(c.Registration__r.Campaign__r.TR_Event_ID__c, 11);
        line += getField(c.Registration__r.Campaign__r.Name, 255);
        return line;
    }
    
    private String getOpportunityFields(Opportunity c)
    {
        String line = '';
        //TODO: Gift ID
        line += getField('');
        
        line += getField(c.Amount, 11);
        line += getField(c.StageName == 'Posted');
        
        //TODO: Anonymous
        line += getField('');
        
        line += getField(translatePaymentType(c.Payment_Type__c), 11);
        line += getField(c.Registration__r.TR_Team_ID__c, 11);
        line += getField(c.Registration__r.Team_Name__c, 255);
        line += getField(c.Primary_Contact_at_Organisation__r.TeamRaiser_CONS_ID__c, 11);
        line += getField(c.Primary_Contact_at_Organisation__r.Contact_ID__c, 11);
        line += getField(c.CloseDate);
        line += getField(c.Donation_Salutation__c, 255);
 
        return line;
    }
    
    private String translatePaymentType(String s)
    {
        if(s == PAYMENTTYPE_BPAY || 
                s == PAYMENTTYPE_DIRECTBANKING ||
                s == PAYMENTTYPE_DIRECTBANKINGEFT ||
                s == PAYMENTTYPE_SHARES ||
                s == PAYMENTTYPE_PAYPAL)
        {
            return PAYMENTTYPE_CASH;
        }
        else if(s == PAYMENTTYPE_ARTEZONLINE ||
                s == PAYMENTTYPE_CREDITCARD)
        {
            return PAYMENTTYPE_CREDITCARD;
        }
        else if(s == PAYMENTTYPE_CHEQUEMONEYORDER)
        {
            return PAYMENTTYPE_CHEQUE;
        }
        else if(s == PAYMENTTYPE_OTHER)
        {
            return PAYMENTTYPE_LATER;
        }
        return PAYMENTTYPE_CASH;
    }
}