public with sharing class LinkSunSmartContactNewController{
    private SunSmart__c[] mSunSmart;
    private Contact mContact;
    String mSunSmartID;
    
    public LinkSunSmartContactNewController(){

        mSunSmartID = ApexPages.CurrentPage().getParameters().get('id');
        
        mSunSmart = [SELECT
            id,
            Web_Postcode__c,
            Web_State__c,
            Web_Street__c,
            Web_Suburb_Town__c,
            First_Name__c,
            Last_Name__c,
            School_Contact_Email__c,
            School_Email__c,
            School_Name__c,
            School_Phone__c,
            School__c,
            Title__c
            FROM SunSmart__c
            WHERE id = :mSunSmartID];
        
        mContact = new Contact();
        if(mSunSmart.size() > 0){
            mContact.RecordTypeId = '01280000000UDUq'; //Contact
            mContact.Salutation = mSunSmart[0].Title__c;
            mContact.FirstName = mSunSmart[0].First_Name__c;
            mContact.LastName = mSunSmart[0].Last_Name__c;
            mContact.Email = mSunSmart[0].School_Contact_Email__c;
            mContact.How_Heard__c = 'SunSmart';
        }
        
    }
    
    public PageReference save(){
        mContact.AccountId = mSunSmart[0].School__c;
        insert mContact;
        mSunSmart[0].Key_Contact__c = mContact.id;
        update mSunSmart;
        return new PageReference('/' + mSunSmartID);
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mSunSmart[0].id);
    }
    
    public SunSmart__c getRegistration(){
        return mSunSmart.size() > 0 ? mSunSmart[0] : null;
    }
    
    public Contact getContact(){
        return mContact;
    }
}