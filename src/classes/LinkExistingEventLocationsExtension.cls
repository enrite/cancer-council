public with sharing class LinkExistingEventLocationsExtension
{    
    public LinkExistingEventLocationsExtension(ApexPages.StandardController c)
    {
        record = (Campaign)c.getRecord();
    }

    private Campaign record;

    public List<EventLocationWrapper> EventLocations
    {
        get
        {
            if (EventLocations == null)
            {
                EventLocations = new List<EventLocationWrapper>();

                // find all Event Locations that aren't already linked to this Campaign
                for (Account r : [SELECT Id,
                                         Name,
                                         ShippingStreet,
                                         ShippingCity,
                                         ShippingState,
                                         ShippingPostalCode
                                  FROM Account
                                  WHERE RecordType.Name = 'Event Location'
                                  AND Id NOT IN (SELECT Event_Location__c FROM Campaign_Event_Location__c WHERE Campaign__c = :record.Id)                      
                                  ORDER BY Name])
                {
                    EventLocations.add(new EventLocationWrapper(r));
                }                                      
            }
            return EventLocations;
        }
        set;
    }
    
    public PageReference Link()
    {
        try
        {
            List<Campaign_Event_Location__c> irs = new List<Campaign_Event_Location__c>();
            
            // loop through the list and find the EventLocations that were checked
            for (EventLocationWrapper rw : EventLocations)
            {
                // if checked then create a junction record and add it to the list
                if (rw.Checked)
                {
                    irs.add(new Campaign_Event_Location__c(Campaign__c = record.Id, Event_Location__c = rw.EventLocation.Id));                
                }    
            }
            
            // insert the records
            insert irs;
            
            // return to the Campaign
            return new PageReference('/' + record.Id);
        }
        catch(Exception e)
        {
            return CustomException.formatException(e);
        }
    }

    private class EventLocationWrapper
    {
        public EventLocationWrapper(Account el)
        {
            EventLocation = el;
        }
    
        public Boolean Checked { get; set; }
        public Account EventLocation { get; set; }
    }
}