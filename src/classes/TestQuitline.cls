/**
 * This class provides test methods for the quitline forms
 */
@isTest
public class TestQuitline {
    public static testMethod void testQuitLineFormController() {

    	setupSettingsData();

    	QuitLineFormController controller = new QuitLineFormController();
    	controller.Submitted = false;
    	// test properties;
    	System.assert(controller.QuitLineCase != null);
    	System.assert(!controller.IsReferral);
    	System.assert(String.isBlank(controller.Comments));
    	System.assert(!controller.Submitted);
    	System.assert(!controller.PreferredDays.isEmpty());
    	System.assert(!controller.BestTimeToCall.isEmpty());
    	System.assert(!controller.AtsiOptions.isEmpty());
    	System.assert(!controller.YesNoOptions.isEmpty());
    	System.assert(!controller.HowDidYouHearAboutQuitline.isEmpty());
    	System.assert(!controller.Genders.isEmpty());
    	System.assert(!controller.InterpreterRequired.isEmpty());
    	System.assert(!controller.InterpreterLanguages.isEmpty());
    	System.assert(!QuitLineFormController.getPickValues(new Case(), 'CALD_Language__c', '', '-- None --').isEmpty());
    	System.assert(!QuitLineFormController.getPickValues(new Case(), 'CALD_Language__c', '', '').isEmpty());
    	System.assert(!String.isBlank(controller.Sitekey));
    	System.assert(String.isBlank(controller.response));

    	controller.QuitLineCase = null;
    	controller.IsReferral = true;
    	controller.Submitted = true;
    	controller.Comments = 'test';


    	// reinitialise for other test.
    	controller = new QuitLineFormController();
    	// this will invoke the validation should fail there.
    	PageReference ref = controller.saveOverride();

    	controller.QuitLineCase.First_Name__c = 'Test';
    	controller.QuitLineCase.Last_Name__c = 'Name';
		controller.QuitLineCase.Contact_Street__c = '123 Fake Street';
		controller.QuitLineCase.Suburb__c = 'Adelaide';
		controller.QuitLineCase.State__c = 'SA';
		controller.QuitLineCase.Contact_Postcode__c = '5000';
		controller.QuitLineCase.Phone__c = '123123123';
    	controller.QuitLineCase.Preferred_Day__c = 'Monday';
		controller.QuitLineCase.Preferred_Time_s__c = 'Evening';

		controller.saveOverride();

		// set to referral and save again to get the validation errors for the referral details
		controller.IsReferral = true;
		controller.QuitLineCase.Interpreter__c = 'Required';
		controller.saveOverride();
    }

    public static testMethod void testQuitlineResourceFormController() {
    	setupSettingsData();
    	setUpItemData();

    	QuitLineResourceOrderFormController controller = new QuitLineResourceOrderFormController();
    	// test properties
    	System.assert(controller.SalesOrder != null);
    	System.assert(!controller.ItemGroups.isEmpty());
    	System.assert(String.isBlank(controller.CreditCardHolderName));
    	System.assert(String.isBlank(controller.CreditCardNumber));
    	System.assert(String.isBlank(controller.CreditCardExpiryDate));
    	System.assert(String.isBlank(controller.CreditCardCvv));
    	System.assert(!controller.PaymentErrored);
    	System.assert(!controller.ProceedToCheckout);
    	System.assert(!controller.YesNoOptions.isEmpty());
    	System.assert(controller.TotalAmountPayable != null);

    	controller.checkout();
    	controller.back();
    	controller.save();
    	controller.performPayment();

    	controller = new QuitLineResourceOrderFormController();
    	controller.ItemGroups.get(0).Items.get(0).Item.PBSI__Quantity_Needed__c = 5;
    	controller.SalesOrder.First_Name__c = 'Billy';
		controller.SalesOrder.Last_Name__c = 'Test';
		controller.SalesOrder.Street__c = '123 Fake Street';
		controller.SalesOrder.City__c = 'Adelaide';
		controller.SalesOrder.Postcode__c = '5000';

		controller.checkout();
		controller.save();
		
		controller.CreditCardCvv = '123123qed';
		controller.CreditCardExpiryDate = '11/2asdfasdf1';

		controller.save();

		controller.CreditCardCvv = '123';
		controller.CreditCardExpiryDate = '11/21';
		controller.CreditCardHolderName = 'Test Buddy';
		controller.CreditCardNumber = '4444333322221111';
		controller.save();
		controller.performPayment();
    }

    public static TestMethod void testSecurePay() 
    {                     
        setupSettingsData();
        Test.setMock(HttpCalloutMock.class, new TestMockSecurePay());  
        
        Test.StartTest();
        
        SecurePayUtility.CreditCardPayment('123', '4444333322221111', '12/' + (String.valueOf(Date.today().year() + 1)).right(2), '123', 100, new PBSI__PBSI_Sales_Order__c());
        
        Test.StopTest();    
    }

	public static TestMethod void testLinkCaseContactController() {
		Id recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND Name='Quitline'].get(0).Id;

		Case c1 = new Case();
		c1.RecordTypeId = recordTypeId;
		c1.First_Name__c = 'Test';
		c1.Last_Name__c = 'Guy';
		c1.Contact_Street__c = '123 Fake Street';
		c1.Suburb__c = 'Adelaide';
		c1.Contact_Postcode__c = '5000';
		c1.State__c = 'SA';
		c1.D_O_B__c = Date.today().addYears(-30);

		insert c1;

		Account acc = new Account();
		acc.Name = 'Test Account';

		insert acc;

		Contact c = new Contact();
		c.AccountId = acc.Id;
		c.FirstName = c1.First_Name__c;
		c.LastName = c1.First_Name__c;
		c.MailingStreet = c1.Contact_Street__c;
		c.MailingCity = c1.Suburb__c;
		c.MailingPostalCode = c1.Contact_Postcode__c;
		c.MailingState = c1.State__c;
		c.Birthdate = c1.D_O_B__c;

		insert c;

		ApexPages.currentPage().getParameters().put('caseId', c1.Id);

		LinkCaseContactController controller = new LinkCaseContactController();
		for (LinkCaseContactController.CaseContactMatch ccm : controller.CaseContactMatches) {
			System.assert(ccm.ContactRecord != null);
			System.assert(ccm.Record != null);
			System.debug(ccm.EmailMatched);
			System.debug(ccm.FirstNameMatched);
			System.debug(ccm.LastNameMatched);
			System.debug(ccm.MobileMatched);
			System.debug(ccm.PhoneMatched);
			System.debug(ccm.PostcodeMatched);
			System.debug(ccm.StreetMatched);
			System.debug(ccm.SuburbMatched);
		}
		controller.closePopup();
		controller.selectContact();
		controller.SelectedContactId = 'sdfasdfasfasf';
		controller.selectContact();
		controller.SelectedContactId = c.Id;
		controller.newContact();
		controller.cancel();

	}

    private static void setUpItemData() {
    	PBSI__PBSI_Item_Group__c grp = new PBSI__PBSI_Item_Group__c();
    	grp.Name = 'Quit Resources';
    	grp.PBSI__Item_Group_Code__c = '123';
    	insert grp;

    	Id grpId = grp.Id;
    	System.assert(grpId != null);

    	PBSI__PBSI_Location__c location = new PBSI__PBSI_Location__c();
    	location.Name = 'LOC1';
    	location.PBSI__aisle__c='123';
    	location.PBSI__rack__c = '12';

    	insert location;
    	System.assert(location.Id != null);

    	Id locationId = location.Id;

    	List<PBSI__PBSI_Item__c> items = new List<PBSI__PBSI_Item__c>();
    	items.add(createItem(grpId, locationId, 'Resources', 'Some resource 1', 10, 100, true));
    	items.add(createItem(grpId, locationId, 'Resources', 'Some resource 2', 0, 10, false));
    	items.add(createItem(grpId, locationId, 'Posters', 'Some resource 3', 1, 100, false));
    	items.add(createItem(grpId, locationId, 'Pamphlets', 'Some resource 4', 2, 100, false));

    	insert items;
    }
    private static Integer ITEM_INDEX = 1;
    private static PBSI__PBSI_Item__c createItem(Id grpId, Id locationId, String name, String description, Decimal price, Decimal max, boolean includeAttachment) {
    	PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c();
    	item.Name='Item000' + (ITEM_INDEX++);
    	item.Item_Name__c = name;
    	item.PBSI__description__c = description;
    	item.Webname__c = description;
    	item.Web_Description__c = description;
    	item.Webname_Oxygen__c = description;
    	item.Item_Group_Webname__c = name;
    	item.PBSI__salesprice__c = price;
    	item.Maximum_Items_That_Can_Be_Purchased__c = max;
    	item.PBSI__Item_Group__c = grpId;
    	item.Publish_to_Website__c = true;
    	item.PBSI__Default_Location__c = locationId;
    	if (includeAttachment) {
    		Attachment at = new Attachment();
    		at.Name = 'quitline-item-thumbnail';
    		Attachment at2 = new Attachment();
    		at2.Name = 'quitline-item-download';
    		item.Attachments.add(at);
    		item.Attachments.add(at2);
    	}
    	item.PBSI__defaultunitofmeasure__c = 'EA';
    	return item;
    } 
    private static void setupSettingsData() {
    	List<QuitlineSettings__c> settings = new List<QuitlineSettings__c>();
    	addSetting('RecaptchaBaseUrl', 'https://google.com', settings);
    	addSetting('RecaptchaSecret', '123213123123', settings);
    	addSetting('RecaptchaSiteKey', '123123123', settings);
    	addSetting('SecurePayUrl', 'https://test.url.com', settings);
    	addSetting('SecurePayMerchantId', '123', settings);
    	addSetting('SecurePayPassword', 'password', settings);
    	addSetting('SecurePayTestEnvironment', 'true', settings);

    	insert settings;
    }
    private static void addSetting(String name, String value, List<QuitlineSettings__c> settings) {
    	QuitlineSettings__c setting = new QuitlineSettings__c();
    	setting.Name = name;
    	setting.Configuration__c = value;
    	settings.add(setting);
    }
}