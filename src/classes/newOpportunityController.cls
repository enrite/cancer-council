public class newOpportunityController {

   // These four member variables maintain the state of the wizard.  
    
   // When users enter data into the wizard, their input is stored  
    
   // in these variables.   
    
   Account account;
   Contact contact;
   Opportunity opportunity;
   OpportunityContactRole role;
   RecordType recType;


   // The next four methods return one of each of the four member  
    
   // variables. If this is the first time the method is called,  
    
   // it creates an empty record for the variable.  
    
   public Account getAccount() {
      if(account == null) account = new Account();
      return account;
   }

   public Contact getContact() {
      if(contact == null) contact = new Contact();
      return contact;
   }

   public Opportunity getOpportunity() {
      if(opportunity == null) opportunity = new Opportunity();
      return opportunity;
   }

   public OpportunityContactRole getRole() {
      if(role == null) role = new OpportunityContactRole();
      return role;
   }


   // The next three methods control navigation through  
    
   // the wizard. Each returns a PageReference for one of the three pages  
    
   // in the wizard. Note that the redirect attribute does not need to  
    
   // be set on the PageReference because the URL does not need to change  
    
   // when users move from page to page.  
    
   public PageReference step1() {
      return Page.opptyStep1;
   }

   public PageReference step2() {
      return Page.opptyStep2;
   }

   public PageReference step3() {
      return Page.opptyStep3;
   }


   // This method cancels the wizard, and returns the user to the   
    
   // Opportunities tab  
    
    public PageReference cancel()
    {      PageReference pageRef = new PageReference('/home/home.jsp');
            pageRef.setRedirect(true);
            return pageRef; 
    }

   // This method performs the final save for all four objects, and  
    
   // then navigates the user to the detail page for the new  
    
   // opportunity.  
    
   public PageReference save() {

      // Create the account. Before inserting, copy the contact's  
    
      // phone number into the account phone number field.  
    
      account.phone = contact.phone;
      if(contact.firstname == null  ) {
      account.name = contact.lastname;
      }
      else{
      account.name = contact.firstname+' '+contact.lastname;
      }
      account.fax = contact.fax;
      recType = [select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1];
      account.recordtypeId = recType.id;
      account.status__c = 'Active';
      insert account;

      // Create the contact. Before inserting, use the id field  
    
      // that's created once the account is inserted to create  
    
      // the relationship between the contact and the account.  
    
      contact.accountId = account.id;
      recType = [select id from recordType where sobjectType='Contact' and name='Contact' limit 1];
      contact.recordtypeId = recType.id;
      contact.status__c = 'Active';
      contact.mailingstreet = account.billingstreet;
      contact.mailingcity = account.billingcity;
      contact.mailingstate = account.billingstate;
      contact.mailingcountry = account.billingcountry;
      contact.mailingPostalCode = account.billingPostalCode;
      contact.otherstreet = account.shippingstreet;
      contact.othercity = account.shippingcity;
      contact.otherstate = account.shippingstate;
      contact.othercountry = account.shippingcountry;
      contact.otherPostalCode = account.shippingPostalCode;      
      contact.phone = account.phone;
      contact.fax = account.fax;
      insert contact;

      // Create the opportunity. Before inserting, create   
    
      // another relationship with the account.  
    
      opportunity.accountId = account.id;
      opportunity.Primary_Contact_at_Organisation__c = contact.id;
      If( contact.firstname == null){
          opportunity.name = contact.lastname+'- Donation '+System.Today().format();
      }
      else{opportunity.name = contact.firstname+ ' ' + contact.lastname+'- Donation '+System.Today().format();
      }
      opportunity.Income_Type__c = 'Donation';
      opportunity.closedate = System.Today();
      opportunity.stagename = 'Posted';
      if(contact.Letter_Salutation__c == null  ) {
          If(contact.firstname == null  ) {
              if(contact.salutation == null) {
                    opportunity.donation_salutation__c = 'Friend';
                    } 
              else {
                    Opportunity.donation_salutation__c = contact.salutation +' '+contact.lastname;
                    }
          }
          else{
              Opportunity.donation_salutation__c = contact.salutation +' '+contact.firstname + ' ' +contact.lastname; 
          }
      }
      else{
          opportunity.donation_salutation__c = contact.Letter_Salutation__c;
      }            
      insert opportunity;

      // Create the junction contact role between the opportunity  
    
      // and the contact.  
    
      role.opportunityId = opportunity.id;
      role.contactId = contact.id;
      role.IsPrimary = true;
      insert role;

      // Finally, send the user to the detail page for   
    
      // the new opportunity.  
    


      PageReference opptyPage = new ApexPages.StandardController(opportunity).view();
      opptyPage.setRedirect(true);

      return opptyPage;
   }
   
   static testMethod void testEverything(){
        Test.startTest();
        newOpportunityController testController = new newOpportunityController();
        testController.getAccount();
        testController.getContact();
        testController.getOpportunity();
        testController.getRole();
        testController.contact.firstName = 'J0hn';
        testController.contact.lastName = 'Sm14h';
        testController.save();
        Test.stopTest();
    }

}