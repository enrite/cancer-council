public class newCaseContactController {
    public string caseidstring = System.currentPagereference().getParameters().get('caseid');
    public string render1{get; set;}
    public string render2{get; set;}
    public string render3{get; set;}
    public boolean updateBD;
    public boolean fieldRequired {
    	get {
    		// set field mandatory for Helpline_Case Cases
    		RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Case' AND Id= :cases.RecordTypeId];
    		if (rt.DeveloperName == 'Helpline_Case') {
    			return true;
    		}
    		else {
    			return false;
    		}
    	}
    }
    
    public newCaseContactController(){
        render1 = 'true';
        render2 = 'false';
        render3 = 'false';
        updateBD = false;
        if(caseidstring != null){
            cases = [SELECT Id, Contact.Id, casenumber, RecordTypeId FROM Case WHERE Id = :caseidstring];
            
            if(cases.Contact != null ){
                render1 = 'false';
                render2 = 'true';
            } 
        }      
    }

// These three member variables maintain the state of the wizard.
// When users enter data into the wizard, their input is stored 
// in these variables.
    Account account;
    Contact contact;
    Case cases;

// The next three methods return one of each of the three member
// variables. If this is the first time the method is called,
// it creates an empty record for the variable. Except the CAse is a lookup
    public Account getAccount() {
        if(account == null){
            account = new Account();
            Case aCase = [SELECT Contact_Street__c, Suburb__c, State__c, Contact_Postcode__c FROM Case WHERE id=:caseidstring];

            account.billingstreet = aCase.Contact_Street__c;
            account.billingcity = aCase.Suburb__c;
            account.billingstate = aCase.State__c;
            account.billingpostalcode = aCase.Contact_Postcode__c;

            account.shippingstreet = aCase.Contact_Street__c;
            account.shippingcity = aCase.Suburb__c;
            account.shippingstate = aCase.State__c;
            account.shippingpostalcode = aCase.Contact_Postcode__c;
        }
        return account;
    }

    public Contact getContact() {
        if(contact == null){ 
        	contact = new Contact();
        	
            Case aCase = [SELECT First_Name__c,Last_Name__c,Mobile__c, Case_Email__c,D_O_B__c,Status, Email__c, Contact_Street__c, Phone__c, Country_fo_Birth__c, Gender__c, ATSI__c FROM Case WHERE id=:caseidstring];

            contact.firstName = aCase.First_Name__c;
            contact.lastName = aCase.Last_Name__c;
            contact.mobilephone = aCase.Mobile__c;
            contact.email = aCase.Email__c;
            contact.phone = aCase.Phone__c;
            contact.Country_of_Birth__c = aCase.Country_fo_Birth__c;
            contact.Gender__c = aCase.Gender__c;
            contact.ATSI__c = aCase.ATSI__c;
            contact.Status__c = aCase.Status;
            contact.Birthdate = aCase.D_O_B__c;
            
            if(contact.Birthdate >= Date.today()){
                contact.Birthdate = null;
                updateBD = true;
            }
        }
        return contact;
    }

    public Case getCase() {  
        if(cases == null) {
        	cases = [SELECT id,casenumber,test__c FROM Case WHERE id=:caseidstring];	
        }
        return cases;
    }

// The next three methods control navigation through
// the wizard. Each returns a PageReference for one of the three pages
// in the wizard. Note that the redirect attribute does not need to
// be set on the PageReference because the URL does not need to change
// when users move from page to page.
    public PageReference step1() {
        return Page.Casecontactstep1;
    }

    public PageReference step2() {
        return Page.Casecontactstep2;
    }

    public PageReference step3() {
        if(contact.Birthdate >= Date.today()){
            render3 = 'true';
            return Page.Casecontactstep1;
        }
        render3 = 'false';
        return Page.Casecontactstep3;
    }

// This method cancels the wizard, and returns the user to the
// Case Record it departed from
    public PageReference cancel() {
    	PageReference pageRef = new PageReference('/'+caseidstring);
    	pageRef.setRedirect(true);
    	return pageRef; 
	}

	// This method performs the final save for all three objects, and 
	// then navigates the user to the detail page for the updated case.
	public PageReference save() {
		// Set Transaction Savepoint for rollback when either Account or Contact creation failed 
		SavePoint sp = Database.setSavepoint();
		
	    // Create the account. Before inserting, copy the contact's
	    // phone number into the account phone number field.
	    account.phone = contact.phone;
	    account.name = contact.firstname+' '+contact.lastname;
	    account.fax = contact.fax;
	    account.recordtypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Non_Organisation'].Id;
	    account.status__c = 'Active';
	    
	    if (!UtilRecordDML.dmlInsertRecord(account, newCaseContactController.class.getName())) {
	    	Database.rollback(sp);
	    	// Set Account to null after rollback
	    	account = null;
	    	return null;
	   	}
	
	    // Create the contact. Before inserting, use the id field
	    // that's created once the account is inserted to create
	    // the relationship between the contact and the account.
	    contact.accountId = account.id;
	    contact.recordtypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Client'].Id;
	    contact.status__c = 'Active';
	    contact.mailingstreet = account.billingstreet;
	    contact.mailingcity = account.billingcity;
	    contact.mailingstate = account.billingstate;
	    contact.mailingcountry = account.billingcountry;
	    contact.mailingPostalCode = account.billingPostalCode;
	    contact.phone = account.phone;
	    contact.fax = account.fax;	    
	    
	    if (!UtilRecordDML.dmlInsertRecord(contact, newCaseContactController.class.getName())) {
	    	// Rollback/delete account record that created previously if contact is failed to create	    	
	    	Database.rollback(sp);
	    	// Set Account and Contact to null after rollback
	    	account = null;
	    	contact = null;
	    	return null;
	    }	
	    
	    // Update Case Contact ID and Case Account ID. Before updating, create another relationship with the account.  
	    if(updateBD) {
	    	cases.D_O_B__c = contact.Birthdate;
	    }
	    
	    cases.AccountId = account.Id;
	    cases.ContactId = contact.Id;	    
	    UtilRecordDML.dmlUpdateRecord(cases, newCaseContactController.class.getName());
	
	    // Finally, send the user to the detail page for the updated case.
	    PageReference CasePage = new PageReference('/'+caseidstring);
	    CasePage.setRedirect(true);
	    return CasePage;
	}
}