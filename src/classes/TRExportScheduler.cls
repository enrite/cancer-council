public class TRExportScheduler 
	implements Schedulable
{
 	public void execute(SchedulableContext info)
 	{
 		TR_Export__c exportRecord = null;
 		for(TR_Export__c ex : [SELECT		ID,
 											Export_Time__c
 								FROM		TR_Export__c
 								WHERE		Finish_Time__c = null
 								ORDER BY  	Export_Time__c DESC])
 		{
 			exportRecord = ex;							
 		}
 		if(exportRecord == null)
 		{
 			exportRecord = new TR_Export__c(Export_Time__c = DateTime.now());
 			insert exportRecord;
 		}							
 		
 		TRExportConstituentBatch batchInstance = new TRExportConstituentBatch(exportRecord); 
 		Database.executeBatch(batchInstance, Batch_Size__c.getInstance('TR_Export').Records__c.IntValue());
 	}
}