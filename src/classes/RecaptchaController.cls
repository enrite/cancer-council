public abstract class RecaptchaController {
 
    private static String baseUrl = getQuitlineSetting('RecaptchaBaseUrl');
	
	private static String secret = getQuitlineSetting('RecaptchaSecret');

	// simple regex for phone validation ensure that only nuber or + sign can be used with a maximum of 12 characters
	private static final String PHONE_REGEX = '^([0-9]{2}\\s)*[0-9]{4}\\s[0-9]{4}$';

	private static final String MOBILE_REGEX = '^[0-9]{4}\\s[0-9]{3}\\s[0-9]{3}$';

	public static final String INVALID_PHONE = 'Invalid Phone number - please enter in the format of 08 1234 5678';

    public static final String INVALID_MOBILE = 'Invalid mobile number - please enter in the format of 0400 123 456';

	public String Sitekey {
		get { 
			return getQuitlineSetting('RecaptchaSiteKey'); 
		}
	}

	public String response  { 
		get { return ApexPages.currentPage().getParameters().get('g-recaptcha-response'); }
	}
	
	// this method is called when the button is clicked
	protected boolean verifyCaptcha () {

		// make the request to google for the captcha
		final String responseBody = makeRequest(baseUrl,
				'secret=' + secret +
				'&response='+ response
		);
		
		String success = getValueFromJson(responseBody, 'success');

		return (success.equalsIgnoreCase('true')) || !responseBody.contains('error-codes');
	}
	
	/**
	 * Make request to verify captcha
	 * @return 		response message from google
	 */
	private static String makeRequest(string url, string body)  {
		HttpResponse response = null;
		HttpRequest req = new HttpRequest();   
		req.setEndpoint(url);
		req.setMethod('POST');
		req.setBody (body);
		
		try {
			Http http = new Http();
			response = http.send(req);
			return response.getBody();
		} catch(System.Exception e) {
			System.debug('ERROR: ' + e);
		}
		return '{"success":false}';
	}   
	
	/**
	 * to get value of the given json string
	 * @param		
	 *	- strJson		json string given
	 *	- field			json key to get the value from
	 * @return			string value
	 */
	public static string getValueFromJson ( String strJson, String field ){
		JSONParser parser = JSON.createParser(strJson);
		while (parser.nextToken() != null) {
			if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
				if(parser.getText() == field){
					// Get the value.
					parser.nextToken();
					return parser.getText();
				}
			}
		}
		return null;
	}

	private static String getQuitlineSetting(String name) {
		return QuitlineSettings__c.getInstance(name).Configuration__c;
	}

	/**
     * This method return true if the phone number passed in is valid. A valid phone number is considered on that
     * contains up to 12 characters all numbers with the exception of the first character which can be a plus sign for
     * international phone number formats.
     * @param phoneNumber - the phone number to validate
     * @return true if the phone number is valid otherwise false is returned.
     */
	protected boolean isValidPhone(String phoneNumber) {
		return Pattern.matches(PHONE_REGEX, phoneNumber);
	}

    protected boolean isValidMobile(String mobile) {
        return Pattern.matches(MOBILE_REGEX, mobile);
    }

}