@isTest
private class UtilRecordDMLTest {

    static testMethod void myUtilRecordDMLInsertTest() {
        test.startTest();
        
        // Create testing Email Settings record
        Email_Settings__c es = new Email_Settings__c(Name = 'IT Helpdesk', Email__c = 'test@cancersa.org.au.test');
        insert es;
        
        Account a = new Account(Name = 'Test');        
        UtilRecordDML.dmlInsertRecord(a, UtilRecordDMLTest.class.getName());
        
        // Test for insert record failed
        UtilRecordDML.dmlInsertRecord(a, UtilRecordDMLTest.class.getName());
        
        list<Account> listAccount = new list<Account>();

        for (integer i = 0; i < 1; i++) {
        	Account acc = new Account(Name = 'Test ' + i);        	
        	listAccount.add(acc);
        }
        // Test for insert records failed due to missing required field
        Account b = new Account();
        listAccount.add(b);
        
        // Add existing record to list to throw error
        listAccount.add(a);
        
        UtilRecordDML.dmlInsertRecord(listAccount, UtilRecordDMLTest.class.getName());
        
        // Test for updating records
        a.Name = 'TestA';        
        UtilRecordDML.dmlUpdateRecord(a, UtilRecordDMLTest.class.getName());
        UtilRecordDML.dmlUpdateRecord(listAccount, UtilRecordDMLTest.class.getName());
        
        // Test for deleting records
        UtilRecordDML.dmlDeleteRecord(a, UtilRecordDMLTest.class.getName());
        
        listAccount.clear();
        
        listAccount = [SELECT Id FROM Account];        
        UtilRecordDML.dmlDeleteRecord(listAccount, UtilRecordDMLTest.class.getName());
        test.stopTest();
    }
  
}