@isTest
private class BPAYNumTest {

    static testMethod void myBPAYNumTest() {
    	
    	test.startTest();
    	
        TestDataFactory.createAccountContactTestRecords(1);
        // To verify that all Accounts and Contacts are assigned with BPAY Number after record creation
        list<Account> listAcc = [SELECT Id FROM Account WHERE BPAY_Number__c != null];
        list<Contact> listCon = [SELECT Id FROM Contact WHERE BPAY_Number__c != null];
        
        system.assertEquals(1, listAcc.size());
        system.assertEquals(1, listCon.size());
        
        test.stopTest();
    }
}