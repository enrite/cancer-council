global with sharing class PublicDataExtension
{
    public PublicDataExtension(ApexPages.StandardController ctrlr)
    {
        controller = ctrlr;
        Success = false;
    }

    private ApexPages.StandardController controller;

    public Boolean CanEdit
    {
        get;
        set;
    }

    public Boolean Success
    {
        get;
        set;
    }

    public Boolean HasMessages
    {
        get
        {
            return ApexPages.hasMessages();
        }
    }

    public Public_Data__c OriginalRecord
    {
        get
        {
            if(OriginalRecord == null)
            {
                OriginalRecord = getOriginal((Public_Data__c)controller.getRecord());
            }
            return OriginalRecord;
        }
        private set;
    }

    public String LastReviewDateStart
    {
        get;
        set;
    }

    public String LastReviewDateEnd
    {
        get;
        set;
    }

    public Boolean IncludeNoReviewDate
    {
        get;
        set;
    }

    public List<ServiceProviderWrapper> ServiceProviders
    {
        get
        {
            if(ServiceProviders== null)
            {
                ServiceProviders = new List<ServiceProviderWrapper>();
            }
            return ServiceProviders;
        }
        set;
    }

    public PageReference redir()
    {
        try
        {
            CanEdit = false;
            for(Public_Data__c pd : getPublicData(controller.getId(), 'Temporary', 'Sent'))
            {
                CanEdit = true;
            }
            return null;
        }
        catch(Exception ex)
        {
            return CustomException.formatException(ex);
        }
    }

    public void search()
    {
        try
        {
            if(LastReviewDateStart == null || LastReviewDateEnd == null)
            {
                CustomException.formatException('Last Review start and end dates are required');
                return;
            }
            Date stDt = parseDate(LastReviewDateStart);
            Date endDt = parseDate(LastReviewDateEnd);

            ServiceProviders = null;
            for(ContentVersion cv : [SELECT ID,
                                            Associated_Account__r.Name,
                                            Last_Review_Date__c,
                                            Next_Review_Date__c
                                    FROM    ContentVersion
                                    WHERE   RecordType.DeveloperName = 'Services1' AND
                                            Target_Audience__c = 'General Public' AND
                                            Type__c = 'Service' AND
                                            External_Review_Author__c != null AND
                                            Email_Sent_to_Service_Provider__c = null AND
                                            IsLatest = true AND
                                            (
                                                (Last_Review_Date__c >= :stDt AND
                                                Last_Review_Date__c <= :endDt) OR
                                                Last_Review_Date__c = :(IncludeNoReviewDate ? null : Date.newInstance(1900, 1, 1))
                                            )
                                    ORDER BY Associated_Account__r.Name])
            {
                ServiceProviders.add(new ServiceProviderWrapper(cv));
            }
        }
        catch(Exception ex)
        {
            CustomException.formatException(ex);
        }
    }

    public void sendEmail()
    {
        try
        {
            List<ContentVersion> contentVersions = new List<ContentVersion>();
            for(ServiceProviderWrapper spw : ServiceProviders)
            {
                if(spw.Selected)
                {
                    spw.ServiceProvider.Send_Email_to_Service_Provider__c = true;
                    contentVersions.add(spw.ServiceProvider);
                }
            }
            if(contentVersions.isEmpty())
            {
                CustomException.formatException('No service providers were selected');
                return;
            }
            update contentVersions;
            CustomException.formatMessage('Emails have been sent');
            search();
        }
        catch(Exception ex)
        {
            CustomException.formatException(ex);
        }
    }

    public void saveOverride()
    {
        try
        {
            save('Submitted');
        }
        catch(Exception ex)
        {
            CustomException.formatException(ex);
        }
    }

    public PageReference accept()
    {
        try
        {
            return save('Verified');
        }
        catch(Exception ex)
        {
            return CustomException.formatException(ex);
        }
    }

    public PageReference reject()
    {
        try
        {
            return save('Rejected');
        }
        catch(Exception ex)
        {
            return CustomException.formatException(ex);
        }
    }

    private PageReference save(String status)
    {
        Public_Data__c record = (Public_Data__c)controller.getRecord();
        record.Status__c = status;
        update record;

        Success = true;
        return new PageReference('/' + record.ID);
    }

    private Date parseDate(String s)
    {
        try
        {
            return Date.parse(s);
        }
        catch(Exception ex)
        {
            throw new CustomException(s + ' is not a valid date');
        }
    }



    public static EmailTemplate ServiceProviderTemplate
    {
        get
        {
            if(ServiceProviderTemplate == null)
            {
                ServiceProviderTemplate = [SELECT ID
                                            FROM    EmailTemplate
                                            WHERE   DeveloperName = 'Servicer_Provider_Content_Update'];
            }
            return ServiceProviderTemplate;
        }
        set;
    }

    webservice static String publishData(ID publicDataId)
    {
        Savepoint sp = Database.setSavepoint();
        try {
            for(Public_Data__c pd : getPublicData(publicDataId, 'Temporary', null)) {
                if(pd.Status__c != 'Submitted')
                {
                    return 'Public data has already been verified';
                }
                Public_Data__c publicContent = getOriginal(pd);
                publicContent.Public_Description__c = pd.Public_Description__c;
                publicContent.Public_Title__c = pd.Public_Title__c;
                publicContent.Public_Website__c = pd.Public_Website__c;
                publicContent.Account_Billing_City__c = pd.Account_Billing_City__c;
                publicContent.Account_Billing_PostalCode__c = pd.Account_Billing_PostalCode__c;
                publicContent.Account_Billing_State__c = pd.Account_Billing_State__c;
                publicContent.Account_Billing_Street__c = pd.Account_Billing_Street__c;
                publicContent.Account_Phone__c = pd.Account_Phone__c;
                update publicContent;

                for(ContentVersion cv : [
                        SELECT ID
                        FROM ContentVersion
                        WHERE ContentDocumentId = :publicContent.Content_Document_ID__c
                        AND
                        IsLatest = true
                ]) {
                    cv.Email_Sent_to_Service_Provider__c = null;
                    cv.Public_Title__c = pd.Public_Title__c;
                    cv.Public_Website__c = pd.Public_Website__c;
                    cv.Public_Description__c = pd.Public_Description__c;

                    TriggerByPass.ContentVersion = true;
                    update cv;
                }

                pd.Status__c = 'Verified';
                update pd;
                return 'Public content has been updated';
            }
            return 'Public data could not be found';
        }
        catch(Exception ex)
        {
            if(sp != null)
            {
                Database.rollback(sp);
            }
            return ex.getMessage();
        }
    }

    public static List<Messaging.SingleEmailMessage> getUpdateMessage(ID publicDataId, ID recipientContactId)
    {
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setTemplateId(ServiceProviderTemplate.ID);
        msg.setWhatId(publicDataId);
        msg.setTargetObjectId(recipientContactId);
        msg.setSaveAsActivity(false);
        return new List<Messaging.SingleEmailMessage>{msg};
    }

    private static List<Public_Data__c> getPublicData(String publicDataId, String recordType, String status)
    {
        return [SELECT ID,
                        Temporary_Content_Document_ID__c,
                        Content_Document_ID__c,
                        Public_Description__c,
                        Public_Title__c,
                        Public_Website__c,
                        Account_Billing_City__c,
                        Account_Billing_PostalCode__c,
                        Account_Billing_State__c,
                        Account_Billing_Street__c,
                        Account_Phone__c,
                        Status__c
                FROM    Public_Data__c
                WHERE   ID = :publicDataId  AND
                        RecordType.DeveloperName = :recordType AND
                        Status__c LIKE :(status == null ? '%' : status)];
    }

    private static Public_Data__c getOriginal(Public_Data__c temp)
    {
        return [
                SELECT ID,
                        Description__c,
                        Content_Document_ID__c,
                        Temporary_Content_Document_ID__c,
                        Public_Description__c,
                        Public_Title__c,
                        Public_Website__c,
                        Account_Billing_City__c,
                        Account_Billing_PostalCode__c,
                        Account_Billing_State__c,
                        Account_Billing_Street__c,
                        Account_Phone__c,
                        Status__c
                FROM    Public_Data__c
                WHERE   Content_Document_ID__c = :temp.Temporary_Content_Document_ID__c
                AND     RecordType.DeveloperName = 'Service_Provider_Services'
        ];
    }

    public class ServiceProviderWrapper
    {
        public ServiceProviderWrapper(ContentVersion sp)
        {
            Selected = false;
            ServiceProvider = sp;
        }

        public ContentVersion ServiceProvider
        {
            get;
            set;
        }

        public Boolean Selected
        {
            get;
            set;
        }
    }
}