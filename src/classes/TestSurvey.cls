@isTest
private class TestSurvey {

    static testMethod void mySurveyTest() {
    	// Load test Account, Contact and Campaign data
		TestDataFactory.createAccountContactTestRecords(1);
		
		Id rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Campaign' and DeveloperName = 'Survey'].Id;
		
		Campaign camp = new Campaign(Name = 'test survey', RecordTypeId = rt, Campaign_Code__c = 'ts', Campaign_Type__c = 'Survey', Reporting_Group__c = 'Survey', Status = 'In Progress', IsActive = true, StartDate = system.today());
		insert camp;
				
		Contact con = [SELECT Id FROM Contact limit 1];
				
		Survey__c s = new Survey__c(Campaign__c = camp.Id);
		insert s;
		
		Survey_Question__c sq = new Survey_Question__c(Survey__c = s.Id, Question_Number__c = '1', Question_Type__c = 'General', Question__c = 'Test Question');
		insert sq;
		
		Survey_Answer__c sa = new Survey_Answer__c(Survey_Question__c = sq.Id, Answer__c = 'Test Answer');
		insert sa;
		
		Survey_Participant__c sp = new Survey_Participant__c(Survey__c=s.Id, Contact__c = con.Id);
		insert sp;
		
		Survey_Response__c sr = new Survey_Response__c(Survey_Question__c = sq.Id, Survey_Participant__c = sp.Id, Survey__c = s.Id, Survey_Answer__c = sa.Id);
		insert sr;
		
    }
}