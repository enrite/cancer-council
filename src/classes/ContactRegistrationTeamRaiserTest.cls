@isTest
private class ContactRegistrationTeamRaiserTest {

    static testMethod void myContactRegistrationTeamRaiserTest() {
        test.startTest();
        
        // Load test Account, Contact and Campaign data
		TestDataFactory.createAccountContactTestRecords(2);
		TestDataFactory.createCampaignTestRecords(1);
		
		Id campaignId = [SELECT Id FROM Campaign LIMIT 1].Id;
		list<Contact> contactId = new list<Contact>([SELECT Id, AccountId FROM Contact]);
		Id recordTypeId = [SELECT Id FROM RecordType WHERE SObjectType='Registration__c' AND DeveloperName='Team_Event_Registration' LIMIT 1].Id;
						
		Registration__c reg = new Registration__c();		
		reg.Campaign__c = campaignId;
		reg.Status__c = 'Registered';
		reg.recordTypeId = recordTypeId;
		insert reg;
		
		Contact_Registrations__c cr = new Contact_Registrations__c();
		cr.Contact__c = contactId.get(0).Id;
		cr.Team_Captain__c = 'Captain';
		cr.Registration__c = reg.Id;
		insert cr;
		
		Contact_Registrations__c cr1 = new Contact_Registrations__c();
		cr1.Contact__c = contactId.get(1).Id;				
		cr1.Registration__c = reg.Id;
		insert cr1;
		
		// Reset TriggerByPass.BPAYNum to continue testing
		TriggerByPass.BPAYNum = false;
		
		// Test Team Captain changed
		cr1.Team_Captain__c = 'Captain';
		update cr1;
		
		cr = [SELECT Id, Team_Captain__c FROM Contact_Registrations__c WHERE Contact__c = :contactId.get(0).Id];
		cr1 = [SELECT Id, Team_Captain__c FROM Contact_Registrations__c WHERE Contact__c = :contactId.get(1).Id];
		
		system.assertEquals(cr.Team_Captain__c, null);
		system.assertEquals(cr1.Team_Captain__c, 'Captain');
		
		delete cr;

        test.stopTest();
        
    }
}