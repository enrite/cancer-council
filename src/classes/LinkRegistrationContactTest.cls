@isTest
public class LinkRegistrationContactTest{
    
    //LinkRegistrationContactController Tests
    static testMethod void testLinkRegistrationContactConstructor() {
        LinkRegistrationContactController vController = new LinkRegistrationContactController();
    }
    
    static testMethod void testLinkRegistrationContactProperties() {
        LinkRegistrationContactController vController = new LinkRegistrationContactController();
        
        vController.maxResultsSelected = '10';
        System.assertEquals('10', vController.maxResultsSelected);
        
        vController.displayPopup = true;
        System.assertEquals(true, vController.displayPopup);
    }
    
    static testMethod void testLinkRegistrationContactMethods() {
        Registration__c vRegistration = new Registration__c();
        vRegistration.Web_Registration__c = true;
        insert vRegistration;
        System.currentPageReference().getParameters().put('id', vRegistration.id);
        LinkRegistrationContactController vController = new LinkRegistrationContactController();
        
        System.assertEquals('0', vController.getTotalContacts());
        System.assertEquals(0, vController.getSortedContacts().size());
        System.assertEquals(vRegistration.id, vController.getRegistration().id);
        vController.setMaxResults(10);
        vController.getResultsOptions();
        vController.updateMaxResults();
        vController.getTotalContactsDisplayed();
		vController.closePopup();
		System.assertEquals(false, vController.displayPopup);
    }
    
    static testMethod void testLinkRegistrationContactMethodsWithData() {
        Registration__c vRegistration = new Registration__c();
        vRegistration.Web_First_Name__c = 'John89657745';
        vRegistration.Web_Last_Name__c = 'Smith';
        vRegistration.Web_Phone__c = '04 1212 1212';
        vRegistration.Web_Email__c = 'test@example.com';
        vRegistration.Web_Registration__c = true;
        insert vRegistration;
        Contact vContact = new Contact();
        vContact.FirstName = 'John89657745';
        vContact.LastName = 'Smith';
        vContact.MobilePhone = '0412 121 212';
        vContact.Email = 'test@example.com';
        insert vContact;
        Account vAccount = new Account();
        vAccount.Phone = '04 1212 1212';
        vAccount.Name = 'John';
        insert vAccount;
        System.currentPageReference().getParameters().put('id', vRegistration.id);
        LinkRegistrationContactController vController = new LinkRegistrationContactController();
        
        System.assertEquals('1', vController.getTotalContacts());
        System.assertEquals(1, vController.getSortedContacts().size());
        System.assertEquals(vRegistration.id, vController.getRegistration().id);
        vController.setMaxResults(10);
        vController.getResultsOptions();
        vController.updateMaxResults();
        vController.getTotalContactsDisplayed();
		vController.closePopup();
		vController.newContact();
		vController.newContactConfirm();
    }
    
    //LinkRegistrationContactAccountController Tests
    static testMethod void testLinkRegistrationContactAccountControllerMethodsWithData() {
        Registration__c vRegistration = new Registration__c();
        vRegistration.Web_First_Name__c = 'John89657745';
        vRegistration.Web_Last_Name__c = 'Smith';
        vRegistration.Web_Phone__c = '04 1212 1212';
        vRegistration.Web_Email__c = 'test@example.com';
        vRegistration.Web_Registration__c = true;
        insert vRegistration;
        Contact vContact = new Contact();
        vContact.FirstName = 'John89657745';
        vContact.LastName = 'Smith';
        vContact.MobilePhone = '0412 121 212';
        vContact.Email = 'test@example.com';
        insert vContact;
        Account vAccount = new Account();
        vAccount.Phone = '04 1212 1212';
        vAccount.Name = 'John';
        insert vAccount;
        System.currentPageReference().getParameters().put('id', vRegistration.id);
        LinkRegistrationContactAccountController vController = new LinkRegistrationContactAccountController();
        
        System.assertEquals('1', vController.getTotalAccounts());
        System.assertEquals(1, vController.getSortedAccounts().size());
        System.assertEquals(vRegistration.id, vController.getRegistration().id);
        vController.setMaxResults(10);
        vController.getResultsOptions();
        vController.updateMaxResults();
        vController.getTotalAccountsDisplayed();
		vController.closePopup();
		vController.newContact();
		vController.newContactConfirm();
    }
        
    //LinkRegistrationContactControllerContact Tests
    static testMethod void testLinkRegistrationContactContactMethodsWithData() {
        Registration__c vRegistration = new Registration__c();
        vRegistration.Web_First_Name__c = 'John89657745';
        vRegistration.Web_Last_Name__c = 'Smith';
        vRegistration.Web_Phone__c = '04 1212 1212';
        vRegistration.Web_Email__c = 'test@example.com';
        Contact vContact = new Contact();
        vContact.FirstName = 'John89657745';
        vContact.LastName = 'Smith';
        vContact.MobilePhone = '0412 121 212';
        vContact.Email = 'test@example.com';
        
        LinkRegistrationContactControllerContact vController = new LinkRegistrationContactControllerContact(vContact, vRegistration);
        vController.getScore();
        vController.getContact();
        vController.registration();
        vController.getMatchFirstName();
        vController.getMatchLastName();
        vController.getMatchMailingStreet();
        vController.getMatchMailingCity();
        vController.getMatchMailingPostalCode();
        vController.getMatchMobilePhone();
        vController.getMatchHomePhone();
        vController.getMatchEmail();
    }
    
    //LinkRegistrationContactAccountContact Tests
    static testMethod void testLinkRegistrationContactAccountMethodsWithData() {
        Registration__c vRegistration = new Registration__c();
        vRegistration.Web_First_Name__c = 'John89657745';
        vRegistration.Web_Last_Name__c = 'Smith';
        vRegistration.Web_Phone__c = '04 1212 1212';
        vRegistration.Web_Email__c = 'test@example.com';
        Account vAccount = new Account();
        vAccount.Phone = '04 1212 1212';
        vAccount.Name = 'John';
        
        LinkRegistrationContactControllerAccount vController = new LinkRegistrationContactControllerAccount(vAccount, vRegistration);
        vController.getScore();
        vController.getAccount();
        vController.registration();
        vController.getMatchBillingStreet();
        vController.getMatchBillingPostalCode();
        vController.getMatchPhone();
    }
    
    //LinkRegistrationContactNewController Tests
    static testMethod void testLinkRegistrationContactNewControllerMethodsWithData() {
        Registration__c vRegistration = new Registration__c();
        vRegistration.Web_First_Name__c = 'John89657745';
        vRegistration.Web_Last_Name__c = 'Smith';
        vRegistration.Web_Phone__c = '04 1212 1212';
        vRegistration.Web_Email__c = 'test@example.com';
        vRegistration.Web_Registration__c = true;
        insert vRegistration;
        Contact vContact = new Contact();
        vContact.FirstName = 'John89657745';
        vContact.LastName = 'Smith';
        vContact.MobilePhone = '0412 121 212';
        vContact.Email = 'test@example.com';
        insert vContact;
        Account vAccount = new Account();
        vAccount.Phone = '04 1212 1212';
        vAccount.Name = 'John';
        insert vAccount;
        System.currentPageReference().getParameters().put('regid', vRegistration.id);
        LinkRegistrationContactNewController vController = new LinkRegistrationContactNewController();
        vController.save();
        vController.getRegistration();
        vController.getContact();
    }
    
    //LinkRegistrationContactNew2Controller Tests
    static testMethod void testLinkRegistrationContactNew2ControllerMethodsWithData() {
        Registration__c vRegistration = new Registration__c();
        vRegistration.Web_First_Name__c = 'John89657745';
        vRegistration.Web_Last_Name__c = 'Smith';
        vRegistration.Web_Phone__c = '04 1212 1212';
        vRegistration.Web_Email__c = 'test@example.com';
        vRegistration.Web_Registration__c = true;
        insert vRegistration;
        Contact vContact = new Contact();
        vContact.FirstName = 'John89657745';
        vContact.LastName = 'Smith';
        vContact.MobilePhone = '0412 121 212';
        vContact.Email = 'test@example.com';
        insert vContact;
        Account vAccount = new Account();
        vAccount.Phone = '04 1212 1212';
        vAccount.Name = 'John';
        insert vAccount;
        System.currentPageReference().getParameters().put('regid', vRegistration.id);
        System.currentPageReference().getParameters().put('accountid', vAccount.id);
        LinkRegistrationContactNew2Controller vController = new LinkRegistrationContactNew2Controller();
        vController.save();
        vController.getRegistration();
        vController.getContact();
    }
    
    //LinkRegistrationContactSubmitController Tests
    static testMethod void testLinkRegistrationContactSubmitControllerMethodsWithData() {
        Registration__c vRegistration = new Registration__c();
        vRegistration.Web_First_Name__c = 'John89657745';
        vRegistration.Web_Last_Name__c = 'Smith';
        vRegistration.Web_Phone__c = '04 1212 1212';
        vRegistration.Web_Email__c = 'test@example.com';
        vRegistration.Web_Registration__c = true;
        insert vRegistration;
        Contact vContact = new Contact();
        vContact.FirstName = 'John89657745';
        vContact.LastName = 'Smith';
        vContact.MobilePhone = '0412 121 212';
        vContact.Email = 'test@example.com';
        insert vContact;
        Account vAccount = new Account();
        vAccount.Phone = '04 1212 1212';
        vAccount.Name = 'John';
        insert vAccount;
        System.currentPageReference().getParameters().put('regid', vRegistration.id);
        System.currentPageReference().getParameters().put('contactid', vContact.id);
        LinkRegistrationContactSubmitController vController = new LinkRegistrationContactSubmitController();
        vController.updateRecord();
        vController.goToRegistration();
    }
}