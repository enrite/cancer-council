public with sharing class EmailUtil {
    
    public EmailUtil (list<string> recipients, string replyTo, string emailSubject, string bodyMessage, string bodyType) {
        try {
            Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        
            if (recipients.size() == 0 || recipients == null) 
                return;
        
            mail.setToAddresses(recipients);
            mail.setReplyTo(replyTo);
            mail.setSubject(emailSubject);
            
            if (bodyType == 'Plain') {
            	mail.setPlainTextBody(bodyMessage);
            }
            
            if (bodyType == 'HTML') {
            	mail.setHtmlBody(bodyMessage);
            }
            
            mail.setSaveAsActivity(false);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            //mail.setSenderDisplayName('IT Helpdesk');
            //OrgWideEmailAddress owa = [select id, Address, DisplayName from OrgWideEmailAddress limit 1];
            //mail.setOrgWideEmailAddressId(owa.id);
        
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch (Exception e) {
        	system.debug('***** EmailUtil ' + e.getMessage());
            //ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Send Email Exception Occured: ' + e.getMessage());
            //ApexPages.addMessage(errorMsg);
        }
    }
    
    public EmailUtil (string recipient, string emailSubject, string bodyMessage, string bodyType) {
        try {
            Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        	
        	list<string> recipients = new list<string>{recipient};
        	
            if (recipients.size() == 0 || recipients == null) 
                return;
                        	
            mail.setToAddresses(recipients);
            mail.setReplyTo('ithelpdesk@cancersa.org.au');
            mail.setSubject(emailSubject);
            
            if (bodyType == 'Plain') {
            	mail.setPlainTextBody(bodyMessage);
            }
            
            if (bodyType == 'HTML') {
            	mail.setHtmlBody(bodyMessage);
            }
            
            mail.setSaveAsActivity(false);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            //mail.setSenderDisplayName('IT Helpdesk');
            //OrgWideEmailAddress owa = [select id, Address, DisplayName from OrgWideEmailAddress limit 1];
            //mail.setOrgWideEmailAddressId(owa.id);
        
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch (Exception e) {
        	system.debug('***** EmailUtil ' + e.getMessage());
            //ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'Send Email Exception Occured: ' + e.getMessage());
            //ApexPages.addMessage(errorMsg);
        }
    }   
    
    static testMethod void testEmailUtil() {
        List<String> recipients = new list<String>{'test@test.com','test2@test.com'};
        EmailUtil email1 = new EmailUtil(recipients, 'test@test.com', 'Test Method', 'This is to test sending email', 'Plain');      
        EmailUtil email2 = new EmailUtil(null, 'test@test.com', 'Test Method', 'This is to test sending email', 'Plain');        
    } 
}