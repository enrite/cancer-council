/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMergedIdNotes {

    static testMethod void myTestMergedIdNotes() {
        // TO DO: implement unit test
        Account[] accList = new Account[]{};
        for (integer x=0; x<3; x++) {
            Account a = new Account(name='Test Account ' + x);
            accList.add(a);
        }
                
        Contact[] conList = new Contact[]{};
        for (integer x=0; x<3; x++) {
            Contact c = new Contact(AccountId = accList[0].Id, FirstName = 'Test First', LastName='Test Last ' + x);
            conList.add(c);
        }
        
        test.startTest();
        insert accList;
        insert conList;        

        Account mergedAccount = [select Org_Id__c from Account where Id= :accList[0].Id];
        Account survivedAccount = [select Org_Id__c from Account where Id= :accList[1].Id];
                
        Contact mergedContact = [select Contact_Id__c from Contact where Id= :conList[0].Id];
        Contact survivedContact = [select Contact_Id__c from Contact where Id= :conList[1].Id];
               
        //syntax merge suvivor merged;
        //to check Note record is created correctly after merged
        
        merge accList[1] accList[0];
        Note[] n = [select Title from Note where ParentId= :accList[1].Id limit 1];
        if (n.size()>0)
            system.assertEquals(n[0].Title, mergedAccount.Org_Id__c + ' merged with ' + survivedAccount.Org_Id__c);
                    
        merge conList[1] conList[0];
        Note[] n1 = [select Title from Note where ParentId= :conList[1].Id limit 1];
        if (n1.size()>0)
            system.assertEquals(n1[0].Title, mergedContact.Contact_Id__c + ' merged with ' + survivedContact.Contact_Id__c);
        
        //to check that Note record would not be created after normal deletion
        delete accList[2];
        Note[] n2 = [select Title from Note where ParentId= :accList[2].Id limit 1];
        system.assertEquals(n2.size(), 0);
                    
        delete conList[2];
        Note[] n3 = [select Title from Note where ParentId= :conList[2].Id limit 1];
        system.assertEquals(n3.size(), 0);
        
        test.stopTest();
    }
}