public with sharing class QuitskillsCourseOverviewController
{
    public List<Ambassador_Activity__c> ambassadorActivities
    {
        get
        {
            return setController.getRecords();
        }
    }

    public Integer PAGE_SIZE
    {
        get
        {
            return 4;
        }
    }

    public ApexPages.StandardSetController setController{get;set;}

    public String CourseName
    {
        get; set;
    }

    public Id SelectedCourseId { get; set; }

    public PageReference Apply()
    {
        PageReference courseApplication = Page.EducationRegistrationCourseApplication;
        courseApplication.getParameters().put('CourseId',SelectedCourseId);

        return courseApplication.setRedirect(true);
    }

    public QuitskillsCourseOverviewController()
    {
        if(ApexPages.currentPage().getParameters().containsKey('Area'))
        {
            this.CourseName  = ApexPages.currentPage().getParameters().get('Area');

            setController = new ApexPages.StandardSetController(getActivities());
            setController.setPageSize(PAGE_SIZE);
        }
    }

    private List<Ambassador_Activity__c> getActivities()
    {
        return [
                SELECT Id,
                        Project_Area__c,
                        Web_Name__c,
                        Session_1__c,
                        Session_2__c,
                        Session_3__c,
                        Session_4__c,
                        Description__c,
                        Address_of_activity__c
                FROM Ambassador_Activity__c
                WHERE Publish_to_Website__c = true
                AND Project_Area__c =: this.CourseName
                ORDER BY Session_1__c
        ];
    }
}