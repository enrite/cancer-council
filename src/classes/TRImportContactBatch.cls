public class TRImportContactBatch 
        extends TRImportBase
        implements Database.Batchable<sObject>, Database.Stateful
{
    protected final String NEW_LINE = '\n';
    
    private RecordType AccountNonOrganisationRecordType
    {
        get
        {
            if(AccountNonOrganisationRecordType == null)
            {
                AccountNonOrganisationRecordType = [SELECT      ID
                                                    FROM        RecordType
                                                    WHERE       Name = 'Non Organisation' AND
                                                                sObjectType = 'Account'];
            }
            return AccountNonOrganisationRecordType;
        } 
        set;
    }
    
    private RecordType ContactRecordType
    {
        get
        {
            if(ContactRecordType == null)
            {
                ContactRecordType = [SELECT     ID
                                    FROM        RecordType
                                    WHERE       Name = 'Contact' AND
                                                sObjectType = 'Contact']; 
            }
            return ContactRecordType;
        }
        set;
    }
    
    public Database.QueryLocator start(Database.BatchableContext info)
    {
        if(processedSuccesfully == null)
        {
            processedSuccesfully = 0;
        }       
        return Database.getQueryLocator([SELECT ID,
                                                TR_ID__c,
                                                TR_Last_Modifed_Date__c,
                                                Member_ID__c,
                                                Error__c,
                                                First_Name__c,
                                                Last_Name__c,
                                                Accept_Email__c,
                                                Accepts_Post__c,
                                                Birthdate__c,
                                                Employer__c,
                                                Gender__c,
                                                Home_City__c,
                                                Home_Phone__c,
                                                Mobile__c,
                                                Home_Postcode__c,
                                                Home_State__c,
                                                Home_Street1__c,
                                                Home_Street2__c,
                                                Home_Street3__c,
                                                Primary_Email__c,
                                                Title__c,
                                                Deceased__c
                                        FROM    TR_Import_Constituent__c
                                        WHERE   Processed__c = null
                                        ORDER BY ID]);
    }
    
    public void execute(Database.BatchableContext info, List<TR_Import_Constituent__c> scope)
    {       
        for(TR_Import_Constituent__c stagingRecord : scope)
        {
            processedSuccesfully++;
            try
            {
                processConstituent(stagingRecord);              
            }           
            catch(Exception ex)
            {
                processedSuccesfully--;
                stagingRecord.Processed__c = null;
                stagingRecord.Contact__c = null;
                stagingRecord.Error__c = ex.getMessage() +  '\n' + ex.getStackTraceString();
            }
        }
        update scope;
        
    }
    
    public void finish(Database.BatchableContext info)
    {
        if(RunThisOnly || Test.isRunningTest())
        {
            return;
        }
        TRImportTeamBatch teamBatch = new TRImportTeamBatch();
        Database.executeBatch(teamBatch, BatchSize); 
    }
    
    private void processConstituent(TR_Import_Constituent__c stagingRecord)
    {
        stagingRecord.Error__c = '';
        stagingRecord.Processing_Result__c = null;
        Contact matchedContact = null;
        Datetime trTimestamp = parseDateTime(stagingRecord.TR_Last_Modifed_Date__c, stagingRecord);
        Decimal trCONSId = stagingRecord.TR_ID__c == null ? -1 : stagingRecord.TR_ID__c;
        for(Contact c : [SELECT    ID,
                                    TeamRaiser_CONS_ID__c,
                                    Contact_Id__c,
                                    FirstName,
                                    LastName,
                                    Phone,
                                    MobilePhone,
                                    Email,
                                    MailingStreet,  
                                    MailingCity,                                
                                    LastModifiedDate
                        FROM        Contact
                        WHERE       TeamRaiser_CONS_ID__c = :trCONSId AND 
                                    Status__c != :CONTACTSTATUS_TOBEMERGED])
        {    
            stagingRecord.Processing_Result__c = RESULT_CONTACTIDMATCH; 
            matchedContact = c;
        }
        if(matchedContact == null && String.isNotBlank(stagingRecord.Member_ID__c))
        {
            for(Contact c : [SELECT    ID,
                                        TeamRaiser_CONS_ID__c,
                                        Contact_Id__c,
                                        FirstName,
                                        LastName,
                                        Phone,
                                        MobilePhone,
                                        Email,
                                        MailingStreet,  
                                        MailingCity,                                
                                        LastModifiedDate
                            FROM        Contact
                            WHERE       Contact_Id__c = :stagingRecord.Member_ID__c AND 
                                        Status__c != :CONTACTSTATUS_TOBEMERGED])
            {  
                stagingRecord.Processing_Result__c = RESULT_MEMBERIDMATCH; 
                matchedContact = c;
            }
        }
        Contact matchedContactOnName = null;
        if(matchedContact == null && String.isBlank(stagingRecord.Home_City__c))
        {
            String contactName = String.isBlank(stagingRecord.First_Name__c) ? '' : (stagingRecord.First_Name__c + ' ');
            contactName += stagingRecord.Last_Name__c;
            // this query was orginally part of the first query
            // but was run seperateley due to indexing problems
            for(Contact c :  [SELECT    ID,
                                        TeamRaiser_CONS_ID__c,
                                        Contact_Id__c,
                                        FirstName,
                                        LastName,
                                        Phone,
                                        MobilePhone,
                                        Email,
                                        MailingStreet,  
                                        MailingCity,                                
                                        LastModifiedDate
                            FROM        Contact
                            WHERE       RecordTypeId = :ContactRecordType.ID AND 
                                        Name = :contactName AND
                                        Email = :stagingRecord.Primary_Email__c AND
                                        Email != null AND
                                        Status__c != :CONTACTSTATUS_TOBEMERGED])    
            {
                stagingRecord.Processing_Result__c = RESULT_CONTACTEMAILMATCH;
                matchedContactOnName = c;
                break;
                /*if(stagingRecord.Home_Street1__c == (c.MailingStreet == null ? null : c.MailingStreet.normalizeSpace()) &&
                        c.MailingCity == stagingRecord.Home_City__c)
                {
                    stagingRecord.Processing_Result__c = RESULT_CONTACTADDRESSMATCH;
                    matchedContactOnName = c;
                    break;  
                }
                if((stagingRecord.Home_Phone__c != null) && (c.Phone == stagingRecord.Home_Phone__c ||
                        c.MobilePhone == stagingRecord.Home_Phone__c))
                {
                    stagingRecord.Processing_Result__c = RESULT_CONTACTPHONEMATCH;
                    matchedContactOnName = c;
                    break;  
                }*/
            }
        }
        if(matchedContactOnName != null)
        {
            /*External_Id__c externalID = new External_Id__c(Contact__c = matchedContactOnName.ID);
            externalID.External_Source__c = 'TeamRaiser';
            // need local variable as sObject field will always display decimal place
            Integer i = stagingRecord.TR_ID__c.intValue();
            externalID.External_Id__c = String.valueOf(i);
            insert externalID;*/
            stagingRecord.Contact__c = matchedContactOnName.ID;
            setProcessedAndOwnerFields(stagingRecord);
            return;
        }
        else
        {
            if(matchedContact == null)
            {
                Account acc = new Account();
                acc.RecordTypeID = AccountNonOrganisationRecordType.ID;
                String accName = (stagingRecord.First_Name__c == null ? '' : (stagingRecord.First_Name__c + ' ')) + stagingRecord.Last_Name__c;
                acc.Name = toProperCase(accName);
                insert acc;

                matchedContact = new Contact(AccountID = acc.ID);
                matchedContact.RecordTypeId = ContactRecordType.ID;
                matchedContact.How_Heard__c = 'TeamRaiser';
                stagingRecord.Processing_Result__c = RESULT_NEWCONTACT;
            }
            else if(matchedContact.LastModifiedDate > trTimestamp)
            {
                stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
                stagingRecord.Processing_Result__c = RESULT_TIMESTAMPCONFLICT;
                stagingRecord.Error__c += getTimeStampDifference(matchedContact.LastModifiedDate, trTimestamp);
                return;
            }
            matchedContact.TeamRaiser_CONS_ID__c = stagingRecord.TR_ID__c;
            matchedContact.FirstName = toCapital(stagingRecord.First_Name__c);
            matchedContact.LastName = toCapital(stagingRecord.Last_Name__c);
            matchedContact.Title = stagingRecord.Title__c;
            matchedContact.Gender__c = stagingRecord.Gender__c;
            matchedContact.Birthdate = parseDate(stagingRecord.Birthdate__c, stagingRecord);
            matchedContact.Email = parseEmail(stagingRecord.Primary_Email__c, stagingRecord);
            matchedContact.HasOptedOutOfEmail = translateEmailOptOut(stagingRecord);
            String street = stagingRecord.Home_Street1__c == null ? '' : stagingRecord.Home_Street1__c;
            street += stagingRecord.Home_Street2__c == null ? '' : ('\n' + stagingRecord.Home_Street2__c);
            street += stagingRecord.Home_Street3__c == null ? '' : ('\n' + stagingRecord.Home_Street3__c);
            matchedContact.MailingStreet = toProperCase(street);
            matchedContact.MailingCity = toCapital(stagingRecord.Home_City__c);
            matchedContact.MailingState = stagingRecord.Home_State__c;
            matchedContact.MailingPostalCode = stagingRecord.Home_Postcode__c;
            matchedContact.Mail_Opt_Out__c = !translateBoolean(stagingRecord.Accepts_Post__c);
            matchedContact.Phone = getFormattedPhone(stagingRecord, stagingRecord.Home_Phone__c, false);
            matchedContact.MobilePhone = getFormattedPhone(stagingRecord, stagingRecord.Mobile__c, true);
            matchedContact.Employer__c = stagingRecord.Employer__c;
            matchedContact.Last_Modified_Teamraiser__c = trTimestamp;
            matchedContact.Last_Updated_by_Team_Raiser__c = datetime.now();
            matchedContact.Privacy_Agree__c = true;
            if(String.isNotBlank(stagingRecord.Deceased__c))
            {
                matchedContact.Status__c = translateBoolean(stagingRecord.Deceased__c) ? CONTACTSTATUS_DECEASEDNONEXTOFKIN : CONTACTSTATUS_ACTIVE;
            }
            upsert matchedContact;
        }
        stagingRecord.Contact__c = matchedContact.ID;
        setProcessedAndOwnerFields(stagingRecord);
    }
    
    /*private void setTimestampConflict(TR_Import_Constituent__c stagingRecord, DateTime SFLastModifiedDate, Datetime TRTimestamp)
    {
        if(Test.isRunningTest())
        {
            return;
        }
        stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
        stagingRecord.Processing_Result__c = RESULT_TIMESTAMPCONFLICT;
        stagingRecord.Error__c += getTimeStampDifference(SFLastModifiedDate, TRTimestamp);
    }*/
    
    private String getFormattedPhone(TR_Import_Constituent__c stagingRecord, String s, Boolean mobileFormat)
    {
        if(String.isBlank(s))
        {
            return null;
        }
        if(!mobileFormat)
        {
            return s;
        }
        s = s.deleteWhitespace().remove('-').remove('(').remove(')');
        if(s.length() == 10)
        {
             return s.left(4) + ' ' + s.substring(4, 7) + ' ' + s.substring(7, 10);
        }
        stagingRecord.Processing_Result__c = RESULT_INVALIDPHONE;
        return null;
    }

    private Boolean translateEmailOptOut(TR_Import_Constituent__c stagingRecord)
    {
        return !(stagingRecord.Accept_Email__c == '1' || 
                    stagingRecord.Accept_Email__c == '2' ||
                    stagingRecord.Accept_Email__c == '7');
    }
}