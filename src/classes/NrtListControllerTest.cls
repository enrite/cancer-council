@isTest
private class NrtListControllerTest {

    static testMethod void testNrtListControllerQuitline() {
        test.startTest();
        
        //Warehouse test data
        PBSI__Warehouse__c w = new PBSI__Warehouse__c(Name='Test Warehouse', PBSI__Description__c='Test Description');
        insert w;
        
        //Location test data
        PBSI__PBSI_Location__c l = new PBSI__PBSI_Location__c(Name='Test Location', PBSI__Warehouselookup__c=w.Id, PBSI__aisle__c='Test Aisle',	PBSI__rack__c='Tst');
        insert l;
        
        //Item Group test data
        PBSI__PBSI_Item_Group__c ig = new PBSI__PBSI_Item_Group__c(Name='Test Item Group', PBSI__Item_Group_Code__c='Tst');
        insert ig;
        
        //Item test data 1
        PBSI__PBSI_Item__c i = new PBSI__PBSI_Item__c(Name='Test Item', PBSI__description__c='Test Description', PBSI__defaultunitofmeasure__c='1'
        												, QuitSA_Category__c='Resource', PBSI__Default_Location__c=l.Id, PBSI__Item_Group__c=ig.Id);
        insert i;
        
        //Item test data 2
        PBSI__PBSI_Item__c i1 = new PBSI__PBSI_Item__c(Name='Test Item1', PBSI__description__c='Test Description', PBSI__defaultunitofmeasure__c='1'
        												, QuitSA_Category__c='Resource', PBSI__Default_Location__c=l.Id, PBSI__Item_Group__c=ig.Id, Discontinued__c=true);        
        insert i1;
        
        //Resource test data 1 - Current Item
        NRT_Resources__c r = new NRT_Resources__c(Name='Test Item', Item__c=i.Id);        
        insert r;
        
        //Resource test data 2 - Discontinued
        NRT_Resources__c r1 = new NRT_Resources__c(Name='Test Item1', Item__c=i1.Id);
        insert r1;
        
        //Quitline Case test data
        Id quitlineCaseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Quitline'].Id;        
        Case c = new Case(RecordTypeId = quitlineCaseRecordTypeId);
        insert c;
        
        //Case NRT/Resource test data 1 - Current Item
        Send_NRT_Resource__c nrt = new Send_NRT_Resource__c(Name='Test', Case__c=c.Id, Item_NRT__c=r.Id, Qty_to_Send__c=1);
        insert nrt;       
        
        list<NRT_Resources__c> listResource = [select Id, Item__c, Product_Status1__c from NRT_Resources__c];
            
        PageReference pageRef = new PageReference('/apex/NrtListPage');

        test.setCurrentPage(pageRef);        
        
        pageRef.getParameters().put('Id', c.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        NrtListController testingNrt = new NrtListController(sc);
        
        //normal input data
        Send_NRT_Resource__c n = new Send_NRT_Resource__c(Case__c = c.Id, Item_NRT__c=r.Id, Qty_to_Send__c=2);
        testingNrt.mapNrtResources.put(n.Id, n);
        testingNrt.updateTable();
        
		//test for Qty_to_Send__c<0
        Send_NRT_Resource__c n1 = new Send_NRT_Resource__c(Case__c = c.Id, Item_NRT__c=r.Id, Qty_to_Send__c=-1);
        testingNrt.mapNrtResources.put(n1.Id, n1);
        testingNrt.updateTable();
        
        //test for Item been sent
        n.Item_Sent_to_Order__c = true;
        update n;
        testingNrt.updateTable();
        
        //test for delete Send_NRT_Resource__c record
        pageRef.getParameters().put('lineNo', n.Id);
        testingNrt.deleteRow();
        testingNrt.insertRow();
                
        test.stopTest();
    }
    
    static testMethod void testNrtListController131120() {
        test.startTest();
        
        //Warehouse test data
        PBSI__Warehouse__c w = new PBSI__Warehouse__c(Name='Test Warehouse', PBSI__Description__c='Test Description');
        insert w;
        
        //Location test data
        PBSI__PBSI_Location__c l = new PBSI__PBSI_Location__c(Name='Test Location', PBSI__Warehouselookup__c=w.Id, PBSI__aisle__c='Test Aisle',	PBSI__rack__c='Tst');
        insert l;
        
        //Item Group test data
        PBSI__PBSI_Item_Group__c ig = new PBSI__PBSI_Item_Group__c(Name='Test Item Group', PBSI__Item_Group_Code__c='Tst');
        insert ig;
        
        //Item test data 1
        PBSI__PBSI_Item__c i = new PBSI__PBSI_Item__c(Name='Test Item', PBSI__description__c='Test Description', PBSI__defaultunitofmeasure__c='1'
        												, QuitSA_Category__c='Resource', PBSI__Default_Location__c=l.Id, PBSI__Item_Group__c=ig.Id);
        insert i;
        
        //Item test data 2
        PBSI__PBSI_Item__c i1 = new PBSI__PBSI_Item__c(Name='Test Item1', PBSI__description__c='Test Description', PBSI__defaultunitofmeasure__c='1'
        												, QuitSA_Category__c='Resource', PBSI__Default_Location__c=l.Id, PBSI__Item_Group__c=ig.Id, Discontinued__c=true);        
        insert i1;
        
        //Resource test data 1 - Current Item
        NRT_Resources__c r = new NRT_Resources__c(Name='Test Item', Item__c=i.Id);        
        insert r;
        
        //Resource test data 2 - Discontinued
        NRT_Resources__c r1 = new NRT_Resources__c(Name='Test Item1', Item__c=i1.Id);
        insert r1;
        
        //131120 Case test data
        Id supportCaseRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'Helpline_Case'].Id;        
        Case c = new Case(RecordTypeId = supportCaseRecordTypeId);
        insert c;
        
        //Case NRT/Resource test data 1 - Current Item
        Send_NRT_Resource__c nrt = new Send_NRT_Resource__c(Name='Test', Case__c=c.Id, Item_NRT__c=r.Id, Qty_to_Send__c=1);
        insert nrt;       
        
        list<NRT_Resources__c> listResource = [select Id, Item__c, Product_Status1__c from NRT_Resources__c];
            
        PageReference pageRef = new PageReference('/apex/NrtListPage2');

        test.setCurrentPage(pageRef);        
        
        pageRef.getParameters().put('Id', c.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        NrtListController2 testingNrt = new NrtListController2(sc);
        
        //normal input data
        Send_NRT_Resource__c n = new Send_NRT_Resource__c(Case__c = c.Id, Item_NRT__c=r.Id, Qty_to_Send__c=2);
        testingNrt.mapNrtResources.put(n.Id, n);
        testingNrt.updateTable();
        
		//test for Qty_to_Send__c<0
        Send_NRT_Resource__c n1 = new Send_NRT_Resource__c(Case__c = c.Id, Item_NRT__c=r.Id, Qty_to_Send__c=-1);
        testingNrt.mapNrtResources.put(n1.Id, n1);
        testingNrt.updateTable();
        
        //test for Item been sent
        n.Item_Sent_to_Order__c = true;
        update n;
        testingNrt.updateTable();
        
        //test for delete Send_NRT_Resource__c record
        pageRef.getParameters().put('lineNo', n.Id);
        testingNrt.deleteRow();
        testingNrt.insertRow();
                
        test.stopTest();    	
    }
}