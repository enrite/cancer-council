public with sharing class EventVolunteerRegistrationEditExtension
{
    public EventVolunteerRegistrationEditExtension(ApexPages.StandardController c)
    {
        record = (Registration__c)c.getRecord();
        
        if (record.Id == null)
        {
            record.Status__c = 'Pending';
        }
        
        //FindEventLocations();
    }

    private Registration__c record;
    public List<SelectOption> AvailableEventLocations { get; set; }        
    public List<SelectOption> SelectedEventLocations { get; set; }
    private String sep = '\n';
    
    /*
    public void FindEventLocations()
    {
        try
        {
            AvailableEventLocations = new List<SelectOption>();
            SelectedEventLocations = new List<SelectOption>();
                            
            // loop through the event locations for the selected campaign
            for (Campaign_Event_Location__c cel : [SELECT Event_Location__r.Name
                                                   FROM Campaign_Event_Location__c
                                                   WHERE Campaign__c = :record.Campaign__c
                                                   ORDER BY Event_Location__r.Name])
            {   
                SelectOption so = new SelectOption(cel.Event_Location__r.Name, cel.Event_Location__r.Name);
                Boolean added = false;
                                                                                                             
                // if the event location is in the saved field then add it to the selected list
                if (record.Event_Locations__c != null
                    && (record.Event_Locations__c + sep).contains(cel.Event_Location__r.Name + sep))
                {
                    SelectedEventLocations.add(so);                 
                }
                else                
                {
                    AvailableEventLocations.add(so);                
                }                                  
            }                             
        }
        catch (Exception e)
        {
            CustomException.FormatException(e);
        }
    }
    */  
    
    public PageReference SaveOverride()
    {
        try
        {
            //FormatEventLocations();
            
            upsert record;
        
            return new PageReference('/' + record.Id);
        }
        catch(Exception e)
        {
            return CustomException.formatException(e);
        }
    }
    
    /*
    private void FormatEventLocations()
    {
        // put together the semi-colon separated list of event locations from the selected list
        String eventLocations = '';
        List<String> sortList = new List<String>();        
                        
        // sort the sub domains alphabetically for consistency                        
        for (SelectOption so : SelectedEventLocations)
        {
            sortList.add(so.getValue());
        }                                        
        sortList.sort();
        
        // add the sorted event locations to a semi-colon separated string
        for (String s : sortList)
        {
            eventLocations += s + sep;
        }                        
        
        // remove the final semi-colon
        if (eventLocations.length() > 0)
        {
            eventLocations = eventLocations.substring(0, eventLocations.length() - 1);
        }            
    
        record.Event_Locations__c = eventLocations;
    } */
}