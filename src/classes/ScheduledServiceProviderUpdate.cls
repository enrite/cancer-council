public class ScheduledServiceProviderUpdate implements Schedulable
{
    public void execute(SchedulableContext context)
    {
        List<ContentVersion> cvs = [SELECT ID
                                    FROM    ContentVersion
                                    WHERE   RecordType.DeveloperName = 'Services1' AND
                                            Target_Audience__c = 'General Public' AND
                                            Type__c = 'Service' AND
                                            Next_Review_Date__c <= :Date.today() AND
                                            Email_Sent_to_Service_Provider__c = null];
        for(ContentVersion cv : cvs)
        {
            cv.Send_Email_to_Service_Provider__c = true;
        }
        update cvs;
    }
}