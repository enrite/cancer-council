public class LinkRegistrationContactControllerContact{
    private static final Integer cWeightFirstName = 10;
    private static final Integer cWeightLastName = 10;
    private static final Integer cWeightMailingStreet = 5;
    private static final Integer cWeightMailingPostalCode = 10;
    private static final Integer cWeightMobilePhone = 40;
    private static final Integer cWeightHomePhone = 5;
    private static final Integer cWeightEmail = 20;
    private static final Integer cWeightAccount = 10;
    
    private boolean mMatchFirstName;
    private boolean mMatchLastName;
    private boolean mMatchMailingStreet;
    private boolean mMatchMailingCity;
    private boolean mMatchMailingPostalCode;
    private boolean mMatchMobilePhone;
    private boolean mMatchHomePhone;
    private boolean mMatchEmail;
    private boolean mMatchAccount;
    
    private Contact mContact;
    private Registration__c mRegistration;
    private Integer mScore;
    
    // Binary search tree links
    private LinkRegistrationContactControllerContact mLeft;
    private LinkRegistrationContactControllerContact mRight;
    
    
    public LinkRegistrationContactControllerContact(Contact pContact, Registration__c pRegistration){
        mContact = pContact;
        mRegistration = pRegistration;
        mScore = 0;
        
        mMatchFirstName = pContact.FirstName != null && pContact.FirstName == pRegistration.Web_First_Name__c;
        mMatchLastName = pContact.LastName != null && pContact.LastName == pRegistration.Web_Last_Name__c;
        mMatchMailingStreet = pContact.MailingStreet != null && pContact.MailingStreet == pRegistration.Web_Street__c;
        mMatchMailingCity = pContact.MailingCity != null && pContact.MailingCity == pRegistration.Web_Suburb__c;
        mMatchMailingPostalCode = pContact.MailingPostalCode != null && pContact.MailingPostalCode == pRegistration.Web_Postcode__c;
        mMatchMobilePhone = pContact.MobilePhone != null && pContact.MobilePhone == pRegistration.Web_Mobile__c;
        mMatchHomePhone = pContact.HomePhone != null && pContact.HomePhone == pRegistration.Web_Phone__c;
        mMatchEmail = pContact.Email != null && pContact.Email == pRegistration.Web_Email__c;
        mMatchAccount = pContact.Account.Name != null && pContact.Account.Name == pRegistration.Web_Account__c;
        
        if(mMatchFirstName) mScore += cWeightFirstName;
        if(mMatchLastName) mScore += cWeightLastName;
        if(mMatchMailingStreet) mScore += cWeightMailingStreet;
        if(mMatchMailingPostalCode) mScore += cWeightMailingPostalCode;
        if(mMatchMobilePhone) mScore += cWeightMobilePhone;
        if(mMatchHomePhone) mScore += cWeightHomePhone;
        if(mMatchEmail) mScore += cWeightEmail;
        if(mMatchAccount) mScore += cWeightAccount;
    }
    
    public Integer getScore(){
        return mScore;
    }
    
    public Contact getContact(){
        return mContact;
    }
    
    public Registration__c registration(){
        return mRegistration;
    }
    
    public void add(LinkRegistrationContactControllerContact pNode){
        if(pNode.getScore() > mScore){
            if(mRight == null) mRight = pNode;
            else mRight.add(pNode);
        }else{
            if(mLeft == null) mLeft = pNode;
            else mLeft.add(pNode);
        }
    }
    
    public void buildArrayDesc(LinkRegistrationContactControllerContact[] pArray){
        if(mRight != NULL) mRight.buildArrayDesc(pArray);
        pArray.add(this);
        if(mLeft != NULL) mLeft.buildArrayDesc(pArray);
    }
    
    public boolean getMatchFirstName(){
        return mMatchFirstName;
    }
    
    public boolean getMatchLastName(){
        return mMatchLastName;
    }
    
    public boolean getMatchMailingStreet(){
        return mMatchMailingStreet;
    }
    
    public boolean getMatchMailingCity(){
        return mMatchMailingCity;
    }
    
    public boolean getMatchMailingPostalCode(){
        return mMatchMailingPostalCode;
    }
    
    public boolean getMatchMobilePhone(){
        return mMatchMobilePhone;
    }
    
    public boolean getMatchHomePhone(){
        return mMatchHomePhone;
    }
    
    public boolean getMatchEmail(){
        return mMatchEmail;
    }
    
    public boolean getMatchAccount(){
        return mMatchAccount;
    }

}