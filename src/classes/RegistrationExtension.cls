global with sharing class RegistrationExtension 
{
    webservice static void cancelContactRegistrations(ID regID)
    {
    	Savepoint sp = Database.setSavepoint();
    	try
    	{
	    	for(Registration__c r : [SELECT ID,
	    									(SELECT ID 
	    									FROM 	Contact_Registrations__r)
									FROM 	Registration__c
									WHERE 	ID = :regId])
			{				
				r.Status__c = 'CANX';
				update r;
				for(Contact_Registrations__c cr : r.Contact_Registrations__r)
				{
					cr.Status__c = 'CANX';
				}
				update r.Contact_Registrations__r;
			}
		}
		catch(Exception ex)
		{
			if(sp != null)
			{
				Database.rollback(sp);
			}
			throw new CCException(ex.getMessage());
		}
    }
}