@isTest
public class TEST_trgLeadAI{
    static TestMethod void testController(){
        Campaign c = new Campaign(Name = 'Major Gifts', isActive=true);
        insert c;
        List<RecordType> rt = new List<RecordType>();
        
        rt  = [Select Id, developerName From RecordType Where sObjectType = 'Lead' and developerName = 'Major_Gifts'];
        
        Lead l = new Lead(LastName = 'Test', Company='Test', Account_Type__c ='Organisation', Status = 'Identified', RecordTypeId =rt[0].Id );
        insert l;
        List<CampaignMember> allMembers = new List<CampaignMember>();
        
        allMembers = [Select Id from CampaignMember];
        System.assertEquals(1,allMembers.size());
        
        
    }
    
}