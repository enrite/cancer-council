public class NrtListController2 {
    public Id caseId {get;set;}
    
    public map<Id, Send_NRT_Resource__c> mapNrtResources {
        get {
            if (mapNrtResources == null) {
                mapNrtResources = new map<Id, Send_NRT_Resource__c>();
                for (Send_NRT_Resource__c r : [SELECT 
                                                Id
                                                , Qty_Sent__c
                                                , Delete__c
                                                , Date_Sent__c
                                                , Item_Sent_to_Order__c
                                                , Item_Sent__c
                                                , Item_NRT__r.Name
                                                , Item_NRT__r.shortDescription__c
                                                , Item_NRT__r.Type_of_Resource__c
                                                , Item_NRT__r.Item_Name1__c                                                
                                                , Qty_to_Send__c
                                                , Item_NRT__r.Default_Unit_of_Measure__c
                                                , Case__c
										        , Price__c
										        , Description_Update__c
										        , Library_Item__c
										        , Unit_Price__c 
                                                FROM Send_NRT_Resource__c 
                                                WHERE Case__c = :caseId]) {
                    mapNrtResources.put(r.Id, r);               
                }
            }
            return mapNrtResources;
        }
        set;
    }    
    
    public NrtListController2(ApexPages.StandardController controller) {
        caseId = ApexPages.currentPage().getParameters().get('Id');
    }  
    
    public PageReference updateTable(){
        Boolean valid = true;
        for (Send_NRT_Resource__c r : mapNrtResources.values()) {
            //if NRT Resource had been sent out, no more changes should be made to it and removed from update list
            if (r.Item_Sent__c != null) {
                mapNrtResources.remove(r.Id);
            }
            
            if (r.Qty_to_Send__c <= 0) {
                valid = false;
                break;              
            }
        }        

        if (valid) {
            upsert(mapNrtResources.values());
        }
                
        //clear out mapNrtResources to refresh list with new data
        mapNrtResources = null;        
        return null;        
        /*
        for(integer i = 0; i < nrtResourcesList.Size() && valid == true; i++){
            if(nrtResourcesList[i].Item_NRT__c == null) valid = false;
            if([SELECT id FROM Send_NRT_Resource__c WHERE id = :nrtResourcesList[i].id].Size() == 1){
                Send_NRT_Resource__c temp = [SELECT id, Item_NRT__c, Item_Sent_to_Order__c, Qty_to_Send__c FROM Send_NRT_Resource__c WHERE id = :nrtResourcesList[i].id];
                if(temp.Item_Sent_to_Order__c == true) {
                    if(nrtResourcesList[i].Item_NRT__c != temp.Item_NRT__c) valid = false;
                    if(nrtResourcesList[i].Qty_to_Send__c != temp.Qty_to_Send__c) valid = false;
                    if(nrtResourcesList[i].Item_Sent_to_Order__c != temp.Item_Sent_to_Order__c) valid = false;
                }
            }
            if(nrtResourcesList[i].Item_Sent_to_Order__c == true){
                if(nrtResourcesList[i].Qty_Sent__c <= 0) valid = false;
                if(nrtResourcesList[i].Qty_Sent__c > nrtResourcesList[i].Qty_to_Send__c) valid = false;
            }
             if(nrtResourcesList[i].Case__c == null) valid = false;
        }
        PageReference curPage = ApexPages.currentPage();
        if(valid){
            upsert(nrtResourcesList);
            curPage.setRedirect(true);
        }
        return curPage;*/
    }
    
    public void insertRow(){        
        Send_NRT_Resource__c r = new Send_NRT_Resource__c(Case__c = caseId);
        mapNrtResources.put(r.Id, r);
        //Send_NRT_Resource__c newResource = new Send_NRT_Resource__c(Case__c = caseId);
        //nrtResourcesList.add(newResource);
    }
    
    public PageReference deleteRow(){
        Send_NRT_Resource__c r = mapNrtResources.get(ApexPages.currentPage().getParameters().get('lineNo'));//System.currentPagereference().getParameters().get('lineNo'));
        
        if (r != null) {
            delete r;
        }
        
        //clear out mapNrtResources to refresh list with new data        
        mapNrtResources = null;
        return null;    	
    	/*
        //[DELETE FROM Send_NRT_Resource__c WHERE id = :System.currentPagereference().getParameters().get('lineNo')];
        //nrtResourcesList.remove(System.currentPagereference().getParameters().get('lineNo'));
        //delete nrtResourcesList;
        PageReference curPage = ApexPages.currentPage();
        if([SELECT id, Item_Sent_to_Order__c FROM Send_NRT_Resource__c WHERE id = :System.currentPagereference().getParameters().get('lineNo') AND Item_Sent_to_Order__c = false].Size() == 1){
            Send_NRT_Resource__c temp = [SELECT id, Item_Sent_to_Order__c FROM Send_NRT_Resource__c WHERE id = :System.currentPagereference().getParameters().get('lineNo') AND Item_Sent_to_Order__c = false];
            delete temp;
            curPage.setRedirect(true);
        }
        return curPage;
        */
    }
}