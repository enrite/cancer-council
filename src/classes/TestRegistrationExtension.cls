@isTest
private class TestRegistrationExtension 
{
	@isTest(SeeAllData=true)
	private static void test1()
	{
	    for(Registration__c r : [SELECT ID 
	    						FROM 	Registration__c 
	    						WHERE 	ID IN (SELECT Registration__c 
	    									FROM 	Contact_Registrations__c)
	    						LIMIT 1])
		{
			RegistrationExtension.cancelContactRegistrations(r.ID);
		}
	}
}