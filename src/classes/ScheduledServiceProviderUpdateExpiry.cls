public class ScheduledServiceProviderUpdateExpiry implements Schedulable
{
    public void execute(SchedulableContext context)
    {
        Public_Data_Publishing__c config = Public_Data_Publishing__c.getOrgDefaults();
        Integer interval = config.Update_Request_Expiry_Interval__c == null ? 1 : config.Update_Request_Expiry_Interval__c.intValue();
        Datetime expiryDate = Datetime.now().addDays(interval * -1);
        List<Public_Data__c> publicDataToUpdate = new List<Public_Data__c>();
        for(Public_Data__c pd : [SELECT ID
                                FROM    Public_Data__c
                                WHERE   RecordType.DeveloperName = 'Temporary' AND
                                        Status__c = 'Sent' AND
                                        Email_Sent_to_Service_Provider__c < :expiryDate])
        {
            pd.Status__c = 'Expired';
            publicDataToUpdate.add(pd);
        }
        update publicDataToUpdate;
    }
}