public with sharing class RecordValueChangedAlert {
// Class used for object field values changed
    
    private static string objectName {get;set;}  
      
    private static boolean fieldValueMatched {
		get {			
        	if (fieldValueMatched == null) {
          	return false;
        	}
        	
        	return fieldValueMatched;
      	}
      	set;
    }
    
    private static boolean fieldValueChanged {
    	get {
        	if (fieldValueChanged == null) {
          	return false;
        	}
        	
        	return fieldValueChanged;
      	}      
      	set;
    }
    
    private static string emailAddress {
    	get {
        	return 'ithelpdesk@cancersa.org.au';
      	}    
    }
    
    //Map Alert_Record__c list to store watch-list field names and corresponding API names
	private static map<Id, Alert_Record__c> mapAlertRecords {
		get {
			if (mapAlertRecords == null) {
				if (objectName != null) {
					mapAlertRecords = new map<Id, Alert_Record__c>([SELECT
							                                          Id
							                                          , Object_API_Name__c
							                                          , Email_To__c
							                                          , Field_API_Name_Alert__c
							                                          , Field_Value_Alert__c
							                                          , When_Value_Changed__c
							                                          , Name
							                                          , Notes__c
							                                          , Friendly_Name__c
							                                          , Alert_Creation__c
							                                          , Alert_Modification__c
							                                          , Alert_Deletion__c
							                                          FROM Alert_Record__c
							                                          WHERE Object_API_Name__c = :objectName
							                                          AND Active__c = true]);				
				}				
			}
			
			return mapAlertRecords;
		}
	}
	
	//Map Alert_Field__c list to store watch-list field names and corresponding API names
	private static map<Id, list<Alert_Field__c>> mapAlertFields {
		get {
			if (mapAlertFields == null) {
				if (mapAlertRecords.size() > 0) {
					mapAlertFields = new map<Id, list<Alert_Field__c>>();
					
					for (Alert_Field__c af : ([SELECT Id, Alert_Record__c, Field_API_Name__c, Field_Label_Name__c FROM Alert_Field__c WHERE Alert_Record__c In :mapAlertRecords.keySet()])) {
						if (mapAlertFields.containsKey(af.Alert_Record__c)) {
							list<Alert_Field__c> listAlertField = mapAlertFields.get(af.Alert_Record__c);
							listAlertField.add(af);
							mapAlertFields.put(af.Alert_Record__c, listAlertField);
						}
						else {
							mapAlertFields.put(af.Alert_Record__c, new list<Alert_Field__c>{af});
						}
					}
				}				
			}
			return mapAlertFields;
		}
	}
    
    private static string getFieldType (string fieldName) {
	    // Get sObjectType name
	    Schema.sObjectType t = Schema.getGlobalDescribe().get(objectName);        
	    Schema.DescribeSObjectResult r = t.getDescribe();
	    
	    // Get field name Data Type
	    Schema.DescribeFieldResult fr = r.fields.getMap().get(fieldName).getDescribe();
	    
		return fr.getType().name();
    }
    
    // Get Dynamic Record Name from Custom Setting object Record_Value_Changed_Record_Name__c
    private static string getRecordName (sObject objRecord) {
    	map<string, Record_Value_Changed_Record_Name__c> mapRecordName = Record_Value_Changed_Record_Name__c.getAll();
    	string recordName = null;
    	
    	for (Record_Value_Changed_Record_Name__c rvc : mapRecordName.values()) {
    		if (rvc.Object_API_Name__c == objectName) {
    			string fieldValue = string.valueOf(objRecord.get(rvc.Field_Name__c));
    			recordName = (recordName == null ? fieldValue : recordName + ' - ' + fieldValue);
    		}
    	}
    	
    	// If no value returned set default value to Id field
    	if (recordName == null)  {
    		recordName = string.valueOf(objRecord.get('Id'));
    	}
    	
    	return recordName;
    }
    
    private static boolean isFieldValueMatched (string matchFieldName, string matchFieldValue, string newRecordField, string oldRecordField) {    	    
	    fieldValueMatched = false;	    
	   
	    // Check if Alert Field Data Type is Multipicklist, if yes then loop through values for matching each value
	    if (getFieldType(matchFieldName) == 'MULTIPICKLIST') {
	    	if (newRecordField != null) {
	        	list<string> listNewFieldValue = new list<string>(string.valueOf(newRecordField).split(';'));
	        
	        	for (integer i = 0; i < listNewFieldValue.size(); i++) {
	          		if (string.valueOf(listNewFieldValue[i]) == string.valueOf(matchFieldValue)) {                  
	            		fieldValueMatched = true;              
	          		}
	        	}          
	      	}
	      	
	      	if (oldRecordField != null) {
	        	list<string> listOldFieldValue = new list<string>(string.valueOf(oldRecordField).split(';'));                
	        
		        for (integer i = 0; i < listOldFieldValue.size(); i++) {
	          		if (string.valueOf(listOldFieldValue[i]) == string.valueOf(matchFieldValue)) {                  
	            		fieldValueMatched = true;            
	          		}
	        	}              
	      	}          
	    }
	    else {
	    	fieldValueMatched = (newRecordField == matchFieldValue || oldRecordField == matchFieldValue) ? true : false;
	    }	    
	    
	    return fieldValueMatched;
    }
    
    // To check if field value is changed when field 'When_Value_Changed__c' is ticked
    private static boolean isFieldValueChanged(string newRecordField, string oldRecordField) {
    	fieldValueChanged = false;
      
      	if (newRecordField != oldRecordField) {
	        fieldValueChanged = true;
      	}
      
      	return fieldValueChanged;
    }
    
    public static string alertNameTable (string alertName, string changeType, string recordLink) {
        string tableHTML = '<table border="1" width="auto">' +
        					'<tr>' +
        					'<th width="50%">Alert Name</th>' +
        					'<th width="25%">Alert Type</th>' +
        					'<th width="25%">Record</th>' +
        					'</tr>' +
							'<tr>' +
							'<td>' + alertName + '</td>' +
							'<td>' + changeType + '</td>' +
							'<td>' + recordLink + '</td>' +
							'</tr>' +
							'</table><br/>';
		return tableHTML;
    }
    
    public static void recordInserted(string objName, map<Id, sObject> newRecord) {
    	try {
       		objectName = objName;
       		
       		// Exit if no Alert Record matched
			if (mapAlertRecords.size() == 0) {
				return;
			}
			
			list<Messaging.SingleEmailMessage> allMails = new list<Messaging.SingleEmailMessage>();
      
      		for (sObject newObject : newRecord.values()) {
        		string recordName = getRecordName(newObject);
        
        		for (Alert_Record__c ar : mapAlertRecords.values()) {
        			if (!ar.Alert_Creation__c) {
        				continue;
        			}
        			
        			fieldValueMatched = false;
        			
            		list<string> listRecipients = new list<string>(ar.Email_To__c.split(';'));
          
	          		if (ar.Object_API_Name__c == objectName) {
		          		if (ar.Field_Value_Alert__c != null) {
		            		isFieldValueMatched (ar.Field_API_Name_Alert__c, ar.Field_Value_Alert__c, string.valueOf(newObject.get(ar.Field_API_Name_Alert__c)), null);  
		          		}	          			
	          		}
	          
	          		if (fieldValueMatched) {
						// HTML message body          			
	                	string message = alertNameTable(ar.Friendly_Name__c, 'New Record', '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + newObject.get('Id') + '">' + recordName) +
	                    	            '<table border="1" width="auto">' +
	                                    '<tr>' +
	                                    '<th width="50%">Field Name</th>' +
	                                    '<th width="50%">Value</th>' +	                                    
	                                    '</tr>';

						for (Alert_Field__c af : mapAlertFields.get(ar.Id)) {
							// Only loop through Alert Fields that linked to Alert Record 
							//if (ar.Id == af.Alert_Record__c) {
								string newValue = string.valueOf(newObject.get(af.Field_API_Name__c));
								message += '<tr>' +
								           '<td>' + af.Field_Label_Name__c + '</td>' + 
										   '<td>' + newValue + '</td>' +											  
										   '</tr>';
							//}
						}
						
						message += '</table><br/>' +
			        	            'Alert Record: <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + ar.Id + '">' + ar.Name + '</a><br/>' +
			        	            'Field API Name Alert: ' + ar.Field_API_Name_Alert__c + '<br/>' +
			        	            'Field Value Alert: ' + ar.Field_Value_Alert__c + '<br/>' +
			        	            'When Value Changed: ' + string.valueOf(ar.When_Value_Changed__c) + '<br/>' +
			        	            'Notes: ' + ar.Notes__c;							                    	            
	                                      
	             		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	             
	             		string subject = ar.Friendly_Name__c + ': New Record Created - ' + recordName;
	             
	            		mail.setToAddresses(listRecipients);
	            		mail.setInReplyTo(emailAddress);                          
	            		mail.setHtmlBody(message);
	            		mail.setSubject(subject);
	                            
	            		allMails.add(mail);          
	          		}
        		}        
      		}
      
      		if (allMails.size() > 0) {
        		Messaging.sendEmail(allMails);
      		}        
      	}
      	catch (exception e) {
        	string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
        						'Cause: ' + e.getCause() + '<br/>' +
        						'Line Number: ' + e.getLineNumber() + '<br/>' +
        						'Type Name: ' + e.getTypeName() + '<br/>' +
        						'Stack Trace String: ' + e.getStackTraceString();
          	system.debug('***** Error RecordValueChangedAlert Insert: ' + errorMessage);
          	EmailUtil email = new EmailUtil(emailAddress, 'RecordValueChangedAlert Insert: ' + objName, errorMessage, 'HTML');        
      	}
    }
 
    public static void recordUpdated(string objName, map<Id, sObject> newRecord, map<Id, sObject> oldRecord) {		
    	try {
      		objectName = objName;

			// Exit if no Alert Record matched
			if (mapAlertRecords.size() == 0) {
				return;
			}
			
      		list<Messaging.SingleEmailMessage> allMails = new list<Messaging.SingleEmailMessage>();
      
      		for (sObject newObject : newRecord.values()) {        
        		sObject oldObject = oldRecord.get(newObject.Id);
        
		        string recordName = getRecordName(newObject);
            
        		for (Alert_Record__c ar : mapAlertRecords.values()) {
        			if (!ar.Alert_Modification__c) {
        				continue;
        			}
        			
          			boolean valueChanged = false;
          			fieldValueMatched = false;
          			fieldValueChanged= false;
          
					list<string> listRecipients = new list<string>(ar.Email_To__c.split(';'));
					
					if (ar.Object_API_Name__c == objectName) {
						if (ar.Field_Value_Alert__c != null) {
							isFieldValueMatched (ar.Field_API_Name_Alert__c, ar.Field_Value_Alert__c, string.valueOf(newObject.get(ar.Field_API_Name_Alert__c)), string.valueOf(oldObject.get(ar.Field_API_Name_Alert__c)));
						}
						else {
							isFieldValueChanged(string.valueOf(newObject.get(ar.Field_API_Name_Alert__c)), string.valueOf(oldObject.get(ar.Field_API_Name_Alert__c)));
						}						
					}
		          
					if (fieldValueMatched || fieldValueChanged) {
						// HTML message body
						string message = alertNameTable(ar.Friendly_Name__c, 'Update Record', '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + newObject.get('Id') + '">' + recordName) +
                                          '<table border="1" width="auto">' +
                                          '<tr>' +
                                          '<th width="50%">Field Name</th>' +
                                          '<th width="25%">New Value</th>' +
                                          '<th width="25%">Old Value</th>' +
                                          '</tr>';  
            
						for (Alert_Field__c af : mapAlertFields.get(ar.Id)) {
							// Only loop through Alert Fields that linked to Alert Record 
							//if (ar.Id == af.Alert_Record__c) {
								string newValue = string.valueOf(newObject.get(af.Field_API_Name__c));
								string oldValue = string.valueOf(oldObject.get(af.Field_API_Name__c));
					
								// Check if value changed
								if ((ar.Field_Value_Alert__c != null && newValue != oldValue) || (ar.When_Value_Changed__c = true && newValue != oldValue)) {								
									valueChanged = true;
									message += '<tr>' +
											  '<td>' + af.Field_Label_Name__c + '</td>' + 
											  '<td>' + newValue + '</td>' +
											  '<td>' + oldValue + '</td>' +
											  '</tr>';
								}
							//}
						}
						
						if (valueChanged) {
							message += '</table><br/>' +
										'Alert Record: <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + ar.Id + '">' + ar.Name + '</a><br/>' +
										'Field API Name Alert: ' + ar.Field_API_Name_Alert__c + '<br/>' +
										'Field Value Alert: ' + ar.Field_Value_Alert__c + '<br/>' +
										'When Value Changed: ' + string.valueOf(ar.When_Value_Changed__c) + '<br/>' +
										'Notes: ' + ar.Notes__c;
										
							Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
							String subject= ar.Friendly_Name__c + ': Value Changed: ' + recordName;
						  
							mail.setToAddresses(listRecipients);
							mail.setInReplyTo(emailAddress);
							mail.setHtmlBody(message);
							mail.setSubject(subject);
						  
							allMails.add(mail);                                          
						}        
					}
				}      
			}
		
			if (allMails.size() > 0) {
				Messaging.sendEmail(allMails);
			}
      
		}
		catch (exception e) {
			string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
								  'Cause: ' + e.getCause() + '<br/>' +
								  'Line Number: ' + e.getLineNumber() + '<br/>' +
								  'Type Name: ' + e.getTypeName() + '<br/>' +
								  'Stack Trace String: ' + e.getStackTraceString();        
			system.debug('***** Error RecordValueChangedAlert Update: ' + errorMessage);
			EmailUtil email = new EmailUtil(emailAddress, 'RecordValueChangedAlert Update: ' + objName, errorMessage, 'HTML');        
		}    
	}
  
	public static void recordDeleted(string objName, map<Id, sObject> oldRecord) {  	
        try {
			objectName = objName;
          
			//List to store MasterRecordId after Record been merged
			list<Id> listSurvivedIds = new list<Id>();
          
			list<Messaging.SingleEmailMessage> allMails = new list<Messaging.SingleEmailMessage>();
          
			map<Id, sObject> mapSurvivedRecords = new map<Id, sObject>();
       
			// Exit if no Alert Record matched
			if (mapAlertRecords.size() == 0) {          	
				return;
			}
			
			if (objName == 'Account' || objName == 'Contact') {
				//Loop through Object Record list to check if Record is Merged and save MasterRecordId/Survived Record Id into list
				for (sObject oldObject : oldRecord.values()) {
					if (oldObject.get('MasterRecordId') != null) {
						listSurvivedIds.add((Id)oldObject.get('MasterRecordId'));
					}
				}         	
			}
          
			// Get Merged Survived Record Information into Map list
			if (listsurvivedIds.size()>0) {
				for (sObject record : database.query('SELECT Id, Name FROM ' + objName + ' WHERE Id In :listSurvivedIds')) {
					mapSurvivedRecords.put((Id)record.get('Id'), record);            
				}
			}        
          
			for (sObject oldObject : oldRecord.values()) {
				for (Alert_Record__c ar : mapAlertRecords.values()) {
        			if (!ar.Alert_Deletion__c) {
        				continue;
        			}
        			
					fieldValueMatched = false;
					
					list<string> listRecipients = new list<string>(ar.Email_To__c.split(';'));
					string message = '';
					string subject = '';
					string recordName = '';
					
					if (ar.Object_API_Name__c == objectName) {
						if (ar.Field_Value_Alert__c != null) {
							isFieldValueMatched (ar.Field_API_Name_Alert__c, ar.Field_Value_Alert__c, null, string.valueOf(oldObject.get(ar.Field_API_Name_Alert__c)));  
						}						
					}
                     
					if (fieldValueMatched) {                        
						// Record has been merged for Account or Contact
						if ((objName == 'Account' || objName == 'Contact') && oldObject.get('MasterRecordId') != null) {							
								sObject survivedRecord = mapSurvivedRecords.get((Id)oldObject.get('MasterRecordId'));
								string oldRecordName = (objName == 'Account') ? string.valueOf(oldObject.get('Name')) : string.valueOf(oldObject.get('Full_Name__c'));
								recordName = string.valueOf(survivedRecord.get('Name'));
		                
	                            subject = ar.Friendly_Name__c + ': Merged to - ' + recordName;
	                                                     
	                            message = alertNameTable(ar.Friendly_Name__c, 'Merge Record', '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + oldObject.get('MasterRecordId') + '">' + recordName) +
	                            			'<table border="1" width="auto">' +
	                            			'<tr>' +
	                            			'<th width="50%">Field Name</th>' +
	                            			'<th width="25%">Current Value</th>' +
	                            			'<th width="25%">Merged Value</th>' + 
	                                        '</tr>' +
	                                        '<tr>' +
	                                        '<td>Name</td>' +
	                                        '<td>' + recordName + '</td>' +
	                                        '<td>' + oldRecordName + '</td>' +                                    
	                                        '</tr></table><br/>' +
	                                        'Alert Record: <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + ar.Id + '">' + ar.Name + '</a><br/>' +
		                              		'Field API Name Alert: ' + ar.Field_API_Name_Alert__c + '<br/>' +
		                              		'Field Value Alert: ' + ar.Field_Value_Alert__c + '<br/>' +
		                              		'When Value Changed: ' + string.valueOf(ar.When_Value_Changed__c) + '<br/>' +
		                              		'Notes: ' + ar.Notes__c;
						}
						else {
							recordName = getRecordName(oldObject);
							subject= ar.Friendly_Name__c + ': Deleted - ' + recordName;                  
							message = alertNameTable(ar.Friendly_Name__c, 'Delete Record', recordName + ' (' + string.valueOf(oldObject.get('Id')) + ')') +
                    	            '<table border="1" width="auto">' +
                                    '<tr>' +
                                    '<th width="50%">Field Name</th>' +
                                    '<th width="50%">Value</th>' +	                                    
                                    '</tr>';
						}
						
						for (Alert_Field__c af : mapAlertFields.get(ar.Id)) {
							// Only loop through Alert Fields that linked to Alert Record 
							//if (ar.Id == af.Alert_Record__c) {
								string oldValue = string.valueOf(oldObject.get(af.Field_API_Name__c));
								message += '<tr>' +
								           '<td>' + af.Field_Label_Name__c + '</td>' + 
										   '<td>' + oldValue + '</td>' +											  
										   '</tr>';
							//}
						}
						
						message +='</table><br/>' +
								  'Alert Record: <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + ar.Id + '">' + ar.Name + '</a><br/>' +
								  'Field API Name Alert: ' + ar.Field_API_Name_Alert__c + '<br/>' +
								  'Field Value Alert: ' + ar.Field_Value_Alert__c + '<br/>' +
								  'When Value Changed: ' + string.valueOf(ar.When_Value_Changed__c) + '<br/>' +
								  'Notes: ' + ar.Notes__c;						
						
						Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
						 
						mail.setToAddresses(listRecipients);
						mail.setInReplyTo(emailAddress);                          
						mail.setHtmlBody(message);
						mail.setSubject(subject);
										
						allMails.add(mail); 						
					}
				}        
			}
			
			if (allMails.size() > 0) {
				Messaging.sendEmail(allMails);
			}
		}
		catch (exception e) {          
			string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
								  'Cause: ' + e.getCause() + '<br/>' +
								  'Line Number: ' + e.getLineNumber() + '<br/>' +
								  'Type Name: ' + e.getTypeName() + '<br/>' +
								  'Stack Trace String: ' + e.getStackTraceString();
			system.debug('***** Error RecordValueChangedAlert Delete: ' + errorMessage);
			EmailUtil email = new EmailUtil(emailAddress, 'RecordValueChangedAlert Delete: ' + objName, errorMessage, 'HTML');
		}        
	}
}