public with sharing class RegistrationCampaignMember {
// Add Registration Campaign to CampaignMember whenever Registration linked Campaign is created

    private static string emailAddress {
        get {
            return 'ithelpdesk@cancersa.org.au';
        }
    }
            
    public static void insertCampaignMember(map<Id, Registration__c> mapRegistration) {
        try {            
            list<Registration__c> listRegistration = new list<Registration__c>([SELECT Id, Host_Team_Captain__c, Org_Primary_Contact__c, Campaign__c, Campaign__r.ParentId FROM Registration__c WHERE Id IN :mapRegistration.keySet()]);

            set<Id> setCampaign = new set<Id>();
            // Set list to hold CampaignMember for checking if CampaignMember already exists
            set<string> setCampaignMember = new set<string>();
            list<CampaignMember> listCampaignMember = new list<CampaignMember>();       
            
            // Retrieve Campaign list
            for (integer i = 0; i < listRegistration.size(); i++) {
            	if (!setCampaign.contains(listRegistration.get(i).Campaign__c)) {
            		setCampaign.add(listRegistration.get(i).Campaign__c);
            	}
            	
            	if (!setCampaign.contains(listRegistration.get(i).Campaign__r.ParentId)) {
            		setCampaign.add(listRegistration.get(i).Campaign__r.ParentId);            		
            	}
            }
            
            // Retrieve CampaignMember list from setCampaign list
            for (CampaignMember cm : [SELECT Id, CampaignId, ContactId, Registration__c FROM CampaignMember WHERE CampaignId IN :setCampaign]) {
            	setCampaignMember.add(string.valueOf(cm.ContactId) + string.valueOf(cm.CampaignId));
            }
            
        	for (integer i = 0; i < listRegistration.size(); i++) {
        		Id contactId = null;        		
        		
        		Registration__c r = listRegistration.get(i);        		
				
                if (r.Host_Team_Captain__c != null) {
                    contactId = r.Host_Team_Captain__c;
                }
                if (r.Org_Primary_Contact__c != null) {
                    contactId= r.Org_Primary_Contact__c;
                }
                
                if (contactId == null) {
                	continue;
                }

                if (r.Campaign__r.ParentId != null) {
                	if (!setCampaignMember.contains(string.valueOf(contactId) + string.valueOf(r.Campaign__r.ParentId))) {
	                    CampaignMember cm = new CampaignMember();
	                    cm.CampaignId = r.Campaign__r.ParentId;           
	                    cm.Registration__c = r.Id;
	                    cm.ContactId = contactId;
	                    listCampaignMember.add(cm);
	                    setCampaignMember.add(string.valueOf(contactId) + string.valueOf(r.Campaign__r.ParentId));                		
                	}
                }
                if (r.Campaign__c != null) {
                	if (!setCampaignMember.contains(string.valueOf(contactId) + string.valueOf(r.Campaign__c))) {
	                    CampaignMember cm = new CampaignMember();
	                    cm.CampaignId = r.Campaign__c;           
	                    cm.Registration__c = r.Id;
	                    cm.ContactId = contactId;
	                    listCampaignMember.add(cm);
	                    setCampaignMember.add(string.valueOf(contactId) + string.valueOf(r.Campaign__c));                 		
                	}                	
                }
        	}
        	
			list<Messaging.SingleEmailMessage> allMails = new list<Messaging.SingleEmailMessage>();
        	
        	if (listCampaignMember.size() > 0) {
	            Database.Saveresult[] saveResult = Database.insert(listCampaignMember, false);
	            
	            for (Database.SaveResult sr : saveResult) {
	            	if (!sr.isSuccess()) {
	            		for (Database.Error err : sr.getErrors()) {
							string subject = 'RegistrationCampaignMember insertCampaignMember';
							string errorMessage = 'Status Code: ' + err.getStatusCode() + '<br/>' +
												  'Message: ' + err.getMessage() + '<br/>' +
												  'User: ' + UserInfo.getUserName() + '<br/>' +
												  'Fields: ' + err.getFields() + '<br/>';
							
							Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
							
							list<string> listRecipients = new list<string>{emailAddress};
							mail.setToAddresses(listRecipients);
							mail.setHtmlBody(errorMessage);
							mail.setSubject(subject);
							 
							allMails.add(mail);          			
	            		}	            		
	            	}	            	
	            }       		
        	}
        	
			if (allMails.size() > 0) {
				Messaging.sendEmail(allMails);
			}        	

        }
        catch (exception e) {
			string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
								  'Cause: ' + e.getCause() + '<br/>' +
								  'Line Number: ' + e.getLineNumber() + '<br/>' +
								  'Type Name: ' + e.getTypeName() + '<br/>' +
								  'Stack Trace String: ' + e.getStackTraceString();        	
            EmailUtil email = new EmailUtil(emailAddress, 'RegistrationCampaignMember insertCampaignMember', errorMessage, 'HTML');
            system.debug('***** ' + errorMessage);
        }       
    }
    
    public static void updateCampaignMember(map<Id, Registration__c> mapNewRegistration, map<Id, Registration__c> mapOldRegistration) {
        try {
            list<Registration__c> listRegistration = new list<Registration__c>([SELECT Id, Host_Team_Captain__c, Org_Primary_Contact__c, Campaign__c, Campaign__r.ParentId FROM Registration__c WHERE Id IN :mapNewRegistration.keySet()]);
            list<CampaignMember> listOldCampaignMember = new list<CampaignMember>();
            list<CampaignMember> listNewCampaignMember = new list<CampaignMember>();            
            
            // Set list to hold CampaignMember for checking if CampaignMember already exists
            set<string> setCampaignMember = new set<string>();
            
            // Retrieve CampaignMember list from setCampaign list
            for (CampaignMember cm : [SELECT Id, CampaignId, ContactId, Registration__c FROM CampaignMember WHERE Registration__c IN :mapNewRegistration.keySet()]) {
            	setCampaignMember.add(string.valueOf(cm.ContactId) + string.valueOf(cm.CampaignId));
            }
            
            for (Registration__c newReg : listRegistration) {
                Id contactId = null;
                Registration__c oldReg = mapOldRegistration.get(newReg.Id);
                
                // Exit if Campaign / Host Team Captain / Org Primary Contact remain same
                if (newReg.Campaign__c == oldReg.Campaign__c && newReg.Host_Team_Captain__c == oldReg.Host_Team_Captain__c && newReg.Org_Primary_Contact__c == oldReg.Org_Primary_Contact__c) {
                    continue;
                } 
                
                // To get CampaignMember Contact from either Host Team Captain or Org Primary Contact
                if (newReg.Host_Team_Captain__c != null) {
                    contactId = newReg.Host_Team_Captain__c;
                }
                if (newReg.Org_Primary_Contact__c != null) {
                    contactId = newReg.Org_Primary_Contact__c;
                }
                
                if (contactId == null) {
                  continue;
                }
                
                if (newReg.Campaign__r.ParentId != null) {                	
                    CampaignMember cm = new CampaignMember();
                    cm.CampaignId = newReg.Campaign__r.ParentId;
                    cm.Registration__c = newReg.Id;
                    cm.ContactId = contactId;
                    listNewCampaignMember.add(cm);                    
                }
                if (newReg.Campaign__c != null) {
                    CampaignMember cm = new CampaignMember();
                    cm.CampaignId = newReg.Campaign__c;
                    cm.Registration__c = newReg.Id;
                    cm.ContactId = contactId;
                    listNewCampaignMember.add(cm);                	
                }
            }
            
            // Get matching CampaignMember from listRegistration for deletion
            for (CampaignMember cm : [SELECT Id, CampaignId, ContactId, Registration__c FROM CampaignMember WHERE Registration__c IN :listRegistration]) {
                listOldCampaignMember.add(cm);
            }
            
            if (listOldCampaignMember.size() > 0) {
            	delete listOldCampaignMember;	
            }
            
            list<Messaging.SingleEmailMessage> allMails = new list<Messaging.SingleEmailMessage>();
            
            if (listNewCampaignMember.size() > 0) {
	            Database.Saveresult[] saveResult = Database.insert(listNewCampaignMember, false);
	            
	            for (Database.SaveResult sr : saveResult) {
	            	if (!sr.isSuccess()) {
	            		for (Database.Error err : sr.getErrors()) {
							string subject = 'RegistrationCampaignMember updateCampaignMember';
							string errorMessage = 'Status Code: ' + err.getStatusCode() + '<br/>' +
												  'Message: ' + err.getMessage() + '<br/>' +
												  'User: ' + UserInfo.getUserName() + '<br/>' +
												  'Fields: ' + err.getFields() + '<br/>';
							
							Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
							
							list<string> listRecipients = new list<string>{emailAddress};
							mail.setToAddresses(listRecipients);
							mail.setHtmlBody(errorMessage);
							mail.setSubject(subject);
							 
							allMails.add(mail);          			
	            		}	            		
	            	}	            	
	            }            	
            }
            
			if (allMails.size() > 0) {
				Messaging.sendEmail(allMails);
			}                         

        }
        catch (exception e) {
			string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
								  'Cause: ' + e.getCause() + '<br/>' +
								  'Line Number: ' + e.getLineNumber() + '<br/>' +
								  'Type Name: ' + e.getTypeName() + '<br/>' +
								  'Stack Trace String: ' + e.getStackTraceString();         	
            EmailUtil email = new EmailUtil(emailAddress, 'RegistrationCampaignMember updateCampaignMember', errorMessage, 'HTML');
            system.debug('***** ' + errorMessage);
        }

    }
    
    public static void deleteCampaignMember(map<Id, Registration__c> mapRegistration) {     
        try {            
        	// Used to match up deleted records' CampaignId and ContactID against current CampaignMember records
            set<Id> setCampaign = new set<Id>();
            set<Id> setContact = new set<Id>();
            
            list<CampaignMember> listCampaignMember = new list<CampaignMember>();
            
            for (Registration__c r : mapRegistration.values()) {        	
            	if (!setCampaign.contains(r.Campaign__c)) {
            		setCampaign.add(r.Campaign__c);
            	}
                
                // If Campaign Parent Id exits - does not work for deleted record
                //if (r.Campaign__r.ParentId != null && !setCampaign.contains(r.Campaign__r.ParentId)) {
                //    setCampaign.add(r.Campaign__r.ParentId);
                //}
                
                // To get CampaignMember Contact from either Host Team Captain or Org Primary Contact
                if (r.Host_Team_Captain__c != null && !setContact.contains(r.Host_Team_Captain__c)) {
                    setContact.add(r.Host_Team_Captain__c);
                }
                if (r.Org_Primary_Contact__c != null && !setContact.contains(r.Org_Primary_Contact__c)) {
                    setContact.add(r.Org_Primary_Contact__c);
                }
            }
            
            // Retrieve Campaign ParentId as Registration.Campaign__r.ParentId does not work on deleted registration records
            for (Campaign camp : [SELECT Id, ParentId FROM Campaign WHERE Id IN :setCampaign]) {
            	if (!setCampaign.contains(camp.ParentId)) {
            		setCampaign.add(camp.ParentId);
            	}
            }
            
            // Get matching CampaignMember from setCampaign and setContact
            for (CampaignMember cm : [SELECT Id FROM CampaignMember WHERE CampaignId IN :setCampaign AND ContactId IN :setContact]) {
                listCampaignMember.add(cm);
            }
            
            // Delete CampaignMember records from matched Campaign and Contact 
            delete listCampaignMember;
                 
        }
        catch (exception e) {
			string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
								  'Cause: ' + e.getCause() + '<br/>' +
								  'Line Number: ' + e.getLineNumber() + '<br/>' +
								  'Type Name: ' + e.getTypeName() + '<br/>' +
								  'Stack Trace String: ' + e.getStackTraceString();          	
            EmailUtil email = new EmailUtil(emailAddress, 'RegistrationCampaignMember deleteCampaignMember', errorMessage, 'HTML');
            system.debug('***** ' + errorMessage);
        }
    }
    
}