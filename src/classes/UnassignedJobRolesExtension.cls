public with sharing class UnassignedJobRolesExtension
{
    public UnassignedJobRolesExtension(ApexPages.StandardController c)
    {
        record = (Event_Details__c)c.getRecord();
    
    }

    private Event_Details__c record;

    public Integer UnassignedCount
    {
        get
        {
            if (record.Start_Date__c == null || record.End_Date__c == null)
            {
                return 0;
            }
        
            return [SELECT COUNT()
                    FROM Job_Role__c
                    WHERE RecordType.Name = 'Event Volunteer'
                    AND Event_Location__c = :record.Event_Location__c
                    AND Campaign__c = :record.Campaign__c
                    AND Date_Started__c >= :record.Start_Date__c
                    AND Date_Started__c <= :record.End_Date__c
                    AND Registration__r.Auto_generated_JR__c = true];
        }            
    }
    
}