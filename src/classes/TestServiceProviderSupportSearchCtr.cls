@isTest
public class TestServiceProviderSupportSearchCtr
{
    public static testMethod void myUnitTest()
    {      
        insert new Reference_Data__c(RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Reference_Data__c' AND DeveloperName = 'Cancer_Categorisation'].Id,
                                     Name = 'test',
                                     Cancer_Category__c = 'test');
    
        ApexPages.CurrentPage().getParameters().put('category', 'test');
        ApexPages.CurrentPage().getParameters().put('serviceType', 'test');
        ApexPages.CurrentPage().getParameters().put('locations', 'test');
        ApexPages.CurrentPage().getParameters().put('atsi', 'test');
        ApexPages.CurrentPage().getParameters().put('cald', 'test');
        ApexPages.CurrentPage().getParameters().put('page', '1');
        ApexPages.CurrentPage().getParameters().put('search', '1');        
                                       
        ServiceProviderSupportSearchController c = new ServiceProviderSupportSearchController();
        
        system.debug(c.category);    
        system.debug(c.serviceTypes);    
        c.createLocationsList();
        c.newSearch();               
    }
}