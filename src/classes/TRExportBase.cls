public virtual class TRExportBase 
{
	public TRExportBase(TR_Export__c exportRecord)
	{
		TRExport = [SELECT ID,
							Name,
							Export_Time__c,
							Finish_Time__c
				FROM 	TR_Export__c
				WHERE 	ID = :exportRecord.ID];
		ConstituentRecords = 0;
		QuestionRecords = 0;
		RegistrationRecords = 0;
		TransactionRecords = 0;
	}
	
	public TRExportBase(TRExportBase prevBatch)
	{
		TRExport = prevBatch.TRExport;
		ConstituentRecords = prevBatch.ConstituentRecords == null ? 0 : prevBatch.ConstituentRecords;
		QuestionRecords = prevBatch.QuestionRecords == null ? 0 : prevBatch.QuestionRecords;
		RegistrationRecords = prevBatch.RegistrationRecords == null ? 0 : prevBatch.RegistrationRecords;
		TransactionRecords = prevBatch.TransactionRecords == null ? 0 : prevBatch.TransactionRecords;
	}
	
	protected final String QUOTE = '"';
	protected final String SEPERATOR = ',';
	protected final String FIELD_END = QUOTE + SEPERATOR;
	protected final String NEW_LINE = '\n';
	protected final String LINE_END = QUOTE + NEW_LINE;

	protected final String PAYMENTTYPE_BPAY = 'BPay';
	protected final String PAYMENTTYPE_DIRECTBANKING = 'Direct Banking';
	protected final String PAYMENTTYPE_DIRECTBANKINGEFT = 'Direct Banking (EFT)';
	protected final String PAYMENTTYPE_SHARES = 'Shares';
	protected final String PAYMENTTYPE_PAYPAL = 'Pay Pal';
	protected final String PAYMENTTYPE_CASH = 'Cash';
	protected final String PAYMENTTYPE_ARTEZONLINE = 'Artez online';
	protected final String PAYMENTTYPE_CREDITCARD = 'Credit Card';
	protected final String PAYMENTTYPE_CHEQUEMONEYORDER = 'Cheque/Money Order';
	protected final String PAYMENTTYPE_CHEQUE = 'Cheque';
	protected final String PAYMENTTYPE_OTHER = 'Other';
	protected final String PAYMENTTYPE_LATER = 'Later';

	protected Integer BatchSize
	{
		get
		{
			return Test.isRunningTest() ? 1 : Batch_Size__c.getInstance('TR_Export').Records__c.IntValue();
		}
	}
	
	public TR_Export__c TRExport
	{
		get;
		set;	
	}
	
	public Integer ConstituentRecords
	{
		get;
		set;
	}
	
	public Integer QuestionRecords
	{
		get;
		set;
	}
	
	public Integer RegistrationRecords
	{
		get;
		set;
	}
	
	public Integer TransactionRecords
	{
		get;
		set;
	}
	
	public Boolean RunThisOnly
	{
		get
		{
			if(RunThisOnly == null)
			{
				return false;
			}
			return RunThisOnly;
		}
		set;
	}
	
	protected Attachment createAttachment(String attachmentName, String header)
	{
		Attachment att = new Attachment();
        att.Name = attachmentName;
        att.ParentId = TRExport.ID;
        att.ContentType = 'text/csv';
        att.Body = Blob.valueOf(header);
        insert att;
        return att;
	}
	
	protected Attachment getAttachment(String attachmentName)
	{
		return [SELECT  ID,
                        Body
            FROM        Attachment
            WHERE       ParentID = :TRExport.ID AND
                        Name = :attachmentName]; 
	}
	
	protected DateTime getLastExportTime()
	{
		// this assumes TR_Export__c records are using current timestamp
		// for Export_Time__c when created, would only be a problem if
		// a record created manually, which should only be possible for System Admin 
		for(TR_Export__c te : [SELECT 	Export_Time__c
								FROM	TR_Export__c
								WHERE	Export_Time__c < :TRExport.Export_Time__c
								ORDER BY Export_Time__c DESC
								LIMIT 1])
		{
			return te.Export_Time__c;
		}
		return DateTime.newInstance(1900, 1, 1);
	}
	
	protected String getFileName(String suffix)
	{
		return 
					Date.today().year().format().replace(',', '') + 
					Date.today().month().format().leftPad(2).replace(' ', '0') +
					Date.today().day().format().leftPad(2).replace(' ', '0') + 
					DateTime.now().hour().format().leftPad(2).replace(' ', '0') + 
					DateTime.now().minute().format().leftPad(2).replace(' ', '0') + 
					'_' +
					suffix +  
					'.csv';
	}
	
	protected String getField(String s)
	{
		return getField(s, 255);
	}
	
	protected String getField(Boolean b)
	{
		return getField(convertBoolean(b));
	}
	
	protected String getField(Integer s, Integer maxLength)
	{
		return getField((s == null ? '' : s.format()).replace(SEPERATOR, ''), maxLength);
	}
	
	protected String getField(Decimal s, Integer maxLength)
	{
		return getField((s == null ? '' : s.format()).replace(SEPERATOR, ''), maxLength);
	}
	
	protected String getField(Date s)
	{
		if(s == null)
		{
			return getField('');
		}
		return getField((s.month().format().leftPad(2) + '/' + s.day().format().leftPad(2) + '/' + s.year().format()).replace(SEPERATOR, '').replace(' ', ''));
	}
	
	protected String getField(String s, Integer maxLength)
	{
		if(String.isBlank(s))
		{
			return QUOTE + FIELD_END;
		}
		if(s.length() > maxLength)
		{
			s = s.left(maxLength);
		}	
		return QUOTE + s.replace(NEW_LINE, '').replace(QUOTE, '') + FIELD_END;
	}
	
	protected String endLine(String s)
	{
		if(String.isBlank(s))
		{
			return NEW_LINE;
		}
		return s.removeEnd(SEPERATOR) + NEW_LINE;
	}
	
	protected virtual String convertBoolean(Boolean b)
	{
		return b ? 'TRUE' : 'FALSE';
	}
	
	protected void finaliseBatch()
	{
		TRExport.Finish_Time__c = DateTime.now();
        update TRExport;
        
		List<Campaign> initialCampaignExports = [SELECT ID
						        				FROM	Campaign
						        				WHERE	TR_Event_ID__c != null AND
						        						Initial_Teamraiser_Export__c = null];
        for(Campaign c : initialCampaignExports)
		{
			c.Initial_Teamraiser_Export__c = Datetime.now();
		}
		update initialCampaignExports;
		
        notifiyUser();
	}
	
	private void notifiyUser()
	{
		String body = ConstituentRecords.format() + ' Constituent records exported\n';
        body += QuestionRecords.format() + ' Question records exported\n';
        body += RegistrationRecords.format() + ' Registration records exported\n';
        body += TransactionRecords.format() + ' Transaction records exported\n';
        
        Messaging.Singleemailmessage msg = new Messaging.Singleemailmessage();
        msg.setPlainTextBody(body);
        msg.setToAddresses(new List<String>{UserInfo.getUserEmail()});
        msg.setSubject('Team Raiser export finished');
        Messaging.sendEmail(new List<Messaging.Singleemailmessage>{msg});
	}
}