public with sharing class LinkSunSmartAccountNew2Controller{
    private SunSmart__c[] mSunSmart;
    private Account mAccount;
    String mSunSmartID;
    String mAccountID;
    
    public LinkSunSmartAccountNew2Controller(){

        mSunSmartID = ApexPages.CurrentPage().getParameters().get('id');
        mAccountID = ApexPages.CurrentPage().getParameters().get('accountid');
        
        mSunSmart = [SELECT
            id,
            Web_Postcode__c,
            Web_State__c,
            Web_Street__c,
            Web_Suburb_Town__c,
            School_Email__c,
            School_Name__c,
            School_Phone__c
            FROM SunSmart__c
            WHERE id = :mSunSmartID];
        
        mAccount = [SELECT
            id,
            Name
            FROM Account
            WHERE id = :mAccountID];
        
        
    }
    
    public PageReference save(){
        mSunSmart[0].School__c = mAccount.id;
        update mSunSmart[0];
        return new PageReference('/' + mSunSmartID);
    }
    
    public PageReference cancel(){
       return new PageReference('/' + mSunSmart[0].id);
    }
    
    public SunSmart__c getSunSmart(){
        return mSunSmart.size() > 0 ? mSunSmart[0] : null;
    }
    
    public Account getAccount(){
        return mAccount;
    }
}