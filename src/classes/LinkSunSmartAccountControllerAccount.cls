public with sharing class LinkSunSmartAccountControllerAccount{
    private static final Integer cWeightBillingStreet = 20;
    private static final Integer cWeightBillingPostalCode = 5;
    private static final Integer cWeightBillingCity = 5;
    private static final Integer cWeightPhone = 40;
    private static final Integer cWeightName = 30;
    
    private boolean mMatchBillingStreet;
    private boolean mMatchBillingPostalCode;
    private boolean mMatchBillingCity;
    private boolean mMatchPhone;
    private boolean mMatchName;
    
    private Account mAccount;
    private SunSmart__c mSunSmart;
    private Integer mScore;
    
    // Binary search tree links
    private LinkSunSmartAccountControllerAccount mLeft;
    private LinkSunSmartAccountControllerAccount mRight;
    
    
    public LinkSunSmartAccountControllerAccount(Account pAccount, SunSmart__c pSunSmart){
        mAccount = pAccount;
        mSunSmart = pSunSmart;
        mScore = 0;
        
        mMatchBillingStreet = pAccount.BillingStreet != null && pAccount.BillingStreet == pSunSmart.Web_Street__c;
        mMatchBillingPostalCode = pAccount.BillingPostalCode != null && pAccount.BillingPostalCode == pSunSmart.Web_Postcode__c;
        mMatchBillingCity = pAccount.BillingCity != null && pAccount.BillingCity == pSunSmart.Web_Suburb_Town__c;
        mMatchPhone = pAccount.Phone != null && pAccount.Phone == pSunSmart.School_Phone__c;
        mMatchName = pAccount.Name != null && pAccount.Name == pSunSmart.School_Name__c;
        
        if(mMatchBillingStreet) mScore += cWeightBillingStreet;
        if(mMatchBillingPostalCode) mScore += cWeightBillingPostalCode;
        if(mMatchBillingCity) mScore += cWeightBillingCity;
        if(mMatchPhone) mScore += cWeightPhone;
        if(mMatchName) mScore += cWeightName;
    }
    
    public Integer getScore(){
        return mScore;
    }
    
    public Account getAccount(){
        return mAccount;
    }
    
    public SunSmart__c sunSmart(){
        return mSunSmart;
    }
    
    public void add(LinkSunSmartAccountControllerAccount pNode){
        if(pNode.getScore() > mScore){
            if(mRight == null) mRight = pNode;
            else mRight.add(pNode);
        }else{
            if(mLeft == null) mLeft = pNode;
            else mLeft.add(pNode);
        }
    }
    
    public void buildArrayDesc(LinkSunSmartAccountControllerAccount[] pArray){
        if(mRight != NULL) mRight.buildArrayDesc(pArray);
        pArray.add(this);
        if(mLeft != NULL) mLeft.buildArrayDesc(pArray);
    }
    
    public boolean getMatchBillingStreet(){
        return mMatchBillingStreet;
    }
    
    public boolean getMatchBillingPostalCode(){
        return mMatchBillingPostalCode;
    }
    
    public boolean getMatchBillingCity(){
        return mMatchBillingCity;
    }
    
    public boolean getMatchPhone(){
        return mMatchPhone;
    }
    
    public boolean getMatchName(){
        return mMatchName;
    }

}