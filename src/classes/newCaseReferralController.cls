public class newCaseReferralController {
		
    public String caseidstring = System.currentPagereference().getParameters().get('caseid');
    public String render1{get; set;}
    public String render2{get; set;}
         
    public newCaseReferralController(){
         render1 = 'true';
         render2 = 'false';
         if(caseidstring != null){
             cases = [SELECT Id, Referring_Contact__c, Referring_Account__c, casenumber FROM Case WHERE Id = :caseidstring];
             if(cases.Referring_Contact__c != null ){
                 render1 = 'false';
                 render2 = 'true';
             } 
         }
    }
    // These three member variables maintain the state of the wizard.
    // When users enter data into the wizard, their input is stored in these variables.
    Account account;
    Contact contact;
    Case cases; 
    boolean existingAccount = false;

    // The next three methods return one of each of the three member 
    // variables. If this is the first time the method is called,  
    // it creates an empty record for the variable. Except the CAse is a lookup
    public Account getAccount() {
    	if(account == null){
        	if(cases.Referring_Account__c == null){
            	account = new Account();
         	}else{
            	account = [SELECT id, phone, name, fax, billingstate, billingstreet, billingcity, billingcountry, billingPostalCode
            			, ShippingStreet, shippingcity, shippingstate, shippingpostalcode, recordtypeId, status__c FROM Account WHERE id = :cases.Referring_Account__c];
            	existingAccount = true;
    		}
      	}
      	return account;
   	}

   	public Contact getContact() {
      	if(cases.Referring_Contact__c == null && contact == null){
         	contact = new Contact();
      	}
      	return contact;
   	}

   	public Case getCase() {  
      	if(cases == null) {
      		cases = [SELECT id,casenumber,test__c FROM case WHERE id=:caseidstring];
      	}
      	return cases;
   	}


   	// The next three methods control navigation through 
   	// the wizard. Each returns a PageReference for one of the three pages  
   	// in the wizard. Note that the redirect attribute does not need to  
   	// be set on the PageReference because the URL does not need to change 
   	// when users move from page to page.  
    
   	public PageReference step1() {
      	return Page.CasereferralStep1;
   	}

   	public PageReference step2() {
      	return Page.CasereferralStep2;
   	}

   	public PageReference step3() {
      	return Page.CasereferralStep3;
   	}

   	// This method cancels the wizard, and returns the user to the
   	// Case Record it departed from  
    public PageReference cancel() {      
    	PageReference pageRef = new PageReference('/'+caseidstring);
        pageRef.setRedirect(true);
        return pageRef; 
    }

   	// This method performs the final save for all three objects, and 
   	// then navigates the user to the detail page for the updated case.
   	public PageReference save() {
		// Set Transaction Savepoint for rollback when either Account or Contact creation failed 
	  	SavePoint sp = Database.setSavepoint();   	
      	// Create the account. Before inserting, copy the contact's  
      	// phone number into the account phone number field.  
    
      	account.phone = contact.phone;
      	account.fax = contact.fax;
      	account.recordtypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Health_Organisation'].Id;
      	account.status__c = 'Active';
      
      	if(existingAccount == false) {			
			if (!UtilRecordDML.dmlInsertRecord(account, newCaseReferralController.class.getName())) {
				Database.rollback(sp);
		    	// Set Account to null after rollback
		    	account = null;
		    	return null;
			}      	
      	}

      	// Create the contact. Before inserting, use the id field 
      	// that's created once the account is inserted to create  
      	// the relationship between the contact and the account.  
    
      	contact.accountId = account.id;
      	contact.recordtypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Health_Contact'].Id;
      	contact.status__c = 'Active';
      	contact.mailingstreet = account.billingstreet;
      	contact.mailingcity = account.billingcity;
      	contact.mailingstate = account.billingstate;
      	contact.mailingcountry = account.billingcountry;
      	contact.mailingPostalCode = account.billingPostalCode;
      	contact.phone = account.phone;
      	contact.fax = account.fax;
      	
	    if (!UtilRecordDML.dmlInsertRecord(contact, newCaseReferralController.class.getName())) {
	    	// Rollback/delete account record that created previously if contact is failed to create	    	
	    	Database.rollback(sp);
	    	// Set Account and Contact to null after rollback
	    	account = null;
	    	contact = null;
	    	return null;
	    }

      	// Update Case Contact ID and Case Account ID. Before updating, create 
      	// another relationship with the account.  
      	cases.Referring_Account__c = account.Id;
      	cases.Referring_Contact__c = contact.Id;      	
      	UtilRecordDML.dmlUpdateRecord(cases, newCaseContactController.class.getName());

      	// Finally, send the user to the detail page for 
      	// the updated case.  

      	PageReference CasePage = new PageReference('/'+caseidstring);
      	CasePage.setRedirect(true);
      	return CasePage;
   	}
}