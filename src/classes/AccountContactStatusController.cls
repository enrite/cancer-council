public with sharing class AccountContactStatusController {

    //propertity variables
    public string objStr {get;set;}
    public string statusStr {get;set;}
    public string inputIdStr {get;set;}
    public string finalIdStr {get;set;}
    public string feedbackStr {get;set;}
    
    //Object Type list
    public list<selectOption> getObjType() {
        list<selectOption> options = new list<selectOption>();
        options.add(new selectOption('', '--None--'));
        options.add(new selectOption('Account', 'Account'));
        options.add(new selectOption('Contact', 'Contact'));                
        return options;
    }
    
    //Status list
    public list<selectOption> getStatus() {
        list<selectOption> options = new list<selectOption>();
        options.add(new selectOption('', '--None--'));        

        Schema.DescribeFieldResult fieldResult;
        List<Schema.PicklistEntry> ple;
        
        if (objStr == 'Account') {
            fieldResult = Account.Status__c.getDescribe();
            ple = fieldResult.getPicklistValues();
        }
        if (objStr == 'Contact') {
            fieldResult = Contact.Status__c.getDescribe();
            ple = fieldResult.getPicklistValues();   
        }                
        if (ple!=null) {
            for( Schema.PicklistEntry f : ple)        
            {
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }
                
        return options;
    }    
    
    public void click(){
        
        try {
            if (inputIdStr!=null && statusStr != null && objStr != null) {              

                if (objStr == 'Account') {
                    if (inputIdStr.contains('O')) {
                        if (inputIdStr.contains('-')) {
                            //is salesforce format O-XXX, could be used directly on SOQL 
                            finalIdStr = inputIdStr;
                        }
                        else {
                            //extracted Id from barcode format for SOQL
                            finalIdStr = 'O-' + inputIdStr.substring(inputIdStr.indexOf('O', 0)+1, inputIdStr.indexOf(' ', 0)); 
                        }
                        
                        Account record = [select Id, Name, Org_ID__c, Status__c from Account where Org_Id__c = : finalIdStr];
                        
                        record.Status__c = statusStr;
                        update record;
                        feedbackStr = record.Name + ' status updated to - ' + record.Status__c;
                    }
                    //if Object=Account selected but Id contains Cxxx
                    else {
                        feedbackStr = 'Wrong Id Type';
                    }
                }
                
                if (objStr == 'Contact') {
                    if (inputIdStr.contains('C')) {
                        if (inputIdStr.contains('-')) {
                            //is salesforce format C-XXX, could be used directly on SOQL
                            finalIdStr = inputIdStr;
                        }
                        else {
                            //extracted Id from barcode format for SOQL
                            finalIdStr = 'C-' + inputIdStr.substring(inputIdStr.indexOf('C', 0)+1, inputIdStr.indexOf(' ', 0));                         
                        }
                        
                        Contact record = [select Id, Full_Name__c, Contact_ID__c, Status__c from Contact where Contact_ID__c = : finalIdStr];
                                                
                        record.Status__c = statusStr;
                        update record;
                        feedbackStr = record.Full_Name__c + ' status updated to - ' + record.Status__c;
                    }
                    //if Object=Account selected but Id contains Oxxx
                    else {
                        feedbackStr = 'Wrong Id Type';
                    }
                }
            }           
        }
        catch (System.QueryException e) {
            //if Id provided returns with no record matched and occurs Exception
            feedbackStr = 'No Record Found';            
        }
        catch (exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        //return null;
    }
    
    static testMethod void test() {
                        
        test.startTest();
        
        string accountId;
        string accountBarId;
        string contactId;
        string contactBarId;
                
        Account testAccount = new Account (Name = 'test account', Status__c = 'Active');
        insert testAccount;
        Account a = [select Org_ID__c from Account where Id = :testAccount.Id];
        accountId = a.Org_ID__c;
        //Id format to match against barcode format Oxxxx
        accountBarId = accountId.replace('-', '');
        
        Contact testContact = new Contact (FirstName = 'test First', LastName = 'test Last', Status__c = 'Active', AccountId = testAccount.Id);
        insert testContact;        
        Contact c = [select Contact_ID__c from Contact where Id = :testContact.Id];
        contactId = c.Contact_ID__c;
        //Id format to match against barcode format Cxxxx
        contactBarId = contactId.replace('-', '');
               
        AccountContactStatusController acs = new AccountContactStatusController();
        
        system.assert(acs.getStatus().size()>0);
        system.assert(acs.getObjType().size()>0);

        acs.objStr = 'Account';
        acs.statusStr = 'Return To Sender';
        acs.inputIdStr = '62,75924193,' + accountBarId + '   ,02 08 52 06';        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'test account status updated to - Return To Sender');
        
        //Account positive and negative testing     
        acs.objStr = 'Account';
        acs.statusStr = 'Return To Sender';        
        acs.inputIdStr = accountId;
        acs.click();    
        system.assertEquals(acs.feedbackStr, 'test account status updated to - Return To Sender');
        
        acs.objStr = 'Account';
        acs.statusStr = 'Return To Sender';        
        acs.inputIdStr = contactId;        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'Wrong Id Type');

        acs.objStr = 'Account';
        acs.statusStr = 'Return To Sender';
        acs.inputIdStr = '62,75924193,' + contactBarId + '   ,02 08 52 06';        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'Wrong Id Type');

        acs.objStr = 'Account';
        acs.statusStr = 'Return To Sender';        
        acs.inputIdStr = accountId + '1';        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'No Record Found');

        acs.objStr = 'Account';
        acs.statusStr = 'Return To Sender';
        acs.inputIdStr = '62,75924193,' + accountBarId + '1   ,02 08 52 06';        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'No Record Found');
        
        //Contact positive and negative testing
        acs.objStr = 'Contact';
        acs.statusStr = 'Return To Sender';
        acs.inputIdStr = '62,75924193,' + contactBarId + '   ,02 08 52 06';        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'test First test Last status updated to - Return To Sender');
             
        acs.objStr = 'Contact';
        acs.statusStr = 'Return To Sender';        
        acs.inputIdStr = contactId;
        acs.click();    
        system.assertEquals(acs.feedbackStr, 'test First test Last status updated to - Return To Sender');
        
        acs.objStr = 'Contact';
        acs.statusStr = 'Return To Sender';        
        acs.inputIdStr = accountId;        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'Wrong Id Type');

        acs.objStr = 'Contact';
        acs.statusStr = 'Return To Sender';
        acs.inputIdStr = '62,75924193,' + accountBarId + '   ,02 08 52 06';        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'Wrong Id Type');

        acs.objStr = 'Contact';
        acs.statusStr = 'Return To Sender';        
        acs.inputIdStr = contactId + '1';        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'No Record Found');

        acs.objStr = 'Contact';
        acs.statusStr = 'Return To Sender';
        acs.inputIdStr = '62,75924193,' + contactBarId + '1   ,02 08 52 06';        
        acs.click();
        system.assertEquals(acs.feedbackStr, 'No Record Found');        

        test.stopTest();
    }
}