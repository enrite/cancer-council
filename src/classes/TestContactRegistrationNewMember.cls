/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestContactRegistrationNewMember {

    static testMethod void myNewMemberTest() {
        
        test.startTest();
        
        map<string, Id> recordTypeMap = new map<string, Id>
                        {'Team'=>[select id from recordType where sobjectType='Registration__c' and name='Team Event Registration' limit 1].Id
                        , 'Non Organisation'=>[select id from recordType where sobjectType='Account' and name='Non Organisation' limit 1].Id
                        , 'Contact'=>[select id from recordType where sobjectType='Contact' and name='Contact' limit 1].Id};
        
        Account acc = new Account(name='Test Test', BillingStreet='Test Test', BillingCity='Test Test', BillingPostalCode='Test Test'
            , BillingState='Test Test', Phone='00 0000 0000', Email__c='Test@Test.com', RecordTypeId=recordTypeMap.get('Non Organisation'));
        insert acc;

        list<Contact> contactList = new list<Contact>();
        
        //create multiple contact records for testing
        for (integer i=0; i<=10; i++) {
            Contact con = new Contact(AccountId=acc.Id, FirstName='Test Test' + i, LastName='Test Test' + i, MailingStreet='Test Test', MailingCity='Test Test'
                , MailingPostalCode='Test Test', MailingState='Test Test', Phone='00 0000 0000', MobilePhone='0000 000 000', Email='Test@Test.com'
                , RecordTypeId=recordTypeMap.get('Contact'));
            contactList.add(con);   
        }
        system.debug('***** contactList size ' + contactList.size());
        insert contactList;
            
        Campaign ca = new Campaign (Name='Test Campaign', Campaign_Code__c='T Campaign', Status = 'In Progress', IsActive=true);
        insert ca;
        
        Registration__c r = new Registration__c(Campaign__c=ca.Id, Status__c='Registered', Host_Team_Captain__c=contactList.get(0).Id
            , Registration_Date__c=system.today(), RecordTypeId=recordTypeMap.get('Team'));
        insert r;
        
        list<Contact_Registrations__c> crList = new list<Contact_Registrations__c>();
        
        for (integer i=1; i<contactList.size(); i++) {
            Contact_Registrations__c cr = new Contact_Registrations__c();
            cr.Registration__c = r.Id;
            cr.Contact__c = contactList.get(i).Id;
            crList.add(cr);
            system.debug('***** crList size ' + crList.size() + ' i ' + i);
        }
        
        insert crList;
        system.assertEquals(crList.size(), 10);
        system.assertEquals([select New_Member_Added__c from Registration__c where Id = :r.Id].New_Member_Added__c, true);
        test.stopTest();
    }
}