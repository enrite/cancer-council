/**
 * Created by georgehalteh on 5/02/2016.
 */

public class LinkCaseContactController {

    // variables for weightings
    private static final Integer WEIGHT_FIRST_NAME = 15;
    private static final Integer WEIGHT_LAST_NAME = 15;
    private static final Integer WEIGHT_MAILING_STREET = 5;
    private static final Integer WEIGHT_MAILING_POSTAL_CODE = 10;
    private static final Integer WEIGHT_MOBILE_PHONE = 40;
    private static final Integer WEIGHT_PHONE = 5;
    private static final Integer WEIGHT_EMAIL = 20;
    private static final Integer MIN_SCORE_SHOW_POPUP = 40;

    // The id of the case to have the case contact linked to.
    private static final String PARAM_CASE_ID = 'caseId';

    // This the case controller that contains the unlinked contact
    public Case Record {get;set;}

    // This property stores thee matching case contacts.
    public List<CaseContactMatch> CaseContactMatches {get;set;}

    // The selected contact id.
    public String SelectedContactId {get;set;}

    public boolean DisplayPopUp {get; set;}

    public boolean QuitLineCase {get;set;}
    /**
     * Default constructor
     */
    public LinkCaseContactController() {
        init();
    }

    /**
     * This method set the contact selected so we can contact id.
     * @return a page reference
     */
    public PageReference selectContact() {
        if (String.isBlank(SelectedContactId)) {
            CustomException.formatException('Failed to get the contact to select');
            return null;
        }

        final Contact selContact = getSelectedContact();
        if (selContact == null) {
            CustomException.formatException('Failed to selected contact Id');
            return null;
        }

        PageReference pageRef = null;
        try {
            Record.ContactId = selContact.Id;
            Record.AccountId = selContact.AccountId;

            update Record;
            // redirect to the view.
            pageRef = new PageReference('/' + Record.Id);
        } catch (Exception e) {
            CustomException.formatException(e);
            pageRef = null;
        }

        return pageRef;
    }

    public PageReference newContact(){
        if(CaseContactMatches.size() > 0){
            if(Integer.valueOf(CaseContactMatches[0].Score) > MIN_SCORE_SHOW_POPUP){
                displayPopup = true;
                return null;
            }
        }
        return createContact();
    }

    /**
     * This method performs a redirect to the existing create contact function.
     * @return PageReference to the existing create case contact.
     */
    public PageReference createContact() {
        return new PageReference('/apex/casecontactstep1?caseid=' + Record.Id);
    }

    /**
     * This method is called when the close
     */
    public void closePopup(){
        displayPopup = false;
    }

    public PageReference cancel() {
        return new PageReference('/' + Record.Id);
    }
    /**
     * This method initialises the method.
     */
    private void init() {
        Record = getCaseRecord();
        if (Record == null) {
            CustomException.formatMessage('Unable to find the case record.');
            return;
        }

        QuitLineCase = (Record.RecordType.Name == 'Quitline');
        // get the matching records.
        final List<Contact> matchingContacts = getMatchingContacts();
        CaseContactMatches = new List<CaseContactMatch>();
        if (matchingContacts.isEmpty()) {
            CustomException.formatMessage('No matching contact record found.');
        } else {
            // populated with contact
            for (Contact contact : matchingContacts) {
                CaseContactMatches.add(new CaseContactMatch(Record, contact));
            }
            // sort the case contacts by the score descending.
            CaseContactMatches.sort();
        }
    }

    /**
     * This method gets the contact details for a quitline cases.
     * @return case record for the passed in case id stored on the request parameters.
     */
    private Case getCaseRecord() {
        if (!ApexPages.currentPage().getParameters().containsKey(PARAM_CASE_ID)) {
            return null;
        }

        final String caseId = ApexPages.currentPage().getParameters().get(PARAM_CASE_ID);
        final List<Case> cases = [
                SELECT  Id,
                        AccountId,
                        First_Name__c,
                        Last_Name__c,
                        Contact_Street__c,
                        Suburb__c,
                        State__c,
                        Contact_Postcode__c,
                        Phone__c,
                        Mobile__c,
                        Email__c,
                        Gender__c,
                        D_O_B__c,
                        ContactId,
                        RecordType.Name
                FROM Case
                WHERE Id = :caseId];

        return (cases.isEmpty()) ? null : cases.get(0);
    }

    /**
     * THis method returns the matching contacts based on the email, mobile/home phone, lastname and the first initial
     * of the contact details within the case record. (These fields are populated throught the quitline forms)
     * @return List containing the matches
     */
    private List<Contact> getMatchingContacts() {
        final String firstInitial = (String.isBlank(Record.First_Name__c)) ? '%' : '%' + Record.First_Name__c.substring(1) + '%';
        final List<Contact> matches = [SELECT
                FirstName,
                LastName,
                AccountId,
                MailingStreet,
                MailingCity,
                MailingPostalCode,
                MobilePhone,
                HomePhone,
                Email,
                Account.Name
        FROM Contact
        WHERE
        (Email = :Record.Email__c AND Email != null) OR
        (MobilePhone = :Record.Mobile__c AND MobilePhone != null) OR
        (HomePhone = :Record.Phone__c AND HomePhone != null) OR
        ((LastName =  :Record.Last_Name__c AND LastName != null) AND
        (FirstName LIKE :firstInitial AND FirstName != null))];

        return matches;
    }

    /**
     * This method selects the contact based on the SelectContactId.
     * @return the contact to link the Case->Contact to or null if a match is not found.
     */
    private Contact getSelectedContact() {
        Contact selectContact = null;
        for (CaseContactMatch match : CaseContactMatches) {
            if (match.ContactRecord.Id == SelectedContactId) {
                selectContact = match.ContactRecord;
                break;
            }
        }
        return selectContact;
    }
    /**
     * This class stores the case contact and contact matches. It implements Comparable to sort
     * of these objects by the score
     */
    public class CaseContactMatch implements Comparable {

        public boolean FirstNameMatched {get { return equalIgnoreCase(ContactRecord.FirstName, Record.First_Name__c); }}

        public boolean LastNameMatched {get { return equalIgnoreCase(ContactRecord.LastName, Record.Last_Name__c); }}

        public boolean StreetMatched {get { return equalIgnoreCase(ContactRecord.MailingStreet, Record.Contact_Street__c);}}

        public boolean SuburbMatched {get { return equalIgnoreCase(ContactRecord.MailingCity, Record.Suburb__c); }}

        public boolean PostcodeMatched { get { return equalIgnoreCase(ContactRecord.MailingPostalCode, Record.Contact_Postcode__c); } }

        public boolean MobileMatched { get { return equalIgnoreCase(ContactRecord.MobilePhone, Record.Mobile__c); } }

        public boolean PhoneMatched { get { return equalIgnoreCase(ContactRecord.HomePhone, Record.Phone__c); }}

        public boolean EmailMatched { get { return equalIgnoreCase(ContactRecord.Email, Record.Email__c);} }

        // This property returns a score based on what properties have matched. The higher the score the better
        // the match is.
        public Integer Score {
            get {
                Integer score = 0;

                if(FirstNameMatched) {
                    score += WEIGHT_FIRST_NAME;
                }
                if (LastNameMatched) {
                    score += WEIGHT_LAST_NAME;
                }
                if (StreetMatched) {
                    score += WEIGHT_MAILING_STREET;
                }
                if (SuburbMatched) {
                    score += WEIGHT_MAILING_POSTAL_CODE;
                }
                if (PostcodeMatched) {
                    score += WEIGHT_MAILING_POSTAL_CODE;
                }
                if (MobileMatched) {
                    score += WEIGHT_MOBILE_PHONE;
                }
                if (PhoneMatched) {
                    score += WEIGHT_PHONE;
                }
                if (EmailMatched) {
                    score += WEIGHT_EMAIL;
                }


                return score;
            }
        }

        // This property stores the case record with the contact detials
        public Case Record {get;set;}

        // The matching contact record.
        public Contact ContactRecord {get;set;}

        /**
         * Constructor sets the values of Record and ContactRecord to the parameters passed in.
         * @param rec - the case record
         * @param con - the matching contact record.
         */
        public CaseContactMatch(Case rec, Contact con) {
            Record = rec;
            ContactRecord = con;
        }

        public boolean equalIgnoreCase(String val, String val2) {
            boolean isEqual;

            if (val == null && val2 == null) {
                isEqual = false;
            } else if (val == null && val2 != null) {
                isEqual = false;
            } else if (val != null && val2 == null) {
                isEqual = false;
            } else {
                isEqual = (val.toLowerCase().trim() == val2.toLowerCase().trim());
            }

            return isEqual;
        }

        public Integer compareTo(Object param1) {
            final LinkCaseContactController.CaseContactMatch cc = (LinkCaseContactController.CaseContactMatch)param1;
            final Integer comp;

            if (this.Score > cc.Score) {
                comp = -1;
            } else if (this.Score < cc.Score) {
                comp = 1;
            } else {
                comp = 0;
            }

            return comp;
        }
    }
}