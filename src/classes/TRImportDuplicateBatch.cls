public class TRImportDuplicateBatch 
		extends TRImportBase
		implements Database.Batchable<sObject>, Database.Stateful
{
	public Database.QueryLocator start(Database.BatchableContext info)
	{
		/*if(!RunThisOnly)
		{
			delete [SELECT	ID
					FROM	TR_Import_Duplicate__c
					WHERE	Processed__c != null];
		}*/
		if(processedSuccesfully == null) 
		{
			processedSuccesfully = 0;
		}		
		return Database.getQueryLocator([SELECT	ID,
												TR_Member_ID__c, 
												TR_Duplicate_Member_ID__c, 
												TR_Duplicate_Constituent_ID__c, 
												TR_Constituent_ID__c,
												Error__c
										FROM	TR_Import_Duplicate__c
										WHERE	Processed__c = null
										ORDER BY ID]);
	}
	
	public void execute(Database.BatchableContext info, List<TR_Import_Duplicate__c> scope)
	{		
		for(TR_Import_Duplicate__c stagingRecord : scope)
		{
			processedSuccesfully++;
			try
			{
				processDuplicate(stagingRecord);				
			}
			catch(Exception ex)
			{
				processedSuccesfully--;
				stagingRecord.Error__c = ex.getMessage() +  '\n' + ex.getStackTraceString(); 
			}
		}
		update scope;
	}
	
	public void finish(Database.BatchableContext info)
	{
	}
	
	private void processDuplicate(TR_Import_Duplicate__c stagingRecord)
	{
		stagingRecord.Error__c = '';
		stagingRecord.Processing_Result__c = null;
		Integer contactId = parseInteger(stagingRecord.TR_Constituent_ID__c, stagingRecord);
		Integer dupContactId = parseInteger(stagingRecord.TR_Duplicate_Constituent_ID__c, stagingRecord);
		Contact constituent = null;
		Contact duplicateConstituent = null;
		for(Contact c : [SELECT	ID,
								TeamRaiser_CONS_ID__c,
								Merge_Contact__r.TeamRaiser_CONS_ID__c
						FROM	Contact
						WHERE	TeamRaiser_CONS_ID__c = :contactId OR
								TeamRaiser_CONS_ID__c = :dupContactId])
		{
			if(c.Merge_Contact__r.TeamRaiser_CONS_ID__c == contactId && c.TeamRaiser_CONS_ID__c == dupContactId)
			{
				stagingRecord.Processing_Result__c = RESULT_MERGEREQUESTTFOUND;
				stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
				return;
			}
			if(c.TeamRaiser_CONS_ID__c == contactId)
			{
				constituent = c;
			}
			if(c.TeamRaiser_CONS_ID__c == dupContactId)
			{
				duplicateConstituent = c;
			}
		}
		if(constituent == null || duplicateConstituent == null)
		{
			stagingRecord.Processing_Result__c = RESULT_CONTACTNOTFOUND;
			stagingRecord.OwnerID = TeamRaiserConflictQueue.ID;
			return;
		}
		duplicateConstituent.Merge_Contact__c = constituent.ID;
		duplicateConstituent.Merge_Request_Date__c = parseDate(stagingRecord.Creation_Date__c, stagingRecord);
		update duplicateConstituent;
		setProcessedAndOwnerFields(stagingRecord);
	}
}