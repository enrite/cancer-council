@isTest
public class SunSmartUpdateTriggerTest{
     static testMethod void testEverything() {
         SunSmart__c s = new SunSmart__c();
         Account a = new Account();
         a.Name = 'Test678568';
         a.RecordTypeId = '01280000000UDUpAAO';
         a.Organization_Category__c = 'Education';
         insert a;
         a = [SELECT Id, Name FROM Account WHERE Name = 'Test678568'][0];
         s.School__c = a.Id;
         insert s;
         s.Status__c = 'Application being assessed';
         update s;
         s.Status__c = 'Application approved';
         update s;
     }
}