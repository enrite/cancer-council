@isTest
private class RegistrationParticipantNumberRollupTest {

    static testMethod void myRegistrationParticipantNumberRollup() {
        test.startTest();
        
        TestDataFactory.createAccountContactTestRecords(1);
        
        map<string, Id> mapRecordType = new map<string, Id>
                        {'Community_Engagement'=>[SELECT Id FROM RecordType WHERE SobjectType='Ambassador_Activity__c' AND Name='Education' LIMIT 1].Id
                        , 'Registration_Enrolment'=>[SELECT Id FROM RecordType WHERE SobjectType='Registration__c' AND Name='Enrolment' LIMIT 1].Id};        

        Contact con = [SELECT Id FROM Contact LIMIT 1];
                
        // Test for Applied Enrolment
        Ambassador_Activity__c aa = new Ambassador_Activity__c(RecordTypeId = mapRecordType.get('Community_Engagement'), Date_of_request__c = date.Today()
        														, Activity_Date__c = date.Today() + 28, Status__c = 'Planned & Date Confirmed');
        insert aa;
                
        Registration__c reg = new Registration__c(RecordTypeId = mapRecordType.get('Registration_Enrolment'), Community_Engagement__c = aa.Id
        											, Host_Team_Captain__c = con.Id, Status__c = 'Applied');
        insert reg;
        
        // Test for Completed Enrolment
        Ambassador_Activity__c aa1 = new Ambassador_Activity__c(RecordTypeId = mapRecordType.get('Community_Engagement'), Date_of_request__c = date.Today()-2
        														, Activity_Date__c = date.Today() -1, Status__c = 'Completed');
        insert aa1;

        // Test for Course changed
        Ambassador_Activity__c aa2 = new Ambassador_Activity__c(RecordTypeId = mapRecordType.get('Community_Engagement'), Date_of_request__c = date.Today()
        														, Activity_Date__c = date.Today() + 30, Status__c = 'Planned & Date Confirmed');
        insert aa2;
        
        Registration__c reg1 = new Registration__c(RecordTypeId = mapRecordType.get('Registration_Enrolment'), Community_Engagement__c = aa1.Id
        											, Host_Team_Captain__c = con.Id, Status__c = 'Completed');
        insert reg1;
        
        reg = [SELECT Id, Community_Engagement__c FROM Registration__c WHERE Id = :reg.Id];
        reg.Community_Engagement__c = aa2.Id;
        update reg;
        
        test.stopTest();
        
    }
}