trigger trgAccount_AIAUAD on Account (after insert, after update, after delete) {
    try {	    	
    	if (trigger.isAfter) {
			if (trigger.isInsert) {
				// Pass in sObject Type Name and BPAY Number template - 9 digits
				BPAYNum.generateBpayNum('Account', '800000000', trigger.newMap);
				
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordInserted('Account', trigger.newMap);  				
			}	    		
        	if (trigger.isUpdate && !TriggerByPass.BPAYNum) {        		
        		//Use Object 'Alert Record' to enable/disable code below to run
        		RecordValueChangedAlert.recordUpdated('Account', trigger.newMap, trigger.oldMap);
        	}        	
        	if (trigger.isDelete) {
        		//Use Object 'Alert Record' to enable/disable code below to run
        		RecordValueChangedAlert.recordDeleted('Account', trigger.oldMap);
        	}
    	}		
    }
    catch (exception e) {
		string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
							  'Cause: ' + e.getCause() + '<br/>' +
							  'Line Number: ' + e.getLineNumber() + '<br/>' +
							  'Type Name: ' + e.getTypeName() + '<br/>' +
							  'Stack Trace String: ' + e.getStackTraceString();     	
        system.debug('***** Error trgAccount_AIAUAD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgAccount_AIAUAD trigger error: ', errorMessage, 'HTML');        
    }
}