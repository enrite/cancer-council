trigger ContactRegistrationNewMember on Contact_Registrations__c (after insert) 
{
    if(TriggerByPass.ContactRegistration)
    {
        return;
    }
    set<Id> registrationIdSet = new set<Id>();
    map<Id, Contact_Registrations__c> contactRegistrationMap = new map<Id, Contact_Registrations__c>(); 
    set<Registration__c> checkRegistrationSet;
    list<Registration__c> registrationList;
    
    for (Contact_Registrations__c cr : trigger.new) {
        registrationIdSet.add(cr.Registration__c);
        contactRegistrationMap.put(cr.Registration__c, cr);
    }
    
    for (Registration__c r : [select New_Member_Added__c, Id from Registration__c where Id in :registrationIdSet]) {
        checkRegistrationSet = new set<Registration__c>();
        registrationList = new list<Registration__c>();     
        if (contactRegistrationMap.containsKey(r.Id) && r.New_Member_Added__c == false && checkRegistrationSet.contains(r) == false) {
            system.debug('***** trigger if condition loaded ');
            r.New_Member_Added__c = true;
            checkRegistrationSet.add(r);
            registrationList.add(r);
        }
    }   
    update registrationList;
    system.debug('***** trigger registrationList ' + registrationList);
}