trigger trgSoftCredit_AIAUAD on Soft_Credit__c (after insert, after update, after delete) 
{
    Map<ID, Ambassador_Assigned__c> assignedActivitiesByID = new Map<ID, Ambassador_Assigned__c>();
    List<Soft_Credit__c> softCredits = Trigger.IsDelete ? Trigger.old: Trigger.new;
    Set<ID> opportunityIds = new Set<ID>();
    for(Soft_Credit__c sc : softCredits)
    {
        if(sc.Ambassador_Assigned_Activity__c != null && 
                !assignedActivitiesByID.containsKey(sc.Ambassador_Assigned_Activity__c))
        {
            assignedActivitiesByID.put(sc.Ambassador_Assigned_Activity__c, new Ambassador_Assigned__c(ID = sc.Ambassador_Assigned_Activity__c,
                                                                                                  In_Kind__c  = 0,
                                                                                                  Donations__c = 0));
        }
    }
   
    AggregateResult[] results;
    if(Trigger.IsDelete)
    {
       results = [SELECT 	SUM(Amount__c)                 amount,
	                        Ambassador_Assigned_Activity__c,
	                        Opportunity__c
	                FROM    Soft_Credit__c 
	                WHERE   Ambassador_Assigned_Activity__c IN :assignedActivitiesByID.keySet() AND
	                        ID NOT IN :Trigger.old
	                GROUP BY Ambassador_Assigned_Activity__c,
	                        Opportunity__c];
    }
    else
    {
        results = [SELECT 	SUM(Amount__c)                 amount,
	                        Ambassador_Assigned_Activity__c,
	                        Opportunity__c
	                FROM    Soft_Credit__c 
	                WHERE   Ambassador_Assigned_Activity__c IN :assignedActivitiesByID.keySet() 
	                GROUP BY Ambassador_Assigned_Activity__c,
	                        Opportunity__c];
    }
    for(AggregateResult result : results)
    {
        opportunityIds.add((ID)result.get('Opportunity__c'));   
    }
    Map<ID, Opportunity> opportunitiesID = new Map<ID, Opportunity>([SELECT  	ID,
	                                                                            RecordType.Name
	                                                                    FROM    Opportunity
	                                                                    WHERE   ID IN :opportunityIds]);
    for(AggregateResult result : results)
    {
        ID oppID = (ID)result.get('Opportunity__c');                
        ID ambActivityID = (ID)result.get('Ambassador_Assigned_Activity__c');
        Decimal total = (Decimal)result.get('amount');
        if(oppID == null || ambActivityID == null)
        {
            continue;
        }
        if(opportunitiesID.get(oppID).RecordType.Name == 'In Kind')
        {
            assignedActivitiesByID.get(ambActivityID).In_Kind__c += total;
        }
        else
        {
            assignedActivitiesByID.get(ambActivityID).Donations__c += total;
        }
    }
    update assignedActivitiesByID.values();
}