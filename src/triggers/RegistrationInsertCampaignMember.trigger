trigger RegistrationInsertCampaignMember on Registration__c (after insert) {
            
    set<Id> campaignSet = new set<Id>();
    map<Id, Id> campaignMap = new map<Id, Id>();
    
    for (Registration__c r : trigger.new) {
        campaignSet.add(r.Campaign__c);
    }
    
    for (Campaign c : [select Id, ParentId from Campaign where Id in : campaignSet]) {
        campaignMap.put(c.Id, c.ParentId);
    }   
    
    RegistrationInsertCampaignMember.returnCampaignMember(trigger.new, campaignMap);
}