trigger PublicDataTrigger on Public_Data__c (before insert, before update, before delete, after insert, after update, after delete)
{
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            ID tempRecordTypeId = Public_Data__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Temporary').getRecordTypeId();
            Set<ID> contentIds = new Set<ID>();
            for(Public_Data__c pd : Trigger.new)
            {
                contentIds.add(pd.Temporary_Content_Document_ID__c);
            }
            Map<String, ContentVersion> contentVersionById = new Map<String, ContentVersion>();
            for(ContentVersion cv : [SELECT ID,
                                            ContentDocumentId,
                                            External_Review_Author__c,
                                            Internal_Reviewer__c
                                    FROM    ContentVersion
                                    WHERE   ContentDocumentId IN :contentIds AND
                                            IsLatest = true])
            {
                contentVersionById.put(cv.ContentDocumentId, cv);
            }

            List<Messaging.SingleEmailMessage> msgs = new List<Messaging.SingleEmailMessage>();

            for(Public_Data__c pd : Trigger.new)
            {
                if(pd.RecordTypeId == tempRecordTypeId)
                {
                    ContentVersion cv = contentVersionById.get(pd.Temporary_Content_Document_ID__c);
                    if(cv != null && cv.External_Review_Author__c != null) {
                        msgs.addAll(PublicDataExtension.getUpdateMessage(pd.ID, cv.External_Review_Author__c));
                    }
                }
            }
            if(!msgs.isEmpty())
            {
                Messaging.sendEmail(msgs);
            }
        }
    }
}