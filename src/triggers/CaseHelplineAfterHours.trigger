trigger CaseHelplineAfterHours on Case (after insert) {
/*
Fire trigger after Helpline Case record is created and check from Time Opened field to determine Yes/No to update on After Hours field.
*/

    try {
        Id recordTypeId = [select Id from RecordType where SobjectType = 'Case' and name = 'Helpline Case'].Id;
        
        if (trigger.isAfter && trigger.isInsert) {
            list<Case> caseList = new list<Case>();
            
            map<Id, Id> caseIds = new map<Id, Id>();
            
            //assign new Case ids to map list.
            for (Case c: trigger.new) {
                caseIds.put(c.Id, c.Id);
            }
            
            //get Case information from map list and update After_Hours__c field based on Time_Opened__c field. After 5pm=Yes.
            for (Case c: [select Id, After_Hours__c, RecordTypeId, Time_Opened__c from Case where Id in: caseIds.values()]) {
                if (recordTypeId == c.RecordTypeId && c.Time_Opened__c != null) {                   
                    string timeValue = string.valueOf(c.Time_Opened__c).substring(11, 13);
                    if (integer.valueOf(timeValue)>=17) {
                        c.After_Hours__c = 'Yes';
                    }
                    else {
                        c.After_Hours__c = 'No';
                    }
                    caseList.add(c);
                }
            }
            update caseList;
        }       
    }
    catch (exception e) {
        system.debug('Error: ' + e);
    }
}