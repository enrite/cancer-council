trigger trgRegistration_AIAUAD on Registration__c (after delete, after insert, after update) {
    try {
        if (trigger.isAfter) {
            if (trigger.isInsert) {
                // Pass in sObject Type Name and BPAY Number template - 9 digits
                BPAYNum.generateBpayNum('Registration__c', '700000000', trigger.newMap);
                
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordInserted('Registration__c', trigger.newMap);
                
                RegistrationParticipantNumberRollup.updateParticipantRollupNumber(trigger.newMap, trigger.newMap);
            }
            
            if (trigger.isUpdate) {
			    if (TriggerByPass.Registration) {
			        return;
			    }
			    
			    if (TriggerByPass.BPAYNum) {
			        return;
			    }
				//Use Object 'Alert Record' to enable/disable code below to run
				RecordValueChangedAlert.recordUpdated('Registration__c', trigger.newMap, trigger.oldMap);  
				
				RegistrationParticipantNumberRollup.updateParticipantRollupNumber(trigger.newMap, trigger.oldMap);
            }
            
            if (trigger.isDelete) {                
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordDeleted('Registration__c', trigger.oldMap);
                
                RegistrationParticipantNumberRollup.updateParticipantRollupNumber(trigger.oldMap, trigger.oldMap);
            }
        }
    }
    catch (exception e) {
		string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
							  'Cause: ' + e.getCause() + '<br/>' +
							  'Line Number: ' + e.getLineNumber() + '<br/>' +
							  'Type Name: ' + e.getTypeName() + '<br/>' +
							  'Stack Trace String: ' + e.getStackTraceString();      	
        system.debug('***** Error trgRegistration_AIAUAD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgRegistration_AIAUAD trigger error: ', errorMessage, 'HTML');        
    }
}