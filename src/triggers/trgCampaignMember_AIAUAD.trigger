trigger trgCampaignMember_AIAUAD on CampaignMember (after delete, after insert, after update) {
    try {
        if (trigger.isAfter) {
            if (trigger.isInsert) {                
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordInserted('CampaignMember', trigger.newMap);                 
            }
            
            if (trigger.isUpdate) {              
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordUpdated('CampaignMember', trigger.newMap, trigger.oldMap);              
            }
            
            if (trigger.isDelete) {                
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordDeleted('CampaignMember', trigger.oldMap);
            }
        }
    }
    catch (exception e) {
        string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                              'Cause: ' + e.getCause() + '<br/>' +
                              'Line Number: ' + e.getLineNumber() + '<br/>' +
                              'Type Name: ' + e.getTypeName() + '<br/>' +
                              'Stack Trace String: ' + e.getStackTraceString();         
        system.debug('***** Error trgCampaignMember_AIAUAD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgCampaignMember_AIAUADtrigger error: ', errorMessage, 'HTML');        
    }
}