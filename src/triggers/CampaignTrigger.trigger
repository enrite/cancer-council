trigger CampaignTrigger on Campaign (before update) 
{
    // find map of campaign details
    Map<Id, Campaign> campaigns = new Map<Id, Campaign> ([SELECT Id,
                                                                 (SELECT Id,
                                                                         Amount,
                                                                         StageName,
                                                                         RecordType.DeveloperName
                                                                  FROM Opportunities)
                                                          FROM Campaign
                                                          WHERE Id IN :Trigger.NewMap.keySet()]);
                                                          
                                                          
    for (Campaign c : Trigger.New)
    {
        c.Total_Value_Opportunities_Posted__c = 0;
        c.Num_Total_Opportunities_Posted__c = 0;
        c.Total_Value_Opportunities_Sales__c = 0;
        c.Num_Total_Opportunities_Sales__c = 0;
        
        for (Opportunity o : campaigns.get(c.Id).Opportunities)
        {
            if (o.StageName == 'Posted')
            {
                c.Total_Value_Opportunities_Posted__c += (o.Amount != null ? o.Amount : 0);
                c.Num_Total_Opportunities_Posted__c++;    
            }
            
            if ((new Set<String> { 'Bequest_Opportunity', 'Major_Gift_Corporate_Donors' }).contains(o.RecordType.DeveloperName))
            {
                c.Total_Value_Opportunities_Sales__c += (o.Amount != null ? o.Amount : 0);
                c.Num_Total_Opportunities_Sales__c++;  
            }      
        }
    }
}