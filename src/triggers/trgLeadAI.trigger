trigger trgLeadAI on Lead (after insert) {
    Id majorGiftRecordType;
    Id MajorGiftCampId;
    List<CampaignMember> newCampMembers = new List<CampaignMember>();
    
    List<RecordType> rt = new List<RecordType>();
    List<Campaign> camps = new List<Campaign>();
    
    rt  = [Select Id, developerName From RecordType Where sObjectType = 'Lead' and developerName = 'Major_Gifts'];
    for(RecordType recType : rt)
    {
        if(recType.developerName == 'Major_Gifts')
        {
            majorGiftRecordType= recType.Id;
        }
    }
    camps = [Select Id From Campaign Where Name = 'Major Gifts' AND isActive = true];
    
    if(camps.size() >0)
    {
        
        for(Lead ld : Trigger.new)
        {
            CampaignMember mem = new CampaignMember (campaignid=camps[0].Id, leadid=ld.id);
            newCampMembers.add(mem);

        }
        
        
        
    }
    if(newCampMembers.size() > 0)
    {
        insert newCampMembers;
    }

}