trigger trgContact_AIAUAD on Contact (after delete, after insert, after update) {
    try {
        if (trigger.isAfter) {
            if (trigger.isInsert) {
                // Pass in sObject Type Name and BPAY Number template - 9 digits
                BPAYNum.generateBpayNum('Contact', '900000000', trigger.newMap);
                
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordInserted('Contact', trigger.newMap);                 
            }
            if (trigger.isUpdate && !TriggerByPass.BPAYNum) {
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordUpdated('Contact', trigger.newMap, trigger.oldMap);
            }           
            if (trigger.isDelete) {
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordDeleted('Contact', trigger.oldMap);
            }
        }
    }
    catch (exception e) {
		string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
							  'Cause: ' + e.getCause() + '<br/>' +
							  'Line Number: ' + e.getLineNumber() + '<br/>' +
							  'Type Name: ' + e.getTypeName() + '<br/>' +
							  'Stack Trace String: ' + e.getStackTraceString();     	
        system.debug('***** Error trgContact_AIAUAD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgContact_AIAUAD trigger error: ', errorMessage, 'HTML');        
    }
}