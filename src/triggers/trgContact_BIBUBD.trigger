trigger trgContact_BIBUBD on Contact (before delete, before insert, before update) {
	try {
		if (trigger.isBefore) {
			if (trigger.isInsert) {
				for (Contact con : trigger.new) {
					con.Record_Type__c = Schema.SObjectType.Contact.getRecordTypeInfosById().get(con.RecordTypeId).getname();
				}
			}
			if (trigger.isUpdate) {
				for (Contact con : trigger.new) {
					con.Record_Type__c = Schema.SObjectType.Contact.getRecordTypeInfosById().get(con.RecordTypeId).getname();
				}
			}
		}
	}
    catch (exception e) {
		string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
							  'Cause: ' + e.getCause() + '<br/>' +
							  'Line Number: ' + e.getLineNumber() + '<br/>' +
							  'Type Name: ' + e.getTypeName() + '<br/>' +
							  'Stack Trace String: ' + e.getStackTraceString();     	
        system.debug('***** Error trgContact_BIBUBD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgContact_BIBUBD trigger error: ', errorMessage, 'HTML');        
    }	
}