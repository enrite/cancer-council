trigger EventDetailJobRoles on Event_Details__c (after insert, after update) 
{                    
    // only continue if we have a dummy registration ID
    if (Registration_Settings__c.getInstance().Auto_generated_JR_Registration__c != null)
    {
        // find the event volunteer record type for job roles
        RecordType rt = [SELECT Id
                         FROM RecordType
                         WHERE sObjectType = 'Job_Role__c'
                         AND Name = 'Event Volunteer'];
    
        // find the relevant event locations, campaigns and dates
        Set<Id> eventLocationIds = new Set<Id>();
        Set<Id> campaignIds = new Set<Id>();
        Date startDate;
        Date endDate;
    
        for (Event_Details__c ed : Trigger.New)
        {                    
            eventLocationIds.add(ed.Event_Location__c);
            campaignIds.add(ed.Campaign__c);
            
            if (startDate == null || ed.Start_Date__c < startDate)
            {
                startDate = ed.Start_Date__c;
            }
            
            if (endDate == null || ed.End_Date__c > endDate)
            {
                endDate = ed.End_Date__c;
            }
        }
        
        // find the existing job roles
        Map<String, List<Job_Role__c>> existingJobRoles = new Map<String, List<Job_Role__c>>();
        for (Job_Role__c jr : [SELECT Id,
                                      Event_Location__c,
                                      Campaign__c,
                                      Date_Started__c
                               FROM Job_Role__c
                               WHERE RecordTypeId = :rt.Id
                               AND Event_Location__c IN :eventLocationIds
                               AND Campaign__c IN :campaignIds
                               AND Date_Started__c >= :startDate
                               AND Date_Started__c <= :endDate])
        {
            String key = jr.Event_Location__c + ':';
            key += jr.Campaign__c + ':';
            key += String.valueOf(jr.Date_Started__c);
            
            if (!existingJobRoles.containsKey(key))
            {
                existingJobRoles.put(key, new List<Job_Role__c>());
            }
            
            existingJobRoles.get(key).add(jr);
        }                               
    
        // add new job roles where appropriate    
        List<Job_Role__c> newJobRoles = new List<Job_Role__c>();
    
        for (Event_Details__c ed : Trigger.New)
        {
            // if no volunteers are required or a date hasn't been supplied then skip
            if (ed.Event_Volunteers_Required__c == null
                || ed.Start_Date__c == null
                || ed.End_Date__c == null)
            {
                continue;
            }
        
            Integer volunteersRequired = Integer.valueOf(ed.Event_Volunteers_Required__c);
        
            // loop through the days for the event detail
            for (Date i = ed.Start_Date__c; i <= ed.End_Date__c; i = i + 1)
            {
                String key = ed.Event_Location__c + ':';
                key += ed.Campaign__c + ':';
                key += String.valueOf(i);
                
                // find how many job roles already exist on that day
                Integer jobRolesFound = 0;
                if (existingJobRoles.containsKey(key))
                {
                    jobRolesFound = existingJobRoles.get(key).size();
                }
                
                // keep creating job roles until we reach the required amount
                while (jobRolesFound < volunteersRequired)
                {      
                    // create the job role          
                    Job_Role__c jr = new Job_Role__c();
                    jr.RecordTypeId = rt.Id;
                    jr.Registration__c = Registration_Settings__c.getInstance().Auto_generated_JR_Registration__c; //dummyRegistration.Id;
                    jr.Event_Location__c = ed.Event_Location__c;
                    jr.Event_Detail__c = ed.Id;
                    jr.Campaign__c = ed.Campaign__c;
                    jr.Date_Started__c = i;
                    jr.Start_Hour__c = '9';
                    jr.Start_Minute__c = '00';
                    jr.Start_AM_PM__c = 'AM';
                    jr.Date_Finished__c = i;
                    jr.End_Hour__c = '1';
                    jr.End_Minute__c = '00';
                    jr.End_AM_PM__c = 'PM';
                    
                    newJobRoles.add(jr);
                    
                    // create the job role          
                    Job_Role__c jr2 = new Job_Role__c();
                    jr2.RecordTypeId = rt.Id;
                    jr2.Registration__c = Registration_Settings__c.getInstance().Auto_generated_JR_Registration__c; //dummyRegistration.Id;
                    jr2.Event_Location__c = ed.Event_Location__c;
                    jr2.Event_Detail__c = ed.Id;
                    jr2.Campaign__c = ed.Campaign__c;
                    jr2.Date_Started__c = i;
                    jr2.Start_Hour__c = '1';
                    jr2.Start_Minute__c = '00';
                    jr2.Start_AM_PM__c = 'PM';
                    jr2.Date_Finished__c = i;
                    jr2.End_Hour__c = '5';
                    jr2.End_Minute__c = '00';
                    jr2.End_AM_PM__c = 'PM';
                    
                    newJobRoles.add(jr2);
                    
                    // add it to the list of existing job roles
                    if (!existingJobRoles.containsKey(key))
                    {
                        existingJobRoles.put(key, new List<Job_Role__c>());
                    }
                    existingJobRoles.get(key).add(jr);
                    existingJobRoles.get(key).add(jr2);
                    
                    // increment the amount of job roles found
                    jobRolesFound++;
                }
            }
        
        }
        
        insert newJobRoles;
    }         
}