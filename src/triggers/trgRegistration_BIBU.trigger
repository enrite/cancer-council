trigger trgRegistration_BIBU on Registration__c (before insert, before update) 
{
	if(TriggerByPass.Registration)
	{
		return;
	}
	Set<ID> campaignIDs = new Set<ID>();
	for(Registration__c r : Trigger.new)
	{
		campaignIDs.add(r.Campaign__c);
	}
	Map<ID, Campaign> campaignsByID = new Map<ID, Campaign>([SELECT ID,
																	(SELECT 	ID
																	FROM	Registrations__r
																	WHERE	Campaign_Donation_Registration__c = true)
															FROM 	Campaign
															WHERE	ID IN :campaignIDs]);
	for(Registration__c registration : Trigger.new)
	{
		if(registration.Campaign_Donation_Registration__c && campaignsByID.containsKey(registration.Campaign__c))
		{
			for(Registration__c donationReg : campaignsByID.get(registration.Campaign__c).Registrations__r)
			{
				// inserts will have null ID, but this is OK
				if(donationReg.ID != registration.ID)
				{
					registration.addError('A Campaign must only have one default Donation Registration.');
				}
			}
		}
	}
}