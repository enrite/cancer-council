trigger MergedIdNotesContact on Contact (after delete) {
/*
Fire trigger after Contact records been merged and auto create Note record with Contact Ids from Survivor and Merged
*/
    //System.debug('Total Number of SOQL Queries allowed in this apex code context: ' +  Limits.getLimitQueries());
    //System.debug('Total Number of records that can be queried  in this apex code context: ' +  Limits.getLimitDmlRows());
    //System.debug('Total Number of DML statements allowed in this apex code context: ' +  Limits.getLimitDmlStatements() );
    //System.debug('Total Number of script statements allowed in this apex code context: ' +  Limits.getLimitScriptStatements());
    //System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
    //System.debug('2.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
    //System.debug('3. Number of script statements used so far : ' +  Limits.getDmlStatements());    
    //System.debug('4.Number of Queries used in this apex code so far: ' + Limits.getQueries());
    //System.debug('5.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());    
    try {
        if (trigger.isAfter && trigger.isDelete) {          
            list<Note> noteList = new list<Note>{};
            
            map<Id, Id> survivedIds = new map<Id, Id>{};
            map<Id, Contact> survivedCons = new map<Id, Contact>{};
                
            //get MasterRecordId from deleted record and reference to it
            for (Contact c : trigger.old) {
                survivedIds.put(c.Id, c.MasterRecordId);
            }
                
            //get record information from MasterRecordId
            for (Contact c : [select Id, Contact_Id__c from Contact where Id in :survivedIds.values()]) {
                survivedCons.put(c.Id, c);
            }

            for (Contact mergedCon : trigger.old) {
                Contact survivedCon = survivedCons.get(survivedIds.get(mergedCon.Id));
                if (survivedCon != null) {
                    Note n = new Note();
                    n.Title = mergedCon.Contact_Id__c + ' merged with ' + survivedCon.Contact_Id__c;    
                    n.ParentId = survivedCon.Id;
                    n.Body = survivedCon.Contact_Id__c + '\r\nbpay no: ' + mergedCon.Bpay_Number__c;
                    noteList.add(n);                        
                }
            }
            insert noteList;            
        }
    }
    catch (exception e) {
        system.debug('Error: ' + e);
    }
}