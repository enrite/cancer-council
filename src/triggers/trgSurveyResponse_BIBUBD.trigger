trigger trgSurveyResponse_BIBUBD on Survey_Response__c (before delete, before insert, before update) {
	try {
		if (trigger.isBefore) {
			// to auto populate Survey Question into Survey Response field Survey Question Desc
			list<Id> listSurveyQuestion = new list<Id>();
			list<Id> listSurveyAnswer = new list<Id>();
			
			for (Survey_Response__c sr : trigger.new){				
				listSurveyQuestion.add(sr.Survey_Question__c);
				listSurveyAnswer.add(sr.Survey_Answer__c);
			}
			
			//Update fields [Survey Question Desc], [Survey Answer Desc] from objects Survey Question and Survey Answer			
			map<Id, Survey_Question__c> mapSurveyQuestion = new map<Id, Survey_Question__c>([SELECT Id, Question__c
																							FROM Survey_Question__c
																							WHERE Id IN :listSurveyQuestion]);
			
			map<Id, Survey_Answer__c> mapSurveyAnswer = new map<Id, Survey_Answer__c>([SELECT Id, Answer__c
																					FROM Survey_Answer__c
																					WHERE Id IN :listSurveyAnswer]);
						
			if (trigger.isInsert || trigger.isUpdate) {
				for (Survey_Response__c sr : trigger.new) {
					sr.Survey_Question_Desc__c = mapSurveyQuestion.get(sr.Survey_Question__c).Question__c;
					sr.Survey_Answer_Desc__c = mapSurveyAnswer.get(sr.Survey_Answer__c).Answer__c;
				}
			}
		}
	}
    catch (exception e) {
        string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                              'Cause: ' + e.getCause() + '<br/>' +
                              'Line Number: ' + e.getLineNumber() + '<br/>' +
                              'Type Name: ' + e.getTypeName() + '<br/>' +
                              'Stack Trace String: ' + e.getStackTraceString();         
        system.debug('***** Error trgSurveyResponse_BIBUBD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgSurveyResponse_BIBUBD trigger error: ', errorMessage, 'HTML');        
    }
}