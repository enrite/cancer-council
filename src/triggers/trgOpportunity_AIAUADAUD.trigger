trigger trgOpportunity_AIAUADAUD on Opportunity (after insert, after update, after delete, after undelete) {
    try {	    
	    if (TriggerByPass.BPAYNum) {
	        return;
	    }
	        	
        if (trigger.isAfter) {
            if (trigger.isInsert) {                
                // Pass in sObject Type Name and BPAY Number template - 9 digits
                BPAYNum.generateBpayNum('Opportunity', '400000000', trigger.newMap);
            
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordInserted('Opportunity', trigger.newMap);
                OpportunityAmountRollup.totalAmountRollup(trigger.newMap, trigger.newMap);
            }
            
            if (trigger.isUpdate) {              
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordUpdated('Opportunity', trigger.newMap, trigger.oldMap);
                OpportunityReallocated.createMajorGiftsOpportunity(trigger.newMap, trigger.oldMap);
                OpportunityAmountRollup.totalAmountRollup(trigger.newMap, trigger.oldMap);
            }
            
            if (trigger.isDelete) {
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordDeleted('Opportunity', trigger.oldMap);
                OpportunityAmountRollup.totalAmountRollup(trigger.oldMap, trigger.oldMap);
            }
            
            if (trigger.isUndelete) {
            	OpportunityAmountRollup.totalAmountRollup(trigger.newMap, trigger.newMap);
            }
        }
    }
    catch (exception e) {
        string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                              'Cause: ' + e.getCause() + '<br/>' +
                              'Line Number: ' + e.getLineNumber() + '<br/>' +
                              'Type Name: ' + e.getTypeName() + '<br/>' +
                              'Stack Trace String: ' + e.getStackTraceString();         
        system.debug('***** Error trgOpportunity_AIAUADAUD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgOpportunity_AIAUADAUD trigger error: ', errorMessage, 'HTML');        
    }
}