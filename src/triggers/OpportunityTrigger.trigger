trigger OpportunityTrigger on Opportunity (before update, after insert, after update, after delete) 
{
    if (Trigger.isBefore)
    {
        // find the opportunity details
        Map<Id, Opportunity> opportunities = new Map<Id, Opportunity> ([SELECT Id,
                                                                               (SELECT Id,
                                                                                       Amount,
                                                                                       StageName
                                                                                FROM Donation_Opportunities__r
                                                                                WHERE StageName = 'Posted')        
                                                                        FROM Opportunity
                                                                        WHERE Id IN :Trigger.NewMap.keySet()]);
        
        for (Opportunity o : Trigger.New)
        {
            o.Total_Donation__c = 0;
            
            for (Opportunity child : opportunities.get(o.Id).Donation_Opportunities__r)
            {
                o.Total_Donation__c += (child.Amount != null ? child.Amount : 0);
            }
        }                
    }
    else
    {
        // trigger an update on parent campaign and sales source opportunity if they exist
        Map<Id, Campaign> campaigns = new Map<Id, Campaign>();
        Map<Id, Opportunity> sourceSalesOpps = new Map<Id, Opportunity>();
        List<Opportunity> opportunities = Trigger.New;
        
        if (Trigger.isDelete)
        {
            opportunities = Trigger.Old;
        }
    
        for (Opportunity o : opportunities)
        {
            if (o.CampaignId != null)
            {
                campaigns.put(o.CampaignId, new Campaign(Id = o.CampaignId));    
            }
            
            if (o.Source_Sales_Opportunity__c != null)
            {
                sourceSalesOpps.put(o.Source_Sales_Opportunity__c, new Opportunity(Id = o.Source_Sales_Opportunity__c));
            }
        }
            
        if (campaigns.size() > 0)
        {
            update campaigns.values();
        }
        
        if (sourceSalesOpps.size() > 0)
        {
            update sourceSalesOpps.values();
        }
    }
}