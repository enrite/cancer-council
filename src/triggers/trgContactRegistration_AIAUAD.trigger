trigger trgContactRegistration_AIAUAD on Contact_Registrations__c (after delete, after insert, after update, after undelete) {
    try {    	
        if (trigger.isAfter) {
            if (trigger.isInsert) {
                // Pass in sObject Type Name and BPAY Number template - 9 digits
                BPAYNum.generateBpayNum('Contact_Registrations__c', '500000000', trigger.newMap);
                            
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordInserted('Contact_Registrations__c', trigger.newMap);
                
                ContactRegistrationTeamRaiser.teamRaiserUpdate(trigger.newMap, trigger.newMap, 'Insert');
            }
            
            if (trigger.isUpdate) {
            	if (TriggerByPass.BPAYNum) {
            		return;
            	}
            	if (TriggerByPass.ContactRegistration) {
            		return;
            	}
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordUpdated('Contact_Registrations__c', trigger.newMap, trigger.oldMap);
                
                ContactRegistrationTeamRaiser.teamRaiserUpdate(trigger.newMap, trigger.oldMap, 'Update');
            	
            }
            
            if (trigger.isDelete) {
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordDeleted('Contact_Registrations__c', trigger.oldMap);
                
                ContactRegistrationTeamRaiser.teamRaiserUpdate(trigger.oldMap, trigger.oldMap, 'Delete');
            }
            
            if (trigger.isUndelete) {
            	ContactRegistrationTeamRaiser.teamRaiserUpdate(trigger.newMap, trigger.newMap, 'Undelete');
            }           
        }       
    }
    catch (exception e) {
        string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                              'Cause: ' + e.getCause() + '<br/>' +
                              'Line Number: ' + e.getLineNumber() + '<br/>' +
                              'Type Name: ' + e.getTypeName() + '<br/>' +
                              'Stack Trace String: ' + e.getStackTraceString();         
        system.debug('***** Error trgContactRegistration_AIAUAD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgContactRegistration_AIAUAD trigger error: ', errorMessage, 'HTML');        
    }
}