trigger trgEventDetail_AIAUAD on Event_Details__c (after delete, after insert, after update) {
    try {
      if (trigger.isAfter) {
        if (trigger.isInsert) {
          // Pass in sObject Type Name and BPAY Number template - 9 digits
          BPAYNum.generateBpayNum('Event_Details__c', '600000000', trigger.newMap);
        }
      }
    }
    catch (exception e) {
        system.debug('***** Error trgEventDetail_AIAUAD: ' + e.getMessage());
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgEventDetail_AIAUAD trigger error: ', e.getMessage(), 'Plain');        
    }
}