trigger trgOpportunity_BIBUBD on Opportunity (before delete, before insert, before update) {
	try {
        if (trigger.isBefore) {
        	if (trigger.isDelete) {                
                //Update Total Amount Rollup before deletion so can update Total Soft Credit before Soft Credit (dependant record) being deleted
                OpportunityAmountRollup.isDelete = true;                
                OpportunityAmountRollup.totalAmountRollup(trigger.oldMap, trigger.oldMap);
        	}
        }
	}
    catch (exception e) {
        string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                              'Cause: ' + e.getCause() + '<br/>' +
                              'Line Number: ' + e.getLineNumber() + '<br/>' +
                              'Type Name: ' + e.getTypeName() + '<br/>' +
                              'Stack Trace String: ' + e.getStackTraceString();         
        system.debug('***** Error trgOpportunity_BIBUBD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgOpportunity_BIBUBD trigger error: ', errorMessage, 'HTML');        
    }	
}