trigger MergedIdNotesAccount on Account (after delete) {
/*
Fire trigger after Account records been merged and auto create Note record with Account Org Ids from Survivor and Merged
*/
    try {
        RecordType recordTypeId = [select Id from RecordType where SobjectType='Account' and name='Non Organisation'];
        if (trigger.isAfter && trigger.isDelete) {          
            list<Note> noteList = new list<Note>{};

            map<Id, Id> survivedIds = new map<Id, Id>{};
            map<Id, Account> survivedAccs = new map<Id, Account>{};
                
            //get MasterRecordId from deleted record and reference to it
            for (Account a : trigger.old) {
                survivedIds.put(a.Id, a.MasterRecordId);
            }
                
            //get record information from MasterRecordId
            for (Account a : [select Id, Org_Id__c from Account where Id in :survivedIds.values()]) {
                survivedAccs.put(a.Id, a);                  
            }

            for (Account mergedAcc : trigger.old) {
                //skip Note creation if merging Non Organisation record type
                if (recordTypeId.Id != mergedAcc.RecordTypeId) {                    
                    Account survivedAcc = survivedAccs.get(survivedIds.get(mergedAcc.Id));
                    if (survivedAcc != null) {
                        Note n = new Note();
                        n.Title = mergedAcc.Org_Id__c + ' merged with ' + survivedAcc.Org_Id__c;
                        n.ParentId = survivedAcc.Id;
                        n.Body = survivedAcc.Org_Id__c + '\r\nbpay no: ' + mergedAcc.Bpay_Number__c;
                        noteList.add(n);                        
                    }                       
                }
            }
            insert noteList;            
        }
    }
    catch (exception e) {
        system.debug('Error: ' + e);
    }
}