trigger updateQuitDate on Callback__c (after update, after insert) {
    String[] callbacksString = new String[100];
    Integer n = 0; // Array Index
    Integer count = 0; // String size;
    for(Callback__c currentCallback : trigger.new){
        if(count == 0) callbacksString[n] = currentCallback.Case__c;
        else callbacksString[n] = callbacksString[n] + ' OR ' + currentCallback.Case__c;
        count++;
        if(count > 90){
            n++;
            count = 0;
        }
    }
    for(Integer j = 0; j <= n; j++){
        Callback__c[] callBacks = [SELECT Case__c, Quit_Date__c, Days_Since_Quit__c, Last_Call_Result__c FROM Callback__c WHERE Case__c = :callbacksString[j] ORDER BY id DESC];
        for(Integer i = 1; i < callBacks.size(); i++){
            if(callBacks[i].Case__c == callBacks[i-1].Case__c){
                callBacks.remove(i);
                i--;
            }
        }
        Case[] currentCases = [SELECT Quit_3_Months__c FROM Case WHERE id = :callbacksString[j]];
        for(Integer i = 0; i < callBacks.size(); i++){
            if(callBacks[i].Days_Since_Quit__c >= 90 && callBacks[i].Last_Call_Result__c == 'Successful - Certificate Call'){
                currentCases[i].Quit_3_Months__c = callBacks[i].Quit_Date__c;
            }
        }
        update currentCases;
    }
}