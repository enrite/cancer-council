trigger trgSurveyQuestion_AIAUAD on Survey_Question__c (after delete, after insert, after update) {
    try {
        if (trigger.isAfter) {
        	if (trigger.isUpdate) {
        		// to update all Survey Response field Survey Question Desc when Survey Question is changed
        		system.debug('***** trgSurveyQuestion_AIAUAD.isAfter.isUpdate');
        		map<Id, Survey_Question__c> mapNewRecords = trigger.newMap;
        		map<Id, Survey_Question__c> mapOldRecords = trigger.oldMap;
        		set<Id> recordChangedId = new set<Id>();
        		list<Survey_Response__c> listSurveyResponse = new list<Survey_Response__c>();
        		
        		for (Survey_Question__c sq : trigger.newMap.values()) {
        			system.debug('***** sq: ' + sq);
        			Survey_Question__c oldSq = trigger.oldMap.get(sq.Id);
        			system.debug('***** oldSq: ' + oldSq);
        			if (sq.Question__c != oldSq.Question__c) {
        				system.debug('***** value changed');
        				recordChangedId.add(sq.Id);
        			}
        		}
        		system.debug('***** recordChangedId: ' + recordChangedId);
        		for (Survey_Response__c sr : [SELECT Id, Survey_Question__c, Survey_Question_Desc__c, Survey_Question__r.Question__c
        										FROM Survey_Response__c WHERE Survey_Question__c IN :recordChangedId]) {
        			sr.Survey_Question_Desc__c = sr.Survey_Question__r.Question__c;//trigger.newMap.get(sr.Survey_Question__c).Question__c;
        			system.debug('***** sr: ' + sr);
        			listSurveyResponse.add(sr);
        		}
        		
        		update listSurveyResponse;
        	}
        }
    }
    catch (exception e) {
        string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
                              'Cause: ' + e.getCause() + '<br/>' +
                              'Line Number: ' + e.getLineNumber() + '<br/>' +
                              'Type Name: ' + e.getTypeName() + '<br/>' +
                              'Stack Trace String: ' + e.getStackTraceString();         
        system.debug('***** Error trgSurveyQuestion_AIAUAD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgSurveyQuestion_AIAUAD trigger error: ', errorMessage, 'HTML');        
    }
    
}