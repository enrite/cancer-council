trigger trgTask_AIAUAD on Task (after delete, after insert, after update) {
    try {
        if (trigger.isAfter) {
            if (trigger.isInsert) {                
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordInserted('Task', trigger.newMap);                 
            }
            
            if (trigger.isUpdate) {              
				//Use Object 'Alert Record' to enable/disable code below to run
				RecordValueChangedAlert.recordUpdated('Task', trigger.newMap, trigger.oldMap);              
            }
            
            if (trigger.isDelete) {                
                //Use Object 'Alert Record' to enable/disable code below to run
                RecordValueChangedAlert.recordDeleted('Task', trigger.oldMap);                             
            }
        }
    }
    catch (exception e) {
		string errorMessage = 'Message: ' + e.getMessage() + '<br/>' +
							  'Cause: ' + e.getCause() + '<br/>' +
							  'Line Number: ' + e.getLineNumber() + '<br/>' +
							  'Type Name: ' + e.getTypeName() + '<br/>' +
							  'Stack Trace String: ' + e.getStackTraceString();      	
        system.debug('***** Error trgTask_AIAUAD: ' + errorMessage);
        EmailUtil email = new EmailUtil('ithelpdesk@cancersa.org.au', 'trgTask_AIAUAD trigger error: ', errorMessage, 'HTML');        
    }
}