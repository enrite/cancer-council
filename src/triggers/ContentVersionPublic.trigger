trigger ContentVersionPublic on ContentVersion (after insert, after update, after delete, before insert, before update, before delete)
{
    if(TriggerByPass.ContentVersion)
    {
        return;
    }
    Set<Id> documentIds = new Set<Id>();
    Set<Id> versionIds = new Set<Id>();
    if(Trigger.isBefore)
    {
        if(Trigger.isUpdate)
        {
            for(ContentVersion cv : Trigger.new)
            {
                if(cv.RecordTypeId == ContentVersion.SObjectType.getDescribe().getRecordTypeInfosByName().get('Links').getRecordTypeId() &&
                        cv.Target_Audience__c == 'General Public' &&
                        cv.Type__c == 'Service' &&
                        cv.External_Review_Author__c != null &&
                        cv.Send_Email_to_Service_Provider__c)
                {
                    cv.Email_Sent_to_Service_Provider__c = Datetime.now();
                }
                cv.Send_Email_to_Service_Provider__c = false;
            }
        }
    }
    else
    {
        if(Trigger.isUpdate || Trigger.isInsert) {
            for(ContentVersion v : Trigger.New) {
                documentIds.add(v.ContentDocumentId);
                versionIds.add(v.Id);
            }

            //Interrogate salesforce data model to build a dynamic soql query of all fields on the content version
            //these will then be mapped by name to the public data object.
            String contentTable = 'ContentVersion';
            String publicDataTable = 'Public_Data__c';

            //Start building the query string.  Include the contentDocumentId here as it has a different name in the
            //Public_Data__c object.
            String query_string = 'SELECT ContentDocumentId, ContentUrl, ';
            List<String> table_list = new List<String>();

            //Add the tables we want to use to a list of tables
            table_list.add(contentTable);
            table_list.add(publicDataTable);

            //get the full list of field names from each table.
            Set<String> table_cols = schema.describeSObjects(table_list)[0].fields.getMap().keyset();
            Set<String> table1_cols = schema.describeSObjects(table_list)[1].fields.getMap().keyset();

            //Create a set to hold the list of commonly named fields between the
            //ContentVersion and Public_Data__c objects
            Set<String> common_cols = new Set<String>();


            //Loop through each column in the ContentVersion object
            for(String col : table_cols) {

                //For each content version filed, loop through the list of Public_Data fields
                //And attempt to find a match
                for(String col1 : table1_cols) {

                    //Only try to compare custom fields, ie those with __c.  The standard fields
                    //will be manually mapped at the end, because the field names will never match.
                    if(col.right(3).equals('__c')) {

                        //Match found add to the list off commonly named columns.
                        if(col.equals(col1)) {
                            common_cols.add(col1);
                        }
                    }

                }
            }
            //clone the common columns object and add the standard ContentVersion fields to
            //the resulting set.  This will be used to query the ContentVersions Object and pull
            //back any fields that match between the two objects.
            Set<String> content_cols = common_cols.clone();
            content_cols.add('Description');
            content_cols.add('TagCsv');
            content_cols.add('Title');
            content_cols.add('Associated_Account__r.Name');
            content_cols.add('Associated_Account__r.BillingStreet');
            content_cols.add('Associated_Account__r.BillingCity');
            content_cols.add('Associated_Account__r.BillingState');
            content_cols.add('Associated_Account__r.BillingPostalCode');
            content_cols.add('Associated_Account__r.Phone');

            //Loop through each returned fieldname and add it to the query string
            //to finish building the select statement
            for(String col : content_cols) {

                query_string += col + ',';
            }

            //Remove the comma on the last field added to the select.
            query_string = query_string.removeEnd(',');

            //add the from and where clause
            query_string += ' FROM ' + contentTable + ' where id in ( ';

            //Dynamically build the in clause so that the trigger handles more than 1 record in thge query.
            for(Id doc : versionIds) {
                query_string += '\'' + doc + '\',';
            }

            //Remove the comma from the end of the in clause. and close the final bracket.
            query_string = query_string.removeEnd(',');
            query_string += ')';

            query_string += ' AND RecordType.DeveloperName = \'Services1\' AND Target_Audience__c = \'General Public\' AND Type__c = \'Service\' ';

            //Call the dynamic SOQL Query to retrieve the ContentVersion record with all its fields
            //Bring it back as an sObject list so we can work with it dynamically.
            List<sObject> allVersions = Database.query(query_string);

            // find the record type for the new public data records
            Map<String, RecordType> pdRecordTypesByName = new Map<String, RecordType>();
            for(RecordType rt : [SELECT Id,
                                        DeveloperName
                                FROM RecordType
                                WHERE sObjectType = 'Public_Data__c'])
            {
                pdRecordTypesByName.put(rt.DeveloperName, rt);
            }

            //Create an object to hold the records for upsert. and proceed if there was at least 1 record found
            //by the query.
            List<Public_Data__c> upsertPublicData = new List<Public_Data__c>();
            List<Public_Data__c> temporaryPublicData = new List<Public_Data__c>();
            if(allVersions.size() > 0) {
                //Loop through each ContentVertsion record returned
                for(sObject cv:allVersions) {
                    //For each record create an sObject record to represent the Public_Data record.
                    //sObject is used so we can work with the fields dynamically rather than hard
                    //coding the names.
                    sObject npd = Schema.getGlobalDescribe().get('Public_Data__c').newSObject() ;

                    // set the public data record type to Service Provider Services
                    ContentVersion oldValue = Trigger.isInsert ? null : (ContentVersion)Trigger.oldMap.get(cv.ID);
                    Datetime emailSent =(DateTime)cv.get(ContentVersion.Email_Sent_to_Service_Provider__c.getDescribe().getName());
                    if(oldValue != null &&
                            emailSent != null &&
                            emailSent != oldValue.Email_Sent_to_Service_Provider__c)
                    {
                        npd.put('RecordTypeId', pdRecordTypesByName.get('Temporary').ID);
                    }
                    else {
                        npd.put('RecordTypeId', pdRecordTypesByName.get('Service_Provider_Services').ID);
                    }

                    //Loop through all columns common to both objects and create a public_data
                    //field record for each.
                    for(String colName : common_cols) {
                        npd.put(colName, cv.get(colName));
                    }

                    //Add the standard fields to the corresponding Public_Data field.
                    //These need to be mapped manually because we have no control over the API name.
                    npd.put('Description__c', cv.get('Description'));
                    npd.put('TagsCsv__c', cv.get('TagCsv'));
                    npd.put('Title__c', cv.get('Title'));
                    npd.put('Content_URL__c', cv.get('ContentUrl'));
                    if(cv.getSobject('Associated_Account__r') != null) {
                        npd.put('Organisation_Name__c', cv.getSobject('Associated_Account__r').get('Name'));
                        npd.put('Account_Billing_Street__c', cv.getSobject('Associated_Account__r').get('BillingStreet'));
                        npd.put('Account_Billing_City__c', cv.getSobject('Associated_Account__r').get('BillingCity'));
                        npd.put('Account_Billing_State__c', cv.getSobject('Associated_Account__r').get('BillingState'));
                        npd.put('Account_Billing_PostalCode__c', cv.getSobject('Associated_Account__r').get('BillingPostalCode'));
                        npd.put('Account_Phone__c', cv.getSobject('Associated_Account__r').get('Phone'));
                    }

                    //Cast the sObject to a Public_Data object and add it to the
                    //Upsert list.
                    if(npd.get('RecordTypeId') == pdRecordTypesByName.get('Temporary').ID)
                    {
                        npd.put('Temporary_Content_Document_ID__c', cv.get('ContentDocumentId'));
                        npd.put('Status__c', 'Sent');
                        temporaryPublicData.add((Public_Data__c)npd);
                    }
                    else
                    {
                        npd.put('Content_Document_ID__c', cv.get('ContentDocumentId'));
                        upsertPublicData.add((Public_Data__c)npd);
                    }
                }
                //If at least 1 record exists in the upsert list then upsert it.
                if(upsertPublicData.size() > 0) {
                    //Using the Database.upsert method here as we can use it to specify the external Id of Content_Docuement_ID.  This allows us to keep only
                    //the latest version of a content object in the Public_Data object.  Each latest version will overwrite the previous rather than creating a new one.

                    Database.upsert(upsertPublicData, Schema.sObjectType.Public_Data__c.fields.Content_Document_ID__c.getsObjectField(), true);
                }
                insert temporaryPublicData;
            }
        }
        else if(Trigger.isDelete) {
            List<Public_Data__c > forDelete = new List<Public_Data__c >();

            forDelete = [Select Id from Public_Data__c where Content_Document_ID__c in :documentIds];
            if(forDelete.size() > 0) {
                delete(forDelete);
            }
        }
    }
}