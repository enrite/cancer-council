trigger SunSmartUpdateTrigger on SunSmart__c (before update) {
    SunSmart__c[] newSunSmart = trigger.new;
    SunSmart__c[] oldSunSmart = trigger.old;
    for(Integer i = 0; i < oldSunSmart.size(); i++){
        if(oldSunSmart[i].Status__c != newSunSmart[i].Status__c){
            Account[] a = [SELECT Sunsmart_Pending_Application__c FROM Account WHERE id = :newSunSmart[i].School__c];
            if(a.size() == 1){
                if(newSunSmart[i].Status__c == 'Application approved'){
                   a[0].SunSmart_Approval_Date__c = Date.today();
                }
                if(newSunSmart[i].Status__c == 'Application being assessed'){
                    a[0].Sunsmart_Pending_Application__c = True;
                }else{
                    a[0].Sunsmart_Pending_Application__c = False;
                }
                update a;
            }
        }
    }
}